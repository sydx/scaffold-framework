package com.scaffold.common.constants;

/**
 * Redis 缓存name常量
 */
public class CacheNameConstant {

    /**
     * 系统功能缓存
     */
    public final static String SYS_MODULE_FUNCTION = "sys:module:function";

    /**
     * 系统操作缓存
     */
    public final static String SYS_MODULE_FUNCTION_ACTION = "sys:module:function:action";

    /**
     * 系统权限缓存
     */
    public final static String SYS_PERMISSION_CODE_URL = "sys:permission:code:url";

    /**
     * 管理后台鉴权token
     */
    public final static String SECURITY_AUTHENTICATION_TOKEN = "security:authentication:token";

    /**
     * 小程序端鉴权token
     */
    public final static String SECURITY_AUTHENTICATION_TOKEN_MINIPROGRAM = "security:authentication:token:miniprogram";

    /**
     * 小程序端RSA秘钥
     */
    public final static String SECURITY_AUTHENTICATION_RSA_MINIPROGRAM = "security:authentication:rsa:miniprogram";

    /**
     * 小程序端图形验证码
     */
    public final static String SECURITY_AUTHENTICATION_CAPTCHA_MINIPROGRAM = "security:authentication:captcha:miniprogram";

}
