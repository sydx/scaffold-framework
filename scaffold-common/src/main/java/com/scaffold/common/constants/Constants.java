package com.scaffold.common.constants;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/13 16:15:26
 * @描述: 常量
 */
public class Constants {

    /**
     * Request ID 属性名称。适用Header、Log等
     */
    public static final String REQUEST_ID_HEADER = "RequestId";
    public static final String ACCESS_TOKEN_HEADER = "AccessToken";

}
