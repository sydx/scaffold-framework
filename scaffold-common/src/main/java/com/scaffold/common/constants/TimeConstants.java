package com.scaffold.common.constants;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/13 16:15:26
 * @描述: 常量
 */
public class TimeConstants {

    public final static int TEN_MINUTES_SECOND = 60 * 10;

    public final static int TEN_MINUTES_MILLISECOND = 60 * 10 * 1000;

    public final static int TWO_MINUTES_SECOND = 60 * 2;

}
