package com.scaffold.common.asserts;

import cn.hutool.core.util.StrUtil;
import com.scaffold.common.exception.BaseException;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Map;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/30 10:18:05
 * @描述: 自定义断言
 */
public class Asserts extends Assert {

    public static <T extends BaseException> void hasText(@Nullable String text, T exception) {
        if (!StringUtils.hasText(text)) {
            throw exception;
        }
    }

    public static <T extends BaseException> void isTrue(boolean expression, T exception) {
        if (!expression) {
            throw exception;
        }
    }

    public static <T extends BaseException> void notNull(@Nullable Object object, T exception) {
        if (object == null) {
            throw exception;
        }
    }

    public static <T extends BaseException> void notEmpty(@Nullable Object[] array, T exception) {
        if (ObjectUtils.isEmpty(array)) {
            throw exception;
        }
    }

    public static <T extends BaseException> void notEmpty(@Nullable Collection<?> collection, T exception) {
        if (CollectionUtils.isEmpty(collection)) {
            throw exception;
        }
    }

    public static <T extends BaseException> void notEmpty(@Nullable Map<?, ?> map, T exception) {
        if (CollectionUtils.isEmpty(map)) {
            throw exception;
        }
    }

    public static <T extends BaseException> void isEquals(String str1, String str2, T exception) {
        if (!StrUtil.equals(str1, str2)) {
            throw exception;
        }
    }
}
