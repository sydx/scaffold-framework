package com.scaffold.common.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/28 14:11:46
 * @描述: 权限编码/组织架构编码工具类
 */
public class CodeUtil {

    /**
     * 编码最小值
     */
    private static final int CODE_MIN = 1;
    /**
     * 编码最大值
     */
    private static final int CODE_MAX = 99;
    /**
     * 编码位数
     */
    private static final int CODE_LENGTH = 2;
    /**
     * 编码不足两位补零
     */
    private static final String ZERO_PADDING = "0";

    /**
     * 模块编码前缀
     */
    public static final String MODULE_PRE = "M";
    /**
     * 功能编码前缀
     */
    public static final String FUNCTION_PRE = "F";
    /**
     * 动作点编码前缀
     */
    public static final String ACTION_PRE = "A";

    /**
     * 生成模块编码
     *
     * @param code
     * @return
     */
    public static String module(String code) {
        return MODULE_PRE + code;
    }

    /**
     * 生成功能编码
     *
     * @param code
     * @return
     */
    public static String function(String code) {
        return FUNCTION_PRE + code;
    }

    /**
     * 生成功能编码
     *
     * @param code
     * @return
     */
    public static String function(String pre, String code) {
        return pre + code;
    }

    /**
     * 生成动作点编码
     *
     * @param code
     * @return
     */
    public static String action(String code) {
        return ACTION_PRE + code;
    }

    /**
     * 生成编码
     *
     * @param used
     * @return
     */
    public static String generate(List<String> used) {
        if (null == used || used.size() == 0) {
            return getCode(CODE_MIN);
        }
        List<String> converted = convert(used);
        for (int i = CODE_MIN; i <= CODE_MAX; i++) {
            String code = getCode(i);
            if (converted.contains(code)) {
                continue;
            }
            return code;
        }
        throw new IllegalArgumentException("权限标识生成失败！");
    }

    private static String getCode(int i) {
        return i >= 10 ? String.valueOf(i) : (ZERO_PADDING + i);
    }

    /**
     * @描述 将完整的权限编码转化，只取最后两位
     * @参数 toBeConverted
     * @返回值: java.util.List<java.lang.String>
     * @创建人 李亮
     * @创建时间 2022-07-28 15:08:34
     */
    private static List<String> convert(List<String> toBeConverted) {
        if (toBeConverted == null) {
            return toBeConverted;
        }
        List<String> converted = new ArrayList<>(toBeConverted.size());
        for (String code : toBeConverted) {
            converted.add(code.substring(code.length() - CODE_LENGTH));
        }
        return converted;
    }

}
