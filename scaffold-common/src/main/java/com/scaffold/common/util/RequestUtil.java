package com.scaffold.common.util;

import cn.hutool.core.util.StrUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.util.StringUtils;

import static com.scaffold.common.constants.Constants.REQUEST_ID_HEADER;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/10 10:18:09
 * @描述: Request工具类
 */
public class RequestUtil {

    /**
     * 获取请求客户端IP
     *
     * @param request
     * @return
     */
    public static String getClientIP(HttpServletRequest request) {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        String[] split = xfHeader.split(",");
        return split.length > 0 ? split[0] : "";
    }

    public static String getRequestId(HttpServletRequest request) {
        String requestId = request.getHeader(REQUEST_ID_HEADER);
        if (StrUtil.isBlank(requestId)) {
            Object reqId = request.getAttribute(REQUEST_ID_HEADER);
            requestId = null != reqId ? (String) reqId : "";
        }
        return requestId;
    }

}
