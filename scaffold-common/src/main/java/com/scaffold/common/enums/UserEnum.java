package com.scaffold.common.enums;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/19 9:16:03
 * @描述: 用户信息相关枚举类
 */
public enum UserEnum {
    ;

    public enum Type {

        SUPPER(99, "超级管理员"),
        GENERAL(1, "普通用户"),
        ADMIN(2, "系统管理员"),
        ;
        private final Integer val;
        private final String label;

        Type(Integer val, String label) {
            this.val = val;
            this.label = label;
        }

        public Integer getVal() {
            return val;
        }

        public String getLabel() {
            return label;
        }

        public static String convert(Integer val) {
            for (Type value : Type.values()) {
                if (value.val.equals(val)) {
                    return value.label;
                }
            }
            return "";
        }

        public static Type convertEnum(Integer val) {
            for (Type value : Type.values()) {
                if (value.val.equals(val)) {
                    return value;
                }
            }
            return null;
        }
    }

}
