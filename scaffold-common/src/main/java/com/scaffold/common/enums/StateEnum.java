package com.scaffold.common.enums;

/**
 * @创建人: 李亮
 * @创建时间: 2023/2/15 9:27:07
 * @描述: 状态枚举
 */
public enum StateEnum {
    ENABLED(1),
    DISABLED(0),
    CANCELED(-1);
    private final int val;
    StateEnum(int val) {
        this.val = val;
    }
    public int val() {
        return val;
    }
}
