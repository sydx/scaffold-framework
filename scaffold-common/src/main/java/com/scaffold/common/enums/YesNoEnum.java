package com.scaffold.common.enums;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/7 11:12:45
 * @描述: 枚举类
 */
public enum YesNoEnum {

    /**
     * 否/未...
     */
    NO(0),
    /**
     * 是/已...
     */
    YES(1);
    private final Integer val;

    YesNoEnum(Integer val) {
        this.val = val;
    }

    public Integer getVal() {
        return val;
    }

}
