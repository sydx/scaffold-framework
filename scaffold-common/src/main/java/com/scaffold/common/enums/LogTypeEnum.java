package com.scaffold.common.enums;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/3 15:54:31
 * @描述: 日志类型
 */
public enum LogTypeEnum {

    OPERATION(1, "操作日志"),
    LOG_IN(2, "登录日志"),
    ;
    private final Integer val;
    private final String label;

    LogTypeEnum(Integer val, String label) {
        this.val = val;
        this.label = label;
    }

    public Integer getVal() {
        return val;
    }

    public String getLabel() {
        return label;
    }

    public static String convert(Integer val) {
        for (LogTypeEnum value : LogTypeEnum.values()) {
            if (value.val.equals(val)) {
                return value.label;
            }
        }
        return "";
    }

}
