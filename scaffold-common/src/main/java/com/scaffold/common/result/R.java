package com.scaffold.common.result;

import cn.hutool.core.util.StrUtil;
import com.scaffold.common.exception.BaseException;

import java.util.HashMap;
import java.util.Map;

/**
 * 向客户端返回结果
 *
 * @author 陈顺华
 */
public class R<T> {

    //数据
    private T data;
    //错误信息
    private Map<String, String> errors;

    private int code = 200;

    private String message;

    private String requestId;

    public R() {
    }

    public R(T data, int code, String message) {
        this.data = data;
        this.code = code;
        this.message = message;
    }

    public R(RCode code) {
        this(code, code.message());
    }

    public R(RCode code, String message) {
        this(null, null, code.code(), message);
        if (StrUtil.isBlank(message)) {
            this.message = code.message();
        }
    }

    public R(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public R(RCode code, T data) {
        this(data, null, code.code(), code.message());
    }

    public R(int code, T data) {
        this(data, null, code, StrUtil.EMPTY);
    }

    public R(T data, Map<String, String> errors, int code, String message) {
        this.data = data;
        this.errors = errors;
        this.code = code;
        this.message = message;
    }

    /**
     * 成功，输入需要返回的结果
     *
     * @param data 输出的结果
     * @return R
     */
    public static <T> R<T> success(T data) {
        return new R<T>(RCode.SUCCESS, data);
    }

    /**
     * 成功，输入需要返回的结果
     *
     * @param data 输出的结果
     * @return R
     */
    public static <T> R<T> success() {
        return new R<T>(RCode.SUCCESS);
    }

    /**
     * 失败，输入一系列错误消息，如果参数只有1个则在errors中增加一个叫message的消息
     * 如果是两个参数则第一个是code，第二个是message
     * 如果是多个参数则按消息名，消息内容；消息名，消息内容增加到errors
     *
     * @param msg
     * @return
     */
    public static <T> R<T> failure(RCode rCode) {
        return new R<T>(rCode);
    }

    public static <T> R<T> failure(RCode rCode, String msg) {
        return new R<T>(rCode, msg);
    }

    public static <T> R<T> failure(RCode rCode, T data, String msg) {
        return new R<T>(data, null, rCode.code(), msg);
    }

    public static <T extends BaseException> R<T> failure(T exception) {
        return new R<T>(exception.getCode(), exception.getMessage());
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }



    public Map<String, String> getErrors() {
        return errors;
    }

    /**
     * 设置错误消息，输入一系列错误消息，如果参数只有1个则在errors中增加一个叫message的消息
     * 如果是两个参数则第一个是code，第二个是message
     * 如果是多个参数则按消息名，消息内容；消息名，消息内容增加到errors
     *
     * @param msg
     * @return
     */
    public void setErrorMessage(String... msg) {
        if (errors == null) {
            errors = new HashMap<String, String>();
        }
        if (msg.length == 1) {
            errors.put("message", msg[0]);
        }
        if (msg.length == 2) {
            errors.put("code", msg[0]);
            errors.put("message", msg[1]);
        } else {
            for (int i = 1; i < msg.length; i += 2) {
                errors.put(msg[i - 1], msg[i]);
            }
        }
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
