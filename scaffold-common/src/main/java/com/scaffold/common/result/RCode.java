package com.scaffold.common.result;

/**
 * @创建人: 李亮
 * @创建时间: 2022/5/27 10:56:16
 * @描述: TODO
 */
public enum RCode {

    SUCCESS(200, "操作成功"),
    /**
     * 身份鉴权错误码100 ~ 199
     **/
    AUTH_NON_CERT(100, "无身份认证凭证"),
    AUTH_FAIL(110, "身份认证失败"),

    AUTH_FAIL_PWD_EXPIRED(111, "密码过期"),
    ACCESS_DENIED(120, "权限不足"),

    TOKEN_EXCEPTION(130, "Token不正确"),
    TOKEN_EXPIRED(132, "Token不存在或已过期"),

    ARGUMENT(300, "参数不正确"),
    EXCEPTION(500, "系统异常"),

    THIRD_USER_NOT_EXIST(140, "三方用户不存在"),

    ;

    private int code;
    private String message;

    RCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int code() {
        return code;
    }

    public String message() {
        return message;
    }
}
