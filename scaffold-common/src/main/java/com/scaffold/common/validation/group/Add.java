package com.scaffold.common.validation.group;

import jakarta.validation.groups.Default;

/**
 * 新增
 */
public interface Add extends Default {
}
