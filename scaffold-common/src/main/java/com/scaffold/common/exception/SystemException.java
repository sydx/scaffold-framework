package com.scaffold.common.exception;


import com.scaffold.common.result.RCode;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/30 10:10:30
 * @描述: 系统异常
 */
public class SystemException extends BaseException {

    public SystemException() {
        this(RCode.EXCEPTION);
    }

    public SystemException(RCode rcode) {
        super(rcode);
    }

    public SystemException(RCode rcode, Throwable throwable) {
        super(rcode, throwable);
    }

    public SystemException(Throwable throwable) {
        this(RCode.EXCEPTION, throwable);
    }
}
