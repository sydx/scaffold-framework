package com.scaffold.common.exception;

import com.scaffold.common.result.RCode;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/30 10:10:30
 * @描述: Token异常
 */
public class TokenException extends BaseException {

    public TokenException() {
        this(RCode.TOKEN_EXCEPTION);
    }

    public TokenException(RCode rcode) {
        super(rcode);
    }

    public TokenException(RCode rcode, Throwable throwable) {
        super(rcode, throwable);
    }

    public TokenException(Throwable throwable) {
        super(RCode.TOKEN_EXCEPTION, throwable);
    }
}
