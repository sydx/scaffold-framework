package com.scaffold.common.exception;

import com.scaffold.common.result.RCode;

public class BaseException extends RuntimeException {

    private int code = RCode.EXCEPTION.code();

    public BaseException(RCode rcode) {
        super(rcode.message());
        this.code = rcode.code();
    }
    public BaseException(RCode rcode, Throwable cause) {
        super(rcode.message(), cause);
        this.code = rcode.code();
    }
    public BaseException(int code, String message) {
        super(message);
        this.code = code;
    }
    public BaseException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public BaseException() {
        super(RCode.EXCEPTION.message());
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    protected BaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public int getCode() {
        return code;
    }
}
