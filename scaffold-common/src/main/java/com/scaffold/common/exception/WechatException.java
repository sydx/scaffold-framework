package com.scaffold.common.exception;

import com.scaffold.common.result.RCode;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/13 10:40:06
 * @描述: 微信异常
 */
public class WechatException extends BaseException {
    public WechatException() {
        this(RCode.EXCEPTION);
    }

    public WechatException(RCode rcode) {
        super(rcode);
    }

    public WechatException(RCode rcode, Throwable throwable) {
        super(rcode, throwable);
    }

    public WechatException(Throwable throwable) {
        super(RCode.EXCEPTION, throwable);
    }
}
