package com.scaffold.mybatisplus.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.scaffold.common.util.SpringUtil;
import com.scaffold.mybatisplus.config.DataOperationConfigurationProperties;
import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;


/**
 * @创建人: 李亮
 * @创建时间: 2022/7/28 17:01:15
 * @描述: 自动填充处理
 * 属性填充生效，更新时必须携带实体类，例如：
 * xxxService.update(goods, goodsUpdateWrapper);
 * 更新字段优先级：goodsUpdateWrapper > MetaObjectHandler
 */
@Configuration
public class AutoFillHandler implements MetaObjectHandler {

    private static final Logger logger = LoggerFactory.getLogger(AutoFillHandler.class);
    @Autowired
    private DataOperationConfigurationProperties dataOperationConfigurationProperties;

    private FillHandler fillHandler;

    /**
     * 插入元对象字段填充（用于插入时对公共字段的填充）
     *
     * @param metaObject 元对象
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        if (dataOperationConfigurationProperties.getFill().getEnable()) {
            if (null != getFillHandler()) {
                getFillHandler().insertFill(this, metaObject);
            }
        }
    }

    /**
     * 更新元对象字段填充（用于更新时对公共字段的填充）
     *
     * @param metaObject 元对象
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        if (dataOperationConfigurationProperties.getFill().getEnable()) {
            if (null != getFillHandler()) {
                getFillHandler().updateFill(this, metaObject);
            }
        }
    }

    private FillHandler getFillHandler() {
        if (fillHandler == null) {
            try {
                Class<?> aClass = Class.forName(dataOperationConfigurationProperties.getFill().getHandlerClass());
                Object bean = SpringUtil.getBean(aClass);
                fillHandler = (FillHandler) bean;
            } catch (ClassNotFoundException e) {
                logger.error("自动填充处理器不存在，" +
                        "请继承com.scaffold.mybatisplus.handler.FillHandler接口并托管到Spring容器，" +
                        "将实现类配置到data.operation.fill.handlerClass");
            }
        }
        return fillHandler;
    }

}
