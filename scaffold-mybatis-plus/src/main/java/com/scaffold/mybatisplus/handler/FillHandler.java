package com.scaffold.mybatisplus.handler;

import org.apache.ibatis.reflection.MetaObject;

/**
 * 字段填充处理器接口
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/23 11:44:17
 */
public interface FillHandler {

    void insertFill(AutoFillHandler handler, MetaObject metaObject);

    void updateFill(AutoFillHandler handler, MetaObject metaObject);
}
