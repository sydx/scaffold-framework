package com.scaffold.mybatisplus.handler;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.handler.DataPermissionHandler;
import com.scaffold.common.util.SpringUtil;
import com.scaffold.mybatisplus.annotations.DataScope;
import com.scaffold.mybatisplus.annotations.DataScopeEnum;
import com.scaffold.mybatisplus.config.DataOperationConfigurationProperties;
import com.scaffold.mybatisplus.model.DataPermissionCondition;
import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SubSelect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

/**
 * @创建人: 李亮
 * @创建时间: 2023/3/14 13:58:24
 * @描述: 数据权限处理
 */
public class DataPermissionExecuteHandler implements DataPermissionHandler {
    private static final Logger logger = LoggerFactory.getLogger(DataPermissionExecuteHandler.class);
    private final DataOperationConfigurationProperties.Permission permission;
    private DataPermissionConditionHandler dataPermissionConditionHandler;

    public DataPermissionExecuteHandler(DataOperationConfigurationProperties.Permission permission) {
        this.permission = permission;
    }

    private DataPermissionConditionHandler getDataConditionHandler() {
        if (dataPermissionConditionHandler == null) {
            try {
                Class<?> aClass = Class.forName(permission.getHandlerClass());
                Object bean = SpringUtil.getBean(aClass);
                dataPermissionConditionHandler = (DataPermissionConditionHandler) bean;
            } catch (ClassNotFoundException e) {
                logger.error("数据权限查询条件处理器不存在，" +
                        "请继承com.scaffold.mybatisplus.handler.DataPermissionConditionHandler接口并托管到Spring容器，" +
                        "将实现类配置到data.operation.permission.handlerClass");
            }
        }
        return dataPermissionConditionHandler;
    }

    @Override
    public Expression getSqlSegment(Expression where, String mappedStatementId) {
        //未开启数据权限
        if (!permission.getEnable()) {
            return where;
        }
        
        //未使用注解标注，或显示标注为忽略数据权限控制
        DataScope dataScope = getDataScopeAnnotation(mappedStatementId);
        if (null != dataScope && dataScope.ignore()) {
            return where;
        }
        
        //没有用户信息，不处理
        DataPermissionCondition condition = getDataConditionHandler().getDataCondition();
        if (null == condition) {
            return where;
        }
        //通过注解获取表别名
        String tableAlias = null != dataScope ? dataScope.tableAlias() : StrUtil.EMPTY;
        return hand(condition, tableAlias, where);
    }

    /**
     * 处理sql语句
     * @param adminUser
     * @param tableAlias
     * @param where
     * @return
     */
    private Expression hand(DataPermissionCondition adminUser, String tableAlias, Expression where) {
        Expression expression = null;
        //默认数据范围为TOP_ORG, 可以查看当前顶级组织的所有数据
        DataScopeEnum dataScope = StrUtil.isNotBlank(adminUser.getDataScope())
                ? DataScopeEnum.valueOf(adminUser.getDataScope())
                : DataScopeEnum.TOP_ORG;
        switch (dataScope) {
            case TOP_ORG:
                expression = getTopOrgExpression(adminUser, tableAlias);
                break;
            case USER_ORG:
                expression = getUserOrgExpression(adminUser, tableAlias);
                break;
            case CREATED:
                expression = getCreatedExpression(adminUser, tableAlias);
                break;
            case ALL:
            case USER_ORG_AND_CHILD:
            default:
                return where;
        }
        return ObjectUtils.isNotEmpty(expression) ? new AndExpression(where, new Parenthesis(expression)) : where;
    }

    /**
     * 获取@DataScope，从父类、类和方法三层查找。 方法标注优先级最高
     * @param mappedStatementId
     * @return
     */
    private DataScope getDataScopeAnnotation(String mappedStatementId) {
        Class<?> mapperClass = getMapperClass(mappedStatementId);
        if (null == mapperClass) {
            return null;
        }
        DataScope dataScope = getClassDataScope(mapperClass);

        String methodName = mappedStatementId.substring(mappedStatementId.lastIndexOf(".") + 1);
        DataScope methodDataScope = getMethodDataScope(mapperClass, methodName);
        if (null != methodDataScope) {
           return methodDataScope;
        }
        return dataScope;
    }

    private Class<?> getMapperClass(String mappedStatementId) {
        try {
            return Class.forName(mappedStatementId.substring(0, mappedStatementId.lastIndexOf(".")));
        } catch (ClassNotFoundException e) {
            logger.warn("Mapper类不存在", e);
            return null;
        }
    }

    /**
     * 获取CREATED级别的数据权限过滤表达式
     * @param adminUser
     * @param tableAlias
     * @return
     */
    private EqualsTo getCreatedExpression(DataPermissionCondition adminUser, String tableAlias) {
        EqualsTo equalsTo = new EqualsTo();
        equalsTo.setLeftExpression(buildColumn(tableAlias, permission.getCreatedColumn()));
        equalsTo.setRightExpression(new LongValue(adminUser.getUserId()));
        return equalsTo;
    }

    /**
     * 获取USER_ORG级别的数据权限过滤表达式
     * @param adminUser
     * @param tableAlias
     * @return
     */
    private InExpression getUserOrgExpression(DataPermissionCondition adminUser, String tableAlias) {
        InExpression inExpression = new InExpression();
        inExpression.setLeftExpression(buildColumn(tableAlias, permission.getOrgCodeColumn()));

        ValueListExpression valueListExpression = new ValueListExpression();
        ExpressionList expressionList = new ExpressionList();
        for (String orgCode : adminUser.getOrgCodes()) {
            expressionList.addExpressions(new StringValue(orgCode));
        }
        valueListExpression.setExpressionList(expressionList);
        inExpression.setRightExpression(valueListExpression);
        return inExpression;
    }

    /**
     * 获取TOP_ORG级别的数据权限过滤表达式
     * @param adminUser
     * @param tableAlias
     * @return
     */
    private InExpression getTopOrgExpression(DataPermissionCondition adminUser, String tableAlias) {
        //in ()
        InExpression inExpression = new InExpression();
        inExpression.setLeftExpression(buildColumn(tableAlias, permission.getOrgCodeColumn()));
        //in (select)
        SubSelect subSelect = new SubSelect();
        PlainSelect select = new PlainSelect();
        //查询字段
        select.setSelectItems(Collections.singletonList(new SelectExpressionItem(new Column(permission.getOrgCodeColumn()))));
        select.setFromItem(new Table(permission.getOrgTable()));

        LikeExpression like = new LikeExpression();
        like.setLeftExpression(new Column(permission.getOrgPathColumn()));
        like.setRightExpression(new StringValue(adminUser.getCurrentTopOrgCode() + "%"));

        EqualsTo notDeleted = new EqualsTo();
        notDeleted.setLeftExpression(buildColumn(tableAlias, "deleted"));
        notDeleted.setRightExpression(new LongValue(0));
        select.setWhere(new AndExpression(like, notDeleted));
        subSelect.setSelectBody(select);
        inExpression.setRightExpression(subSelect);
        return inExpression;
    }

    /**
     * 构建Column
     *
     * @param tableAlias 表别名
     * @param columnName 字段名称
     * @return 带表别名字段
     */
    public Column buildColumn(String tableAlias, String columnName) {
        if (StrUtil.isNotBlank(tableAlias)) {
            columnName = tableAlias + "." + columnName;
        }
        return new Column(columnName);
    }

    /**
     * 递归获取方法标注的DataScope
     * @param clazz
     * @param methodName
     * @return
     */
    private DataScope getMethodDataScope(Class<?> clazz, String methodName) {
        DataScope dataScope = null;
        Optional<Method> first = Arrays.stream(clazz.getDeclaredMethods()).filter(o -> o.getName().equals(methodName)).findFirst();
        //当前类找不到方法，去父类找
        if (first.isPresent()) {
            dataScope = first.get().getAnnotation(DataScope.class);
            if (dataScope != null) {
                return dataScope;
            }
        }
        Class<?> superclass = clazz.getSuperclass();
        if (null == superclass) {
            return null;
        }
        return getMethodDataScope(superclass, methodName);
    }

    /**
     * 递归获取类标注的DataScope
     * @param clazz
     * @return
     */
    private DataScope getClassDataScope(Class<?> clazz) {
        DataScope annotation = clazz.getAnnotation(DataScope.class);
        if (annotation != null) {
            return annotation;
        }
        Class<?> superclass = clazz.getSuperclass();
        if (null == superclass) {
            return null;
        }
        return getClassDataScope(superclass);
    }

}
