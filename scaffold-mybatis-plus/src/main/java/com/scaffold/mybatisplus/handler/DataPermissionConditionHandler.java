package com.scaffold.mybatisplus.handler;

import com.scaffold.mybatisplus.model.DataPermissionCondition;

/**
 * 数据权限查询条件值处理器
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/23 14:26:53
 */
public interface DataPermissionConditionHandler {

    DataPermissionCondition getDataCondition();

}
