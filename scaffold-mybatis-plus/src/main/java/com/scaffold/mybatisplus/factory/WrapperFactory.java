package com.scaffold.mybatisplus.factory;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import com.baomidou.mybatisplus.core.enums.SqlLike;
import com.scaffold.mybatisplus.annotations.Condition;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/16 10:08:58
 * @描述: Wrapper工厂
 */
public class WrapperFactory<T> {

    public static <T> WrapperFactory<T> build() {
        return new WrapperFactory<T>();
    }

    /**
     * @描述 根据query对象创建wrapper
     * 的字段使用Condition{@link com.scaffold.mybatisplus.annotations.Condition}注解标注即可作为查询条件生成wrapper
     * 2022-08-16:
     * 目前仅适用于单表查询
     * 仅支持and/in/like，其他条件未完成开发
     * @参数 query
     * @返回值: com.baomidou.mybatisplus.core.conditions.Wrapper<T>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-16 11:28:23
     */
    public <E> QueryWrapper<T> query(E query) {
        QueryWrapper<T> wrapper = new QueryWrapper<>();
        Class<?> clazz = query.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            setCondition(wrapper, field, query);
        }
        return wrapper;
    }

    private <E> void setCondition(QueryWrapper<T> wrapper, Field field, E query) {
        Object value = null;
        try {
            value = field.get(query);
        } catch (IllegalAccessException e) {
            return;
        }
        if (!isExecute(value)) {
            return;
        }

        Condition annotation = field.getAnnotation(Condition.class);
        if (annotation == null) {
            return;
        }
        SqlKeyword keyword = annotation.keyword();
        if (null == keyword) {
            return;
        }
        String name = annotation.value();
        if (StrUtil.isBlank(name)) {
            name = StrUtil.toUnderlineCase(field.getName());
        }

        switch (keyword) {
            case EQ:
                wrapper.eq(name, value);
                break;
            case LIKE:
                SqlLike sqlLike = annotation.like();
                setLike(wrapper, sqlLike, name, value);
                break;
            case IN:
                String formatter = annotation.formatter();
                Class<?> valueType = annotation.type();
                setIn(wrapper, formatter, valueType, name, value);
                break;
        }
    }

    private void setIn(QueryWrapper<T> wrapper, String formatter, Class<?> valueType, String name, Object value) {
        if (StrUtil.equals(formatter, "jsonArray")) {
            List<?> list = JSON.parseArray((String) value, valueType);
            if (CollUtil.isNotEmpty(list)) {
                wrapper.in(name, list);
            }
            return;
        }

        if (StrUtil.equals(formatter, "list")) {
            wrapper.in(name, value);
            return;
        }

        if (StrUtil.equals(formatter, "array")) {
            wrapper.in(name, value);
            return;
        }
        List<String> split = StrUtil.split((CharSequence) value, formatter, true, true);
        if (valueType == Integer.class) {
            List<Integer> list = new ArrayList<>(split.size());
            for (String s : split) {
                list.add(Integer.parseInt(s));
            }
            wrapper.in(name, list);
            return;
        }

        if (valueType == Long.class) {
            List<Long> list = new ArrayList<>(split.size());
            for (String s : split) {
                list.add(Long.parseLong(s));
            }
            wrapper.in(name, list);
        }
    }

    private void setLike(QueryWrapper<T> wrapper, SqlLike sqlLike, String name, Object value) {
        if (!isExecute(value)) {
            return;
        }
        switch (sqlLike) {
            case LEFT:
                wrapper.likeLeft(name, value);
                break;
            case RIGHT:
                wrapper.likeRight(name, value);
                break;
            case DEFAULT:
            default:
                wrapper.like(name, value);
        }
    }

    private boolean isExecute(Object o) {
        if (o instanceof String) {
            return StrUtil.isNotBlank((CharSequence) o);
        }
        return null != o;
    }

}
