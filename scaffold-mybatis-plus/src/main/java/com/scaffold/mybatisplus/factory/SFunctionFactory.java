package com.scaffold.mybatisplus.factory;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/13 9:40:18
 * @描述: TODO
 */
public class SFunctionFactory<T> {

    public static <T> SFunction<Object, T> newMapper() {
        return new SFunction<Object, T>() {
            /**
             * Applies this function to the given argument.
             *
             * @param o the function argument
             * @return the function result
             */
            @Override
            public T apply(Object o) {
                return (T) o;
            }
        };
    }
}
