package com.scaffold.mybatisplus.model;

import com.scaffold.mybatisplus.annotations.DataScopeEnum;
import lombok.Data;

import java.util.List;

/**
 * 数据权限查询值对象
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/23 14:17:07
 */
@Data
public class DataPermissionCondition {

    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 数据权限类型，预留字段
     * {@link DataScopeEnum}
     */
    private String dataScope;

    /**
     * 查询的组织编号列表
     */
    private List<String> orgCodes;
    /**
     * 当前组织的顶级组织编号
     */
    private String currentTopOrgCode;

}
