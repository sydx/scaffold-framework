package com.scaffold.mybatisplus.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/29 9:30:36
 * @描述: 实体对象基类
 */
@Data
public class MPBaseModel {

    @TableField(fill = FieldFill.INSERT)
    private Integer createdBy;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer updatedBy;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedTime;

    private Integer deleted = 0;

}
