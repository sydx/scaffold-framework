package com.scaffold.mybatisplus.model;

import com.scaffold.mybatisplus.model.MPBaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/29 9:30:36
 * @描述: 实体对象基类
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BaseModel extends MPBaseModel {

}
