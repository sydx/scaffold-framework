package com.scaffold.mybatisplus.model;


import lombok.Data;

/**
 * 查询对象基类
 */
@Data
public class BaseQuery {
    protected long size = 10;
    protected long page = 1;
}
