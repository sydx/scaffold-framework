package com.scaffold.mybatisplus.annotations;

import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import com.baomidou.mybatisplus.core.enums.SqlLike;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/16 10:21:49
 * @描述: 查询条件
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Condition {
    /**
     * 表字段名称，如果不设置则使用字段名
     *
     * @return
     */
    String value() default "";

    /**
     * 查询关键词
     *
     * @return
     * @link com.baomidou.mybatisplus.core.enums.SqlKeyword
     */
    SqlKeyword keyword() default SqlKeyword.EQ;

    /**
     * like查询方式
     *
     * @return
     * @link com.baomidou.mybatisplus.core.enums.SqlLike
     */
    SqlLike like() default SqlLike.DEFAULT;

    /**
     * 当keyword为in时，值的格式。
     * 可选值：
     * jsonArray： json数组
     * list：list
     * array：数组
     * 如果不是上述几种格式，而是以某种字符串分隔，直接传入分隔的字符串即可。
     * 例如，以逗号分隔则传入','
     *
     * @return
     */
    String formatter() default ",";

    /**
     * 当keyword为in时，值的类型。
     *
     * @return
     */
    Class<?> type() default String.class;
}
