package com.scaffold.mybatisplus.annotations;

import java.lang.annotation.*;

/**
 * @创建人: 李亮
 * @创建时间: 2022/10/25 11:34:42
 * @描述: 数据范围配置, 通用注解
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface DataScope {

    /**
     * 是否忽略数据范围控制
     *
     * @return
     */
    boolean ignore() default false;

    /**
     * 表别名
     * @return
     */
    String tableAlias() default "";

}
