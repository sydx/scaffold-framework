package com.scaffold.mybatisplus.annotations;


/**
 * @创建人: 李亮
 * @创建时间: 2023/3/15 10:26:08
 * @描述: 数据范围枚举
 */
public enum DataScopeEnum {
    /**
     * 所有
     */
    ALL,
    /**
     * 用户当前激活的顶级组织
     */
    TOP_ORG,
    /**
     * 用户所属组织
     */
    USER_ORG,
    /**
     * 用户所属组织及其子组织
     */
    USER_ORG_AND_CHILD,
    /**
     * 仅用户创建数据
     */
    CREATED;

    public static DataScopeEnum convert(String scope) {
        return DataScopeEnum.valueOf(scope);
    }
}
