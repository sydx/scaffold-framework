package com.scaffold.mybatisplus.config;

import com.scaffold.mybatisplus.handler.FillHandler;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 数据配置
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/23 11:36:18
 */
@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "data.operation")
public class DataOperationConfigurationProperties {

    /**
     * 数据权限配置
     */
    private Permission permission = new Permission();
    /**
     * 数据填充配置
     */
    private Fill fill = new Fill();

    /**
     * 数据权限配置
     */
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    public static class Permission {
        /**
         * 是否启用数据权限配置
         */
        private Boolean enable = Boolean.FALSE;

        /**
         * 数据权限查询条件处理器类全名。必须实现DataPermissionConditionHandler接口
         */
        private String handlerClass;
        /**
         * 创建人字段名
         */
        private String createdColumn = "created_by";
        /**
         * 组织编号字段
         */
        private String orgCodeColumn = "org_code";
        /**
         * 组织路径字段（用户查看组织下所有数据）
         */
        private String orgPathColumn = "org_path";
        /**
         * 组织架构表名
         */
        private String orgTable = "sys_organization";
    }

    /**
     * 自动填充配置
     */
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    public static class Fill {
        /**
         * 是否启用数据权限配置
         */
        private Boolean enable = Boolean.FALSE;

        /**
         * 自动填充字段处理器，必须实现FillHandler接口
         */
        private String handlerClass;
    }

}
