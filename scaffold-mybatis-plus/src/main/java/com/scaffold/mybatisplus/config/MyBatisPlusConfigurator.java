package com.scaffold.mybatisplus.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.DataPermissionInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.scaffold.mybatisplus.handler.DataPermissionExecuteHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MyBatis-Plus配置类
 */
@Configuration
public class MyBatisPlusConfigurator {

    @Autowired
    private DataOperationConfigurationProperties dataOperationConfigurationProperties;

    /**
     * 启用拦截器
     *
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(getPaginationInnerInterceptor());
        if (dataOperationConfigurationProperties.getPermission().getEnable()) {
            interceptor.addInnerInterceptor(getDataPermissionInterceptor());
        }
        return interceptor;
    }

    /**
     * 获取分页插件
     *
     * @return
     */
    private PaginationInnerInterceptor getPaginationInnerInterceptor() {
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor(DbType.MYSQL);
        //每页最大数量
        paginationInnerInterceptor.setMaxLimit(500L);
        return paginationInnerInterceptor;
    }

    /**
     * 获取数据权限控制
     *
     * @return
     */
    private DataPermissionInterceptor getDataPermissionInterceptor() {
        return new DataPermissionInterceptor(
                new DataPermissionExecuteHandler(dataOperationConfigurationProperties.getPermission()));
    }
}
