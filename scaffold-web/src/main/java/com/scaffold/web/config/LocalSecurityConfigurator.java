package com.scaffold.web.config;

import com.scaffold.redis.RedisService;
import com.scaffold.security.WebSecurityConfigurator;
import com.scaffold.security.authentication.UserAuthenticationFilter;
import com.scaffold.security.authentication.user.UserDetailsManager;
import com.scaffold.security.authorization.AccessDecisionHandler;
import com.scaffold.security.authorization.DynamicAccessDecisionManager;
import com.scaffold.web.handler.LocalAccessDecisionHandler;
import com.scaffold.web.handler.LocalCaptchaHandler;
import com.scaffold.web.handler.LocalPasswordHandler;
import com.scaffold.web.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * 本地security配置
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/7/24 20:48:24
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
public class LocalSecurityConfigurator extends WebSecurityConfigurator {
    @Autowired
    private UserManager userManager;
    @Autowired
    private RedisService redisService;
    @Autowired
    private LocalAccessDecisionHandler localAccessDecisionHandler;

    @Override
    public UserAuthenticationFilter userAuthenticationFilter() throws Exception {
        UserAuthenticationFilter userAuthenticationFilter = super.userAuthenticationFilter();
        userAuthenticationFilter.setPasswordHandler(getLocalPasswordHandler());
        userAuthenticationFilter.setCaptchaHandler(getLocalCaptchaHandler());
        return userAuthenticationFilter;
    }

    @Bean
    public LocalCaptchaHandler getLocalCaptchaHandler() {
        return new LocalCaptchaHandler(redisService);
    }

    @Bean
    public LocalPasswordHandler getLocalPasswordHandler() {
        return new LocalPasswordHandler(redisService);
    }


    @Bean
    public UserDetailsManager getUserDetailsService() {
        UserDetailsManager userDetailsManager = new UserDetailsManager();
        userDetailsManager.setUserService(userManager);
        return userDetailsManager;
    }
    @Bean
    public AuthenticationManager authenticationManagerBean(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        AuthenticationManager authenticationManager = authenticationConfiguration.getAuthenticationManager();
        return authenticationManager;
    }

    @Override
    public AccessDecisionHandler accessDecisionHandler() {
        return localAccessDecisionHandler;
    }
}
