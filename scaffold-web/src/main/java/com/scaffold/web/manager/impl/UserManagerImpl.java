package com.scaffold.web.manager.impl;

import cn.hutool.core.util.StrUtil;
import com.scaffold.system.entity.model.SysUser;
import com.scaffold.system.entity.model.SysUserAccount;
import com.scaffold.security.authentication.password.PasswordUtil;
import com.scaffold.web.entity.AdminUser;
import com.scaffold.system.service.ISysUserService;
import com.scaffold.web.manager.UserManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.*;

/**
 * TODO
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/7/25 16:33:31
 */
@Component
@Slf4j
public class UserManagerImpl implements UserManager {

    private final ISysUserService sysUserService;

    public UserManagerImpl(ISysUserService sysUserService) {
        this.sysUserService = sysUserService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        SysUser sysUser = sysUserService.getByUsernameOrUid(username);
        Assert.notNull(sysUser, "用户名或密码错误");
        SysUserAccount sysUserSalt = sysUserService.getSaltByUserId(sysUser.getUserId());
        Assert.notNull(sysUserSalt, "用户名或密码错误");
        AdminUser adminUser = new AdminUser();
        BeanUtils.copyProperties(sysUser, adminUser);
        boolean before = sysUserSalt.getPasswordExpireTime().isAfter(LocalDateTime.now());
        adminUser.setCredentialsNonExpired(before);
        adminUser.setPasswordNeedReset(sysUserSalt.getPasswordNeedReset() != 0);
        adminUser.setPassword(PasswordUtil.getWrappedPassword(sysUser.getPassword(), sysUserSalt.getSalt()));
        if (before) {
            //获取权限集合
            List<String> permissionCodeList = sysUserService.getUserAllPermissionCode(sysUser.getUserId());
            Set<String> collect = new HashSet<>(permissionCodeList);
            adminUser.setPermissions(collect);
        }

        List<String> orgCodes = sysUserService.getUserOrgCodes(sysUser.getUserId());
        if (null != orgCodes && !orgCodes.isEmpty()) {
            adminUser.setOrgCodes(orgCodes);
            adminUser.setCurrentTopOrgCode(getCurrentTopOrgCode(sysUser));
        } else {
            log.warn("用户没有设置组织架构");
        }

        return adminUser;
    }

    private String getCurrentTopOrgCode(SysUser sysUser) {
        return StrUtil.isNotBlank(sysUser.getCurrentOrgCode()) ? sysUserService.getTopOrgCode(sysUser.getCurrentOrgCode()) : "";
    }
}
