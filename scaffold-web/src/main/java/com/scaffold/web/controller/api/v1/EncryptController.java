package com.scaffold.web.controller.api.v1;

import com.scaffold.common.asserts.Asserts;
import com.scaffold.common.result.R;
import com.scaffold.common.util.RequestUtil;
import com.scaffold.security.authentication.password.PasswordHandler;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @创建人: 李亮
 * @创建时间: 2022/7/13 11:01:18
 * @描述: 秘钥接口
 */
@RequestMapping("/api/v1/encrypt")
@RestController
public class EncryptController {

    private PasswordHandler passwordHandler;

    public EncryptController(PasswordHandler passwordHandler) {
        this.passwordHandler = passwordHandler;
    }

    @GetMapping("/publicKey")
    public R getPublicKey(String requestId) {
        Asserts.hasText(requestId, "RequestID不能为空");
        R<String> r = R.success(passwordHandler.createKey(requestId));
        r.setRequestId(requestId);
        return r;
    }

}
