package com.scaffold.web.controller.api.v1;


import cn.hutool.http.Header;
import com.scaffold.common.result.R;
import com.scaffold.common.util.RequestUtil;
import com.scaffold.security.authentication.captcha.CaptchaHandler;
import com.scaffold.web.handler.LocalCaptchaHandler;
import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.base.Captcha;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.scaffold.common.constants.Constants.REQUEST_ID_HEADER;

/**
 * 图形验证码生成
 */
@RequestMapping("/api/v1/captcha")
@RestController
public class CaptchaController {

    private static final String CONTENT_TYPE = "image/gif";
    private static final String EXPIRES = "Expires";
    private static final String NO_CACHE = "No-cache";
    private static final int WIDTH = 130;
    private static final int HEIGHT = 48;
    private static final int LEN = 4;

    private final CaptchaHandler localCaptchaHandler;

    public CaptchaController(CaptchaHandler localCaptchaHandler) {
        this.localCaptchaHandler = localCaptchaHandler;
    }

    @GetMapping("")
    public R captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String requestId = RequestUtil.getRequestId(request);
        Object o = localCaptchaHandler.create(requestId);
        ArithmeticCaptcha captcha = (ArithmeticCaptcha) o;
        R<String> r = R.success(captcha.toBase64());
        r.setRequestId(requestId);
        return r;
    }
}