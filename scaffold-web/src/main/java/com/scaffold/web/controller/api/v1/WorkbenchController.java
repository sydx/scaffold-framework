package com.scaffold.web.controller.api.v1;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.scaffold.common.asserts.Asserts;
import com.scaffold.common.result.R;
import com.scaffold.system.entity.dto.SysUserUpdatePasswordDTO;
import com.scaffold.system.entity.model.SysOrganization;
import com.scaffold.system.entity.vo.OrganizationTreeVO;
import com.scaffold.system.service.ISysOrganizationService;
import com.scaffold.system.service.ISysUserService;
import com.scaffold.web.entity.AdminUser;
import com.scaffold.web.entity.vo.LoginUserVO;
import com.scaffold.web.manager.PermissionManager;
import com.scaffold.web.utils.LoginUserUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 工作台
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/16 9:51:02
 */
@RestController
@RequestMapping("/api/v1/workbench")
public class WorkbenchController {

    private final PermissionManager permissionManager;
    private final ISysOrganizationService sysOrganizationService;
    private ISysUserService sysUserService;

    public WorkbenchController(PermissionManager permissionManager, ISysOrganizationService sysOrganizationService,
                               ISysUserService sysUserService) {
        this.permissionManager = permissionManager;
        this.sysOrganizationService = sysOrganizationService;
        this.sysUserService = sysUserService;
    }

    /**
     * 获取菜单列表
     * @param
     * @return com.scaffold.common.result.R<?>
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-08-16 10:56:04
     */
    @GetMapping("/menu")
    public R<?> menu() {
        int systemId = 1;
        AdminUser user = LoginUserUtil.getUser();
        return R.success(permissionManager.getMenu(systemId, user));
    }

    @GetMapping("/user")
    public R user() {
        AdminUser adminUser = LoginUserUtil.getUser();
        LoginUserVO loginUserVO = new LoginUserVO();
        BeanUtils.copyProperties(adminUser, loginUserVO);
        if (StrUtil.isNotBlank(adminUser.getCurrentTopOrgCode())) {
            SysOrganization current = sysOrganizationService.getByOrgCode(adminUser.getCurrentTopOrgCode());
            loginUserVO.setCurrentTopOrgName(null != current ? current.getOrgName() : null);
            loginUserVO.setCurrentTopOrgCode(null != current ? current.getOrgCode() : null);
        }
        if (CollUtil.isNotEmpty(adminUser.getOrgCodes())) {
            List<OrganizationTreeVO> organizationTree = sysOrganizationService.getOrganizationTree(adminUser.getOrgCodes());
            loginUserVO.setOrganizationTree(organizationTree);
        }
        return R.success(loginUserVO);
    }

    @PatchMapping("/updateCurrentOrg/{orgCode}")
    public R updateUserCurrentTopOrgCode(@PathVariable String orgCode) {
        AdminUser user = LoginUserUtil.getUser();
        boolean result = sysUserService.updateUserCurrentOrgCode(user.getUserId(), orgCode);
        Asserts.isTrue(result, "更新组织失败");
        return R.success();
    }

    @PatchMapping("/password")
    public R updatePassword(@RequestBody @Validated SysUserUpdatePasswordDTO dto) {
        AdminUser user = LoginUserUtil.getUser();
        dto.setUid(user.getUid());
        sysUserService.updatePassword(dto);
        return R.success();
    }
}
