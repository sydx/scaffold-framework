package com.scaffold.web.controller.api.v1;

import com.scaffold.common.result.R;
import com.scaffold.security.authentication.token.Token;
import com.scaffold.security.authentication.token.TokenManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * token控制器
 */
@RequestMapping("/api/v1/token")
@RestController
public class TokenController {
    private final TokenManager tokenManager;

    public TokenController(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    @GetMapping("/getRefreshToken")
    public R<String> getRefreshToken(String refreshCode) {
        String refreshToken = tokenManager.getRefreshTokenByCode(TokenManager.System.MANAGER, refreshCode);
        return R.success(refreshToken);
    }

    @GetMapping("/refresh")
    public R<?> refresh(String refreshToken) {
        Token userToken = tokenManager.refreshAccessToken(TokenManager.System.MANAGER, refreshToken);
        return R.success(userToken.generateTokenJson());
    }

}