package com.scaffold.web.handler;

import com.scaffold.mybatisplus.handler.DataPermissionConditionHandler;
import com.scaffold.mybatisplus.model.DataPermissionCondition;
import com.scaffold.web.entity.AdminUser;
import com.scaffold.web.utils.LoginUserUtil;
import org.springframework.stereotype.Component;

/**
 * 数据权限条件处理器
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/23 14:54:51
 */
@Component
public class LocalDataPermissionConditionHandler implements DataPermissionConditionHandler {
    @Override
    public DataPermissionCondition getDataCondition() {
        AdminUser user = LoginUserUtil.getUser();
        if (null == user) {
            return null;
        }
        DataPermissionCondition condition = new DataPermissionCondition();
        condition.setUserId(user.getUserId());
        condition.setDataScope(user.getDataScope());
        condition.setOrgCodes(user.getOrgCodes());
        condition.setCurrentTopOrgCode(user.getCurrentTopOrgCode());
        return condition;
    }
}
