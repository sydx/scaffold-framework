package com.scaffold.web.handler;

import com.scaffold.redis.CacheKeyConstant;
import com.scaffold.redis.RedisService;
import com.scaffold.security.authentication.captcha.CaptchaHandler;
import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.base.Captcha;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.IOException;
import java.util.Objects;

/**
 * 验证码处理
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/7/24 20:51:43
 */
@Slf4j
public class LocalCaptchaHandler implements CaptchaHandler {

    private static final int WIDTH = 130;
    private static final int HEIGHT = 48;

    private final RedisService redisService;

    public LocalCaptchaHandler(RedisService redisService) {
        this.redisService = redisService;
    }

    /**
     * 校验验证码是否正确
     *
     * @param requestId 请求ID
     * @param captcha   验证码
     * @return boolean
     * @throws
     * @version 1.0
     * @author 李亮
     * @time 2023-07-24 11:20:47
     */
    @Override
    public boolean validate(String requestId, String captcha) {
        String key = getKey(requestId);
        String o = redisService.getString(key);
        if (null == o) {
            return false;
        }
        boolean result = Objects.equals(captcha, o);
        remove(requestId);
        return result;
    }

    @Override
    public Object create(String requestId) throws IOException, FontFormatException {
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(WIDTH, HEIGHT);
        captcha.setFont(Captcha.FONT_5);
        captcha.setLen(3);  // 几位数运算，默认是两位
        captcha.getArithmeticString();  // 获取运算的公式：3+2=?
        String text = captcha.text();// 获取运算的结果：5
        String key = getKey(requestId);
        redisService.set(key, text, 300);
        return captcha;
    }

    @Override
    public void remove(String requestId) {
        redisService.delete(getKey(requestId));
    }

    private String getKey(String requestId) {
        return String.format(CacheKeyConstant.SECURITY_CAPTCHA_KEY_FMT, requestId);
    }
}
