package com.scaffold.web.handler;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.scaffold.common.exception.BaseException;
import com.scaffold.common.exception.TokenException;
import com.scaffold.common.result.R;
import com.scaffold.common.result.RCode;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 统一异常处理
 *
 * @Auther: LiLiang
 * @Date: 2020-05-09 13:49
 * @Description:
 */
@RestControllerAdvice
public class UnifyExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(UnifyExceptionHandler.class);

    /**
     * 公共异常处理方法
     *
     * @param e
     * @return
     */
    @ExceptionHandler
    public R doError(Exception e) {
        logger.error("操作失败", e);
        if (e instanceof TokenException) {
            return R.failure((TokenException) e);
        } else if (e instanceof BaseException) {
            return R.failure((BaseException) e);
        }
        return R.failure(RCode.EXCEPTION);
    }

    /**
     * 参数校验不通过
     * 方法参数为对象Object的校验
     *
     * @param e
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public R validException(MethodArgumentNotValidException e) {
        String[] errors = new String[e.getBindingResult().getFieldErrors().size()];
        int i = 0;
        for (FieldError error : e.getBindingResult().getFieldErrors()) {
            errors[i] = error.getDefaultMessage();
            i++;
        }
        return R.failure(RCode.ARGUMENT, !ArrayUtil.isEmpty(errors) ? ArrayUtil.join(errors, " | ") : StrUtil.EMPTY);
    }

    /**
     * 参数校验不通过
     * 方法参数为List<Object>的校验
     *
     * @param e
     * @return
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public R validException(ConstraintViolationException e) {
        String[] errors = new String[e.getConstraintViolations().size()];
        int i = 0;
        for (ConstraintViolation<?> error : e.getConstraintViolations()) {
            errors[i] = error.getMessage();
            i++;
        }
        return R.failure(RCode.ARGUMENT, !ArrayUtil.isEmpty(errors) ? ArrayUtil.join(errors, " | ") : StrUtil.EMPTY);
    }

    /**
     * 参数校验不通过
     * 方法参数为List<Object>的校验
     *
     * @param e
     * @return
     */
    @ExceptionHandler(BindException.class)
    public R validException(BindException e) {
        String[] errors = new String[e.getFieldErrors().size()];
        int i = 0;
        for (FieldError error : e.getFieldErrors()) {
            errors[i] = error.getDefaultMessage();
            i++;
        }
        return R.failure(RCode.ARGUMENT, !ArrayUtil.isEmpty(errors) ? ArrayUtil.join(errors, " | ") : StrUtil.EMPTY);
    }

    /**
     * 断言错误
     * 方法参数为List<Object>的校验
     *
     * @param e
     * @return
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public R validException(IllegalArgumentException e) {
        return R.failure(RCode.ARGUMENT, e.getMessage());
    }

}
