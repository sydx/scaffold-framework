package com.scaffold.web.handler;

import com.scaffold.mybatisplus.handler.AutoFillHandler;
import com.scaffold.mybatisplus.handler.FillHandler;
import com.scaffold.security.util.AuthenticationUtil;
import com.scaffold.web.entity.AdminUser;
import com.scaffold.web.utils.LoginUserUtil;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 自定义自动填充字段处理器
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/23 13:22:35
 */
@Component
public class LocalFillHandler implements FillHandler {
    private enum CommonFiled {
        createdTime, createdBy, creator, updatedTime, updatedBy
    }

    @Override
    public void insertFill(AutoFillHandler handler, MetaObject metaObject) {
        AdminUser user = LoginUserUtil.getUser();
        if (null != user) {
            handler.setFieldValByName(CommonFiled.createdBy.name(), user.getUserId(), metaObject)
                    .setFieldValByName(CommonFiled.updatedBy.name(), user.getUserId(), metaObject);
        }
        handler.setFieldValByName(CommonFiled.createdTime.name(), LocalDateTime.now(), metaObject)
                .setFieldValByName(CommonFiled.updatedTime.name(), LocalDateTime.now(), metaObject);
    }

    @Override
    public void updateFill(AutoFillHandler handler, MetaObject metaObject) {
        handler.setFieldValByName(CommonFiled.updatedTime.name(), LocalDateTime.now(), metaObject);
        handler.setFieldValByName(CommonFiled.updatedBy.name(), LoginUserUtil.getUserId(), metaObject);
    }
}
