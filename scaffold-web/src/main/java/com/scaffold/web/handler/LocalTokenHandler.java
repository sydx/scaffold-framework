package com.scaffold.web.handler;

import cn.hutool.core.util.StrUtil;
import com.scaffold.system.entity.model.SysUserToken;
import com.scaffold.redis.RedisService;
import com.scaffold.security.authentication.token.Token;
import com.scaffold.security.authentication.token.TokenHandler;
import com.scaffold.system.service.SysUserTokenService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.time.Instant;

/**
 * Token处理
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/7/26 9:23:36
 */
@Component
public class LocalTokenHandler implements TokenHandler {
    private final RedisService redisService;
    private final SysUserTokenService sysUserTokenService;

    public LocalTokenHandler(RedisService redisService, SysUserTokenService sysUserTokenService) {
        this.redisService = redisService;
        this.sysUserTokenService = sysUserTokenService;
    }

    @Override
    public void save(Token token) {
        SysUserToken userToken = sysUserTokenService.getTokenByUid(token.getUid());
        if (null == userToken) {
            userToken = new SysUserToken();
        }
        BeanUtils.copyProperties(token, userToken);
        userToken.setAccessTokenExpiresIn(token.getAccessTokenExpiresIn().intValue());
        userToken.setAccessTokenCreateTime(Instant.now().getEpochSecond());
        userToken.setAccessTokenExpiresTime(userToken.getAccessTokenCreateTime() + userToken.getAccessTokenExpiresIn());
        userToken.setRefreshTokenExpiresIn(token.getRefreshTokenExpiresIn().intValue());
        userToken.setRefreshTokenCreateTime(Instant.now().getEpochSecond());
        userToken.setRefreshTokenExpiresTime(userToken.getRefreshTokenCreateTime() + userToken.getRefreshTokenExpiresIn());
        sysUserTokenService.saveOrUpdate(userToken);
    }

    @Override
    public void set(String key, String token, long expiresIn) {
        redisService.set(key, token, expiresIn);
    }

    @Override
    public String getAccessToken(String key, String uid) {
        String token = redisService.getString(key);
        if (StrUtil.isNotBlank(token)) {
            return token;
        }
        SysUserToken userToken = sysUserTokenService.getAccessToken(uid);
        if (null == userToken) {
            return StrUtil.EMPTY;
        }
        set(key, userToken.getAccessToken(), userToken.getAccessTokenExpiresTime());
        return userToken.getAccessToken();
    }

    @Override
    public String getRefreshToken(String key, String uid) {
        String token = redisService.getString(key);
        if (StrUtil.isNotBlank(token)) {
            return token;
        }
        SysUserToken userToken = sysUserTokenService.getRefreshToken(uid);
        if (null == userToken) {
            return StrUtil.EMPTY;
        }
        set(key, userToken.getRefreshToken(), userToken.getRefreshTokenExpiresTime());
        return userToken.getRefreshToken();
    }

    @Override
    public void delete(String key, String uid) {
        redisService.delete(key);
        sysUserTokenService.destroyToken(uid);
    }
}
