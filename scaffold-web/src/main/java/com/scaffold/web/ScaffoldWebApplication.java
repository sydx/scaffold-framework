package com.scaffold.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
		"com.scaffold.common",
		"com.scaffold.system",
		"com.scaffold.web",
		"com.scaffold.mybatisplus",
		"com.scaffold.security",
		"com.scaffold.redis",
})
@MapperScan(basePackages = {"com.scaffold.system.dao"})
public class ScaffoldWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScaffoldWebApplication.class, args);
	}

}
