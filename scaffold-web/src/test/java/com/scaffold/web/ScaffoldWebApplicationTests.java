package com.scaffold.web;

import com.scaffold.common.util.PasswordUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

//@SpringBootTest
class ScaffoldWebApplicationTests {
	private static final Logger logger = LoggerFactory.getLogger(ScaffoldWebApplicationTests.class);

	@Test
	void contextLoads() {
	}

	@Test
	public void getPwd() {
		logger.info(PasswordUtil.encodeMD5Password("12345678" + "a4ec34b6651f404bb92c0e888938f415"));;
	}
}
