package com.scaffold.web;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.AntPathMatcher;

/**
 * TODO
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/22 11:53:06
 */
public class AntPathMatcherTest {
    private static final Logger logger = LoggerFactory.getLogger(AntPathMatcherTest.class);

    @Test
    void test() {
        AntPathMatcher matcher = new AntPathMatcher();
        logger.info("" + matcher.match("user/123123/role", "user/123123/role"));
        logger.info("" + matcher.match("/{abc}/abc{id}/role", "/user/abc123123/role"));
    }

}
