# 版本说明
## V1.0
## V1.1.0
调整1.0版本在结构上不合理的地方：
1. system模块作为独立模块，其dao、service和entity不再作为独立模块
2. web模块作为独立模块，其dao、service和entity可以作为独立模块，也可以内置在包中
3. common、mybatis-plus、security和redis模块不变
本次调整的目的在于scaffold-framework可以作为一个独立的模块，作为parent被任意项目使用