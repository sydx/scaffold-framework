package com.prefab.factory.entity.query;

import com.baomidou.mybatisplus.core.enums.SqlKeyword;

import com.scaffold.mybatisplus.annotations.Condition;
import com.scaffold.mybatisplus.model.BaseQuery;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 数据源-查询对象
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@Getter
@Setter
public class DatasourceQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer datasourceId;
    /**
     * 数据源名称
     */
    @Condition(keyword = SqlKeyword.LIKE)
    private String datasourceName;
    /**
     * ip或域名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String host;
    /**
     * 端口
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer port;
    /**
     * 数据库
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String dataBase;
    /**
     * 用户名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String username;
    /**
     * 密码
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String password;
    /**
     * 状态。0：停用；1：启用
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer state;
    /**
     * 创建人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer createdBy;
    /**
     * 创建时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date createdTime;
    /**
     * 修改人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer updatedBy;
    /**
     * 修改时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date updatedTime;
    /**
     * 删除状态。等于0：否；大于0：是
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer deleted;

}