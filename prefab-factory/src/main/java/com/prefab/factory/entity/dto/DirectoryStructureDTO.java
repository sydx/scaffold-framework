package com.prefab.factory.entity.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @创建人: 李亮
 * @创建时间: 2022/10/9 9:18:17
 * @描述: TODO
 */
@Getter
@Setter
public class DirectoryStructureDTO {

    private Integer dsId;
    /**
     * 代码生成设置ID
     */
    private Integer settingId;
    /**
     * 生成文件保存根目录。系统绝对路径
     */
    private String baseFolder;
    /**
     * java代码目录。根目录相对路径
     */
    private String javaFolder;
    /**
     * 资源文件目录。根目录相对路径
     */
    private String resourcesFolder;
    /**
     * 基础包名
     */
    private String basePackage;
    /**
     * 模块包名
     */
    private String modulePackage;
    /**
     * 实体类包名
     */
    private String entityPackage;
    /**
     * 实体类后缀
     */
    private String entitySuffix;
    /**
     * 查询实体类包名
     */
    private String entityQueryPackage;
    /**
     * 查询实体类后缀
     */
    private String entityQuerySuffix;
    /**
     * 映射类包名
     */
    private String mapperPackage;
    /**
     * 映射类后缀
     */
    private String mapperSuffix;
    /**
     * 映射文件包名
     */
    private String mapperXmlPackage;
    /**
     * 映射文件后缀
     */
    private String mapperXmlSuffix;
    /**
     * 映射文件保存到。0：resources目录；1：保存到java文件目录
     * 默认保存到resources目录
     */
    private Integer mapperXmlSaveTo;
    /**
     * Service接口包名
     */
    private String servicePackage;
    /**
     * Service接口后缀
     */
    private String serviceSuffix;
    /**
     * Service实现类包名
     */
    private String serviceImplPackage;
    /**
     * Service实现类后缀
     */
    private String serviceImplSuffix;
    /**
     * 控制器包名
     */
    private String controllerPackage;
    /**
     * 控制器后缀
     */
    private String controllerSuffix;
    /**
     * 接口控制器包名
     */
    private String controllerApiPackage;
    /**
     * 接口控制器后缀
     */
    private String controllerApiSuffix;
    /**
     * 页面根目录
     */
    private String pageRootFolder;
    /**
     * 页面目录
     */
    private String pageFolder;
    /**
     * 页面后缀
     */
    private String pageSuffix;
    /**
     * 页面扩展名
     */
    private String pageExtension;
    /**
     * js文件目录
     */
    private String jsFolder;
    /**
     * js文件后缀
     */
    private String jsSuffix;
    /**
     * js文件扩展名
     */
    private String jsExtension;
    /**
     * 状态。0：停用；1：启用
     */
    private Integer state;

}
