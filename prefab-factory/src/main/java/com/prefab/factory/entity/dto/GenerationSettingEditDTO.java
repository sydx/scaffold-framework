package com.prefab.factory.entity.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @创建人: 李亮
 * @创建时间: 2022/10/9 9:16:36
 * @描述: 生成设置保存数据传输对象
 */
@Getter
@Setter
public class GenerationSettingEditDTO {

    private GenerationSettingDTO generationSetting;
    private DirectoryStructureDTO directoryStructure;
}
