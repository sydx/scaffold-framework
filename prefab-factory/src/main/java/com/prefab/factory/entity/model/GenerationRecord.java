package com.prefab.factory.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 代码生成记录
 *
 * @author: 李亮
 * @since: 0.0.1
 * @datetime: 2022-10-10 15:06:44
 */
@Getter
@Setter
@TableName("generation_record")
public class GenerationRecord extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(value = "record_id", type = IdType.AUTO)
    private Integer recordId;
    /**
     * 记录编号
     */
    private Long recordCode;
    /**
     * 方案ID
     */
    private Integer schemeId;
    /**
     * 使用的模板。模板id的json array
     */
    private String templates;
    /**
     * 数据源ID
     */
    private Integer datasourceId;
    /**
     * 数据库
     */
    private String dataBase;
    /**
     * 生成的数据表。表名的json array
     */
    private String tables;
    /**
     * 代码生成方案ID
     */
    private Integer settingId;
    /**
     * 文件保存路径
     */
    private String filePath;
    /**
     * 生成结果.0: 生成中；1：成功；-1：失败
     */
    private Integer state;
    /**
     * 失败原因
     */
    private String reason;

}