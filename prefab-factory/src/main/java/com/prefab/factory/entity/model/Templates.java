package com.prefab.factory.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 模板
 *
 * @author: 李亮
 * @since: 3.2.1
 * @datetime: 2022-09-22 16:44:14
 */
@Getter
@Setter
@TableName("templates")
public class Templates extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(value = "template_id", type = IdType.AUTO)
    private Integer templateId;
    /**
     * 模板分组
     */
    private Integer schemeId;
    /**
     * 模板名称
     */
    private String templateName;
    /**
     * 模板类型
     */
    private String templateType;
    /**
     * 模板引擎
     */
    private String templateEngine;
    /**
     * 模板内容
     */
    private String templateContent;
    /**
     * 状态。0：停用；1：启用
     */
    private Integer state;

}