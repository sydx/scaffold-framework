package com.prefab.factory.entity.query;

import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import com.scaffold.mybatisplus.annotations.Condition;
import com.scaffold.mybatisplus.model.BaseQuery;
import com.scaffold.mybatisplus.model.BaseQuery;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 代码生成记录-查询对象
 * @author: 李亮
 * @since: 0.0.1
 * @datetime: 2022-10-10 15:06:44
 */
@Getter
@Setter
public class GenerationRecordQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer recordId;
    /**
     * 方案ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer schemeId;
    /**
     * 使用的模板。模板id的json array
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String templates;
    /**
     * 数据源ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer datasourceId;
    /**
     * 数据库
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String dataBase;
    /**
     * 生成的数据表。表名的json array
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String tables;
    /**
     * 代码生成方案ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer settingId;
    /**
     * 文件保存路径
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String filePath;
    /**
     * 生成结果.0: 生成中；1：成功；-1：失败
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer state;
    /**
     * 失败原因
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String reason;
    /**
     * 创建人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer createdBy;
    /**
     * 创建时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date createdTime;
    /**
     * 修改人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer updatedBy;
    /**
     * 修改时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date updatedTime;
    /**
     * 删除状态。等于0：否；大于0：是
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer deleted;

}