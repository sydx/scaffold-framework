package com.prefab.factory.entity.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @创建人: 李亮
 * @创建时间: 2022/10/9 9:17:30
 * @描述: TODO
 */
@Getter
@Setter
public class GenerationSettingDTO {
    private Integer settingId;
    /**
     * 代码生成名称
     */
    private String generationName;
    /**
     * 启用lombok。0：不启用；1：启用
     */
    private Integer enableLombok = 0;
    /**
     * 启用MyBatisPlus。0：不启用；1：启用
     */
    private Integer enableMbPlus = 0;
    /**
     * 文件作者
     */
    private String author = "prefab";
    /**
     * 引入版本
     */
    private String since = "ver.0.0.1";
    /**
     * 状态。0：停用；1：启用
     */
    private Integer state;
}
