package com.prefab.factory.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 代码生成设置
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@Getter
@Setter
@TableName("generation_setting")
public class GenerationSetting extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(value = "setting_id", type = IdType.AUTO)
    private Integer settingId;
    /**
     * 代码生成名称
     */
    private String generationName;
    /**
     * 启用lombok。0：不启用；1：启用
     */
    private Integer enableLombok = 0;
    /**
     * 启用MyBatisPlus。0：不启用；1：启用
     */
    private Integer enableMbPlus = 0;
    /**
     * 文件作者
     */
    private String author = "prefab";
    /**
     * 引入版本
     */
    private String since = "ver.0.0.1";
    /**
     * 状态。0：停用；1：启用
     */
    private Integer state;

}