package com.prefab.factory.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 数据源
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@Getter
@Setter
@TableName("datasource")
public class Datasource extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(value = "datasource_id", type = IdType.AUTO)
    private Integer datasourceId;
    /**
     * 数据源名称
     */
    private String datasourceName;
    /**
     * 数据源类型。MySQL、SQLServer等
     */
    private String datasourceType;
    /**
     * ip或域名
     */
    private String host;
    /**
     * 端口
     */
    private Integer port;
    /**
     * 数据库
     */
    private String dataBase;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 状态。0：停用；1：启用
     */
    private Integer state;

}