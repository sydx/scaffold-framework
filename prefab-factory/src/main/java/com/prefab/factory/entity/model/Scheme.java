package com.prefab.factory.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.scaffold.common.validation.group.Update;
import com.scaffold.mybatisplus.model.BaseModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

/**
 * 生成方案
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@Getter
@Setter
@TableName("scheme")
public class Scheme extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(value = "scheme_id", type = IdType.AUTO)
    @NotNull(message = "方案ID不能为空", groups = Update.class)
    private Integer schemeId;
    /**
     * 方案名称
     */
    @NotBlank(message = "方案名称不能为空")
    private String schemeName;
    /**
     * 状态。0：停用；1：启用
     */
    private Integer state;

}