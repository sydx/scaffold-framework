package com.prefab.factory.entity.query;

import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import com.scaffold.mybatisplus.annotations.Condition;
import com.scaffold.mybatisplus.model.BaseQuery;
import com.scaffold.mybatisplus.model.BaseQuery;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 目录结构设置-查询对象
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@Getter
@Setter
public class DirectoryStructureQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer dsId;
    /**
     * 代码生成设置ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer settingId;
    /**
     * 生成文件保存根目录。系统绝对路径
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String baseFolder;
    /**
     * java代码目录。根目录相对路径
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String javaFolder;
    /**
     * 资源文件目录。根目录相对路径
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String resourcesFolder;
    /**
     * 基础包名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String basePackage;
    /**
     * 模块包名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String modulePackage;
    /**
     * 实体类包名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String entityPackage;
    /**
     * 实体类后缀
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String entitySuffix;
    /**
     * 查询实体类包名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String entityQueryPackage;
    /**
     * 查询实体类后缀
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String entityQuerySuffix;
    /**
     * 映射类包名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String mapperPackage;
    /**
     * 映射类后缀
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String mapperSuffix;
    /**
     * 映射文件包名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String mapperXmlPackage;
    /**
     * 映射文件后缀
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String mapperXmlSuffix;
    /**
     * 映射文件保存到。0：resources目录；1：保存到java文件目录
     * 默认保存到resources目录
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer mapperXmlSaveTo;
    /**
     * Service接口包名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String servicePackage;
    /**
     * Service接口后缀
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String serviceSuffix;
    /**
     * Service实现类包名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String serviceImplPackage;
    /**
     * Service实现类后缀
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String serviceImplSuffix;
    /**
     * 控制器包名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String controllerPackage;
    /**
     * 控制器后缀
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String controllerSuffix;
    /**
     * 接口控制器包名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String controllerApiPackage;
    /**
     * 接口控制器后缀
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String controllerApiSuffix;
    /**
     * 页面根目录
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String pageRootFolder;
    /**
     * 页面目录
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String pageFolder;
    /**
     * 页面后缀
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String pageSuffix;
    /**
     * 页面扩展名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String pageExtension;
    /**
     * js文件目录
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String jsFolder;
    /**
     * js文件后缀
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String jsSuffix;
    /**
     * js文件扩展名
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String jsExtension;
    /**
     * 状态。0：停用；1：启用
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer state;
    /**
     * 创建人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer createdBy;
    /**
     * 创建时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date createdTime;
    /**
     * 修改人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer updatedBy;
    /**
     * 修改时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date updatedTime;
    /**
     * 删除状态。等于0：否；大于0：是
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer deleted;

}