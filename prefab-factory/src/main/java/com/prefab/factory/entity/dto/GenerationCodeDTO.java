package com.prefab.factory.entity.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @创建人: 李亮
 * @创建时间: 2022/10/9 9:16:36
 * @描述: 代码生成数据传输对象
 */
@Getter
@Setter
public class GenerationCodeDTO {

    /**
     * 模板方案ID
     */
    private Integer schemeId;
    /**
     * 模板ID，JSON Array字符串
     */
    private String templates;
    /**
     * 数据源ID
     */
    private Integer datasourceId;
    /**
     * 数据库
     */
    private String dataBase;
    /**
     * 数据表，JSON Array字符串
     */
    private String tables;
    /**
     * 生成方案ID
     */
    private Integer settingId;
}
