package com.prefab.factory.entity.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @创建人: 李亮
 * @创建时间: 2022/10/9 9:16:36
 * @描述: 代码预览数据传输对象
 */
@Getter
@Setter
public class GenerationPreviewDTO {
    /**
     * 模板ID
     */
    private Integer templateId;
    /**
     * 模板内容
     */
    private String template;
    /**
     * 模板类型
     */
    private String templateType;
    /**
     * 数据源ID
     */
    private Integer datasourceId;
    /**
     * 数据库
     */
    private String dataBase;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 生成方案ID
     */
    private Integer settingId;
}
