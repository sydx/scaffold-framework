package com.prefab.factory.entity.vo;


import com.prefab.factory.entity.model.DirectoryStructure;
import com.prefab.factory.entity.model.GenerationSetting;

/**
 * @创建人: 李亮
 * @创建时间: 2022/10/8 15:12:46
 * @描述: 代码生成设置
 */
public class GenerationSettingVO {

    private String deployment;
    private GenerationSetting generationSetting;
    private DirectoryStructure directoryStructure;

    public String getDeployment() {
        return deployment;
    }

    public void setDeployment(String deployment) {
        this.deployment = deployment;
    }

    public GenerationSetting getGenerationSetting() {
        return generationSetting;
    }

    public void setGenerationSetting(GenerationSetting generationSetting) {
        this.generationSetting = generationSetting;
    }

    public DirectoryStructure getDirectoryStructure() {
        return directoryStructure;
    }

    public void setDirectoryStructure(DirectoryStructure directoryStructure) {
        this.directoryStructure = directoryStructure;
    }
}
