package com.prefab.factory.entity.query;

import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import com.scaffold.mybatisplus.annotations.Condition;
import com.scaffold.mybatisplus.model.BaseQuery;
import com.scaffold.mybatisplus.model.BaseQuery;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 模板-查询对象
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-29 15:20:15
 */
@Getter
@Setter
public class TemplatesQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer templateId;
    /**
     * 方案ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer schemeId;
    /**
     * 模板名称
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String templateName;
    /**
     * 模板类型
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String templateType;
    /**
     * 模板引擎
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String templateEngine;
    /**
     * 模板内容
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String templateContent;
    /**
     * 状态。0：停用；1：启用
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer state;
    /**
     * 创建人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer createdBy;
    /**
     * 创建时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date createdTime;
    /**
     * 修改人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer updatedBy;
    /**
     * 修改时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date updatedTime;
    /**
     * 删除状态。等于0：否；大于0：是
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer deleted;

}