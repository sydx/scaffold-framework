package com.prefab.factory.entity;

import cn.hutool.core.util.ArrayUtil;
import com.scaffold.system.entity.model.SysUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @创建人: 李亮
 * @创建时间: 2022/5/27 9:57:01
 * @描述: 管理员用户
 */
public class AdminUser extends SysUser implements UserDetails {

    private List<String> orgCodes;
    private String currentTopOrgCode;

    private boolean credentialsNonExpired = true;

    private boolean passwordNeedReset = false;
    //权限编码
    private Set<String> permissions;

    Collection<? extends GrantedAuthority> authorities;

    public List<String> getOrgCodes() {
        return orgCodes;
    }

    public void setOrgCodes(List<String> orgCodes) {
        this.orgCodes = orgCodes;
    }

    public String getCurrentTopOrgCode() {
        return currentTopOrgCode;
    }

    public void setCurrentTopOrgCode(String currentTopOrgCode) {
        this.currentTopOrgCode = currentTopOrgCode;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<String> permissions) {
        this.permissions = permissions;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList(ArrayUtil.toArray(permissions, String.class));
    }

    /**
     * @描述 表示用户的账号是否已经过期。无法验证过期的帐户。
     * @参数
     * @返回值: boolean 如果用户的帐户有效（即未过期）则为true，如果不再有效（即已过期）则为false
     * @创建人 李亮
     * @创建时间 2022-05-27 10:15:46
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * @描述 指示用户是被锁定还是解锁。无法验证锁定的用户
     * @参数
     * @返回值: boolean 如果用户未锁定，则为 true，否则为 false
     * @创建人 李亮
     * @创建时间 2022-05-27 10:16:35
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    /**
     * @描述 指示用户的凭据（密码）是否已过期。过期的凭据会阻止身份验证。
     * @参数
     * @返回值: boolean true用户的凭据有效（即未过期）则为true ，如果不再有效（即过期）则为 false
     * @创建人 李亮
     * @创建时间 2022-05-27 10:17:22
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    /**
     * @描述 指示用户是启用还是禁用。无法验证禁用的用户。
     * @参数
     * @返回值: boolean 如果用户已启用，则为true ，否则为false
     * @创建人 李亮
     * @创建时间 2022-05-27 10:17:58
     */
    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPasswordNeedReset(boolean passwordNeedReset) {
        this.passwordNeedReset = passwordNeedReset;
    }

    /**
     * @描述 是否需要重置密码
     * @参数
     * @返回值: boolean
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-18 14:18:19
     */
    public boolean isPasswordNeedReset() {
        return passwordNeedReset;
    }
}
