package com.prefab.factory.utils;

import com.scaffold.security.util.AuthenticationUtil;
import com.prefab.factory.entity.AdminUser;

/**
 * 获取登录用户信息
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/23 15:16:10
 */
public class LoginUserUtil {

    public static AdminUser getUser() {
        Object user = AuthenticationUtil.getUser();
        return null != user ? (AdminUser)user : null;
    }

    public static Integer getUserId() {
        AdminUser user = getUser();
        return null != user ? user.getUserId() : null;
    }

}
