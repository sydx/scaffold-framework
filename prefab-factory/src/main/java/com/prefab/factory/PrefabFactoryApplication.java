package com.prefab.factory;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
        "com.scaffold.common",
        "com.scaffold.system",
        "com.scaffold.mybatisplus",
        "com.scaffold.security",
        "com.scaffold.redis",
        "com.prefab.factory",
        "com.prefab.generation",
})
@MapperScan(basePackages = {"com.scaffold.system.dao", "com.prefab.factory.dao"})
public class PrefabFactoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrefabFactoryApplication.class, args);
    }

}
