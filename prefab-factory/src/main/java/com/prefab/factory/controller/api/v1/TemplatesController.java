package com.prefab.factory.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.prefab.factory.entity.model.Templates;
import com.prefab.factory.entity.query.TemplatesQuery;
import com.prefab.factory.service.TemplatesService;
import com.scaffold.common.result.R;
import com.scaffold.mybatisplus.factory.WrapperFactory;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 模板-接口控制器
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-29 15:20:15
 */
@RestController
@RequestMapping("/api/v1/templates")
public class TemplatesController {

    private final TemplatesService templatesService;

    public TemplatesController(TemplatesService templatesService) {
        this.templatesService = templatesService;
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping
    public R<Page<Templates>> list(TemplatesQuery query) {
        WrapperFactory<Templates> factory = WrapperFactory.build();
        LambdaQueryWrapper<Templates> queryWrapper = factory.query(query).lambda()
                .orderByAsc(Templates::getTemplateId);
        Page<Templates> page = new Page<>(query.getPage(), query.getSize());
        templatesService.page(page, queryWrapper);
        return R.success(page);
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping("/all")
    public R all(TemplatesQuery query) {
        WrapperFactory<Templates> factory = WrapperFactory.build();
        LambdaQueryWrapper<Templates> queryWrapper = factory.query(query).lambda()
                .select(
                        Templates::getTemplateId,
                        Templates::getTemplateName,
                        Templates::getTemplateType,
                        Templates::getTemplateEngine,
                        Templates::getState,
                        Templates::getUpdatedTime
                )
                .orderByAsc(Templates::getTemplateType);
        List<Templates> list = templatesService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping("/{templateId}")
    public R getById(@PathVariable Integer templateId) {
        Templates templates = templatesService.getTemplatesById(templateId);
        Assert.notNull(templates, "模板不存在！");
        return R.success(templates);
    }

    /**
     * 新增
     *
     * @return
     */
    @PostMapping
    public R add(@RequestBody @Validated Templates templates) {
        boolean update = templatesService.save(templates);
        Assert.isTrue(update, "新增失败！");
        return R.success();
    }

    /**
     * 修改
     *
     * @return
     */
    @PutMapping
    public R update(@RequestBody @Validated Templates templates) {
        boolean update = templatesService.updateById(templates);
        Assert.isTrue(update, "修改失败！");
        return R.success();
    }

    /**
     * 删除
     *
     * @return
     */
    @DeleteMapping("/{templateId}")
    public R delete(@PathVariable Integer templateId) {
        Assert.notNull(templateId, "templateId不能为空");
        boolean update = templatesService.removeById(templateId);
        Assert.isTrue(update, "删除失败！");
        return R.success();
    }

    /**
     * 修改状态
     *
     * @return
     */
    @PatchMapping("/state/{templateId}/{state}")
    public R state(@PathVariable Integer templateId, @PathVariable Integer state) {
        Assert.notNull(templateId, "templateId不能为空");
        Assert.notNull(state, "状态不能为空");
        Templates templates = templatesService.getById(templateId);
        Assert.notNull(templates, "数据不存在");
        Templates update = new Templates();
        update.setTemplateId(templates.getTemplateId());
        update.setState(state);
        boolean result = templatesService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }

    /**
     * 复制方案
     *
     * @return
     */
    @PostMapping("/copy/{templateId}")
    public R copy(@PathVariable Integer templateId) {
        Assert.notNull(templateId, "templateId不能为空");
        templatesService.copyTemplate(templateId);
        return R.success();
    }

}