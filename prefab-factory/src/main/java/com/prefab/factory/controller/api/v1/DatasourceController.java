package com.prefab.factory.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.prefab.factory.entity.model.Datasource;
import com.prefab.factory.entity.query.DatasourceQuery;
import com.prefab.factory.service.DatasourceService;
import com.scaffold.common.result.R;
import com.scaffold.mybatisplus.factory.WrapperFactory;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 数据源-接口控制器
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@RestController
@RequestMapping("/api/v1/datasource")
public class DatasourceController {

    private final DatasourceService datasourceService;

    public DatasourceController(DatasourceService datasourceService) {
        this.datasourceService = datasourceService;
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping
    public R<Page<Datasource>> list(DatasourceQuery query) {
        WrapperFactory<Datasource> factory = WrapperFactory.build();
        LambdaQueryWrapper<Datasource> queryWrapper = factory.query(query).lambda()
                .orderByAsc(Datasource::getDatasourceId);
        Page<Datasource> page = new Page<>(query.getPage(), query.getSize());
        datasourceService.page(page, queryWrapper);
        return R.success(page);
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping("/all")
    public R all(DatasourceQuery query) {
        WrapperFactory<Datasource> factory = WrapperFactory.build();
        LambdaQueryWrapper<Datasource> queryWrapper = factory.query(query).lambda()
                .orderByAsc(Datasource::getDatasourceId);
        List<Datasource> list = datasourceService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 新增
     *
     * @return
     */
    @PostMapping
    public R add(@RequestBody @Validated Datasource datasource) {
        boolean update = datasourceService.save(datasource);
        Assert.isTrue(update, "新增失败！");
        return R.success();
    }

    /**
     * 修改
     *
     * @return
     */
    @PutMapping
    public R update(@RequestBody @Validated Datasource datasource) {
        boolean update = datasourceService.updateById(datasource);
        Assert.isTrue(update, "修改失败！");
        return R.success();
    }

    /**
     * 删除
     *
     * @return
     */
    @DeleteMapping("/{datasourceId}")
    public R delete(@PathVariable Integer datasourceId) {
        Assert.notNull(datasourceId, "datasourceId不能为空");
        boolean update = datasourceService.removeById(datasourceId);
        Assert.isTrue(update, "删除失败！");
        return R.success();
    }

    /**
     * 修改状态
     *
     * @return
     */
    @PatchMapping("/state/{datasourceId}/{state}")
    public R state1(@PathVariable Integer datasourceId, @PathVariable Integer state) {
        Assert.notNull(datasourceId, "datasourceId不能为空");
        Assert.notNull(state, "状态不能为空");
        Datasource datasource = datasourceService.getById(datasourceId);
        Assert.notNull(datasource, "数据不存在");
        Datasource update = new Datasource();
        update.setDatasourceId(datasource.getDatasourceId());
        update.setState(state);
        boolean result = datasourceService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }

    /**
     * @描述 获取数据库列表
     * @参数 datasourceId
     * @返回值: com.prefab.common.tools.result.R
     * @异常
     * @创建人 李亮
     * @创建时间 2022-10-10 09:37:38
     */
    @GetMapping("/dataBase/{datasourceId}")
    public R getDataBase(@PathVariable Integer datasourceId) {
        return R.success(datasourceService.listDataBase(datasourceId));
    }

    /**
     * @描述 获取数据库中的表
     * @参数 datasourceId
     * @参数 dataBase
     * @返回值: com.prefab.common.tools.result.R
     * @异常
     * @创建人 李亮
     * @创建时间 2022-10-10 09:37:53
     */
    @GetMapping("/tables/{datasourceId}/{dataBase}")
    public R getDataBaseTable(@PathVariable Integer datasourceId, @PathVariable String dataBase) {
        return R.success(datasourceService.listTable(datasourceId, dataBase));
    }
}