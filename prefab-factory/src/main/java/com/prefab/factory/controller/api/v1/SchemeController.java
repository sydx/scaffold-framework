package com.prefab.factory.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.prefab.factory.entity.model.Scheme;
import com.prefab.factory.entity.query.SchemeQuery;
import com.prefab.factory.service.SchemeService;
import com.scaffold.common.result.R;
import com.scaffold.common.validation.group.Update;
import com.scaffold.mybatisplus.factory.WrapperFactory;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 生成方案-接口控制器
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@RestController
@RequestMapping("/api/v1/scheme")
public class SchemeController {

    private final SchemeService schemeService;

    public SchemeController(SchemeService schemeService) {
        this.schemeService = schemeService;
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping
    public R<Page<Scheme>> list(SchemeQuery query) {
        WrapperFactory<Scheme> factory = WrapperFactory.build();
        LambdaQueryWrapper<Scheme> queryWrapper = factory.query(query).lambda()
                .orderByAsc(Scheme::getSchemeId);
        Page<Scheme> page = new Page<>(query.getPage(), query.getSize());
        schemeService.page(page, queryWrapper);
        return R.success(page);
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping("/all")
    public R all(SchemeQuery query) {
        WrapperFactory<Scheme> factory = WrapperFactory.build();
        LambdaQueryWrapper<Scheme> queryWrapper = factory.query(query).lambda()
                .orderByAsc(Scheme::getSchemeId);
        List<Scheme> list = schemeService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 新增
     *
     * @return
     */
    @PostMapping
    public R add(@RequestBody @Validated Scheme scheme) {
        boolean update = schemeService.save(scheme);
        Assert.isTrue(update, "新增失败！");
        return R.success();
    }

    /**
     * 修改
     *
     * @return
     */
    @PutMapping
    public R update(@RequestBody @Validated({Update.class}) Scheme scheme) {
        boolean update = schemeService.updateById(scheme);
        Assert.isTrue(update, "修改失败！");
        return R.success();
    }

    /**
     * 删除
     *
     * @return
     */
    @DeleteMapping("/{schemeId}")
    public R delete(@PathVariable Integer schemeId) {
        Assert.notNull(schemeId, "schemeId不能为空");
        schemeService.deleteScheme(schemeId);
        return R.success();
    }

    /**
     * 修改状态
     *
     * @return
     */
    @PatchMapping("/state/{schemeId}/{state}")
    public R state(@PathVariable Integer schemeId, @PathVariable Integer state) {
        Assert.notNull(schemeId, "schemeId不能为空");
        Assert.notNull(state, "状态不能为空");
        Scheme scheme = schemeService.getById(schemeId);
        Assert.notNull(scheme, "数据不存在");
        Scheme update = new Scheme();
        update.setSchemeId(scheme.getSchemeId());
        update.setState(state);
        boolean result = schemeService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }


    /**
     * 复制方案
     *
     * @return
     */
    @PostMapping("/copy/{schemeId}")
    public R copy(@PathVariable Integer schemeId) {
        Assert.notNull(schemeId, "schemeId不能为空");
        schemeService.copyScheme(schemeId);
        return R.success();
    }

}