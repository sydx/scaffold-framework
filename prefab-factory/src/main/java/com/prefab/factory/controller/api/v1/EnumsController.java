package com.prefab.factory.controller.api.v1;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.prefab.generation.enums.DatasourceTypeEnum;
import com.prefab.generation.enums.TemplateTypeEnum;
import com.scaffold.common.result.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/22 16:51:33
 * @描述: 工作台
 */
@RestController
@RequestMapping("/api/v1/enums")
public class EnumsController {

    @GetMapping("/templateType")
    public R templateType() {
        JSONArray enums = new JSONArray();
        for (TemplateTypeEnum typeEnum : TemplateTypeEnum.values()) {
            enums.add(JSONObject.of("name", typeEnum.name(), "abbr", typeEnum.getAbbr(), "label", typeEnum.getLabel()));
        }
        return R.success(enums);
    }

    @GetMapping("/datasourceType")
    public R datasourceType() {
        JSONArray enums = new JSONArray();
        for (DatasourceTypeEnum typeEnum : DatasourceTypeEnum.values()) {
            enums.add(JSONObject.of("name", typeEnum.name(), "label", typeEnum.name()));
        }
        return R.success(enums);
    }

}
