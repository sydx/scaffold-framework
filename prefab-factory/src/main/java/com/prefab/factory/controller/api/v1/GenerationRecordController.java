package com.prefab.factory.controller.api.v1;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.prefab.factory.entity.model.GenerationRecord;
import com.prefab.factory.entity.query.GenerationRecordQuery;
import com.prefab.factory.service.GenerationRecordService;
import com.scaffold.common.result.R;
import com.scaffold.mybatisplus.factory.WrapperFactory;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * 代码生成记录-接口控制器
 *
 * @author: 李亮
 * @since: 0.0.1
 * @datetime: 2022-10-10 14:26:39
 */
@RestController
@RequestMapping("/api/v1/generation/record")
public class GenerationRecordController {

    private final GenerationRecordService generationRecordService;

    public GenerationRecordController(GenerationRecordService generationRecordService) {
        this.generationRecordService = generationRecordService;
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping
    public R<Page<GenerationRecord>> list(GenerationRecordQuery query) {
        WrapperFactory<GenerationRecord> factory = WrapperFactory.build();
        LambdaQueryWrapper<GenerationRecord> queryWrapper = factory.query(query).lambda()
                .orderByDesc(GenerationRecord::getCreatedTime);
        Page<GenerationRecord> page = new Page<>(query.getPage(), query.getSize());
        generationRecordService.page(page, queryWrapper);
        return R.success(page);
    }

    /**
     * 删除
     *
     * @return
     */
    @DeleteMapping("/{recordId}")
    public R delete(@PathVariable Integer recordId) {
        Assert.notNull(recordId, "recordId不能为空");
        GenerationRecord record = generationRecordService.getById(recordId);
        Assert.notNull(record, "记录不存在");
        boolean update = generationRecordService.removeById(recordId);
        Assert.isTrue(update, "删除失败！");
        //删除文件
        FileUtil.del(record.getFilePath());
        return R.success();
    }


}