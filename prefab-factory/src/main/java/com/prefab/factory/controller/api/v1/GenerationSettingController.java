package com.prefab.factory.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.prefab.factory.entity.dto.GenerationSettingEditDTO;
import com.prefab.factory.entity.model.GenerationSetting;
import com.prefab.factory.entity.query.GenerationSettingQuery;
import com.prefab.factory.service.GenerationSettingService;
import com.scaffold.common.result.R;
import com.scaffold.mybatisplus.factory.WrapperFactory;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 代码生成设置-接口控制器
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@RestController
@RequestMapping("/api/v1/generation/setting")
public class GenerationSettingController {

    private final GenerationSettingService generationSettingService;

    public GenerationSettingController(GenerationSettingService generationSettingService) {
        this.generationSettingService = generationSettingService;
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping
    public R<Page<GenerationSetting>> listPage(GenerationSettingQuery query) {
        WrapperFactory<GenerationSetting> factory = WrapperFactory.build();
        LambdaQueryWrapper<GenerationSetting> queryWrapper = factory.query(query).lambda()
                .orderByAsc(GenerationSetting::getSettingId);
        Page<GenerationSetting> page = new Page<>(query.getPage(), query.getSize());
        generationSettingService.page(page, queryWrapper);
        return R.success(page);
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping("/all")
    public R<List<GenerationSetting>> listAll(GenerationSettingQuery query) {
        WrapperFactory<GenerationSetting> factory = WrapperFactory.build();
        LambdaQueryWrapper<GenerationSetting> queryWrapper = factory.query(query).lambda()
                .orderByAsc(GenerationSetting::getSettingId);
        return R.success(generationSettingService.list(queryWrapper));
    }


    /**
     * 根据ID获取设置信息
     *
     * @return
     */
    @GetMapping("/{settingId}")
    public R getById(@PathVariable Integer settingId) {
        Assert.notNull(settingId, "settingId不能为空");
        return R.success(generationSettingService.getGenerationSettingById(settingId));
    }

    /**
     * 新增
     *
     * @return
     */
    @PostMapping
    public R add(@RequestBody @Validated GenerationSetting generationSetting) {
        boolean update = generationSettingService.save(generationSetting);
        Assert.isTrue(update, "新增失败！");
        return R.success();
    }

    /**
     * 修改
     *
     * @return
     */
    @PutMapping
    public R update(@RequestBody @Validated GenerationSetting generationSetting) {
        boolean update = generationSettingService.updateById(generationSetting);
        Assert.isTrue(update, "修改失败！");
        return R.success();
    }

    /**
     * 保存设置
     *
     * @return
     */
    @PutMapping("/saveSetting")
    public R saveSetting(@RequestBody @Validated GenerationSettingEditDTO dto) {
        generationSettingService.saveSetting(dto);
        return R.success();
    }

    /**
     * 删除
     *
     * @return
     */
    @DeleteMapping("/{settingId}")
    public R delete(@PathVariable Integer settingId) {
        Assert.notNull(settingId, "settingId不能为空");
        generationSettingService.deleteSetting(settingId);
        return R.success();
    }

    /**
     * 修改状态
     *
     * @return
     */
    @PatchMapping("/state/{settingId}/{state}")
    public R state(@PathVariable Integer settingId, @PathVariable Integer state) {
        Assert.notNull(settingId, "settingId不能为空");
        Assert.notNull(state, "状态不能为空");
        GenerationSetting generationSetting = generationSettingService.getById(settingId);
        Assert.notNull(generationSetting, "数据不存在");
        GenerationSetting update = new GenerationSetting();
        update.setSettingId(generationSetting.getSettingId());
        update.setState(state);
        boolean result = generationSettingService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }

    /**
     * 复制方案
     *
     * @return
     */
    @PostMapping("/copy/{settingId}")
    public R copy(@PathVariable Integer settingId) {
        Assert.notNull(settingId, "settingId不能为空");
        generationSettingService.copySetting(settingId);
        return R.success();
    }

}