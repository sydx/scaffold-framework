package com.prefab.factory.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.prefab.factory.entity.model.DirectoryStructure;
import com.prefab.factory.entity.query.DirectoryStructureQuery;
import com.prefab.factory.service.DirectoryStructureService;
import com.scaffold.common.result.R;
import com.scaffold.mybatisplus.factory.WrapperFactory;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 目录结构设置-接口控制器
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@RestController
@RequestMapping("/api/v1/directory/structure")
public class DirectoryStructureController {

    private final DirectoryStructureService directoryStructureService;

    public DirectoryStructureController(DirectoryStructureService directoryStructureService) {
        this.directoryStructureService = directoryStructureService;
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping
    public R<Page<DirectoryStructure>> list(DirectoryStructureQuery query) {
        WrapperFactory<DirectoryStructure> factory = WrapperFactory.build();
        LambdaQueryWrapper<DirectoryStructure> queryWrapper = factory.query(query).lambda()
                .orderByAsc(DirectoryStructure::getDsId);
        Page<DirectoryStructure> page = new Page<>(query.getPage(), query.getSize());
        directoryStructureService.page(page, queryWrapper);
        return R.success(page);
    }

    /**
     * 新增
     *
     * @return
     */
    @PostMapping
    public R add(@RequestBody @Validated DirectoryStructure directoryStructure) {
        boolean update = directoryStructureService.save(directoryStructure);
        Assert.isTrue(update, "新增失败！");
        return R.success();
    }

    /**
     * 修改
     *
     * @return
     */
    @PutMapping
    public R update(@RequestBody @Validated DirectoryStructure directoryStructure) {
        boolean update = directoryStructureService.updateById(directoryStructure);
        Assert.isTrue(update, "修改失败！");
        return R.success();
    }

    /**
     * 删除
     *
     * @return
     */
    @DeleteMapping("/{dsId}")
    public R delete(@PathVariable Integer dsId) {
        Assert.notNull(dsId, "dsId不能为空");
        boolean update = directoryStructureService.removeById(dsId);
        Assert.isTrue(update, "删除失败！");
        return R.success();
    }

    /**
     * 修改状态
     *
     * @return
     */
    @PatchMapping("/state/{dsId}/{state}")
    public R state1(@PathVariable Integer dsId, @PathVariable Integer state) {
        Assert.notNull(dsId, "dsId不能为空");
        Assert.notNull(state, "状态不能为空");
        DirectoryStructure directoryStructure = directoryStructureService.getById(dsId);
        Assert.notNull(directoryStructure, "数据不存在");
        DirectoryStructure update = new DirectoryStructure();
        update.setDsId(directoryStructure.getDsId());
        update.setState(state);
        boolean result = directoryStructureService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }

}