package com.prefab.factory.controller.api.v1;

import com.prefab.factory.entity.dto.GenerationCodeDTO;
import com.prefab.factory.entity.dto.GenerationPreviewDTO;
import com.prefab.factory.service.GenerationCodeService;
import com.prefab.factory.service.GenerationRecordService;
import com.scaffold.common.result.R;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * 代码生成设置-接口控制器
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@RestController
@RequestMapping("/api/v1/generation/code")
public class GenerationCodeController {

    private final GenerationCodeService generationCodeService;
    private final GenerationRecordService generationRecordService;

    public GenerationCodeController(GenerationCodeService generationCodeService, GenerationRecordService generationRecordService) {
        this.generationCodeService = generationCodeService;
        this.generationRecordService = generationRecordService;
    }

    /**
     * 生成代码
     *
     * @return
     */
    @PostMapping
    public R generation(@RequestBody @Validated GenerationCodeDTO generationCodeDTO) {
        return R.success(generationCodeService.generation(generationCodeDTO));
    }

    /**
     * 下载代码
     *
     * @param response
     * @param recordId
     * @throws IOException
     */
    @GetMapping
    public void download(HttpServletResponse response, Integer recordId) throws IOException {
        generationCodeService.download(response, recordId);
    }

    /**
     * 代码预览
     *
     * @param generationPreviewDTO 数据传输对象
     * @return R
     */
    @PostMapping("/preview")
    public R preview(@RequestBody @Validated GenerationPreviewDTO generationPreviewDTO) {
        return R.success(generationCodeService.preview(generationPreviewDTO));
    }

}