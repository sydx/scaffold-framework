package com.prefab.factory.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prefab.factory.dao.GenerationRecordMapper;
import com.prefab.factory.entity.model.GenerationRecord;
import com.prefab.factory.service.GenerationRecordService;
import org.springframework.stereotype.Service;
/**
 * 代码生成记录-业务处理实现类
 * @author: 李亮
 * @since: 0.0.1
 * @datetime: 2022-10-10 14:26:39
 */
@Service
public class GenerationRecordServiceImpl extends ServiceImpl<GenerationRecordMapper, GenerationRecord> implements GenerationRecordService {

}