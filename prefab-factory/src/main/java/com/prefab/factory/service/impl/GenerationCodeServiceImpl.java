package com.prefab.factory.service.impl;

import cn.hutool.core.compress.ZipWriter;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.prefab.factory.config.GenerationProperties;
import com.prefab.factory.entity.dto.GenerationCodeDTO;
import com.prefab.factory.entity.dto.GenerationPreviewDTO;
import com.prefab.factory.entity.model.Datasource;
import com.prefab.factory.entity.model.DirectoryStructure;
import com.prefab.factory.entity.model.GenerationRecord;
import com.prefab.factory.entity.model.GenerationSetting;
import com.prefab.factory.service.*;
import com.prefab.generation.entity.dto.DataSourceDTO;
import com.prefab.generation.entity.model.GenerationAttributes;
import com.prefab.generation.service.CodeGenerationV2Service;
import com.scaffold.common.enums.YesNoEnum;
import com.scaffold.common.exception.SystemException;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/10/10 9:54:10
 * @描述: 生成代码业务处理
 */
@Service
public class GenerationCodeServiceImpl implements GenerationCodeService {

    private final CodeGenerationV2Service codeGenerationV2Service;
    private final DatasourceService datasourceService;
    private final GenerationSettingService generationSettingService;
    private final DirectoryStructureService directoryStructureService;
    private final GenerationRecordService generationRecordService;
    private final GenerationProperties generationProperties;

    public GenerationCodeServiceImpl(CodeGenerationV2Service codeGenerationV2Service, DatasourceService datasourceService, GenerationSettingService generationSettingService, DirectoryStructureService directoryStructureService, GenerationProperties generationProperties, GenerationRecordService generationRecordService) {
        this.codeGenerationV2Service = codeGenerationV2Service;
        this.datasourceService = datasourceService;
        this.generationSettingService = generationSettingService;
        this.directoryStructureService = directoryStructureService;
        this.generationProperties = generationProperties;
        this.generationRecordService = generationRecordService;
    }

    @Override
    public GenerationRecord generation(GenerationCodeDTO dto) {
        GenerationAttributes attributes = getGenerationAttributes(dto);

        GenerationRecord record = null;
        if (StrUtil.equals(generationProperties.getDeployment(), "server")) {
            record = new GenerationRecord();
            BeanUtils.copyProperties(dto, record);
            record.setRecordCode(Instant.now().toEpochMilli());
            record.setState(0);
            generationRecordService.save(record);
            attributes.setBaseFolder(generationProperties.getSaveFolder() + "/" + record.getRecordCode());
        }

        try {
            codeGenerationV2Service.generation(attributes);
            if (null != record) {
                record.setState(1);
                record.setFilePath(attributes.getBaseFolder());
            }
        } catch (Exception e) {
            if (null != record) {
                record.setState(-1);
                record.setReason(e.getMessage());
                FileUtil.del(attributes.getBaseFolder());
            }
            throw new SystemException(e);
        } finally {
            if (null != record) {
                generationRecordService.updateById(record);
            }
        }
        return record;
    }

    @NotNull
    private GenerationAttributes getGenerationAttributes(GenerationCodeDTO dto) {
        GenerationAttributes attributes = new GenerationAttributes();
        attributes.setTemplateIds(dto.getTemplates());
        attributes.setGenerationTables(dto.getTables());
        attributes.setDataSource(getDataSourceDTO(dto.getDatasourceId(), dto.getDataBase()));
        setGenerationSetting(attributes, dto.getSettingId());
        setGenerationFolder(attributes, dto.getSettingId());
        return attributes;
    }

    @NotNull
    private DataSourceDTO getDataSourceDTO(Integer datasourceId, String dataBase) {
        Datasource datasource = datasourceService.getById(datasourceId);
        Assert.notNull(datasource, "数据源不存在！");
        datasource.setDataBase(dataBase);
        DataSourceDTO dataSourceDTO = new DataSourceDTO();
        BeanUtils.copyProperties(datasource, dataSourceDTO);
        dataSourceDTO.setDb(dataBase);
        dataSourceDTO.setJdbcUrl(datasourceService.getJdbcUrl(datasource));
        return dataSourceDTO;
    }

    private void setGenerationSetting(GenerationAttributes attributes, Integer settingId) {
        GenerationSetting setting = generationSettingService.getById(settingId);
        Assert.notNull(setting, "代码生成方案不存在");
        attributes.setEnableLombok(setting.getEnableLombok().equals(YesNoEnum.YES.getVal()));
        attributes.setEnableMBPlus(setting.getEnableMbPlus().equals(YesNoEnum.YES.getVal()));
        attributes.setAuthor(setting.getAuthor());
        attributes.setSince(setting.getSince());
    }

    private void setGenerationFolder(GenerationAttributes attributes, Integer settingId) {
        Wrapper<DirectoryStructure> wrapper = new LambdaQueryWrapper<DirectoryStructure>()
                .eq(DirectoryStructure::getSettingId, settingId);
        DirectoryStructure dbDirectory = directoryStructureService.getOne(wrapper);
        Assert.notNull(dbDirectory, "没有设置目录信息");
        BeanUtils.copyProperties(dbDirectory, attributes);
    }

    @Override
    public void download(HttpServletResponse response, Integer recordId) throws IOException {
        GenerationRecord record = generationRecordService.getById(recordId);
        Assert.notNull(record, "代码不存在");
        File file = new File(record.getFilePath());
        Assert.isTrue(file.exists(), "代码文件不存在");
        response.setContentType("application/zip");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename=code" + record.getRecordCode() + ".zip");
        ZipWriter.of(response.getOutputStream(), StandardCharsets.UTF_8).
                add(false, null, file).close();
    }

    @Override
    public List<String> preview(GenerationPreviewDTO dto) {
        GenerationAttributes attributes = new GenerationAttributes();
        attributes.setDataSource(getDataSourceDTO(dto.getDatasourceId(), dto.getDataBase()));
        setGenerationSetting(attributes, dto.getSettingId());
        setGenerationFolder(attributes, dto.getSettingId());
        return codeGenerationV2Service.preview(attributes, dto);
    }
}
