package com.prefab.factory.service;

import com.prefab.factory.entity.dto.GenerationCodeDTO;
import com.prefab.factory.entity.dto.GenerationPreviewDTO;
import com.prefab.factory.entity.model.GenerationRecord;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

/**
 * 代码生成-业务处理接口
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
public interface GenerationCodeService {

    GenerationRecord generation(GenerationCodeDTO dto);

    void download(HttpServletResponse response, Integer recordId) throws IOException;

    List<String> preview(GenerationPreviewDTO dto);
}