package com.prefab.factory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prefab.factory.entity.model.Scheme;
/**
 * 生成方案-业务处理接口
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
public interface SchemeService extends IService<Scheme>  {

    void deleteScheme(Integer schemeId);

    void copyScheme(Integer schemeId);
}