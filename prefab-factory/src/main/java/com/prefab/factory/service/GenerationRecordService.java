package com.prefab.factory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prefab.factory.entity.model.GenerationRecord;
/**
 * 代码生成记录-业务处理接口
 * @author: 李亮
 * @since: 0.0.1
 * @datetime: 2022-10-10 14:26:39
 */
public interface GenerationRecordService extends IService<GenerationRecord>  {

}