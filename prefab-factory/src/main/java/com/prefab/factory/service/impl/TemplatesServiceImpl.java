package com.prefab.factory.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prefab.factory.dao.TemplatesMapper;
import com.prefab.factory.entity.model.Templates;
import com.prefab.factory.service.TemplatesService;
import com.scaffold.common.asserts.Asserts;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/22 16:50:56
 * @描述: TODO
 */
@Service
public class TemplatesServiceImpl extends ServiceImpl<TemplatesMapper, Templates> implements TemplatesService {

    @Override
    public Templates getTemplatesByName(String name) {
        Wrapper<Templates> wrapper = new LambdaQueryWrapper<Templates>().eq(Templates::getTemplateName, name);
        return baseMapper.selectOne(wrapper);
    }

    @Override
    public Templates getTemplatesById(Integer templateId) {
        return baseMapper.selectById(templateId);
    }

    @Override
    public List<Templates> getTemplatesByIds(List<Integer> idList) {
        Wrapper<Templates> wrapper = new LambdaQueryWrapper<Templates>().in(Templates::getTemplateId, idList);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public void saveTemplates(Templates templates) {
        baseMapper.updateById(templates);
    }

    @Override
    public void copyTemplate(Integer templateId) {
        Templates templates = getById(templateId);
        Asserts.notNull(templates, "模板不存在");
        Templates temp = new Templates();
        temp.setSchemeId(templates.getSchemeId());
        temp.setTemplateName("复制_" + templates.getTemplateName());
        temp.setTemplateType(templates.getTemplateType());
        temp.setTemplateContent(templates.getTemplateContent());
        temp.setTemplateEngine(templates.getTemplateEngine());
        save(temp);
    }
}
