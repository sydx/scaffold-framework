package com.prefab.factory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prefab.factory.entity.dto.GenerationSettingEditDTO;
import com.prefab.factory.entity.model.GenerationSetting;
import com.prefab.factory.entity.vo.GenerationSettingVO;

/**
 * 代码生成设置-业务处理接口
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
public interface GenerationSettingService extends IService<GenerationSetting> {

    GenerationSettingVO getGenerationSettingById(Integer settingId);

    void saveSetting(GenerationSettingEditDTO settingEditDTO);

    void deleteSetting(Integer settingId);

    void copySetting(Integer settingId);
}