package com.prefab.factory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prefab.factory.entity.model.Datasource;
import com.prefab.generation.entity.model.DBTables;

import java.util.List;

/**
 * 数据源-业务处理接口
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
public interface DatasourceService extends IService<Datasource> {

    List<String> listDataBase(Integer datasourceId);

    List<DBTables> listTable(Integer datasourceId, String dataBase);

    String getJdbcUrl(Datasource datasource);
}