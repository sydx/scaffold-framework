package com.prefab.factory.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prefab.factory.dao.DirectoryStructureMapper;
import com.prefab.factory.entity.model.DirectoryStructure;
import com.prefab.factory.service.DirectoryStructureService;
import org.springframework.stereotype.Service;
/**
 * 目录结构设置-业务处理实现类
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@Service
public class DirectoryStructureServiceImpl extends ServiceImpl<DirectoryStructureMapper, DirectoryStructure> implements DirectoryStructureService {

}