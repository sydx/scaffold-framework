package com.prefab.factory.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prefab.factory.config.GenerationProperties;
import com.prefab.factory.dao.DirectoryStructureMapper;
import com.prefab.factory.dao.GenerationSettingMapper;
import com.prefab.factory.entity.dto.DirectoryStructureDTO;
import com.prefab.factory.entity.dto.GenerationSettingEditDTO;
import com.prefab.factory.entity.model.DirectoryStructure;
import com.prefab.factory.entity.model.GenerationSetting;
import com.prefab.factory.entity.vo.GenerationSettingVO;
import com.prefab.factory.service.GenerationSettingService;
import com.prefab.generation.entity.model.GenerationAttributes;
import com.scaffold.common.asserts.Asserts;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * 代码生成设置-业务处理实现类
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@Service
public class GenerationSettingServiceImpl extends ServiceImpl<GenerationSettingMapper, GenerationSetting> implements GenerationSettingService {

    private DirectoryStructureMapper directoryStructureMapper;
    private final GenerationProperties generationProperties;

    public GenerationSettingServiceImpl(DirectoryStructureMapper directoryStructureMapper, GenerationProperties generationProperties) {
        this.directoryStructureMapper = directoryStructureMapper;
        this.generationProperties = generationProperties;
    }

    /**
     * @描述 根据ID获取代码生成设置
     * @参数 settingId
     * @返回值: com.prefab.factory.entity.vo.GenerationSettingVO
     * @异常
     * @创建人 李亮
     * @创建时间 2022-10-08 15:16:51
     */
    @Override
    public GenerationSettingVO getGenerationSettingById(Integer settingId) {
        GenerationSetting generationSetting = baseMapper.selectById(settingId);
        Assert.notNull(generationSetting, "代码生成设置不存在");
        Wrapper<DirectoryStructure> wrapper = new LambdaQueryWrapper<DirectoryStructure>()
                .eq(DirectoryStructure::getSettingId, settingId);
        DirectoryStructure directoryStructure = directoryStructureMapper.selectOne(wrapper);

        GenerationSettingVO vo = new GenerationSettingVO();
        vo.setDeployment(generationProperties.getDeployment());
        vo.setGenerationSetting(generationSetting);
        if (null == directoryStructure) {
            directoryStructure = new DirectoryStructure();
            GenerationAttributes attributes = new GenerationAttributes();
            BeanUtils.copyProperties(attributes, directoryStructure);
            directoryStructure.setEntityPackage(attributes.getEntityPackage());
            directoryStructure.setEntityQueryPackage(attributes.getEntityQueryPackage());
            directoryStructure.setMapperPackage("mapper");
            directoryStructure.setMapperXmlPackage("mapper");
            directoryStructure.setServicePackage(attributes.getServicePackage());
            directoryStructure.setServiceImplPackage(attributes.getServiceImplPackage());
            directoryStructure.setControllerPackage("controller.view");
            directoryStructure.setControllerApiPackage("controller.api");
            directoryStructure.setPageRootFolder(attributes.getPageRootFolder());
            directoryStructure.setPageFolder(attributes.getPageFolder());
            directoryStructure.setJsFolder(attributes.getJsFolder());
            directoryStructure.setJsExtension(attributes.getJsExtension());
        }
        vo.setDirectoryStructure(directoryStructure);
        return vo;
    }


    /**
     * @描述 保存设置
     * @参数 settingEditDTO
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-10-09 09:29:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveSetting(GenerationSettingEditDTO settingEditDTO) {
        Assert.notNull(settingEditDTO.getGenerationSetting().getSettingId(), "设置ID不能为空");
        GenerationSetting generationSetting = new GenerationSetting();
        BeanUtils.copyProperties(settingEditDTO.getGenerationSetting(), generationSetting);
        baseMapper.updateById(generationSetting);

        DirectoryStructureDTO directoryStructureDTO = settingEditDTO.getDirectoryStructure();
        DirectoryStructure directoryStructure = new DirectoryStructure();
        BeanUtils.copyProperties(directoryStructureDTO, directoryStructure);
        directoryStructure.setSettingId(generationSetting.getSettingId());

        Wrapper<DirectoryStructure> wrapper = new LambdaQueryWrapper<DirectoryStructure>()
                .eq(DirectoryStructure::getSettingId, generationSetting.getSettingId());
        DirectoryStructure dbDirectory = directoryStructureMapper.selectOne(wrapper);
        if (null != dbDirectory) {
            directoryStructure.setDsId(dbDirectory.getDsId());
        }
        if (directoryStructure.getDsId() == null) {
            directoryStructureMapper.insert(directoryStructure);
        } else {
            directoryStructureMapper.updateById(directoryStructure);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteSetting(Integer settingId) {
        removeById(settingId);
        Wrapper<DirectoryStructure> wrapper = new LambdaQueryWrapper<DirectoryStructure>().eq(DirectoryStructure::getSettingId, settingId);
        directoryStructureMapper.delete(wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void copySetting(Integer settingId) {
        GenerationSetting old = getById(settingId);
        Asserts.notNull(old, "方案不存在");
        GenerationSetting setting = new GenerationSetting();
        BeanUtils.copyProperties(old, setting);
        setting.setGenerationName("复制_" + old.getGenerationName());
        setting.setState(old.getState());
        setting.setSettingId(null);
        save(setting);

        Wrapper<DirectoryStructure> wrapper = new LambdaQueryWrapper<DirectoryStructure>().eq(DirectoryStructure::getSettingId, settingId);
        DirectoryStructure oldDir = directoryStructureMapper.selectOne(wrapper);
        if (null!=oldDir) {
            DirectoryStructure newDir = new DirectoryStructure();
            BeanUtils.copyProperties(oldDir, newDir);
            newDir.setDsId(null);
            newDir.setSettingId(setting.getSettingId());
            directoryStructureMapper.insert(newDir);
        }

    }
}