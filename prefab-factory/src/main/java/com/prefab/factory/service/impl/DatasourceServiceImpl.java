package com.prefab.factory.service.impl;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prefab.factory.dao.DatasourceMapper;
import com.prefab.factory.entity.model.Datasource;
import com.prefab.factory.service.DatasourceService;
import com.prefab.generation.dao.TablesDAO;
import com.prefab.generation.entity.model.DBTables;
import com.prefab.generation.enums.DatasourceTypeEnum;
import com.prefab.generation.service.TablesService;
import com.prefab.generation.service.impl.TablesServiceImpl;
import com.scaffold.common.exception.SystemException;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 数据源-业务处理实现类
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@Service
public class DatasourceServiceImpl extends ServiceImpl<DatasourceMapper, Datasource> implements DatasourceService {

    @Override
    public List<String> listDataBase(Integer datasourceId) {
        Datasource datasource = getById(datasourceId);
        Assert.notNull(datasource, "数据源不存在");
        datasource.setDataBase("information_schema");
        HikariDataSource dataSource = null;
        try {
            dataSource = getDataSource(datasource);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            TablesService tablesService = new TablesServiceImpl(new TablesDAO(jdbcTemplate));
            return tablesService.listDataBase();
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            if (dataSource != null && !dataSource.isClosed()) {
                dataSource.close();
            }
        }
    }

    @Override
    public List<DBTables> listTable(Integer datasourceId, String dataBase) {
        Datasource datasource = getById(datasourceId);
        Assert.notNull(datasource, "数据源不存在");
        datasource.setDataBase(dataBase);
        HikariDataSource dataSource = null;
        try {
            dataSource = getDataSource(datasource);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            TablesService tablesService = new TablesServiceImpl(new TablesDAO(jdbcTemplate));
            return tablesService.listTable(dataBase);
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            if (dataSource != null && !dataSource.isClosed()) {
                dataSource.close();
            }
        }
    }

    private HikariDataSource getDataSource(Datasource datasource) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(getJdbcUrl(datasource));
        hikariConfig.setUsername(datasource.getUsername());
        hikariConfig.setPassword(datasource.getPassword());
        hikariConfig.setConnectionTimeout(5000L);
        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        return dataSource;
    }

    @Override
    public String getJdbcUrl(Datasource datasource) {
        DatasourceTypeEnum type = DatasourceTypeEnum.valueOf(datasource.getDatasourceType());
        switch (type) {
            case MySQL:
                String fmt = "jdbc:mysql://%s:%d/%s?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true";
                return String.format(fmt, datasource.getHost(), datasource.getPort(), datasource.getDataBase());
            case SQLServer:
            default:
                throw new RuntimeException("不支持的数据源类型");
        }
    }

}