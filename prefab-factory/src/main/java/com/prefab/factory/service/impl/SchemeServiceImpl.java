package com.prefab.factory.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prefab.factory.dao.SchemeMapper;
import com.prefab.factory.dao.TemplatesMapper;
import com.prefab.factory.entity.model.Scheme;
import com.prefab.factory.entity.model.Templates;
import com.prefab.factory.service.SchemeService;
import com.scaffold.common.asserts.Asserts;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 生成方案-业务处理实现类
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
@Service
public class SchemeServiceImpl extends ServiceImpl<SchemeMapper, Scheme> implements SchemeService {

    private TemplatesMapper templatesMapper;

    public SchemeServiceImpl(TemplatesMapper templatesMapper) {
        this.templatesMapper = templatesMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteScheme(Integer schemeId) {
        removeById(schemeId);
        Wrapper<Templates> wrapper = new LambdaQueryWrapper<Templates>().eq(Templates::getSchemeId, schemeId);
        templatesMapper.delete(wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void copyScheme(Integer schemeId) {
        Scheme old = getById(schemeId);
        Asserts.notNull(old, "方案不存在");
        Scheme scheme = new Scheme();
        scheme.setSchemeName("复制_" + old.getSchemeName());
        scheme.setState(old.getState());
        save(scheme);

        Wrapper<Templates> wrapper = new LambdaQueryWrapper<Templates>().eq(Templates::getSchemeId, schemeId);
        List<Templates> odlTemplates = templatesMapper.selectList(wrapper);

        for (Templates odlTemplate : odlTemplates) {
            Templates temp = new Templates();
            temp.setSchemeId(scheme.getSchemeId());
            temp.setTemplateName(odlTemplate.getTemplateName());
            temp.setTemplateType(odlTemplate.getTemplateType());
            temp.setTemplateContent(odlTemplate.getTemplateContent());
            temp.setTemplateEngine(odlTemplate.getTemplateEngine());
            templatesMapper.insert(temp);
        }
    }

}