package com.prefab.factory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prefab.factory.entity.model.Templates;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/22 16:50:30
 * @描述: TODO
 */
public interface TemplatesService extends IService<Templates> {
    Templates getTemplatesByName(String name);

    Templates getTemplatesById(Integer templateId);

    List<Templates> getTemplatesByIds(List<Integer> idList);

    void saveTemplates(Templates templates);

    void copyTemplate(Integer templateId);
}
