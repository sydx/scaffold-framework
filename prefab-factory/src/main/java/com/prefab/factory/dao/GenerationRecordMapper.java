package com.prefab.factory.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prefab.factory.entity.model.GenerationRecord;

/**
* 代码生成记录
* @author: 李亮
* @since: 0.0.1
* @datetime: 2022-10-10 14:26:39
*/
public interface GenerationRecordMapper extends BaseMapper<GenerationRecord>  {

}