package com.prefab.factory.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prefab.factory.entity.model.Datasource;

/**
* 数据源
* @author: prefab
* @since: v_0.0.1
* @datetime: 2022-09-28 16:48:12
*/
public interface DatasourceMapper extends BaseMapper<Datasource>  {

}