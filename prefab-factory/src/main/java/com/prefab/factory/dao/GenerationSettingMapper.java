package com.prefab.factory.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prefab.factory.entity.model.GenerationSetting;

/**
 * 代码生成设置
 *
 * @author: prefab
 * @since: v_0.0.1
 * @datetime: 2022-09-28 16:48:12
 */
public interface GenerationSettingMapper extends BaseMapper<GenerationSetting> {

}