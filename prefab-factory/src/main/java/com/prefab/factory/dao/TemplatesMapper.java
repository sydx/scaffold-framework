package com.prefab.factory.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prefab.factory.entity.model.Templates;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/28 14:25:16
 * @描述: 模板映射类
 */
public interface TemplatesMapper extends BaseMapper<Templates> {
}
