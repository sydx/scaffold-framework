package com.prefab.factory.handler;

import cn.hutool.core.collection.CollUtil;
import com.scaffold.common.asserts.Asserts;
import com.scaffold.common.enums.UserEnum.Type;
import com.scaffold.security.util.AuthenticationUtil;
import com.scaffold.system.entity.dto.PermissionDTO;
import com.scaffold.security.SecurityConfigProperties;
import com.prefab.factory.entity.AdminUser;
import com.scaffold.security.authorization.AccessDecisionHandler;
import com.scaffold.system.service.ISysModuleFunctionActionService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 自定义权限处理器
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/22 10:02:47
 */
@Component
@Slf4j
public class LocalAccessDecisionHandler implements AccessDecisionHandler {

    @Autowired
    private SecurityConfigProperties securityConfigProperties;

    @Autowired
    private ISysModuleFunctionActionService sysModuleFunctionActionService;

    @Override
    public boolean decide(Supplier<Authentication> authentication, HttpServletRequest request) {
        if (log.isDebugEnabled()) {
            log.info("权限检查：{} {}", request.getMethod(), request.getRequestURI());
        }
        if (isWhiteList(request)) {
            return true;
        }
//        Object principal = authentication.get().getPrincipal();
        Object user = AuthenticationUtil.getUser();
        Asserts.notNull(user, "请先登录系统");
        AdminUser adminUser = (AdminUser) user;
        if (isSupperAdmin(adminUser.getUserType())) {
            return true;
        }
        return checkPermission(adminUser, request);
    }

    /**
     * 白名单检查。
     * 放行登录和授权白名单地址
     * @param request
     * @return boolean
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-08-22 14:43:36
     */
    private boolean isWhiteList(HttpServletRequest request) {
        List<String> ignoreList = new ArrayList<>();
        PathMatcher pathMatcher = new AntPathMatcher();
        Optional<String> optional =
                getIgnoreList().stream().filter(o -> pathMatcher.match(o, request.getRequestURI())).findFirst();
        return optional.isPresent();
    }

    private List<String> getIgnoreList() {
        List<String> ignoreList = new ArrayList<>();
        if (CollUtil.isNotEmpty(securityConfigProperties.getAuthentication().getIgnoredUrls())) {
            ignoreList.addAll(securityConfigProperties.getAuthentication().getIgnoredUrls());
        }
        if (CollUtil.isNotEmpty(securityConfigProperties.getAuthorization().getIgnoredUrls())) {
            ignoreList.addAll(securityConfigProperties.getAuthorization().getIgnoredUrls());
        }
        return ignoreList;
    }

    /**
     * 是否为超级管理员。超级管理员无需验证权限
     * @param userType
     * @return boolean
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-08-22 14:44:38
     */
    private boolean isSupperAdmin(int userType) {
        Type type = Type.convertEnum(userType);
        return Objects.equals(type, Type.SUPPER);
    }

    /**
     * 权限检查
     *
     * @param adminUser
     * @param request
     * @return boolean
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-08-22 14:45:17
     */
    private boolean checkPermission(AdminUser adminUser, HttpServletRequest request) {
        HttpMethod httpMethod = HttpMethod.valueOf(request.getMethod());
        List<PermissionDTO> permissionList = getPermissionDTOList(httpMethod);
        if (CollUtil.isEmpty(permissionList)) {
            return false;
        }
        Set<String> permissionCodes = getPermissionCodeByRequest(permissionList, request);
        if (CollUtil.isEmpty(permissionCodes)) {
            return false;
        }
        return checkPermissionCode(permissionCodes, adminUser.getPermissions());
    }

    /**
     * 获取相同method的所有操作对象
     * @param httpMethod
     * @return java.util.List<com.scaffold.system.entity.dto.PermissionDTO>
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-08-22 14:46:12
     */
    private List<PermissionDTO> getPermissionDTOList(HttpMethod httpMethod) {
        List<PermissionDTO> permissionList = sysModuleFunctionActionService.getAllPermission();
        return permissionList.stream().filter(o -> o.getMethod().equals(httpMethod.name())).toList();
    }

    /**
     * 获取与请求地址匹配的权限编码
     * @param permissions
     * @param request
     * @return java.util.Set<java.lang.String>
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-08-22 14:46:47
     */
    private Set<String> getPermissionCodeByRequest(List<PermissionDTO> permissions, HttpServletRequest request) {
        List<PermissionDTO> collect = permissions.stream().filter(o -> isMatching(o, request)).toList();
        return collect.stream().map(PermissionDTO::getPermissionCode).collect(Collectors.toSet());
    }

    /**
     * 从请求方法、地址层级、基路径和全地址几个方面匹配
     * 1. 请求方法是否一致
     * 2. 动态地址是否能够匹配
     * 3. 非动态地址是否完全一致
     * @param dto
     * @param request
     * @return
     */
    private boolean isMatching(PermissionDTO dto, HttpServletRequest request) {
        //请求方法
        if (!dto.getMethod().equalsIgnoreCase(request.getMethod())) {
            return false;
        }

        //动态地址
        if (dto.isHasPathVariable()) {
            return dynamicAddress(dto.getUrl(), request.getRequestURI());
        }

        //全路径
        return Objects.equals(request.getRequestURI(), dto.getUrl());
    }

    private boolean dynamicAddress(String dynamicURL, String requestURI) {
        AntPathMatcher matcher = new AntPathMatcher();
        return matcher.match(dynamicURL, requestURI);
    }

    /**
     * 检查权限编码
     * 通过请求地址匹配到系统中的权限编码，检查其是否存在在用户权限列表中
     * @param permissionCodeList
     * @param userPermissionCodeList
     * @return boolean
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-08-22 14:48:39
     */
    private boolean checkPermissionCode(Set<String> permissionCodeList, Set<String> userPermissionCodeList) {
        for (String s : permissionCodeList) {
            if (userPermissionCodeList.contains(s)) {
                return true;
            }
        }
        return false;
    }
}
