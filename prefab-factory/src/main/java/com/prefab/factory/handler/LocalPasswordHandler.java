package com.prefab.factory.handler;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.scaffold.common.constants.TimeConstants;
import com.scaffold.system.entity.dto.RSACacheDTO;
import com.scaffold.redis.CacheKeyConstant;
import com.scaffold.redis.RedisService;
import com.scaffold.security.authentication.password.PasswordHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;

/**
 * 密码处理
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/7/24 20:51:15
 */
@Slf4j
public class LocalPasswordHandler implements PasswordHandler {

    private final RedisService redisService;

    public LocalPasswordHandler(RedisService redisService) {
        this.redisService = redisService;
    }

    /**
     * 密码解码
     *
     * @param requestId        请求ID
     * @param encryptLPassword 已加密的密码
     * @return java.lang.String 解密后的密码
     * @throws
     * @version 1.0
     * @author 李亮
     * @time 2023-07-24 11:26:26
     */
    @Override
    public String decryptPassword(String requestId, String encryptLPassword) {
        String key = getKey(requestId);
        log.info(key);
        Object rsaObj = redisService.get(key);
        if (null == rsaObj) {
            throw new BadCredentialsException("加密信息不正确！");
        }
        RSACacheDTO rsa = (RSACacheDTO) rsaObj;
        try {
            return rsa.rsa().decryptStr(encryptLPassword, KeyType.PrivateKey);
        } catch (Exception e) {
            log.error("密码解密失败", e);
            throw new BadCredentialsException("密码解密失败！");
        } finally {
            redisService.delete(key);
        }
    }

    /**
     * 创建加密密钥
     *
     * @param requestId 请求ID
     * @return java.lang.String 解密后的密码
     * @throws
     * @version 1.0
     * @author 李亮
     * @time 2023-07-24 11:26:26
     */
    @Override
    public String createKey(String requestId) {
        String key = getKey(requestId);
        log.info(key);
        Object rsaObj = redisService.get(key);
        if (null != rsaObj) {
            RSACacheDTO rsa = (RSACacheDTO) rsaObj;
            return rsa.getPublicKey();
        }
        RSACacheDTO rsa = new RSACacheDTO(new RSA());
        redisService.set(key, rsa, TimeConstants.TWO_MINUTES_SECOND);
        return rsa.getPublicKey();
    }

    private String getKey(String requestId) {
        return String.format(CacheKeyConstant.SECURITY_PASSWORD_RSA_KEY_FMT, requestId);
    }
}
