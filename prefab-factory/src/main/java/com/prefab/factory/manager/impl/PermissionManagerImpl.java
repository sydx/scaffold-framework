package com.prefab.factory.manager.impl;

import cn.hutool.core.collection.CollUtil;
import com.scaffold.common.enums.UserEnum;
import com.scaffold.system.entity.dto.SysModuleDTO;
import com.scaffold.system.entity.model.SysModuleFunction;
import com.scaffold.system.entity.vo.MenuVO;
import com.prefab.factory.entity.AdminUser;
import com.scaffold.system.service.ISysModuleService;
import com.prefab.factory.manager.PermissionManager;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * 权限相关处理类
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/16 11:06:55
 */
@Service
public class PermissionManagerImpl implements PermissionManager {

    private final ISysModuleService sysModuleService;

    public PermissionManagerImpl(ISysModuleService sysModuleService) {
        this.sysModuleService = sysModuleService;
    }

    @Override
    public List<MenuVO> getMenu(int systemId, AdminUser user) {
        UserEnum.Type type = UserEnum.Type.convertEnum(user.getUserType());
        List<MenuVO> menus = new ArrayList<>();
        List<SysModuleDTO> moduleFunctionList = null;
        if (Objects.equals(type, UserEnum.Type.SUPPER)) {
            moduleFunctionList = sysModuleService.getModuleFunction(systemId);
        } else {
            moduleFunctionList = sysModuleService.getUserModuleFunction(systemId, new ArrayList<>(user.getPermissions()));
        }
        for (SysModuleDTO dto : moduleFunctionList) {
            menus.add(sysModuleConvert(dto));
        }
        return menus;
    }

    private MenuVO sysModuleConvert(SysModuleDTO dto) {
        MenuVO menuVO = new MenuVO();
        menuVO.setCode(dto.getPermissionCode());
        menuVO.setName(dto.getModuleName());
        menuVO.setUrl(dto.getModuleLink());
        menuVO.setIcon(dto.getModuleLogo());
        menuVO.setIndex(dto.getModuleIndex());
        menuVO.setChildren(getMenuChild(dto.getFunctions(), 0));
        menuVO.setHasChild(CollUtil.isNotEmpty(menuVO.getChildren()));
        return menuVO;
    }

    private List<MenuVO> getMenuChild(List<SysModuleFunction> functions, int parentId) {
        List<MenuVO> menus = new ArrayList<>();
        List<SysModuleFunction> list = functions.stream().filter(o -> o.getParentId() == parentId).toList();
        for (SysModuleFunction function : list) {
            MenuVO menu = sysModuleFunctionConvert(function);
            menu.setChildren(getMenuChild(functions, function.getFunctionId()));
            menu.setHasChild(CollUtil.isNotEmpty(menu.getChildren()));
            menus.add(menu);
        }
        menus.sort(Comparator.comparingInt(MenuVO::getIndex));
        return menus;
    }

    private MenuVO sysModuleFunctionConvert(SysModuleFunction function) {
        MenuVO menuVO = new MenuVO();
        menuVO.setCode(function.getPermissionCode());
        menuVO.setName(function.getFunctionName());
        menuVO.setUrl(function.getUrl());
        menuVO.setIcon(function.getFunctionIcon());
        menuVO.setIndex(function.getFunctionIndex().intValue());
        return menuVO;
    }

}
