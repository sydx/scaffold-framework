package com.prefab.factory.manager;

import com.scaffold.system.entity.vo.MenuVO;
import com.prefab.factory.entity.AdminUser;

import java.util.List;

/**
 * 权限管理
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/16 11:05:28
 */
public interface PermissionManager {
    List<MenuVO> getMenu(int systemId, AdminUser user);
}
