package com.prefab.factory.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/3 13:54:37
 * @描述: 代码生成 配置项
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "generation", ignoreInvalidFields = true)
public class GenerationProperties {

    /**
     * 服务部署模式。local：本地部署；server：服务器部署
     * 服务器部署代码生成到服务器上的目录中，代码生成后需要下载到本地
     */
    private String deployment = "local";

    /**
     * 代码生成临时目录。server模式下生成的文件保存目录
     */
    private String saveFolder = "/usr/local/generation";

}
