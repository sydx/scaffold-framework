package com.prefab.factory.config;

import cn.hutool.core.date.DatePattern;
import com.alibaba.fastjson2.JSONReader;
import com.alibaba.fastjson2.JSONWriter;
import com.alibaba.fastjson2.support.config.FastJsonConfig;
import com.alibaba.fastjson2.support.spring6.http.converter.FastJsonHttpMessageConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/23 10:55:49
 * @描述: Web MVC 配置
 */
@Configuration
public class WebMVCConfigurator implements WebMvcConfigurer {

    /**
     * Configure the {@link HttpMessageConverter HttpMessageConverter}s for
     * reading from the request body and for writing to the response body.
     * <p>By default, all built-in converters are configured as long as the
     * corresponding 3rd party libraries such Jackson JSON, JAXB2, and others
     * are present on the classpath.
     * <p><strong>Note</strong> use of this method turns off default converter
     * registration. Alternatively, use
     * {@link #extendMessageConverters(List)} to modify that default
     * list of converters.
     *
     * @param converters initially an empty list of converters
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        //使用fastjson2
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        FastJsonConfig config = new FastJsonConfig();
        config.setDateFormat(DatePattern.NORM_DATETIME_PATTERN);
        config.setReaderFeatures(JSONReader.Feature.FieldBased, JSONReader.Feature.SupportArrayToBean);
        config.setWriterFeatures(JSONWriter.Feature.WriteMapNullValue, JSONWriter.Feature.PrettyFormat);
        converter.setFastJsonConfig(config);
        converter.setDefaultCharset(StandardCharsets.UTF_8);
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.APPLICATION_JSON));
        converters.add(0, converter);
    }
}
