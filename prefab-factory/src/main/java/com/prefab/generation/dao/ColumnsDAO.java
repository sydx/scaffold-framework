package com.prefab.generation.dao;

import com.prefab.generation.entity.model.DBTableColumns;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 11:33:20
 * @描述: 列
 */
public class ColumnsDAO {

    private JdbcTemplate jdbcTemplate;

    private BeanPropertyRowMapper<DBTableColumns> beanPropertyRowMapper = new BeanPropertyRowMapper<>(DBTableColumns.class);

    public ColumnsDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<DBTableColumns> listTableColumns(String tableSchema, String tableName) {
        String sql = "select * from information_schema.COLUMNS where TABLE_SCHEMA=? and TABLE_NAME=? order by TABLE_NAME,ORDINAL_POSITION";
        List<DBTableColumns> dbTableColumns = jdbcTemplate.query(sql, beanPropertyRowMapper, tableSchema, tableName);
        return dbTableColumns;
    }


}
