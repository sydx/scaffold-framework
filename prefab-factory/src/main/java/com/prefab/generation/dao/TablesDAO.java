package com.prefab.generation.dao;

import com.prefab.generation.entity.model.DBTables;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 11:33:04
 * @描述: 表
 */
public class TablesDAO {

    private JdbcTemplate jdbcTemplate;

    private BeanPropertyRowMapper<DBTables> beanPropertyRowMapper = new BeanPropertyRowMapper<>(DBTables.class);

    public TablesDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<String> listDataBase() {
        String sql = "select SCHEMA_NAME from information_schema.SCHEMATA";
        List<String> dataBaseList = jdbcTemplate.queryForList(sql, String.class);
        return dataBaseList;
    }

    public List<DBTables> listTable(String tableSchema) {
        String sql = "select * from information_schema.TABLES where TABLE_SCHEMA=?";
        List<DBTables> dbTables = jdbcTemplate.query(sql, beanPropertyRowMapper, tableSchema);
        return dbTables;
    }

    public DBTables getTableByName(String tableSchema, String tableName) {
        String sql = "select * from information_schema.TABLES where TABLE_SCHEMA=? and TABLE_NAME=?";
        return jdbcTemplate.queryForObject(sql, beanPropertyRowMapper, tableSchema, tableName);
    }

}
