package com.prefab.generation.entity.model;

import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 11:34:25
 * @描述: TODO
 */
@Data
public class DBTableColumns {
    /**
     * 数据库名
     */
    private String tableSchema;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 列名
     */
    private String columnName;
    /**
     * 列排序
     */
    private Integer ordinalPosition;
    /**
     * 默认值
     */
    private String columnDefault;
    /**
     * 是否可空。NO: 不可空；YES：可空
     */
    private String isNullable;
    /**
     * 数据类型。
     */
    private String dataType;
    /**
     * 字符串类型字段的最大长度
     */
    private Long characterMaximumLength;
    /**
     * 数字类型的精度
     */
    private Long numericPrecision;
    /**
     * 数字类型的小数位
     */
    private Long numericScale;
    /**
     * 日期时间的精度
     */
    private Integer datetimePrecision;
    /**
     * 字符集名称
     */
    private String characterSetName;
    /**
     * 列类型。与数据类型的区别是带长度或精度
     * 也就是建表时字段里描述的字段类型
     */
    private String columnType;
    /**
     * 索引。
     * '': 没有索引
     * PRI:主键
     * UNI: 唯一索引
     * MUL: 组合索引或其它索引
     */
    private String columnKey;
    /**
     * 扩展。例如：auto_increment
     */
    private String extra;
    /**
     * 字段注释
     */
    private String columnComment;

}
