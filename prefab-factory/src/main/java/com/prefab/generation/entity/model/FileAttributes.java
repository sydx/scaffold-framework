package com.prefab.generation.entity.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/20 11:49:54
 * @描述: TODO
 */
@Getter
@Setter
public class FileAttributes {

    /**
     * 文件作者
     */
    private String author;
    /**
     * 引入版本
     */
    private String since;
    /**
     * 创建日期yyyy-MM-dd
     */
    private String date;
    /**
     * 创建时间HH:mm:ss
     */
    private String time;

}
