package com.prefab.generation.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataSourceDTO {
    private String jdbcUrl;
    private String username;
    private String password;
    private String host;
    private String port;
    private String db;
}
