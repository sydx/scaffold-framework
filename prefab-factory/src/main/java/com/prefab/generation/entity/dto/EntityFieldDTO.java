package com.prefab.generation.entity.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 14:03:58
 * @描述: TODO
 */
@Getter
@Setter
public class EntityFieldDTO {
    /**
     * 字段名
     */
    private String name;
    /**
     * 备注
     */
    private String comment;
    /**
     * 字段类型
     */
    private String type;
    /**
     * 字段名首字母大写
     */
    private String upperFirstName;
    /**
     * 表字段名
     */
    private String tableColumnName;
    /**
     * 表字段类型
     */
    private String tableColumnDataType;
    /**
     * 表字段对应的jdbc类型
     */
    private String tableColumnJdbcType;
    /**
     * 是否主键
     */
    private boolean primaryKey;
    /**
     * 是否自动增长
     */
    private boolean autoIncrement;
    /**
     * 是否公共字段。例如createdTime、deleted等
     */
    private boolean commonField;
}
