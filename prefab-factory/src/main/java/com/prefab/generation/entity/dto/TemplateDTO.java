package com.prefab.generation.entity.dto;


import com.prefab.generation.entity.model.DBTableColumns;
import com.prefab.generation.entity.model.DBTables;
import com.prefab.generation.entity.model.GenerationAttributes;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TemplateDTO {
    /**
     * 表信息
     */
    private DBTables table;
    /**
     * 表字段信息
     */
    private List<DBTableColumns> columns;
    /**
     * 主键列名
     */
    private String pkColumnName;
    /**
     * 主键列数据类型
     */
    private String pkColumnType;
    /**
     * 实体对象主键属性名
     */
    private String pkFieldName;
    /**
     * 实体对象主键属性类型
     */
    private String pkFieldType;
    /**
     * 驼峰表名
     */
    private String tableNameCamel;
    /**
     * 实体对象名
     */
    private String entityName;
    /**
     * 实体对象beanName
     */
    private String entityBeanName;
    /**
     * 表名转换成的path，用户controller中的request mapping
     * 如：system_module ->  system/module
     */
    private String tableNameConvertPath;
    /**
     * 类名
     */
    private String className;
    /**
     * 实例名称bean name
     */
    private String beanName;
    /**
     * 包名
     */
    private String packageName;
    /**
     * 实体类属性列表
     */
    private List<EntityFieldDTO> fields;
    /**
     * 代码生成配置属性
     */
    private GenerationAttributes attributes;
}
