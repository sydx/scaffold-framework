package com.prefab.generation.entity.model;

import cn.hutool.core.date.DateUtil;
import com.prefab.generation.entity.dto.DataSourceDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/20 11:50:23
 * @描述: TODO
 */
@Getter
@Setter
public class GenerationAttributes {

    /**
     * 需要生成的模板ID，JSON Array
     */
    private String templateIds;

    /**
     * 数据源
     */
    private DataSourceDTO dataSource;

    /**
     * 需要生成的表名，JSON Array
     */
    private String generationTables;

    /**
     * 文件保存目录绝对路径
     */
    private String baseFolder = "";

    /**
     * java代码目录绝对路径
     */
    private String javaFolder = "/src/main/java";
    /**
     * 配置文件目录绝对路径
     */
    private String resourcesFolder = "/src/main/resources";
    /**
     * 基础包名
     */
    private String basePackage = "com.prefab";
    /**
     * 模块包名，非必须。如果没有不会创建模块目录
     */
    private String modulePackage;
    /**
     * 实体类包名
     */
    private String entityPackage = "model";
    /**
     * 实体类后缀
     */
    private String entitySuffix = "";
    /**
     * 查询条件实体类包名
     */
    private String entityQueryPackage = "query";
    /**
     * 查询条件实体类后缀
     */
    private String entityQuerySuffix = "Query";
    /**
     * 映射类包名
     */
    private String mapperPackage = "mapper";
    /**
     * 映射类后缀
     */
    private String mapperSuffix = "Mapper";
    /**
     * 映射文件包名
     */
    private String mapperXmlPackage = "mapper";
    /**
     * 映射文件后缀
     */
    private String mapperXmlSuffix = "Mapper";
    /**
     * 映射文件保存到。0：resources目录；1：保存到java文件目录
     * 默认保存到resources目录
     */
    private Integer mapperXmlSaveTo = 0;
    /**
     * service接口包名
     */
    private String servicePackage = "service";
    /**
     * service接口后缀
     */
    private String serviceSuffix = "Service";
    /**
     * service实现类包名
     */
    private String serviceImplPackage = "service.impl";
    /**
     * service实现类后缀
     */
    private String serviceImplSuffix = "ServiceImpl";
    /**
     * 页面控制类类包名
     */
    private String controllerPackage = "controller.view";
    /**
     * 页面控制类后缀
     */
    private String controllerSuffix = "Controller";
    /**
     * 接口控制类类包名
     */
    private String controllerApiPackage = "controller.api";
    /**
     * 接口控制类后缀
     */
    private String controllerApiSuffix = "ApiController";
    /**
     * 页面根路径
     */
    private String pageRootFolder = "/templates";
    /**
     * 页面包名
     */
    private String pageFolder = "";
    /**
     * 页面后缀
     */
    private String pageSuffix = "";
    /**
     * 页面扩展名
     */
    private String pageExtension = "html";
    /**
     * 页面js包名
     */
    private String jsFolder = "static/js/business";
    /**
     * 页面js后缀
     */
    private String jsSuffix = "";
    /**
     * 页面js扩展名
     */
    private String jsExtension = "js";
    /**
     * 启用lombok
     */
    private boolean enableLombok = false;
    /**
     * 启用MyBatis Plus
     */
    private boolean enableMBPlus = false;

    /**
     * 文件作者
     */
    private String author = "prefab";
    /**
     * 引入版本
     */
    private String since = "v_0.0.1";
    /**
     * 创建日期yyyy-MM-dd
     */
    private String date = DateUtil.formatDate(new Date());
    /**
     * 创建时间HH:mm:ss
     */
    private String time = DateUtil.formatTime(new Date());

}
