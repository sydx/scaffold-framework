package com.prefab.generation.entity.model;

import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 11:34:05
 * @描述: TODO
 */
@Data
public class DBTables {

    /**
     * 数据库名
     */
    private String tableSchema;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 表类型
     */
    private String tableType;
    /**
     * 存储引擎
     */
    private String engine;
    /**
     * 版本
     */
    private String version;
    /**
     * 行格式
     */
    private String rowFormat;
    /**
     *  表排序规则
     */
    private String tableCollation;
    /**
     * 表注释
     */
    private String tableComment;
}
