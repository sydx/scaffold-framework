package com.prefab.generation.entity.dto;

import com.prefab.generation.entity.model.DBTableColumns;
import com.prefab.generation.entity.model.DBTables;
import com.prefab.generation.entity.model.FileAttributes;
import com.prefab.generation.entity.model.GenerationAttributes;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 14:07:23
 * @描述: TODO
 */
public class ClassDTO {
    private String packageName;
    private String entityName;
    private String className;
    private DBTables table;
    private List<DBTableColumns> columns;
    private String classNameSuffix;

    private FileAttributes file;

    private GenerationAttributes folder;

    private List<String> imports;

    public List<String> getImports() {
        return imports;
    }

    public void setImports(List<String> imports) {
        this.imports = imports;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public FileAttributes getFile() {
        return file;
    }

    public void setFile(FileAttributes file) {
        this.file = file;
    }

    public GenerationAttributes getFolder() {
        return folder;
    }

    public void setFolder(GenerationAttributes folder) {
        this.folder = folder;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public DBTables getTable() {
        return table;
    }

    public void setTable(DBTables table) {
        this.table = table;
    }

    public List<DBTableColumns> getColumns() {
        return columns;
    }

    public void setColumns(List<DBTableColumns> columns) {
        this.columns = columns;
    }

    public String getClassNameSuffix() {
        return classNameSuffix;
    }

    public void setClassNameSuffix(String classNameSuffix) {
        this.classNameSuffix = classNameSuffix;
    }
}
