package com.prefab.generation.entity.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 14:03:58
 * @描述: TODO
 */
@Getter
@Setter
public class ServiceDTO extends ClassDTO {
    private String interfaceName;
    private String mapperName;
}
