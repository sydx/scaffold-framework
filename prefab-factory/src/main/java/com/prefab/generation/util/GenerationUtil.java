package com.prefab.generation.util;

import cn.hutool.core.util.StrUtil;
import com.prefab.generation.entity.model.GenerationAttributes;

import java.io.File;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/23 16:26:46
 * @描述: TODO
 */
public class GenerationUtil {

    public static String getUpperFirstCamelCase(String str) {
        return StrUtil.upperFirst(StrUtil.toCamelCase(str));
    }

    public static String getLowerFirstCameCase(String str) {
        return StrUtil.lowerFirst(StrUtil.toCamelCase(str));
    }

    public static StringBuilder getJavaFileFolderPathByPackage(GenerationAttributes folderAttributes, String packageName) {
        String packagePath = packageName.replaceAll("\\.", StrUtil.SLASH);
        return new StringBuilder(folderAttributes.getBaseFolder())
                .append(folderAttributes.getJavaFolder()).append(StrUtil.SLASH).append(packagePath);
    }

    public static StringBuilder getResourcesFileFolderPathByPackage(GenerationAttributes folderAttributes, String packageName) {
        return new StringBuilder(folderAttributes.getBaseFolder())
                .append(folderAttributes.getResourcesFolder()).append(StrUtil.SLASH).append(packageName);
    }

    public static String getJavaFilePath(StringBuilder folderPath, String className) {
        return getFilePath(folderPath, className, "java");
    }

    public static String getFilePath(StringBuilder folderPath, String className, String extensionName) {
        createFolder(folderPath.toString());
        return folderPath.append(StrUtil.SLASH)
                .append(className)
                .append(StrUtil.DOT).append(extensionName).toString();
    }

    public static void createFolder(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static void deleteFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }
    }

}
