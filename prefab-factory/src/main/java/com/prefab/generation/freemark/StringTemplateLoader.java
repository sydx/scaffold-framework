package com.prefab.generation.freemark;

import freemarker.cache.TemplateLoader;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/22 15:46:29
 * @描述: 字符串模板加载
 */
public class StringTemplateLoader implements TemplateLoader {

    private final Map<String, StringTemplateSource> templates = new HashMap<>();

    public void putTemplate(StringTemplateSource templateSource) {
        templates.put(templateSource.getName(), templateSource);
    }

    public void putTemplate(List<StringTemplateSource> templateSourceList) {
        for (StringTemplateSource source : templateSourceList) {
            templates.put(source.getName(), source);
        }
    }

    @Override
    public StringTemplateSource findTemplateSource(String templateName) throws IOException {
        return this.templates.get(templateName);
    }

    @Override
    public long getLastModified(Object templateSource) {
        String templateName = ((StringTemplateSource) templateSource).getName();
        return this.templates.get(String.valueOf(templateName)).getLastModified();
    }

    @Override
    public Reader getReader(Object templateSource, String encoding) throws IOException {
        return new StringReader(((StringTemplateSource) templateSource).getTemplateContent());
    }

    @Override
    public void closeTemplateSource(Object o) throws IOException {

    }

    public static class StringTemplateSource {
        private final String name;
        private final String templateContent;
        private final long lastModified;

        public StringTemplateSource(String name, String templateContent, long lastModified) {
            if (name == null) {
                throw new IllegalArgumentException("name == null");
            } else if (templateContent == null) {
                throw new IllegalArgumentException("source == null");
            } else if (lastModified < -1L) {
                throw new IllegalArgumentException("lastModified < -1L");
            } else {
                this.name = name;
                this.templateContent = templateContent;
                this.lastModified = lastModified;
            }
        }

        public String getName() {
            return name;
        }

        public String getTemplateContent() {
            return templateContent;
        }

        public long getLastModified() {
            return lastModified;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            } else if (obj == null) {
                return false;
            } else if (this.getClass() != obj.getClass()) {
                return false;
            } else {
                StringTemplateSource other = (StringTemplateSource) obj;
                if (this.name == null) {
                    if (other.name != null) {
                        return false;
                    }
                } else if (!this.name.equals(other.name)) {
                    return false;
                }

                return true;
            }
        }

        public String toString() {
            return this.name;
        }
    }

}
