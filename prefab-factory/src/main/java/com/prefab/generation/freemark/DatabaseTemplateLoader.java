package com.prefab.generation.freemark;

import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import com.prefab.factory.entity.model.Templates;
import com.prefab.factory.service.TemplatesService;
import com.scaffold.common.util.SpringUtil;
import freemarker.cache.TemplateLoader;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/22 15:46:29
 * @描述: TODO
 */
public class DatabaseTemplateLoader implements TemplateLoader {

    @Override
    public Object findTemplateSource(String templateId) throws IOException {
        //传入template名字时，会带上Locale，例如传入templateId=1时，实际templateId=1_zh_CN
        List<String> split = StrUtil.split(templateId, CharUtil.UNDERLINE);
        TemplatesService templatesService = SpringUtil.getBean(TemplatesService.class);
        Templates templatesByName = templatesService.getTemplatesById(Integer.parseInt(split.get(0)));
        return new DatabaseTemplateSource(templatesByName.getTemplateName(), templatesByName.getTemplateId(), templatesByName.getTemplateContent(),
                null != templatesByName.getUpdatedTime()
                        ? templatesByName.getUpdatedTime().toInstant(ZoneOffset.ofHours(8)).toEpochMilli()
                        : Instant.now().toEpochMilli());
    }

    @Override
    public long getLastModified(Object templateSource) {
        TemplatesService templatesService = SpringUtil.getBean(TemplatesService.class);
        Integer templateId = ((DatabaseTemplateSource) templateSource).getId();
        Templates templatesByName = templatesService.getTemplatesById(templateId);
        return null != templatesByName.getUpdatedTime()
                ? templatesByName.getUpdatedTime().toInstant(ZoneOffset.ofHours(8)).toEpochMilli()
                : Instant.now().toEpochMilli();
    }

    @Override
    public Reader getReader(Object templateSource, String encoding) throws IOException {
        return new StringReader(((DatabaseTemplateSource) templateSource).getTemplateContent());
    }

    @Override
    public void closeTemplateSource(Object o) throws IOException {

    }

    private static class DatabaseTemplateSource {
        private final String name;
        private final Integer id;
        private final String templateContent;
        private final long lastModified;

        DatabaseTemplateSource(String name, Integer id, String templateContent, long lastModified) {
            if (name == null) {
                throw new IllegalArgumentException("name == null");
            } else if (templateContent == null) {
                throw new IllegalArgumentException("source == null");
            } else if (lastModified < -1L) {
                throw new IllegalArgumentException("lastModified < -1L");
            } else {
                this.name = name;
                this.id = id;
                this.templateContent = templateContent;
                this.lastModified = lastModified;
            }
        }

        public String getName() {
            return name;
        }

        public Integer getId() {
            return id;
        }

        public String getTemplateContent() {
            return templateContent;
        }

        public long getLastModified() {
            return lastModified;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            } else if (obj == null) {
                return false;
            } else if (this.getClass() != obj.getClass()) {
                return false;
            } else {
                DatabaseTemplateSource other = (DatabaseTemplateSource) obj;
                if (this.name == null) {
                    if (other.name != null) {
                        return false;
                    }
                } else if (!this.name.equals(other.name)) {
                    return false;
                }

                return true;
            }
        }

        public String toString() {
            return this.name;
        }
    }

}
