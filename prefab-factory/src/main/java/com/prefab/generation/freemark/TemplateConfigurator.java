package com.prefab.generation.freemark;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.CharsetUtil;
import com.prefab.generation.entity.model.GenerationAttributes;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateModelException;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/20 10:27:29
 * @描述: TODO
 */
public class TemplateConfigurator {

    private final static String FTL_DIRECTORY = "/ftls";
    /**
     * 默认配置。从templates/ftls加载模板文件
     */
    private final static Configuration configuration = new Configuration(Configuration.VERSION_2_3_31);

    static {
//        try {
//            ClassPathResource cpr = new ClassPathResource(FTL_DIRECTORY);
//            configuration.setDirectoryForTemplateLoading(cpr.getFile());
//            configuration.setDefaultEncoding(CharsetUtil.UTF_8);
//            configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
//            Map<String, Object> variables = new HashMap<>();
//            configuration.setSharedVariables(variables);
//            configuration.setTemplateLoader(new DatabaseTemplateLoader());
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }

    }

    private static Configuration instance() {
        Configuration instance = new Configuration(Configuration.VERSION_2_3_31);
        instance.setDefaultEncoding(CharsetUtil.UTF_8);
        instance.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        return instance;
    }

    /**
     * @描述 字符串模板。初始化配置即加载所有模板
     * @参数 templateSourceList
     * @参数 variables
     * @返回值: freemarker.template.Configuration
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-27 10:15:33
     */
    public static Configuration stringTemplateInstance(List<StringTemplateLoader.StringTemplateSource> templateSourceList, Map<String, Object> variables) {
        StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();
        stringTemplateLoader.putTemplate(templateSourceList);
        return instance(stringTemplateLoader, variables);
    }

    /**
     * @描述 从数据库中加载模板。每次查询数据库获取模板信息
     * @参数 variables
     * @返回值: freemarker.template.Configuration
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-27 10:16:00
     */
    public static Configuration databaseTemplateInstance(Map<String, Object> variables) {
        return instance(new DatabaseTemplateLoader(), variables);
    }

    public static Configuration instance(TemplateLoader templateLoader, Map<String, Object> variables) {
        Configuration instance = instance();
        if (templateLoader != null) {
            instance.setTemplateLoader(templateLoader);
        }
        if (CollUtil.isEmpty(variables)) {
            return instance;
        }
        try {
            instance.setSharedVariables(variables);
        } catch (TemplateModelException e) {
            throw new RuntimeException(e);
        }
        return instance;
    }

    /**
     * 从指定目录获取模板
     *
     * @param templateFolder
     * @param variables
     * @return
     */
    public static Configuration instance(String templateFolder, Map<String, Object> variables) {
        Configuration instance = instance();
        try {
            instance.setDirectoryForTemplateLoading(new File(templateFolder));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return instance;
    }

    public static Configuration cfg() {
        return configuration;
    }

    public static Configuration setSharedVariables(Map<String, Object> variables) {
        if (CollUtil.isEmpty(variables)) {
            return configuration;
        }
        try {
            configuration.setSharedVariables(variables);
        } catch (TemplateModelException e) {
            throw new RuntimeException(e);
        }
        return configuration;
    }

    public static Configuration setSharedVariables(GenerationAttributes generationAttributes) {
        if (null == generationAttributes) {
            return configuration;
        }
        try {
            configuration.setSharedVariable("enableLombok", generationAttributes.isEnableLombok());
            configuration.setSharedVariable("enableMBPlus", generationAttributes.isEnableMBPlus());
        } catch (TemplateModelException e) {
            throw new RuntimeException(e);
        }
        return configuration;
    }

}
