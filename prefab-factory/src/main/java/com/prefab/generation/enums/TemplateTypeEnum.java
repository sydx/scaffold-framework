package com.prefab.generation.enums;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 15:10:57
 * @描述: 模板类型
 */
public enum TemplateTypeEnum {
    entity("E", "实体类"),
    entityQuery("EQ", "查询实体类"),
    mapper("M", "映射类"),
    mapperXml("MX", "映射文件"),
    service("S", "业务处理接口"),
    serviceImpl("SI", "业务处理实现类"),
    controller("CT", "控制器"),
    controllerApi("CTA", "接口控制器"),
    page("P", "页面文件"),
    pageJs("JS", "JS文件");

    private final String abbr;
    private final String label;

    TemplateTypeEnum(String abbr, String label) {
        this.abbr = abbr;
        this.label = label;
    }

    public String getAbbr() {
        return abbr;
    }

    public String getLabel() {
        return label;
    }

    public static TemplateTypeEnum getType(String type) {
        for (TemplateTypeEnum typeEnum : TemplateTypeEnum.values()) {
            if (typeEnum.name().equals(type)) {
                return typeEnum;
            }
        }
        return null;
    }
}
