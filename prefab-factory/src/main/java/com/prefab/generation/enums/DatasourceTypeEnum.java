package com.prefab.generation.enums;

/**
 * @创建人: 李亮
 * @创建时间: 2022/10/8 13:12:11
 * @描述: 数据源类型
 */
public enum DatasourceTypeEnum {
    MySQL, SQLServer;
}
