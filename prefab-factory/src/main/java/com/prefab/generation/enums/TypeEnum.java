package com.prefab.generation.enums;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 15:10:57
 * @描述: TODO
 */
public enum TypeEnum {
    INT("Integer", "INTEGER"),
    TINYINT("Integer", "TINYINT"),
    SMALLINT("Integer", "SMALLINT"),
    BIGINT("Long", "BIGINT"),
    DECIMAL("Double", "DECIMAL"),
    VARCHAR("String", "VARCHAR"),
    CHAR("String", "CHAR"),
    TEXT("String", "VARCHAR"),
    MEDIUMTEXT("String", "VARCHAR"),
    LONGTEXT("String", "VARCHAR"),
    DATETIME("Date", "TIMESTAMP"),
    DATE("Date", "TIMESTAMP"),
    ;

    private final String javaType;
    private final String jdbcType;

    TypeEnum(String javaType, String jdbcType) {
        this.javaType = javaType;
        this.jdbcType = jdbcType;
    }

    public String getJavaType() {
        return javaType;
    }

    public String getJdbcType() {
        return jdbcType;
    }

    public static String getJavaType(String columnDataType) {
        for (TypeEnum value : TypeEnum.values()) {
            if (value.name().equalsIgnoreCase(columnDataType)) {
                return value.getJavaType();
            }
        }
        return "String";
    }

    public static String getJdbcType(String columnDataType) {
        for (TypeEnum value : TypeEnum.values()) {
            if (value.name().equalsIgnoreCase(columnDataType)) {
                return value.getJdbcType();
            }
        }
        return "String";
    }
}
