package com.prefab.generation.service;


import com.prefab.generation.entity.model.DBTables;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 11:52:42
 * @描述: TODO
 */
public interface TablesService {
    List<DBTables> listTable(String tableSchema);

    DBTables getTableByName(String tableSchema, String tableName);

    List<String> listDataBase();
}
