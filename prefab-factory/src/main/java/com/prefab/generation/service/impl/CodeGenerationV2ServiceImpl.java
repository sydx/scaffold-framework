package com.prefab.generation.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.prefab.factory.entity.dto.GenerationPreviewDTO;
import com.prefab.factory.entity.model.Templates;
import com.prefab.factory.service.TemplatesService;
import com.prefab.generation.dao.ColumnsDAO;
import com.prefab.generation.dao.TablesDAO;
import com.prefab.generation.entity.dto.EntityFieldDTO;
import com.prefab.generation.entity.dto.TemplateDTO;
import com.prefab.generation.entity.model.DBTableColumns;
import com.prefab.generation.entity.model.DBTables;
import com.prefab.generation.entity.model.GenerationAttributes;
import com.prefab.generation.enums.ColumnKeyEnum;
import com.prefab.generation.enums.TemplateTypeEnum;
import com.prefab.generation.enums.TypeEnum;
import com.prefab.generation.freemark.StringTemplateLoader;
import com.prefab.generation.freemark.TemplateConfigurator;
import com.prefab.generation.service.CodeGenerationV2Service;
import com.prefab.generation.service.TableColumnsService;
import com.prefab.generation.service.TablesService;
import com.prefab.generation.util.GenerationUtil;
import com.scaffold.common.exception.SystemException;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import freemarker.template.Configuration;
import freemarker.template.Template;
import jakarta.validation.constraints.NotNull;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.FileWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/23 15:25:07
 * @描述: 代码生成处理
 */
@Service
public class CodeGenerationV2ServiceImpl implements CodeGenerationV2Service {

    private TemplatesService templatesService;

    public CodeGenerationV2ServiceImpl(TemplatesService templatesService) {
        this.templatesService = templatesService;
    }

    private List<String> commonFieldList = CollUtil.newArrayList("createdBy", "createdTime", "updatedBy", "updatedTime", "deleted");

    @Override
    public List<String> preview(GenerationAttributes generationAttributes, GenerationPreviewDTO preview) {
        List<Templates> templates = null;
        if (preview.getTemplateId() != null) {
            generationAttributes.setTemplateIds(JSONArray.of(preview.getTemplateId()).toJSONString());
            templates = getTemplatesList(generationAttributes);
        } else {
            Assert.hasText(preview.getTemplate(), "模板内容不能为空");
            templates = getTemplatesList(preview.getTemplate(), preview.getTemplateType());
        }
        List<String> tableNameList = CollUtil.newArrayList(preview.getTableName());
        return generationForPreview(generationAttributes, templates, tableNameList);
    }

    private List<String> generationForPreview(GenerationAttributes generationAttributes, List<Templates> templates, List<String> tableNameList) {
        Configuration templateConfiguration = getTemplateConfiguration(generationAttributes, templates);
//        JdbcTemplate jdbcTemplate = getJdbcTemplate(generationAttributes);

        HikariDataSource dataSource = null;
        try {
            dataSource = getDataSource(generationAttributes);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            TablesService tablesService = new TablesServiceImpl(new TablesDAO(jdbcTemplate));
            TableColumnsService tableColumnsService = new TableColumnsServiceImpl(new ColumnsDAO(jdbcTemplate));
            String tableSchema = generationAttributes.getDataSource().getDb();
            List<String> generationResult = new ArrayList<>();
            for (String tableName : tableNameList) {
                DBTables dbTables = tablesService.getTableByName(tableSchema, tableName);
                List<DBTableColumns> columns = tableColumnsService.listTableColumns(tableSchema, tableName);
                TemplateDTO dto = createTemplateDTO(generationAttributes, dbTables, columns);
                getEntityFieldDTOList(columns, dto);
                for (Templates template : templates) {
                    getFilePath(template, dto);
                    StringWriter stringWriter = new StringWriter();
                    doGeneration(templateConfiguration, dto, stringWriter, getTemplateName(template));
                    generationResult.add(stringWriter.toString());
                }
            }
            return generationResult;
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            if (dataSource != null && !dataSource.isClosed()) {
                dataSource.close();
            }
        }
    }

    /**
     * @描述 代码生成
     * @参数 generationAttributes
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-27 15:19:25
     */
    @Override
    public void generation(GenerationAttributes generationAttributes) {
        List<Templates> templates = getTemplatesList(generationAttributes);
        List<String> tableNameList = JSON.parseArray(generationAttributes.getGenerationTables(), String.class);
        generationByTable(generationAttributes, templates, tableNameList);
    }

    @NotNull
    private JdbcTemplate getJdbcTemplate(GenerationAttributes generationAttributes) {
        HikariDataSource dataSource = getDataSource(generationAttributes);
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate;
    }

    private void generationByTable(GenerationAttributes generationAttributes, List<Templates> templates, List<String> tableNameList) {

        Configuration templateConfiguration = getTemplateConfiguration(generationAttributes, templates);
//        JdbcTemplate jdbcTemplate = getJdbcTemplate(generationAttributes);
        HikariDataSource dataSource = null;
        try {
            dataSource = getDataSource(generationAttributes);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            TablesService tablesService = new TablesServiceImpl(new TablesDAO(jdbcTemplate));
            TableColumnsService tableColumnsService = new TableColumnsServiceImpl(new ColumnsDAO(jdbcTemplate));
            String tableSchema = generationAttributes.getDataSource().getDb();
            for (String tableName : tableNameList) {
                DBTables dbTables = tablesService.getTableByName(tableSchema, tableName);
                List<DBTableColumns> columns = tableColumnsService.listTableColumns(tableSchema, tableName);
                TemplateDTO dto = createTemplateDTO(generationAttributes, dbTables, columns);
                getEntityFieldDTOList(columns, dto);
                for (Templates template : templates) {
                    String filePath = getFilePath(template, dto);
                    doGeneration(templateConfiguration, dto, filePath, getTemplateName(template));
                }
            }
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            if (dataSource != null && !dataSource.isClosed()) {
                dataSource.close();
            }
        }
    }

    /**
     * @描述 获取模板
     * @参数 generationAttributes
     * @返回值: java.util.List<com.prefab.factory.entity.model.Templates>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-27 15:19:44
     */
    private List<Templates> getTemplatesList(GenerationAttributes generationAttributes) {
        List<Integer> templateIdList = JSON.parseArray(generationAttributes.getTemplateIds(), Integer.class);
        List<Templates> templates = templatesService.getTemplatesByIds(templateIdList);
        return templates;
    }


    /**
     * @描述 获取模板
     * @参数 generationAttributes
     * @返回值: java.util.List<com.prefab.factory.entity.model.Templates>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-27 15:19:44
     */
    private List<Templates> getTemplatesList(String templateContent, String templateType) {
        Templates template = new Templates();
        template.setTemplateId(9999);
        template.setTemplateContent(templateContent);
        template.setTemplateType(templateType);
        template.setTemplateName("preview");
        template.setTemplateEngine("freemarker");
        return CollUtil.newArrayList(template);
    }

    /**
     * 根据表字段获取实体对象的属性信息
     *
     * @param columns
     * @param dto
     * @return
     */
    private List<EntityFieldDTO> getEntityFieldDTOList(List<DBTableColumns> columns, TemplateDTO dto) {
        List<EntityFieldDTO> fields = new ArrayList<>();
        EntityFieldDTO field = null;
        for (DBTableColumns column : columns) {
            field = new EntityFieldDTO();
            field.setName(GenerationUtil.getLowerFirstCameCase(column.getColumnName()));
            field.setUpperFirstName(GenerationUtil.getUpperFirstCamelCase(column.getColumnName()));
            field.setType(TypeEnum.getJavaType(column.getDataType()));
            field.setComment(column.getColumnComment());
            field.setTableColumnName(column.getColumnName());
            field.setTableColumnDataType(column.getDataType());
            field.setTableColumnJdbcType(TypeEnum.getJdbcType(column.getDataType()));
            field.setPrimaryKey(StrUtil.equals(column.getColumnKey(), ColumnKeyEnum.PRI.name()));
            field.setAutoIncrement(StrUtil.equals(column.getExtra(), "auto_increment"));
            field.setCommonField(commonFieldList.contains(field.getName()));
            if (field.isPrimaryKey()) {
                dto.setPkColumnName(column.getColumnName());
                dto.setPkColumnType(column.getDataType());
                dto.setPkFieldName(field.getName());
                dto.setPkFieldType(field.getType());
            }
            fields.add(field);
        }
        dto.setFields(fields);
        return fields;
    }

    /**
     * @描述 创建模板对象，并设置模板的基本信息
     * @参数 generationAttributes
     * @参数 dbTables
     * @参数 columns
     * @返回值: com.prefab.generation.entity.dto.TemplateDTO
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-27 15:22:00
     */
    private TemplateDTO createTemplateDTO(GenerationAttributes generationAttributes, DBTables dbTables, List<DBTableColumns> columns) {
        TemplateDTO dto = new TemplateDTO();
        dto.setAttributes(generationAttributes);
        dto.setTable(dbTables);
        dto.setColumns(columns);
        dto.setTableNameCamel(GenerationUtil.getUpperFirstCamelCase(dbTables.getTableName()));
        dto.setEntityName(GenerationUtil.getUpperFirstCamelCase(dbTables.getTableName()) + generationAttributes.getEntitySuffix());
        dto.setEntityBeanName(GenerationUtil.getLowerFirstCameCase(dbTables.getTableName()) + generationAttributes.getEntitySuffix());
        dto.setTableNameConvertPath(StrUtil.toSymbolCase(dto.getTableNameCamel(), CharUtil.SLASH));
        return dto;
    }

    /**
     * @描述 创建数据库连接数据源
     * @参数 generationAttributes
     * @返回值: com.zaxxer.hikari.HikariDataSource
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-27 15:21:01
     */
    private HikariDataSource getDataSource(GenerationAttributes generationAttributes) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(generationAttributes.getDataSource().getJdbcUrl());
        hikariConfig.setUsername(generationAttributes.getDataSource().getUsername());
        hikariConfig.setPassword(generationAttributes.getDataSource().getPassword());
        hikariConfig.setMaximumPoolSize(1);
        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        return dataSource;
    }

    /**
     * 初始化模板配置。
     * 1. 加载模板
     * 2. 设置公共属性
     *
     * @param generationAttributes
     * @param templates
     * @return
     */
    private Configuration getTemplateConfiguration(GenerationAttributes generationAttributes, List<Templates> templates) {
        List<StringTemplateLoader.StringTemplateSource> templateSourceList = new ArrayList<>();
        for (Templates template : templates) {
            templateSourceList.add(
                    new StringTemplateLoader.StringTemplateSource(
                            getTemplateName(template),
                            template.getTemplateContent(),
                            null != template.getUpdatedTime()
                                    ? template.getUpdatedTime().toInstant(ZoneOffset.ofHours(8)).toEpochMilli()
                                    : Instant.now().toEpochMilli()
                    ));
        }
        Map<String, Object> variables = new HashMap<>();
        variables.put("enableMBPlus", generationAttributes.isEnableMBPlus());
        variables.put("enableLombok", generationAttributes.isEnableLombok());
        return TemplateConfigurator.stringTemplateInstance(templateSourceList, variables);
    }

    @NotNull
    private static String getTemplateName(Templates template) {
        return template.getTemplateId() + "_" + template.getTemplateName();
    }

    /**
     * 获取文件路径
     *
     * @param template
     * @param dto
     * @return
     */
    private String getFilePath(Templates template, TemplateDTO dto) {
        GenerationAttributes attributes = dto.getAttributes();
        TemplateTypeEnum typeEnum = TemplateTypeEnum.getType(template.getTemplateType());
        //java文件保存根路径
        StringBuilder javaPath = new StringBuilder(attributes.getBaseFolder()).append(attributes.getJavaFolder());
        //资源文件保存根路径
        StringBuilder resourcePath = new StringBuilder(attributes.getBaseFolder()).append(attributes.getResourcesFolder());
        //当前java文件的根包名，或者资源文件在根路径下的相对路径
        StringBuilder packageName = new StringBuilder(attributes.getBasePackage());
        //文件的后缀名。例如：Service\Mapper\Controller等
        String suffix = "";
        //包路径。在根路径下的相对路径
        String packagePath = "";
        boolean isResources = false;
        switch (typeEnum) {
            case entity:
                packagePath = attributes.getEntityPackage();
                suffix = attributes.getEntitySuffix();
                break;
            case entityQuery:
                packagePath = attributes.getEntityQueryPackage();
                suffix = attributes.getEntityQuerySuffix();
                break;
            case mapper:
                packagePath = attributes.getMapperPackage();
                suffix = attributes.getMapperSuffix();
                break;
            case mapperXml:
                packagePath = attributes.getMapperXmlPackage();
                suffix = attributes.getMapperXmlSuffix();
                //如果映射文件保存到java目录，则作为非资源文件处理
                isResources = attributes.getMapperXmlSaveTo() != 1;
                break;
            case service:
                packagePath = attributes.getServicePackage();
                suffix = attributes.getServiceSuffix();
                break;
            case serviceImpl:
                packagePath = attributes.getServiceImplPackage();
                suffix = attributes.getServiceImplSuffix();
                break;
            case controller:
                packagePath = attributes.getControllerPackage();
                suffix = attributes.getControllerSuffix();
                break;
            case controllerApi:
                packagePath = attributes.getControllerApiPackage();
                suffix = attributes.getControllerApiSuffix();
                break;
            case page:
                packagePath = attributes.getPageRootFolder() +
                        StrUtil.SLASH + attributes.getPageFolder();
                suffix = attributes.getPageSuffix();
                isResources = true;
                break;
            case pageJs:
                packagePath = attributes.getJsFolder();
                suffix = attributes.getJsSuffix();
                isResources = true;
                break;
            default:
                throw new RuntimeException("不支持的模板类型");
        }

        setClassAndBeanName(dto, suffix);
        String fileExt = getFileExt(typeEnum, attributes);
        if (isResources) {
            //资源文件路径处理
            //资源文件根路径 + 当前文件所在目录
            resourcePath.append(StrUtil.SLASH)
                    .append(packagePath);
            //如果有子模块名称，则需要添加子模块目录
            if (StrUtil.isNotBlank(attributes.getModulePackage())) {
                resourcePath.append(StrUtil.SLASH).append(attributes.getModulePackage());
            }
            //目录不存在时创建目录
            GenerationUtil.createFolder(resourcePath.toString());
            //拼接文件名，mapper文件使用类目，页面或js文件使用实例名称
            String fileName = fileExt.equals(".xml") ? dto.getClassName() : dto.getBeanName();
            //最后拼接路径和文件名
            return resourcePath.append(StrUtil.SLASH).append(fileName).append(fileExt).toString();
        } else {
            //java文件路径处理
            packageName.append(StrUtil.DOT).append(packagePath);
            //如果有子模块名称，则需要添加子模块包
            if (StrUtil.isNotBlank(attributes.getModulePackage())) {
                packageName.append(StrUtil.DOT).append(attributes.getModulePackage());
            }
            dto.setPackageName(packageName.toString());
            //文件路径由报名转换而来，将“.”替换为“/”
            String packageToPath = packageName.toString().replaceAll("\\.", StrUtil.SLASH);
            javaPath.append(StrUtil.SLASH).append(packageToPath);
            //目录不存在时创建目录
            GenerationUtil.createFolder(javaPath.toString());
            //最后拼接路径和文件名
            return javaPath.append(StrUtil.SLASH)
                    .append(dto.getClassName()).append(fileExt).toString();
        }
    }

    /**
     * @描述 获取文件扩展名
     * @参数 typeEnum
     * @返回值: java.lang.String
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-27 15:28:51
     */
    private String getFileExt(TemplateTypeEnum typeEnum, GenerationAttributes attributes) {
        switch (typeEnum) {
            case mapperXml:
                return ".xml";
            case page:
                return "." + attributes.getPageExtension();
            case pageJs:
                return "." + attributes.getJsExtension();
            default:
                return ".java";
        }
    }

    /**
     * 设置当前文件的类名/文件名和实例对象名
     *
     * @param dto
     * @param suffix
     */
    private void setClassAndBeanName(TemplateDTO dto, String suffix) {
        dto.setClassName(dto.getTableNameCamel() + suffix);
        dto.setBeanName(StrUtil.lowerFirst(dto.getTableNameCamel()) + suffix);
    }

    /**
     * 执行代码生成
     *
     * @param templateConfiguration
     * @param dto
     * @param filePath
     * @param templateName
     */
    @Override
    public void doGeneration(Configuration templateConfiguration, TemplateDTO dto, String filePath, String templateName) {
        try (Writer out = new FileWriter(filePath)) {
            Template template = templateConfiguration.getTemplate(templateName);
            Map<String, Object> root = new HashMap<>();
            root.put("tp", dto);
            template.process(root, out);
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * 执行代码生成
     *
     * @param templateConfiguration
     * @param dto
     * @param out
     * @param templateName
     */
    @Override
    public void doGeneration(Configuration templateConfiguration, TemplateDTO dto, Writer out, String templateName) {
        try {
            Template template = templateConfiguration.getTemplate(templateName);
            Map<String, Object> root = new HashMap<>();
            root.put("tp", dto);
            template.process(root, out);
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * @描述 删除已生成的文件
     * @参数 generationAttributes
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-27 15:39:21
     */
    @Override
    public void delete(GenerationAttributes generationAttributes) {
        List<Templates> templates = getTemplatesList(generationAttributes);
        HikariDataSource dataSource = null;
        try {
            dataSource = getDataSource(generationAttributes);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            TablesService tablesService = new TablesServiceImpl(new TablesDAO(jdbcTemplate));
            List<String> tableNameList = JSON.parseArray(generationAttributes.getGenerationTables(), String.class);
            String tableSchema = generationAttributes.getDataSource().getDb();
            for (String tableName : tableNameList) {
                DBTables dbTables = tablesService.getTableByName(tableSchema, tableName);
                TemplateDTO dto = new TemplateDTO();
                dto.setAttributes(generationAttributes);
                dto.setEntityName(GenerationUtil.getUpperFirstCamelCase(dbTables.getTableName()));
                dto.setEntityBeanName(GenerationUtil.getLowerFirstCameCase(dbTables.getTableName()));
                for (Templates template : templates) {
                    String filePath = getFilePath(template, dto);
                    GenerationUtil.deleteFile(filePath);
                }
            }
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            if (dataSource != null && !dataSource.isClosed()) {
                dataSource.close();
            }
        }
    }
}
