package com.prefab.generation.service.impl;

import com.prefab.generation.dao.ColumnsDAO;
import com.prefab.generation.entity.model.DBTableColumns;
import com.prefab.generation.service.TableColumnsService;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 13:36:43
 * @描述: TODO
 */
public class TableColumnsServiceImpl implements TableColumnsService {
    private final ColumnsDAO columnsDAO;

    public TableColumnsServiceImpl(ColumnsDAO columnsDAO) {
        this.columnsDAO = columnsDAO;
    }

    @Override
    public List<DBTableColumns> listTableColumns(String tableSchema, String tableName) {
        return columnsDAO.listTableColumns(tableSchema, tableName);
    }
}
