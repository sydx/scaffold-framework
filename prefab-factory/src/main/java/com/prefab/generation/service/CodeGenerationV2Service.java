package com.prefab.generation.service;


import com.prefab.factory.entity.dto.GenerationPreviewDTO;
import com.prefab.generation.entity.dto.TemplateDTO;
import com.prefab.generation.entity.model.GenerationAttributes;
import freemarker.template.Configuration;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 13:54:47
 * @描述: TODO
 */
public interface CodeGenerationV2Service {
    List<String> preview(GenerationAttributes generationAttributes, GenerationPreviewDTO preview);

    void generation(GenerationAttributes folderAttributes);

    void doGeneration(Configuration templateConfiguration, TemplateDTO dto, String filePath, String templateName);

    void doGeneration(Configuration templateConfiguration, TemplateDTO dto, Writer out, String templateName) throws IOException;

    void delete(GenerationAttributes generationAttributes);
}
