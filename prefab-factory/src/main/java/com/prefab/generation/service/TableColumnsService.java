package com.prefab.generation.service;


import com.prefab.generation.entity.model.DBTableColumns;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 11:52:42
 * @描述: TODO
 */
public interface TableColumnsService {
    List<DBTableColumns> listTableColumns(String tableSchema, String tableName);
}
