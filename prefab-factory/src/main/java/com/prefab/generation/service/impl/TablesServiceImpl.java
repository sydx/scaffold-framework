package com.prefab.generation.service.impl;

import com.prefab.generation.dao.TablesDAO;
import com.prefab.generation.entity.model.DBTables;
import com.prefab.generation.service.TablesService;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/21 11:52:54
 * @描述: TODO
 */
public class TablesServiceImpl implements TablesService {

    private final TablesDAO tablesDAO;

    public TablesServiceImpl(TablesDAO tablesDAO) {
        this.tablesDAO = tablesDAO;
    }

    @Override
    public List<DBTables> listTable(String tableSchema) {
        return tablesDAO.listTable(tableSchema);
    }

    @Override
    public DBTables getTableByName(String tableSchema, String tableName) {
        return tablesDAO.getTableByName(tableSchema, tableName);
    }

    @Override
    public List<String> listDataBase() {
        return tablesDAO.listDataBase();
    }
}
