/*

 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Schema         : scaffold-framework

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 24/08/2023 16:44:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_enum
-- ----------------------------
DROP TABLE IF EXISTS `sys_enum`;
CREATE TABLE `sys_enum`  (
  `enum_id` int NOT NULL AUTO_INCREMENT,
  `enum_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '枚举编号',
  `enum_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '枚举名称',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '枚举说明',
  `state` tinyint NULL DEFAULT 0 COMMENT '状态。0：停用；1：启用',
  `created_by` int NULL DEFAULT 0 COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT 0 COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` bigint NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`enum_id`) USING BTREE,
  UNIQUE INDEX `enum_code`(`enum_code` ASC, `deleted` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统枚举' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_enum
-- ----------------------------
INSERT INTO `sys_enum` VALUES (4, 'SPLX', '商品类型', '商品类型', 1, 1, '2023-02-11 16:57:12', 1, '2023-02-13 09:28:33', 1676251713);
INSERT INTO `sys_enum` VALUES (5, 'SPLX', '商品类型', '阿萨德飞阿萨德飞', 1, 1, '2023-02-13 09:57:19', NULL, '2023-08-21 16:09:36', 0);
INSERT INTO `sys_enum` VALUES (6, 'ABCD', '测试枚举12', '爱上对方爱上对方爱上对方22', 1, NULL, '2023-08-21 16:09:47', NULL, '2023-08-21 16:10:49', 1692605449);
INSERT INTO `sys_enum` VALUES (7, 'ABCD', '爱上对方爱上对方', '爱上对方爱上对方', 1, NULL, '2023-08-21 16:16:49', NULL, '2023-08-21 16:19:49', 1692605989);

-- ----------------------------
-- Table structure for sys_enum_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_enum_item`;
CREATE TABLE `sys_enum_item`  (
  `item_id` int NOT NULL AUTO_INCREMENT,
  `enum_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '枚举编号',
  `item_value` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '枚举项值',
  `item_label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '枚举项显示名称',
  `item_index` int NULL DEFAULT 0 COMMENT '枚举项显示顺序',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '枚举项说明',
  `state` tinyint NULL DEFAULT 0 COMMENT '状态。0：停用；1：启用',
  `created_by` int NULL DEFAULT 0 COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT 0 COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` bigint NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`item_id`) USING BTREE,
  UNIQUE INDEX `enum_code`(`enum_code` ASC, `item_value` ASC, `deleted` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统枚举项' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_enum_item
-- ----------------------------
INSERT INTO `sys_enum_item` VALUES (1, 'SPLX', '1', '毛猪', 0, '毛猪', 1, 1, '2023-02-13 10:04:26', 1, '2023-02-13 10:08:34', 0);
INSERT INTO `sys_enum_item` VALUES (2, 'SPLX', '2', '白条', 0, '白条', 1, 1, '2023-02-13 10:04:45', 1, '2023-02-13 10:04:45', 0);
INSERT INTO `sys_enum_item` VALUES (3, 'SPLX', '3', '副产', 0, '生猪宰杀后的副产，例如猪头、内脏等', 1, 1, '2023-02-13 10:05:17', 1, '2023-02-13 10:05:17', 0);
INSERT INTO `sys_enum_item` VALUES (5, 'SPLX', '4', '伤猪', 0, '运输货存栏过程中受伤的生猪', 1, 1, '2023-02-13 10:05:49', 1, '2023-02-13 10:05:49', 0);
INSERT INTO `sys_enum_item` VALUES (6, 'SPLX', '5', '阿斯顿', 0, '阿萨德飞', 0, 1, '2023-02-13 10:08:42', 1, '2023-02-13 10:08:45', 1676254124);
INSERT INTO `sys_enum_item` VALUES (7, 'ABCD', 'A', 'AAAA1', 0, 'AAAAAA2', 1, NULL, '2023-08-21 16:10:19', NULL, '2023-08-21 16:10:53', 1692605453);
INSERT INTO `sys_enum_item` VALUES (8, 'ABCD', 'B', 'BBBB', 0, 'BBBBB', 0, NULL, '2023-08-21 16:10:29', NULL, '2023-08-21 16:10:46', 1692605446);
INSERT INTO `sys_enum_item` VALUES (9, 'ABCD', '爱上对方', '爱上对方', 0, '爱上对方', 1, NULL, '2023-08-21 16:16:57', NULL, '2023-08-21 16:19:46', 1692605986);

-- ----------------------------
-- Table structure for sys_module
-- ----------------------------
DROP TABLE IF EXISTS `sys_module`;
CREATE TABLE `sys_module`  (
  `module_id` int NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模块名称',
  `permission_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `system_id` int NULL DEFAULT 1 COMMENT '模块所属系统。1:管理后台，2:小程序，3：APP',
  `module_logo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模块图标',
  `module_index` int NULL DEFAULT 0 COMMENT '模块显示顺序',
  `module_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模块外链',
  `state` tinyint NULL DEFAULT 0 COMMENT '状态。0：停用；1：启用',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`module_id`) USING BTREE,
  UNIQUE INDEX `permission_code`(`permission_code` ASC, `deleted` ASC) USING BTREE,
  INDEX `module_index`(`module_index` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统模块' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_module
-- ----------------------------
INSERT INTO `sys_module` VALUES (1, '系统管理', 'M01', 1, 'icon-xitongguanli-copy', 1, NULL, 1, 1, '2022-08-09 15:27:02', NULL, NULL, 0);
INSERT INTO `sys_module` VALUES (2, '商品管理', 'M02', 1, 'icon-packaging', 2, NULL, 1, 1, '2022-08-11 11:33:46', NULL, '2023-08-17 15:50:51', 0);
INSERT INTO `sys_module` VALUES (3, '代码生成', 'M03', 1, 'icon-office', 3, NULL, 1, 1, '2022-09-22 11:42:01', NULL, '2023-08-17 15:55:21', 0);
INSERT INTO `sys_module` VALUES (5, '1123', 'M04', 1, 'icon-phone', 5, '123', 1, 1, '2023-08-23 13:46:41', 1, '2023-08-23 13:46:41', 0);

-- ----------------------------
-- Table structure for sys_module_function
-- ----------------------------
DROP TABLE IF EXISTS `sys_module_function`;
CREATE TABLE `sys_module_function`  (
  `function_id` int NOT NULL AUTO_INCREMENT,
  `module_id` int NOT NULL COMMENT '模块ID',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '功能名称',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '功能地址',
  `permission_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `function_level` int NULL DEFAULT NULL COMMENT '1-一级菜单   2-二级菜单   3-三级菜单',
  `function_path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '路径',
  `parent_id` int NULL DEFAULT 0 COMMENT '父级功能ID',
  `function_icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '功能图标',
  `function_index` decimal(3, 0) NULL DEFAULT NULL COMMENT '显示顺序',
  `state` tinyint NULL DEFAULT 0 COMMENT '状态。0：停用；1：启用',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`function_id`) USING BTREE,
  UNIQUE INDEX `permission_code`(`permission_code` ASC, `deleted` ASC) USING BTREE,
  INDEX `idx_module_id`(`module_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统模块下的功能' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_module_function
-- ----------------------------
INSERT INTO `sys_module_function` VALUES (1, 1, '模块功能', '/module', 'M01F01', 1, 'M01F03', 0, 'icon-mokuai', 1, 1, 1, '2022-08-09 15:27:02', 15, '2022-08-25 22:43:02', 0);
INSERT INTO `sys_module_function` VALUES (2, 1, '组织架构', '/organization', 'M01F02', 1, 'M01F02', 0, 'icon-zuzhijiagou1', 2, 1, 1, '2022-08-09 15:55:45', NULL, '2023-08-19 16:46:16', 0);
INSERT INTO `sys_module_function` VALUES (4, 1, '角色管理', '/role', 'M01F03', 1, 'M01F03', 0, 'icon-jiaoseguanli', 4, 1, 1, '2022-08-10 13:14:41', NULL, '2023-08-21 11:54:30', 0);
INSERT INTO `sys_module_function` VALUES (7, 2, '仓库管理', '/cangku', 'M02F01', 1, 'M02F01', 0, 'icon-phone', 7, 1, 1, '2022-08-11 11:34:06', 1, '2022-08-11 11:34:06', 0);
INSERT INTO `sys_module_function` VALUES (8, 2, '商品管理', '/product', 'M02F02', 1, 'M02F02', 0, 'icon-product', 8, 1, 1, '2022-08-11 11:34:26', 1, '2022-08-11 11:34:26', 0);
INSERT INTO `sys_module_function` VALUES (9, 1, '用户管理', '/user', 'M01F04', 1, 'M01F04', 0, 'icon-yonghuguanli', 9, 1, 1, '2022-08-15 13:48:28', NULL, '2023-08-19 10:48:30', 0);
INSERT INTO `sys_module_function` VALUES (10, 3, '测试功能', '/view/sys/module/parent/index', 'M03F01', 1, 'M03F01', 0, 'icon-process', 10, 1, 1, '2022-09-22 11:42:27', 1, '2022-09-22 11:42:27', 0);
INSERT INTO `sys_module_function` VALUES (11, 3, '测试功能2', '/view/sys/module/function/parent/index', 'M03F02', 1, 'M03F02', 0, 'icon-packing-labeling', 11, 1, 1, '2022-09-27 14:57:43', 1, '2022-09-27 14:58:15', 0);
INSERT INTO `sys_module_function` VALUES (12, 1, '枚举管理', '/enums', 'M01F05', 1, 'M01F05', 0, 'icon-office', 12, 1, 1, '2023-02-11 16:40:16', NULL, '2023-08-21 16:09:09', 0);
INSERT INTO `sys_module_function` VALUES (15, 1, '子功能01', '1123', 'M01F0601', 2, 'M01F06|M01F0601', 14, 'icon-play1', 15, 1, NULL, '2023-08-17 16:56:21', NULL, '2023-08-17 16:56:21', 0);

-- ----------------------------
-- Table structure for sys_module_function_action
-- ----------------------------
DROP TABLE IF EXISTS `sys_module_function_action`;
CREATE TABLE `sys_module_function_action`  (
  `action_id` int NOT NULL AUTO_INCREMENT,
  `module_id` int NOT NULL COMMENT '模块ID',
  `function_id` int NOT NULL COMMENT '功能ID',
  `action_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作点名称',
  `permission_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `action_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'GET' COMMENT '操作类型',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作点地址',
  `state` tinyint NULL DEFAULT 0 COMMENT '状态。0：停用；1：启用',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`action_id`) USING BTREE,
  UNIQUE INDEX `permission_code`(`permission_code` ASC, `deleted` ASC) USING BTREE,
  INDEX `idx_module_id_function_id`(`module_id` ASC, `function_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '功能的操作点' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_module_function_action
-- ----------------------------
INSERT INTO `sys_module_function_action` VALUES (1, 1, 1, '查询模块列表', 'M01F01A01', 'GET', '/api/v1/system/modules', 1, 1, '2022-08-09 15:27:02', NULL, '2023-08-17 17:16:16', 0);
INSERT INTO `sys_module_function_action` VALUES (2, 1, 1, '新增模块', 'M01F01A02', 'POST', '/api/v1/system/modules', 1, 1, '2022-08-09 15:27:02', 15, '2022-08-25 22:43:14', 0);
INSERT INTO `sys_module_function_action` VALUES (3, 1, 1, '修改模块', 'M01F01A03', 'PUT', '/api/v1/system/modules', 1, 1, '2022-08-09 15:27:02', 1, '2022-08-22 17:03:11', 0);
INSERT INTO `sys_module_function_action` VALUES (4, 1, 1, '启用/停用模块', 'M01F01A04', 'PATCH', '/api/v1/system/modules/state/{moduleId}/{state}', 1, 1, '2022-08-09 15:27:02', 1, '2022-08-25 22:02:51', 0);
INSERT INTO `sys_module_function_action` VALUES (5, 1, 1, '删除模块', 'M01F01A05', 'DELETE', '/api/v1/system/modules/{moduleId}', 1, 1, '2022-08-09 15:27:02', 1, '2022-08-22 17:04:44', 0);
INSERT INTO `sys_module_function_action` VALUES (6, 1, 1, '模块排序', 'M01F01A06', 'PATCH', '/api/v1/system/modules/sort/{sourceId}/{targetId}', 1, 1, '2022-08-09 15:27:02', 1, '2022-08-25 22:06:14', 0);
INSERT INTO `sys_module_function_action` VALUES (7, 1, 4, '查询角色列表', 'M01F03A01', 'GET', '/api/v1/system/roles', 1, 1, '2022-08-10 17:01:03', 1, '2022-08-10 17:01:03', 0);
INSERT INTO `sys_module_function_action` VALUES (8, 1, 5, '查询列表', 'M01F0201A01', 'GET', '/get', 1, 1, '2022-08-10 17:02:13', 1, '2022-08-13 13:30:21', 1660368621);
INSERT INTO `sys_module_function_action` VALUES (9, 2, 7, '查询仓库列表', 'M02F01A01', 'GET', '/list', 1, 1, '2022-08-11 11:34:40', 1, '2022-08-11 11:34:40', 0);
INSERT INTO `sys_module_function_action` VALUES (10, 2, 7, '新增仓库', 'M02F01A02', 'POST', '/add', 1, 1, '2022-08-11 11:34:51', 1, '2022-08-11 11:34:59', 0);
INSERT INTO `sys_module_function_action` VALUES (11, 2, 8, '查询商品列表', 'M02F02A01', 'GET', '/list', 1, 1, '2022-08-11 11:35:19', 1, '2022-08-11 11:35:19', 0);
INSERT INTO `sys_module_function_action` VALUES (12, 2, 8, '新增商品', 'M02F02A02', 'POST', '/add', 1, 1, '2022-08-11 11:35:34', 1, '2022-08-11 11:35:34', 0);
INSERT INTO `sys_module_function_action` VALUES (13, 1, 6, '查询列表', 'M01F0202A01', 'GET', '/list', 1, 1, '2022-08-11 11:36:09', 1, '2022-08-13 13:30:25', 1660368625);
INSERT INTO `sys_module_function_action` VALUES (14, 1, 2, '查看列表', 'M01F02A01', 'GET', '/api/v1/system/organizations', 1, 1, '2022-08-13 14:59:32', 1, '2022-08-13 14:59:32', 0);
INSERT INTO `sys_module_function_action` VALUES (15, 1, 9, '查看列表', 'M01F04A01', 'GET', '/api/v1/system/users', 1, 1, '2022-08-15 13:51:00', 1, '2022-08-15 13:51:00', 0);
INSERT INTO `sys_module_function_action` VALUES (16, 1, 1, '查询功能列表', 'M01F01A07', 'GET', '/api/v1/system/functions', 1, 1, '2022-08-25 22:33:03', 1, '2022-08-25 22:33:03', 0);
INSERT INTO `sys_module_function_action` VALUES (17, 1, 1, '新增功能', 'M01F01A08', 'POST', '/api/v1/system/functions', 1, 1, '2022-08-25 22:33:20', 1, '2022-08-25 22:33:20', 0);
INSERT INTO `sys_module_function_action` VALUES (18, 1, 1, '修改功能', 'M01F01A09', 'PUT', '/api/v1/system/functions', 1, 1, '2022-08-25 22:33:36', 1, '2022-08-25 22:33:36', 0);
INSERT INTO `sys_module_function_action` VALUES (19, 1, 1, '启用/停用功能', 'M01F01A10', 'PATCH', '/api/v1/system/functions/state/{functionId}/{state}', 1, 1, '2022-08-25 22:34:32', 1, '2022-08-25 22:34:32', 0);
INSERT INTO `sys_module_function_action` VALUES (20, 1, 1, '删除功能', 'M01F01A11', 'DELETE', '/api/v1/system/functions/{functionId}', 1, 1, '2022-08-25 22:35:04', 1, '2022-08-25 22:35:04', 0);
INSERT INTO `sys_module_function_action` VALUES (21, 1, 1, '功能排序', 'M01F01A12', 'PATCH', '/api/v1/system/functions/sort/{sourceId}/{targetId}', 1, 1, '2022-08-25 22:35:51', 1, '2022-08-25 22:35:51', 0);
INSERT INTO `sys_module_function_action` VALUES (22, 1, 1, '查询功能树', 'M01F01A13', 'GET', '/api/v1/system/functions/tree/{moduleId}', 1, 1, '2022-08-25 22:37:09', 1, '2022-08-25 22:37:09', 0);
INSERT INTO `sys_module_function_action` VALUES (23, 1, 1, '查询操作列表', 'M01F01A14', 'GET', '/api/v1/system/actions', 1, 1, '2022-08-25 22:39:08', 1, '2022-08-25 22:39:08', 0);
INSERT INTO `sys_module_function_action` VALUES (24, 1, 1, '新增操作', 'M01F01A15', 'POST', '/api/v1/system/actions', 1, 1, '2022-08-25 22:39:33', 1, '2022-08-25 22:39:33', 0);
INSERT INTO `sys_module_function_action` VALUES (25, 1, 1, '修改操作', 'M01F01A16', 'PUT', '/api/v1/system/actions', 1, 1, '2022-08-25 22:39:43', 1, '2022-08-25 22:39:43', 0);
INSERT INTO `sys_module_function_action` VALUES (26, 1, 1, '启用/停用操作', 'M01F01A17', 'PATCH', '/api/v1/system/actions/state/{actionId}/{state}', 1, 1, '2022-08-25 22:40:18', 1, '2022-08-25 22:40:18', 0);
INSERT INTO `sys_module_function_action` VALUES (27, 1, 1, '删除操作', 'M01F01A18', 'DELETE', '/api/v1/system/actions/{actionId}', 1, 1, '2022-08-25 22:40:45', 1, '2022-08-25 22:40:45', 0);
INSERT INTO `sys_module_function_action` VALUES (28, 1, 1, '获取权限树', 'M01F01A19', 'GET', '/api/v1/system/actions/permissionTree', 1, 1, '2022-08-25 22:41:36', 1, '2022-08-25 22:41:36', 0);
INSERT INTO `sys_module_function_action` VALUES (29, 3, 10, '列表', 'M03F01A01', 'GET', '/api/v1/sys/module/parent', 1, 1, '2022-09-22 11:42:59', 1, '2022-09-22 11:42:59', 0);
INSERT INTO `sys_module_function_action` VALUES (30, 3, 10, '新增', 'M03F01A02', 'POST', '/api/v1/sys/module/parent', 1, 1, '2022-09-22 11:43:12', 1, '2022-09-22 11:43:12', 0);
INSERT INTO `sys_module_function_action` VALUES (31, 3, 10, '修改', 'M03F01A03', 'PUT', '/api/v1/sys/module/parent', 1, 1, '2022-09-22 11:43:23', 1, '2022-09-22 11:43:23', 0);
INSERT INTO `sys_module_function_action` VALUES (32, 3, 10, '删除', 'M03F01A04', 'DELETE', '/api/v1/sys/module/parent', 1, 1, '2022-09-22 11:43:34', 1, '2022-09-22 11:43:34', 0);
INSERT INTO `sys_module_function_action` VALUES (33, 1, 12, '查看列表', 'M01F05A01', 'GET', '/api/v1/system/enum', 1, 1, '2023-02-11 16:42:25', 1, '2023-02-11 16:42:25', 0);
INSERT INTO `sys_module_function_action` VALUES (34, 1, 12, '新增枚举', 'M01F05A02', 'POST', '/api/v1/system/enum', 1, 1, '2023-02-13 10:11:12', 1, '2023-02-13 10:11:26', 0);
INSERT INTO `sys_module_function_action` VALUES (35, 1, 12, '修改枚举', 'M01F05A03', 'PUT', '/api/v1/system/enum', 1, 1, '2023-02-13 10:11:21', 1, '2023-02-13 10:11:21', 0);
INSERT INTO `sys_module_function_action` VALUES (36, 1, 12, '启用/停用枚举', 'M01F05A04', 'PATCH', '/api/v1/system/enum/state/{enumId}/{state}', 1, 1, '2023-02-13 10:12:02', 1, '2023-02-13 10:12:33', 0);
INSERT INTO `sys_module_function_action` VALUES (37, 1, 12, '删除枚举', 'M01F05A05', 'DELETE', '/api/v1/system/enum/{enumId}', 1, 1, '2023-02-13 10:13:03', 1, '2023-02-13 10:13:03', 0);
INSERT INTO `sys_module_function_action` VALUES (38, 1, 12, '查看枚举项列表', 'M01F05A06', 'GET', '/api/v1/system/enum/item', 1, 1, '2023-02-13 10:13:36', 1, '2023-02-13 10:13:36', 0);
INSERT INTO `sys_module_function_action` VALUES (39, 1, 12, '新增枚举项', 'M01F05A07', 'POST', '/api/v1/system/enum/item', 1, 1, '2023-02-13 10:13:50', 1, '2023-02-13 10:13:50', 0);
INSERT INTO `sys_module_function_action` VALUES (40, 1, 12, '修改枚举项', 'M01F05A08', 'PUT', '/api/v1/system/enum/item', 1, 1, '2023-02-13 10:14:00', 1, '2023-02-13 10:14:00', 0);
INSERT INTO `sys_module_function_action` VALUES (41, 1, 12, '启用/停用枚举项', 'M01F05A09', 'PATCH', '/api/v1/system/enum/item/state/{itemId}/{state}', 1, 1, '2023-02-13 10:14:27', 1, '2023-02-13 10:14:27', 0);
INSERT INTO `sys_module_function_action` VALUES (42, 1, 12, '删除枚举项', 'M01F05A10', 'DELETE', '/api/v1/system/enum/item/{itemId}', 1, 1, '2023-02-13 10:14:47', 1, '2023-02-13 10:14:47', 0);
INSERT INTO `sys_module_function_action` VALUES (43, 1, 1, '测试操作1', 'M01F01A20', 'POST', '1231231', 0, NULL, '2023-08-17 17:16:30', NULL, '2023-08-17 17:16:50', 1692263811);

-- ----------------------------
-- Table structure for sys_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation_log`;
CREATE TABLE `sys_operation_log`  (
  `log_id` bigint NOT NULL AUTO_INCREMENT,
  `log_type` tinyint NULL DEFAULT 1 COMMENT '日志类型。1：操作日志；2：登录日志',
  `operation` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '日志名称',
  `time` datetime NULL DEFAULT NULL COMMENT '执行时间',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类名',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '方法名',
  `ip` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求方法',
  `request_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求地址',
  `success` tinyint NULL DEFAULT 0 COMMENT '是否成功。0：否；1：是',
  `creator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `idx_created_time`(`created_time` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2839 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_operation_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_operation_log_detail
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation_log_detail`;
CREATE TABLE `sys_operation_log_detail`  (
  `detail_id` bigint NOT NULL AUTO_INCREMENT,
  `log_id` bigint NOT NULL COMMENT '日志ID',
  `params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '请求内容',
  `response` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '响应内容',
  `fail_reason` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '失败原因（异常堆栈、跟踪日志等）',
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '响应消息',
  `creator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`detail_id`) USING BTREE,
  INDEX `idx_log_id`(`log_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2640 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_operation_log_detail
-- ----------------------------

-- ----------------------------
-- Table structure for sys_organization
-- ----------------------------
DROP TABLE IF EXISTS `sys_organization`;
CREATE TABLE `sys_organization`  (
  `org_id` int NOT NULL AUTO_INCREMENT,
  `org_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组织名称',
  `org_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组织编码',
  `org_level` tinyint NULL DEFAULT 0 COMMENT '0：集团/公司；1：公司/子公司；2：部门',
  `parent_id` int NULL DEFAULT 0 COMMENT '父级组织',
  `org_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组织路径',
  `state` tinyint NULL DEFAULT 0 COMMENT '状态。0：停用；1：启用',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`org_id`) USING BTREE,
  UNIQUE INDEX `org_code`(`org_code` ASC, `deleted` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '组织架构' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_organization
-- ----------------------------
INSERT INTO `sys_organization` VALUES (1, '方圆百里', '01', 0, 0, '01', 1, 1, '2022-08-14 10:56:53', 1, '2022-08-15 10:54:22', 0);
INSERT INTO `sys_organization` VALUES (3, '方圆10里', '0101', 1, 1, '01|0101', 1, 1, '2022-08-15 10:24:57', 1, '2022-08-15 10:24:57', 0);
INSERT INTO `sys_organization` VALUES (7, '海底两万里', '02', 0, 0, '02', 1, 1, '2022-08-15 11:01:38', 1, '2022-08-15 11:01:38', 0);
INSERT INTO `sys_organization` VALUES (8, '测试组织011', '03', 0, 0, '03', 1, NULL, '2023-08-19 16:49:15', NULL, '2023-08-21 09:51:06', 0);
INSERT INTO `sys_organization` VALUES (10, '测试子组织001', '0301', 1, 8, '03|0301', 1, NULL, '2023-08-19 17:14:04', NULL, '2023-08-21 09:51:16', 0);
INSERT INTO `sys_organization` VALUES (14, '123123123', '04', 0, 0, '04', 1, NULL, '2023-08-19 17:40:14', NULL, '2023-08-21 15:02:11', 0);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色名称',
  `state` tinyint NULL DEFAULT 0 COMMENT '状态。0：停用；1：启用',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色说明',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `role_name`(`role_name` ASC, `deleted` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 1, '系统管理员2', 1, '2022-08-10 15:03:09', 1, '2022-08-10 15:04:04', 1660115044);
INSERT INTO `sys_role` VALUES (2, '系统管理员11', 1, '系统管理员22', 1, '2022-08-10 15:05:06', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role` VALUES (3, '管理员', 1, '管理员', 1, '2022-08-11 14:34:29', 1, '2022-08-11 14:36:56', 1660199816);
INSERT INTO `sys_role` VALUES (6, '管理员', 1, '爱死飞', 1, '2022-08-11 14:39:05', 1, '2022-08-11 14:46:04', 1660200364);
INSERT INTO `sys_role` VALUES (7, '管理员', 1, '阿萨德飞管理员', 1, '2022-08-11 14:49:26', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role` VALUES (8, '测试角色01', 1, '是的发爱上对方爱上对方', NULL, '2023-08-21 14:12:33', NULL, '2023-08-22 16:26:36', 0);

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `rp_id` int NOT NULL AUTO_INCREMENT,
  `role_id` int NOT NULL COMMENT '角色ID',
  `module_id` int NOT NULL COMMENT '模块ID',
  `function_id` int NOT NULL COMMENT '功能ID',
  `action_id` int NOT NULL COMMENT '操作ID',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`rp_id`) USING BTREE,
  INDEX `idx_role_module_function_action_id`(`role_id` ASC, `module_id` ASC, `function_id` ASC, `action_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 126 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户角色权限' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (13, 2, 1, 1, 1, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (14, 2, 1, 1, 2, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (15, 2, 1, 1, 4, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (16, 2, 1, 1, 6, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (17, 2, 1, 4, 7, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (18, 2, 1, 5, 8, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (19, 2, 2, 7, 9, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (20, 2, 2, 7, 10, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (21, 2, 2, 8, 11, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (22, 2, 2, 8, 12, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (23, 2, 1, 6, 13, 1, '2022-08-11 15:21:53', 1, '2022-08-11 15:21:53', 0);
INSERT INTO `sys_role_permission` VALUES (31, 7, 1, 1, 1, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (32, 7, 1, 1, 2, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (33, 7, 1, 1, 3, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (34, 7, 1, 1, 4, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (35, 7, 1, 1, 5, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (36, 7, 1, 1, 6, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (37, 7, 2, 7, 9, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (38, 7, 1, 2, 14, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (39, 7, 1, 1, 16, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (40, 7, 1, 1, 17, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (41, 7, 1, 1, 18, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (42, 7, 1, 1, 19, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (43, 7, 1, 1, 20, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (44, 7, 1, 1, 21, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (45, 7, 1, 1, 22, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (46, 7, 1, 1, 23, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (47, 7, 1, 1, 24, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (48, 7, 1, 1, 25, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (49, 7, 1, 1, 26, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (50, 7, 1, 1, 27, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (51, 7, 1, 1, 28, 1, '2022-08-25 22:42:16', 1, '2022-08-25 22:42:16', 0);
INSERT INTO `sys_role_permission` VALUES (122, 8, 2, 7, 9, NULL, '2023-08-22 16:26:36', NULL, '2023-08-22 16:26:36', 0);
INSERT INTO `sys_role_permission` VALUES (123, 8, 2, 7, 10, NULL, '2023-08-22 16:26:36', NULL, '2023-08-22 16:26:36', 0);
INSERT INTO `sys_role_permission` VALUES (124, 8, 2, 8, 11, NULL, '2023-08-22 16:26:36', NULL, '2023-08-22 16:26:36', 0);
INSERT INTO `sys_role_permission` VALUES (125, 8, 2, 8, 12, NULL, '2023-08-22 16:26:36', NULL, '2023-08-22 16:26:36', 0);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `uid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户UUID',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户登录名',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户登录密码',
  `full_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户手机号',
  `mail` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户邮箱',
  `user_type` tinyint NULL DEFAULT 1 COMMENT '用户类型。99：超级管理员；1：普通用户；2：系统管理员；',
  `state` tinyint NULL DEFAULT 0 COMMENT '状态。0：停用；1：启用',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  `current_org_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '当前激活的组织编码',
  `data_scope` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'TOP_ORG' COMMENT '数据权限类型',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `uid`(`uid` ASC) USING BTREE,
  UNIQUE INDEX `username`(`username` ASC, `deleted` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户，保存用户登录信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, '123123root', 'root', '4c9e21b3097684249eaac791d4a859a3', '系统管理员', '13655548952', NULL, 99, 1, NULL, '2022-08-09 15:27:02', 1, '2023-08-24 15:39:00', 0, '0301', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (2, '5a7e1371a3f74fffb9e99447a15fa34a', 'admin', 'a36205a3968ea2be7a912eb753b403ca', '管理员', '13400908765', NULL, 2, 1, 1, '2022-08-17 11:20:31', 1, '2022-08-17 13:32:23', 1660714343, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (3, '037e62496d0c4f579549f2e04eb65846', 'waq', 'd9a17ccbebf14ab42a81e92f8ecea453', '王大锤2', '18078786434', '123@123.abc', 1, 1, 1, '2022-08-17 11:24:13', 1, '2022-08-17 11:31:03', 1660707063, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (4, 'a26dd4165fd14a09bc3ffa275e3e23e6', 'admin', 'c2104c99a5f3a1e21e5801becc02bd97', '管理员', '18972722323', NULL, 2, 1, 1, '2022-08-17 13:32:55', 1, '2022-08-18 11:24:12', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (5, 'be86b6da6f8042d695c72ea745a0a768', 'test', 'c1b94da3fa13b842b8de2bb0ce60558e', '测试用户011', '12390908989', NULL, 1, 1, 1, '2022-08-17 16:52:21', 1, '2023-08-23 16:16:38', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (6, 'be86b6da6f8042d695c72ea745a0a111', 'test1', 'c1b94da3fa13b842b8de2bb0ce60558e', '测试用户01', '12390908989', NULL, 1, 1, 1, '2022-08-17 16:52:21', 1, '2022-08-17 16:52:32', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (7, 'be86b6da6f8042d695c72ea745a0a112', 'test2', 'c1b94da3fa13b842b8de2bb0ce60558e', '测试用户01', '12390908989', NULL, 1, 1, 1, '2022-08-17 16:52:21', 1, '2022-08-17 16:52:32', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (8, 'be86b6da6f8042d695c72ea745a0a113', 'test3', 'c1b94da3fa13b842b8de2bb0ce60558e', '测试用户01', '12390908989', NULL, 1, 1, 1, '2022-08-17 16:52:21', 1, '2022-08-17 16:52:32', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (9, 'be86b6da6f8042d695c72ea745a0a114', 'test4', 'c1b94da3fa13b842b8de2bb0ce60558e', '测试用户01', '12390908989', NULL, 1, 1, 1, '2022-08-17 16:52:21', 1, '2022-08-17 16:52:32', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (10, 'be86b6da6f8042d695c72ea745a0a115', 'test5', 'c1b94da3fa13b842b8de2bb0ce60558e', '测试用户01', '12390908989', NULL, 1, 1, 1, '2022-08-17 16:52:21', 1, '2022-08-17 16:52:32', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (11, 'be86b6da6f8042d695c72ea745a0a116', 'test6', 'c1b94da3fa13b842b8de2bb0ce60558e', '测试用户01', '12390908989', NULL, 1, 1, 1, '2022-08-17 16:52:21', 1, '2022-08-17 16:52:32', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (12, 'be86b6da6f8042d695c72ea745a0a117', 'test7', 'c62ba8312ce62012d226bf804ee72674', '测试用户01', '12390908989', NULL, 1, 1, 1, '2022-08-17 16:52:21', 1, '2023-08-19 13:45:13', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (13, 'be86b6da6f8042d695c72ea745a0a118', 'test8', 'c1b94da3fa13b842b8de2bb0ce60558e', '测试用户01', '12390908989', NULL, 1, 1, 1, '2022-08-17 16:52:21', 1, '2022-08-17 16:52:32', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (14, 'be86b6da6f8042d695c72ea745a0a119', 'test9', 'c1b94da3fa13b842b8de2bb0ce60558e', '测试用户01', '12390908989', NULL, 1, 1, 1, '2022-08-17 16:52:21', 1, '2022-08-17 16:52:32', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (15, 'f6ae7c60b13a426ba82be254be550f3b', 'test_role1', 'dda71c2b2313393c9960be9b34abd415', '测试', '13699545454', NULL, 1, 1, 1, '2022-08-22 16:32:53', 15, '2022-08-22 16:33:53', 0, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (16, 'c5a24affe7e04498a10d1647ac7bf26c', 'test999', 'e4c51123f8f92ec3181170482ee5f1c8', '测试用999', '1892222222', '23@123.com', 1, 1, NULL, '2023-08-19 16:10:12', NULL, '2023-08-19 16:11:20', 1692432680, '', 'TOP_ORG');
INSERT INTO `sys_user` VALUES (17, 'fff645adabcc471f98c63ba023304f69', 'liliang', '9a406a42377b763f3aaf83f310639b77', '力量', '18566564544', '', 1, 1, NULL, '2023-08-22 13:20:09', NULL, '2023-08-22 13:26:38', 0, '', 'TOP_ORG');

-- ----------------------------
-- Table structure for sys_user_account
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_account`;
CREATE TABLE `sys_user_account`  (
  `salt_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL COMMENT '用户ID',
  `salt` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '盐',
  `salt_type` tinyint NULL DEFAULT 0 COMMENT '盐类型。0：密码盐',
  `password_expire_time` datetime NULL DEFAULT NULL COMMENT '密码失效时间',
  `password_need_reset` tinyint NULL DEFAULT 0 COMMENT '密码是否需要重新设置。0：否；1：是',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`salt_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户账号' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_account
-- ----------------------------
INSERT INTO `sys_user_account` VALUES (1, 1, '05af24bfbb3149aa82ee23d6fa5cd492', 0, '2024-02-20 15:38:59', 0, NULL, NULL, 1, '2023-08-24 15:38:59', 0);
INSERT INTO `sys_user_account` VALUES (2, 15, '25b566db64ce42038c16595f6e697cea', 0, '2023-02-18 16:33:53', 0, 1, '2022-08-22 16:32:53', 15, '2022-08-22 16:33:53', 0);
INSERT INTO `sys_user_account` VALUES (3, 12, '14e3de3e9ddf4e67bfcbc1247bae964e', 0, '2024-02-15 13:45:13', 1, NULL, '2023-08-19 13:45:13', NULL, '2023-08-19 13:45:13', 0);
INSERT INTO `sys_user_account` VALUES (4, 16, 'd0b8c24d08564bfd9397ca81da957713', 0, '2024-02-15 16:10:12', 1, NULL, '2023-08-19 16:10:12', NULL, '2023-08-19 16:10:12', 0);
INSERT INTO `sys_user_account` VALUES (5, 17, '530b957d563548078b671a08267e187a', 0, '2024-02-18 13:20:09', 1, NULL, '2023-08-22 13:20:09', NULL, '2023-08-22 13:20:09', 0);

-- ----------------------------
-- Table structure for sys_user_account_third
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_account_third`;
CREATE TABLE `sys_user_account_third`  (
  `third_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `uid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户UUID',
  `third_plant` tinyint NULL DEFAULT 0 COMMENT '第三方平台。0：微信',
  `third_user_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '第三方用户ID',
  `third_union_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '第三方平台用户ID',
  `nick_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '第三方平台的用户名（昵称）',
  `head_picture` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '第三方平台用户头像',
  `gender` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '未知' COMMENT '性别。男、女、未知',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`third_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id` ASC) USING BTREE,
  INDEX `idx_third_user_id`(`third_user_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户第三方平台账户信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_account_third
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_organization
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_organization`;
CREATE TABLE `sys_user_organization`  (
  `user_org_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL COMMENT '用户ID',
  `org_id` int NOT NULL COMMENT '组织ID',
  `user_org_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '组织编号',
  `org_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组织路径',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  `org_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '组织名称',
  PRIMARY KEY (`user_org_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id` ASC) USING BTREE,
  INDEX `idx_org_id`(`org_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户组织' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_organization
-- ----------------------------
INSERT INTO `sys_user_organization` VALUES (1, 1, 3, '0101', '01|0101', 1, '2022-08-15 13:07:58', 1, '2022-08-15 13:07:58', 1660540424, '方圆10里');
INSERT INTO `sys_user_organization` VALUES (2, 1, 1, '01', '01', 1, '2022-08-15 13:10:30', 1, '2022-08-15 13:10:30', 1660540403, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (3, 1, 7, '02', '02', 1, '2022-08-15 13:11:23', 1, '2022-08-15 13:11:23', 1660540389, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (4, 1, 7, '02', '02', 1, '2022-08-15 13:13:15', 1, '2022-08-15 13:13:15', 1692843685, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (5, 1, 3, '0101', '01|0101', 1, '2022-08-15 13:41:50', 1, '2022-08-15 13:41:50', 1660542123, '方圆10里');
INSERT INTO `sys_user_organization` VALUES (6, 1, 1, '01', '01', 1, '2022-08-15 13:41:56', 1, '2022-08-15 13:41:56', 1660542123, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (7, 1, 1, '01', '01', 1, '2022-08-15 16:00:05', 1, '2022-08-15 16:00:05', 1692586455, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (8, 3, 1, '01', '01', 1, '2022-08-17 11:24:13', 1, '2022-08-17 11:24:13', 1660706679, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (9, 3, 7, '02', '02', 1, '2022-08-17 11:24:13', 1, '2022-08-17 11:24:13', 1660706679, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (10, 3, 7, '02', '02', 1, '2022-08-17 11:24:40', 1, '2022-08-17 11:24:40', 1660706691, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (11, 3, 7, '02', '02', 1, '2022-08-17 11:24:51', 1, '2022-08-17 11:24:51', 0, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (12, 4, 7, '02', '02', 1, '2022-08-17 13:32:55', 1, '2022-08-17 13:32:55', 1660792972, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (13, 5, 3, '0101', '01|0101', 1, '2022-08-17 16:52:21', 1, '2022-08-17 16:52:21', 1660726352, '方圆10里');
INSERT INTO `sys_user_organization` VALUES (14, 5, 3, '0101', '01|0101', 1, '2022-08-17 16:52:32', 1, '2022-08-17 16:52:32', 1692586455, '方圆10里');
INSERT INTO `sys_user_organization` VALUES (15, 5, 7, '02', '02', 1, '2022-08-17 16:52:32', 1, '2022-08-17 16:52:32', 1692586228, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (16, 4, 7, '02', '02', 1, '2022-08-18 11:22:52', 1, '2022-08-18 11:22:52', 1660793052, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (17, 4, 7, '02', '02', 1, '2022-08-18 11:24:13', 1, '2022-08-18 11:24:13', 0, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (18, 15, 1, '01', '01', 1, '2022-08-22 16:32:53', 1, '2022-08-22 16:32:53', 1692586445, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (19, 1, 1, '01', '01', NULL, '2023-08-21 11:29:09', NULL, '2023-08-21 11:29:09', 1692843685, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (20, 4, 1, '01', '01', NULL, '2023-08-21 11:29:09', NULL, '2023-08-21 11:29:09', 0, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (21, 4, 3, '0101', '01|0101', NULL, '2023-08-21 11:29:23', NULL, '2023-08-21 11:29:23', 1692588590, '方圆10里');
INSERT INTO `sys_user_organization` VALUES (22, 5, 3, '0101', '01|0101', NULL, '2023-08-21 11:29:23', NULL, '2023-08-21 11:29:23', 1692588602, '方圆10里');
INSERT INTO `sys_user_organization` VALUES (23, 6, 3, '0101', '01|0101', NULL, '2023-08-21 11:29:23', NULL, '2023-08-21 11:29:23', 1692588602, '方圆10里');
INSERT INTO `sys_user_organization` VALUES (24, 5, 7, '02', '02', NULL, '2023-08-21 11:43:35', NULL, '2023-08-21 11:43:35', 1692589642, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (25, 5, 7, '02', '02', NULL, '2023-08-21 11:47:23', NULL, '2023-08-21 11:47:23', 1692778430, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (26, 5, 8, '03', '03', NULL, '2023-08-21 11:47:23', NULL, '2023-08-21 11:47:23', 1692778430, '测试组织011');
INSERT INTO `sys_user_organization` VALUES (27, 5, 10, '0301', '03|0301', NULL, '2023-08-21 11:47:23', NULL, '2023-08-21 11:47:23', 1692778430, '测试子组织001');
INSERT INTO `sys_user_organization` VALUES (28, 7, 1, '01', '01', NULL, '2023-08-21 14:45:49', NULL, '2023-08-21 14:45:49', 1692600353, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (29, 4, 14, '04', '04', NULL, '2023-08-21 14:48:26', NULL, '2023-08-21 14:48:26', 1692600511, '123123123');
INSERT INTO `sys_user_organization` VALUES (30, 1, 14, '04', '04', NULL, '2023-08-21 14:48:26', NULL, '2023-08-21 14:48:26', 1692600515, '123123123');
INSERT INTO `sys_user_organization` VALUES (31, 5, 14, '04', '04', NULL, '2023-08-21 14:48:26', NULL, '2023-08-21 14:48:26', 1692600515, '123123123');
INSERT INTO `sys_user_organization` VALUES (32, 6, 14, '04', '04', NULL, '2023-08-21 14:48:26', NULL, '2023-08-21 14:48:26', 1692600515, '123123123');
INSERT INTO `sys_user_organization` VALUES (33, 7, 14, '04', '04', NULL, '2023-08-21 14:48:26', NULL, '2023-08-21 14:48:26', 0, '123123123');
INSERT INTO `sys_user_organization` VALUES (34, 8, 14, '04', '04', NULL, '2023-08-21 14:48:26', NULL, '2023-08-21 14:48:26', 0, '123123123');
INSERT INTO `sys_user_organization` VALUES (35, 9, 14, '04', '04', NULL, '2023-08-21 14:48:26', NULL, '2023-08-21 14:48:26', 0, '123123123');
INSERT INTO `sys_user_organization` VALUES (36, 10, 14, '04', '04', NULL, '2023-08-21 14:48:26', NULL, '2023-08-21 14:48:26', 0, '123123123');
INSERT INTO `sys_user_organization` VALUES (37, 11, 14, '04', '04', NULL, '2023-08-21 14:48:26', NULL, '2023-08-21 14:48:26', 0, '123123123');
INSERT INTO `sys_user_organization` VALUES (38, 12, 14, '04', '04', NULL, '2023-08-21 14:48:26', NULL, '2023-08-21 14:48:26', 0, '123123123');
INSERT INTO `sys_user_organization` VALUES (39, 17, 1, '01', '01', NULL, '2023-08-22 13:20:09', NULL, '2023-08-22 13:20:09', 1692681917, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (40, 17, 1, '01', '01', NULL, '2023-08-22 13:25:18', NULL, '2023-08-22 13:25:18', 1692681985, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (41, 17, 1, '01', '01', NULL, '2023-08-22 13:26:26', NULL, '2023-08-22 13:26:26', 1692681997, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (42, 17, 1, '01', '01', NULL, '2023-08-22 13:26:38', NULL, '2023-08-22 13:26:38', 0, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (43, 5, 7, '02', '02', 1, '2023-08-23 16:13:51', 1, '2023-08-23 16:13:51', 1692778597, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (44, 5, 8, '03', '03', 1, '2023-08-23 16:13:51', 1, '2023-08-23 16:13:51', 1692778597, '测试组织011');
INSERT INTO `sys_user_organization` VALUES (45, 5, 10, '0301', '03|0301', 1, '2023-08-23 16:13:51', 1, '2023-08-23 16:13:51', 1692778597, '测试子组织001');
INSERT INTO `sys_user_organization` VALUES (46, 5, 7, '02', '02', 1, '2023-08-23 16:16:38', 1, '2023-08-23 16:16:38', 0, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (47, 5, 8, '03', '03', 1, '2023-08-23 16:16:38', 1, '2023-08-23 16:16:38', 0, '测试组织011');
INSERT INTO `sys_user_organization` VALUES (48, 5, 10, '0301', '03|0301', 1, '2023-08-23 16:16:38', 1, '2023-08-23 16:16:38', 0, '测试子组织001');
INSERT INTO `sys_user_organization` VALUES (49, 1, 1, '01', '01', 1, '2023-08-24 10:21:26', 1, '2023-08-24 10:21:26', 0, '方圆百里');
INSERT INTO `sys_user_organization` VALUES (50, 1, 7, '02', '02', 1, '2023-08-24 10:21:26', 1, '2023-08-24 10:21:26', 0, '海底两万里');
INSERT INTO `sys_user_organization` VALUES (51, 1, 10, '0301', '03|0301', 1, '2023-08-24 10:21:26', 1, '2023-08-24 10:21:26', 0, '测试子组织001');

-- ----------------------------
-- Table structure for sys_user_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_permission`;
CREATE TABLE `sys_user_permission`  (
  `up_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL COMMENT '用户ID',
  `module_id` int NOT NULL COMMENT '模块ID',
  `function_id` int NOT NULL COMMENT '功能ID',
  `action_id` int NOT NULL COMMENT '操作ID',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`up_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id` ASC) USING BTREE,
  INDEX `idx_module_id`(`module_id` ASC) USING BTREE,
  INDEX `idx_function_id`(`function_id` ASC) USING BTREE,
  INDEX `idx_action_id`(`action_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户权限' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_permission
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_role_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL COMMENT '用户ID',
  `role_id` int NOT NULL COMMENT '角色ID',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`user_role_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id` ASC) USING BTREE,
  INDEX `idx_role_id`(`role_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 2, NULL, NULL, NULL, NULL, 1660363909);
INSERT INTO `sys_user_role` VALUES (2, 1, 7, NULL, NULL, NULL, NULL, 1660368370);
INSERT INTO `sys_user_role` VALUES (3, 1, 7, NULL, NULL, NULL, NULL, 1692843685);
INSERT INTO `sys_user_role` VALUES (4, 1, 2, NULL, NULL, NULL, NULL, 1692843685);
INSERT INTO `sys_user_role` VALUES (5, 3, 7, NULL, NULL, NULL, NULL, 1660706679);
INSERT INTO `sys_user_role` VALUES (6, 3, 7, NULL, NULL, NULL, NULL, 1660706691);
INSERT INTO `sys_user_role` VALUES (7, 3, 7, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_role` VALUES (8, 4, 7, NULL, NULL, NULL, NULL, 1660792972);
INSERT INTO `sys_user_role` VALUES (9, 5, 7, NULL, NULL, NULL, NULL, 1660726352);
INSERT INTO `sys_user_role` VALUES (10, 5, 7, NULL, NULL, NULL, NULL, 1692589344);
INSERT INTO `sys_user_role` VALUES (11, 4, 7, NULL, NULL, NULL, NULL, 1660793052);
INSERT INTO `sys_user_role` VALUES (12, 4, 7, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_role` VALUES (13, 15, 7, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_role` VALUES (14, 4, 8, NULL, NULL, NULL, NULL, 1692600486);
INSERT INTO `sys_user_role` VALUES (15, 1, 8, NULL, NULL, NULL, NULL, 1692600486);
INSERT INTO `sys_user_role` VALUES (16, 5, 8, NULL, NULL, NULL, NULL, 1692682160);
INSERT INTO `sys_user_role` VALUES (17, 6, 8, NULL, NULL, NULL, NULL, 1692682160);
INSERT INTO `sys_user_role` VALUES (18, 7, 8, NULL, NULL, NULL, NULL, 1692682160);
INSERT INTO `sys_user_role` VALUES (19, 8, 8, NULL, NULL, NULL, NULL, 1692682160);
INSERT INTO `sys_user_role` VALUES (20, 9, 8, NULL, NULL, NULL, NULL, 1692682160);
INSERT INTO `sys_user_role` VALUES (21, 10, 8, NULL, NULL, NULL, NULL, 1692682160);
INSERT INTO `sys_user_role` VALUES (22, 11, 8, NULL, NULL, NULL, NULL, 1692600471);
INSERT INTO `sys_user_role` VALUES (23, 12, 8, NULL, NULL, NULL, NULL, 1692600471);
INSERT INTO `sys_user_role` VALUES (24, 1, 8, NULL, NULL, NULL, NULL, 1692682160);
INSERT INTO `sys_user_role` VALUES (25, 4, 8, NULL, NULL, NULL, NULL, 1692682160);
INSERT INTO `sys_user_role` VALUES (26, 11, 8, NULL, NULL, NULL, NULL, 1692682160);
INSERT INTO `sys_user_role` VALUES (27, 12, 8, NULL, NULL, NULL, NULL, 1692682160);
INSERT INTO `sys_user_role` VALUES (28, 13, 8, NULL, NULL, NULL, NULL, 1692682165);
INSERT INTO `sys_user_role` VALUES (29, 14, 8, NULL, NULL, NULL, NULL, 1692682165);
INSERT INTO `sys_user_role` VALUES (30, 15, 8, NULL, NULL, NULL, NULL, 1692682165);
INSERT INTO `sys_user_role` VALUES (31, 17, 7, NULL, NULL, NULL, NULL, 1692681997);
INSERT INTO `sys_user_role` VALUES (32, 17, 8, NULL, NULL, NULL, NULL, 1692681997);
INSERT INTO `sys_user_role` VALUES (33, 17, 8, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_role` VALUES (34, 1, 2, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_role` VALUES (35, 1, 7, NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `token_id` int NOT NULL AUTO_INCREMENT,
  `uid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '用户ID',
  `access_token` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '调用受限资源的凭证',
  `access_token_expires_in` int NULL DEFAULT 0 COMMENT 'Access Token有效时长',
  `access_token_expires_time` bigint NULL DEFAULT 0 COMMENT 'Access Token失效时间戳',
  `access_token_create_time` bigint NULL DEFAULT 0 COMMENT 'Access Token创建时间戳',
  `refresh_code` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '获取Refresh Token的code',
  `refresh_token` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用于刷新Access Token的凭证',
  `refresh_token_expires_in` int NULL DEFAULT 0 COMMENT 'Refresh Token有效时长',
  `refresh_token_expires_time` bigint NULL DEFAULT 0 COMMENT 'Refresh Token失效时间戳',
  `refresh_token_create_time` bigint NULL DEFAULT 0 COMMENT 'Refresh Token创建时间戳',
  `created_by` int NULL DEFAULT NULL COMMENT '创建人ID',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int NULL DEFAULT NULL COMMENT '修改人ID',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int NULL DEFAULT 0 COMMENT '删除状态。等于0：否；大于0：是',
  PRIMARY KEY (`token_id`) USING BTREE,
  UNIQUE INDEX `uni_uid`(`uid` ASC, `deleted` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户访问凭证' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
