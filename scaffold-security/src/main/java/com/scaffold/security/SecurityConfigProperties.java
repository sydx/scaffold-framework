package com.scaffold.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/8 14:22:52
 * @描述: Spring Security配置文件对象
 */
@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "security.config")
public class SecurityConfigProperties {
    /**
     * 认证配置
     */
    private Authentication authentication = new Authentication();
    /**
     * 授权配置
     */
    private Authorization authorization = new Authorization();

    public Jwt getJwtConfig() {
        return getAuthentication().getJwt();
    }
    public Login getLoginConfig() {
        return getAuthentication().getLogin();
    }

    public Token getTokenConfig() {
        return getAuthentication().getToken();
    }

    /**
     * 认证配置
     */
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    public static class Authentication {
        /**
         * 无需身份认证的地址
         */
        private List<String> ignoredUrls;

        /**
         * 登录配置
         */
        private Login login = new Login();
        /**
         * jwt配置
         */
        private Jwt jwt = new Jwt();

        /**
         * Token配置
         */
        private Token token = new Token();


    }

    /**
     * 登录配置
     */
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    public static class Login {
        /**
         * 登录页地址
         */
        private String loginPage;
        /**
         * 登录接口
         */
        private String loginApi;
        /**
         * 退出登录接口
         */
        private String logoutApi;
        /**
         * 登录用户名参数名称
         */
        private String usernameParameter = "username";
        /**
         * 登录密码参数名称
         */
        private String passwordParameter = "password";
        /**
         * 登录是否需要验证码
         */
        private Boolean enableLoginCaptcha = true;
        /**
         * 登录验证码参数名称
         */
        private String captchaParameter = "captcha";
        /**
         * 是否启用登录扩展属性
         */
        private Boolean enableExtendParameter = false;
        /**
         * 登录扩展属性参数名
         */
        private String extendParameter = "extend";
    }
    /**
     * 授权配置
     */
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    public static class Authorization {
        /**
         * 开启动态授权。true:开启；false：关闭
         */
        private Boolean enableDynamic = false;
        /**
         * 无需权限验证的地址
         */
        private List<String> ignoredUrls;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    public static class Jwt {
        /**
         * 加密秘钥
         */
        private String secretKey;
        /**
         * 有效期，单位：天。默认为7天
         */
        private Integer validityPeriod = 7;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    public static class Token {

        /**
         * Access Token 加密秘钥。128, 192或256位秘钥通过base64编码后的字符串
         */
        private String accessTokenSecretKey;
        /**
         * Access Token 有效期，单位：秒。默认为7200秒
         */
        private Long accessTokenExpiresIn = 7200L;

        /**
         * Refresh Token 加密秘钥。128, 192或256位秘钥通过base64编码后的字符串
         */
        private String refreshTokenSecretKey;
        /**
         * Refresh Token 有效期，单位：秒。默认为14400秒。
         * 一般为Access Token有效期的两倍
         */
        private Long refreshTokenExpiresIn = 14400L;

    }


}
