package com.scaffold.security.authorization;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;

import java.util.function.Supplier;

/**
 * 动态权限决策
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/22 9:49:06
 */
public interface AccessDecisionHandler {

    boolean decide(Supplier<Authentication> authentication, HttpServletRequest object);
}
