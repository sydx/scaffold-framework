package com.scaffold.security.authorization;

import com.scaffold.security.SecurityConfigProperties;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.AuthorizationFilter;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import java.io.IOException;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/8 14:06:33
 * @描述: 动态权限过滤器
 */
public class DynamicSecurityFilter extends AuthorizationFilter {

    private final SecurityConfigProperties securityConfigProperties;

    /**
     * Creates an instance.
     *
     * @param authorizationManager the {@link AuthorizationManager} to use
     */
    public DynamicSecurityFilter(AuthorizationManager<HttpServletRequest> authorizationManager,
                                 SecurityConfigProperties securityConfigProperties) {
        super(authorizationManager);
        this.securityConfigProperties = securityConfigProperties;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        FilterInvocation fi = new FilterInvocation(servletRequest, servletResponse, filterChain);
        if (securityConfigProperties.getAuthorization().getEnableDynamic()) {
            //开启动态权限验证，此处会调用AccessDecisionManager中的check方法进行鉴权操作
            super.doFilter(servletRequest, servletResponse, filterChain);
        } else {
            //未开启动态权限验证，直接放行
            fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
        }


    }
}
