package com.scaffold.security.authorization;

import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.core.Authentication;

import java.util.function.Supplier;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/8 10:56:55
 * @描述: 授权访问决策。确定当前用户是否拥有指定的权限
 */
public class DynamicAccessDecisionManager implements AuthorizationManager<HttpServletRequest> {
    private static final Logger logger = LoggerFactory.getLogger(DynamicAccessDecisionManager.class);

    private final AccessDecisionHandler accessDecisionHandler;

    public DynamicAccessDecisionManager(AccessDecisionHandler accessDecisionHandler) {
        this.accessDecisionHandler = accessDecisionHandler;
    }

    /**
     * Determines if access should be granted for a specific authentication and object.
     *
     * @param authentication the {@link Supplier} of the {@link Authentication} to check
     * @param object         the {@link T} object to check
     * @throws AccessDeniedException if access is not granted
     */
    @Override
    public void verify(Supplier<Authentication> authentication, HttpServletRequest object) {
        if (logger.isDebugEnabled()) {
            logger.debug("授权访问决策verify");
        }
        AuthorizationManager.super.verify(authentication, object);
    }

    /**
     * Determines if access is granted for a specific authentication and object.
     *
     * @param authentication the {@link Supplier} of the {@link Authentication} to check
     * @param object         the {@link T} object to check
     * @return an {@link AuthorizationDecision} or null if no decision could be made
     */
    @Override
    public AuthorizationDecision check(Supplier<Authentication> authentication, HttpServletRequest request) {
        if (logger.isDebugEnabled()) {
            logger.debug("授权访问决策check");
        }
        return new AuthorizationDecision(accessDecisionHandler.decide(authentication, request));
    }

}
