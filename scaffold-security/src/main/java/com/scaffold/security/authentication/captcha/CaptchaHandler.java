package com.scaffold.security.authentication.captcha;

import java.awt.*;
import java.io.IOException;

/**
 * 验证码处理接口
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/7/24 9:55:13
 */
public interface CaptchaHandler {

    /**
     * 创建验证码
     * @param requestId 请求ID
     * @return boolean
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-07-24 11:20:47
     */
   Object create(String requestId) throws IOException, FontFormatException;

    /**
     * 校验验证码是否正确
     * @param requestId 请求ID
     * @param captcha 验证码
     * @return boolean
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-07-24 11:20:47
     */
   boolean validate(String requestId, String captcha);

   /**
    * 删除验证码
    * @param requestId
    * @return void
    * @exception
    * @version 1.0
    * @author 李亮
    * @time 2023-08-10 15:21:30
    */
   void remove(String requestId);

}
