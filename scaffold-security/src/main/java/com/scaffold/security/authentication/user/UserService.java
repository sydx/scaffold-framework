package com.scaffold.security.authentication.user;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * 用户查询接口
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/6/26 11:36:22
 */
public interface UserService {
    UserDetails loadUserByUsername(String username);
}
