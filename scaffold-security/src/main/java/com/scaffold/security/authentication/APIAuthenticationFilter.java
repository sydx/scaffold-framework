package com.scaffold.security.authentication;

import cn.hutool.core.util.StrUtil;
import com.auth0.jwt.interfaces.Claim;
import com.scaffold.common.exception.TokenException;
import com.scaffold.common.result.R;
import com.scaffold.common.result.RCode;
import com.scaffold.common.util.SpringUtil;
import com.scaffold.security.SecurityConfigProperties;
import com.scaffold.security.authentication.token.TokenManager;
import com.scaffold.security.util.HandlerUtil;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Map;

import static com.scaffold.common.constants.Constants.ACCESS_TOKEN_HEADER;
import static com.scaffold.security.util.AuthenticationUtil.UID_KEY;


/**
 * @创建人: 李亮
 * @创建时间: 2022/6/2 13:28:07
 * @描述: jwt登录验证过滤器
 */
public class APIAuthenticationFilter extends OncePerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(APIAuthenticationFilter.class);

    private final UserDetailsService userDetailsManager;

    private final SecurityConfigProperties securityConfigProperties;

    private PathMatcher pathMatcher = new AntPathMatcher();


    public APIAuthenticationFilter(UserDetailsService userDetailsManager, SecurityConfigProperties securityConfigProperties) {
        this.userDetailsManager = userDetailsManager;
        this.securityConfigProperties = securityConfigProperties;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("接口过滤器！{}", request.getRequestURI());
        }
        try {

            //白名单请求直接放行
            for (String path : securityConfigProperties.getAuthentication().getIgnoredUrls()) {
                if (pathMatcher.match(path, request.getRequestURI())) {
                    filterChain.doFilter(request, response);
                    return;
                }
            }
            //已登录
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (null != authentication && null != authentication.getPrincipal()
                    && authentication.getPrincipal() instanceof UserDetails) {
                filterChain.doFilter(request, response);
                return;
            }

            String token = getAccessToken(request);
            if (StrUtil.isBlank(token)) {
                filterChain.doFilter(request, response);
                return;
            }
            String uid = getUid(token);

            //通过token自动登录
            if (StrUtil.isNotBlank(uid)) {
                UserDetails userDetails = userDetailsManager.loadUserByUsername(uid);
                if (null != userDetails) {
                    UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
                            userDetails, null, userDetails.getAuthorities());
                    authRequest.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authRequest);
                }
            }
            filterChain.doFilter(request, response);
        } catch (TokenException e) {
            logger.error("Token异常！", e);
            HandlerUtil.responseHandler(request, response, R.failure(e));
        } catch (Exception e) {
            logger.error("系统异常！", e);
            HandlerUtil.responseHandler(request, response, R.failure(RCode.EXCEPTION));
        }
    }

    /**
     * @描述 获取access token
     * @参数 request
     * @返回值: java.lang.String
     * @创建人 李亮
     * @创建时间 2022-07-12 09:39:06
     */
    private String getAccessToken(HttpServletRequest request) {
        //从header中获取token
        return request.getHeader(ACCESS_TOKEN_HEADER);
    }

    /**
     * @描述 从token中解析用户信息
     * @参数 token
     * @返回值: java.lang.String
     * @创建人 李亮
     * @创建时间 2022-07-12 09:40:19
     */
    private String getUid(String token) {
        TokenManager tokenManager = SpringUtil.getBean(TokenManager.class);
        //客户端验证
        String uid = null;
        Map<String, Claim> claims = tokenManager.decryptAccessToken(token);
        Claim uidClaim = claims.get(UID_KEY);
        if (null == uidClaim) {
            throw new TokenException();
        }
        uid = uidClaim.asString();
        //服务端验证 TODO 是否需要要根据不同的系统区分处理
        tokenManager.verifyAccessToken(TokenManager.System.MANAGER, uid, token);
        return uid;
    }

}
