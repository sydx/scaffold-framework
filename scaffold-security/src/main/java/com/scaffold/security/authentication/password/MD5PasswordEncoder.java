package com.scaffold.security.authentication.password;

import cn.hutool.core.util.StrUtil;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;

/**
 * @创建人: 李亮
 * @创建时间: 2022/6/2 16:22:03
 * @描述: 密码处理
 */
public class MD5PasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        Assert.notNull(rawPassword, "密码不能为空");
        return PasswordUtil.encodeMD5Password(rawPassword.toString());
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        WrappedPassword wrappedPassword = PasswordUtil.parseWrappedPassword(encodedPassword);
        //加盐处理
        if (StrUtil.isNotBlank(wrappedPassword.getSalt())) {
            rawPassword = encode(rawPassword + wrappedPassword.getSalt());
        } else {
            rawPassword = encode(rawPassword);
        }
        return rawPassword.equals(wrappedPassword.getEncodedPassword());
    }
}
