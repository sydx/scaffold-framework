package com.scaffold.security.authentication.password;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson2.JSON;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/7 11:27:24
 * @描述: 密码工具类
 */
public class PasswordUtil {

    public final static String BASE_CHAR_UP = "ABCDEFGHIJKLMNOPQRSTUVWXYZ!_";

    public final static String BASE_CHAR_NUMBER = RandomUtil.BASE_CHAR_NUMBER + BASE_CHAR_UP;

    public final static int PASSWORD_DEFAULT_LENGTH = 8;

    /**
     * 密码过期时长：毫秒
     * 默认：180天
     */
    public final static long EXPIRE_TIME_MILLI = 60L * 60 * 24 * 180 * 1000;

    /**
     * 将加密密码和盐值通过密码参数传入PasswordEncoder，用于密码比较和加密处理
     *
     * @param encodedPassword
     * @param salt
     * @return
     */
    public static String getWrappedPassword(String encodedPassword, String salt) {
        WrappedPassword wrappedPassword = new WrappedPassword(encodedPassword, salt);
        return JSON.toJSONString(wrappedPassword);
    }

    public static WrappedPassword parseWrappedPassword(String wrappedPassword) {
        return JSON.to(WrappedPassword.class, wrappedPassword);
    }

    /**
     * md5加密密码
     *
     * @param rawPassword 密码明文
     * @return
     */
    public static String encodeMD5Password(String rawPassword) {
        return DigestUtils.md5DigestAsHex(rawPassword.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * @描述 随机密码生成
     * @参数 length
     * @返回值: java.lang.String
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-17 14:04:58
     */
    public static String randomPassword() {
        return RandomUtil.randomString(BASE_CHAR_NUMBER, PASSWORD_DEFAULT_LENGTH);
    }

    /**
     * @描述 随机密码生成
     * @参数 length
     * @返回值: java.lang.String
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-17 14:04:58
     */
    public static String randomPassword(int length) {
        return RandomUtil.randomString(BASE_CHAR_NUMBER, length > 0 ? length : PASSWORD_DEFAULT_LENGTH);
    }
}
