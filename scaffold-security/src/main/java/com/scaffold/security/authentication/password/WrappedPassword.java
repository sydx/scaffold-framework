package com.scaffold.security.authentication.password;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/7 11:39:00
 * @描述: 包装后的密码对象
 */
public class WrappedPassword {

    /**
     * 加密的密码
     */
    private String encodedPassword;

    /**
     * 密码加密盐
     */
    private String salt;

    public WrappedPassword(String encodedPassword, String salt) {
        this.encodedPassword = encodedPassword;
        this.salt = salt;
    }

    public WrappedPassword() {
    }

    public String getEncodedPassword() {
        return encodedPassword;
    }

    public void setEncodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
