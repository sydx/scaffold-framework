package com.scaffold.security.authentication.token;

/**
 * Token缓存处理接口
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/7/24 15:02:54
 */
public interface TokenHandler {
    void save(Token token);

    void set(String key, String token, long expiresIn);

    String getAccessToken(String key, String uid);

    String getRefreshToken(String key, String uid);

    void delete(String key, String uid);
}
