package com.scaffold.security.authentication.token;

import com.alibaba.fastjson2.JSONObject;
import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/29 15:30:14
 * @描述: token对象
 */
@Data
public class Token {

    /**
     * 用户ID
     */
    private String uid;

    /**
     * 用于调用受限资源的凭证，用户登录后颁发
     */
    private String accessToken;

    private Long accessTokenExpiresIn;

    /**
     * 用于刷新accessToken的凭证，用户登录后颁发
     */
    private String refreshToken;

    private Long refreshTokenExpiresIn;

    /**
     * 获取refreshToken的code
     */
    private String refreshCode;

    public JSONObject generateTokenJson() {
        return JSONObject.of(
                "accessToken", getAccessToken(),
                "expiresIn", getAccessTokenExpiresIn(),
                "refreshCode", getRefreshCode());
    }

}
