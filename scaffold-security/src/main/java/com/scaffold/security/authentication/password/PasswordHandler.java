package com.scaffold.security.authentication.password;

/**
 * 密码处理接口
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/7/24 11:24:21
 */
public interface PasswordHandler {

    /**
     * 密码解码
     * @param requestId 请求ID
     * @param encryptLPassword 已加密的密码
     * @return java.lang.String 解密后的密码
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-07-24 11:26:26
     */
    String decryptPassword(String requestId, String encryptLPassword);

    /**
     * 创建加密密钥
     * @param requestId 请求ID
     * @return java.lang.String 解密后的密码
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-07-24 11:26:26
     */
    String createKey(String requestId);

}
