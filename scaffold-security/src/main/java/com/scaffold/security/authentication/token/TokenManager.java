package com.scaffold.security.authentication.token;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.symmetric.AES;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.scaffold.common.asserts.Asserts;
import com.scaffold.common.constants.CacheNameConstant;
import com.scaffold.common.exception.TokenException;
import com.scaffold.common.result.RCode;
import com.scaffold.security.SecurityConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static com.scaffold.security.util.AuthenticationUtil.UID_KEY;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/29 9:48:54
 * @描述: token管理
 */
@Component
public class TokenManager {
    @Autowired
    private SecurityConfigProperties securityConfigProperties;
    @Autowired
    private TokenHandler tokenHandler;

    /**
     * 系统枚举
     */
    public enum System {
        MANAGER(CacheNameConstant.SECURITY_AUTHENTICATION_TOKEN),
        MINI_PROGRAM(CacheNameConstant.SECURITY_AUTHENTICATION_TOKEN_MINIPROGRAM);
        /**
         * redis cache name
         */
        private final String catchName;

        System(String catchName) {
            this.catchName = catchName;
        }

        public String getCatchName() {
            return catchName;
        }
    }

    /**
     * refresh token redis cache key
     */
    private final static String REFRESH_TOKEN_KEY_FMT = "%s:refresh_token_%s";
    /**
     * access token redis cache key
     */
    private final static String ACCESS_TOKEN_KEY_FMT = "%s:access_token_%s";

    private AES accessTokenAes;
    private AES refreshTokenAes;

    public AES getAccessTokenAes() {
        if (null == accessTokenAes) {

            accessTokenAes =
                    new AES(Base64.getDecoder().decode(securityConfigProperties.getTokenConfig().getAccessTokenSecretKey()));
        }
        return accessTokenAes;
    }

    public AES getRefreshTokenAes() {
        if (refreshTokenAes == null) {
            refreshTokenAes =
                    new AES(Base64.getDecoder().decode(securityConfigProperties.getTokenConfig().getRefreshTokenSecretKey()));
        }
        return refreshTokenAes;
    }

    public String generateJwtToken(Map<String, String> claim, String secretKey, Long expiresIn) {
        JWTCreator.Builder builder = JWT.create();
        for (Map.Entry<String, String> entry : claim.entrySet()) {
            builder.withClaim(entry.getKey(), entry.getValue());
        }
        return builder.withExpiresAt(Instant.now().plusSeconds(expiresIn))
                .sign(Algorithm.HMAC256(secretKey));
    }

    public Map<String, Claim> verifyJwtToken(String token, String secretKey) {
        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(secretKey)).build();
        try {
            return jwtVerifier.verify(token).getClaims();
        } catch (TokenExpiredException expiredException) {
            throw new TokenException(RCode.TOKEN_EXPIRED);
        } catch (JWTVerificationException expiredException) {
            throw new TokenException(RCode.TOKEN_EXCEPTION, expiredException);
        }
    }

    /**
     * @描述 生成AccessToken
     * @参数 claim
     * @返回值: java.lang.String
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-30 10:31:56
     */
    public String generateAccessToken(Map<String, String> claim) {
        String jwtToken = generateJwtToken(
                claim,
                securityConfigProperties.getJwtConfig().getSecretKey(),
                securityConfigProperties.getTokenConfig().getAccessTokenExpiresIn());
        return getAccessTokenAes().encryptBase64(jwtToken);
    }

    /**
     * @描述 解码AccessToken
     * @参数 claim
     * @返回值: java.lang.String
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-30 10:31:56
     */
    public Map<String, Claim> decryptAccessToken(String data) {
        String jwtToken = getAccessTokenAes().decryptStr(data);
        return verifyJwtToken(jwtToken, securityConfigProperties.getJwtConfig().getSecretKey());
    }

    /**
     * @描述 生成RefreshToken
     * @参数 claim
     * @返回值: java.lang.String
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-30 10:31:56
     */
    public String generateRefreshToken(Map<String, String> claim) {
        String jwtToken = generateJwtToken(
                claim,
                securityConfigProperties.getJwtConfig().getSecretKey(),
                securityConfigProperties.getTokenConfig().getRefreshTokenExpiresIn());
        return getRefreshTokenAes().encryptBase64(jwtToken);
    }

    /**
     * @描述 解码RefreshToken
     * @参数 claim
     * @返回值: java.lang.String
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-30 10:31:56
     */
    public Map<String, Claim> decryptRefreshToken(String data) {
        String jwtToken = getRefreshTokenAes().decryptStr(data);
        return verifyJwtToken(jwtToken, securityConfigProperties.getJwtConfig().getSecretKey());
    }

    /**
     * 生成token
     *
     * @param claim
     * @param uid
     * @return
     */
    public Token generateToken(Map<String, String> claim, String uid) {
        Token token = new Token();
        token.setUid(uid);
        token.setAccessToken(generateAccessToken(claim));
        token.setAccessTokenExpiresIn(securityConfigProperties.getTokenConfig().getAccessTokenExpiresIn());
        token.setRefreshToken(generateRefreshToken(claim));
        token.setRefreshTokenExpiresIn(securityConfigProperties.getTokenConfig().getRefreshTokenExpiresIn());
        String refreshCode = String.format("%s,%d", uid, Instant.now().toEpochMilli() + (token.getRefreshTokenExpiresIn() * 1000));
        token.setRefreshCode(getRefreshTokenAes().encryptBase64(refreshCode));
        return token;
    }

    /**
     * 保存token
     *
     * @param token
     * @param system
     */
    public void saveToken(Token token, System system) {
        //保存到数据库
        tokenHandler.save(token);
        //缓存过期时间以时间长的refresh token为准
        tokenHandler.set(
                getAccessTokenKey(system, token.getUid()),
                token.getAccessToken(),
                Instant.now().getEpochSecond() + token.getRefreshTokenExpiresIn()
        );
        tokenHandler.set(
                getRefreshTokenKey(system, token.getUid()),
                token.getRefreshToken(),
                Instant.now().getEpochSecond() + token.getRefreshTokenExpiresIn()
        );
    }

    /**
     * @描述 生成token并保存
     * @参数 claim
     * @参数 uid
     * @参数 system
     * @返回值: com.lls.security.authentication.token.Token
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-07 11:45:28
     */
    public Token generateSaveToken(Map<String, String> claim, String uid, System system) {
        Token token = generateToken(claim, uid);
        saveToken(token, system);
        return token;
    }

    /**
     * @描述 验证access token
     * @参数 system
     * @参数 uid
     * @参数 accessToken
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-09-07 11:48:29
     */
    public void verifyAccessToken(System system, String uid, String accessToken) {
        String token = getAccessToken(system, uid);
        Asserts.isEquals(token, accessToken, new TokenException());
    }

    /**
     * 获取AccessToken
     *
     * @param uid
     * @return
     */
    public String getAccessToken(System system, String uid) {
        String token = tokenHandler.getAccessToken(getAccessTokenKey(system, uid), uid);
        Asserts.notNull(token, new TokenException(RCode.TOKEN_EXPIRED));
        return token;
    }

    /**
     * 获取RefreshToken
     *
     * @param uid
     * @return
     */
    public String getRefreshToken(System system, String uid) {
        String token = tokenHandler.getRefreshToken(getRefreshTokenKey(system, uid), uid);
        Asserts.notNull(token, new TokenException(RCode.TOKEN_EXPIRED));
        return token;
    }

    /**
     * 使用refreshCode获取RefreshToken
     *
     * @param refreshCode
     * @return
     */
    public String getRefreshTokenByCode(System system, String refreshCode) {
        String decryptStr = getRefreshTokenAes().decryptStr(refreshCode);
        String[] strings = decryptStr.split(StrUtil.COMMA);

        //是否过期
        long timestamp = Long.parseLong(strings[1]);
        boolean notExpired = timestamp >= Instant.now().toEpochMilli();
        Asserts.isTrue(notExpired, new TokenException(RCode.TOKEN_EXPIRED));

        String uid = strings[0];
        String token = tokenHandler.getRefreshToken(getRefreshTokenKey(system, uid), uid);
        Asserts.hasText(token, new TokenException(RCode.TOKEN_EXPIRED));
        return token;
    }

    /**
     * 刷新AccessToken
     *
     * @param refreshToken
     * @return
     */
    public Token refreshAccessToken(System system, String refreshToken) {
        Map<String, Claim> claimMap = decryptRefreshToken(refreshToken);
        Asserts.notEmpty(claimMap, new TokenException(RCode.TOKEN_EXCEPTION));
        String uid = claimMap.get(UID_KEY).asString();
        String token = getRefreshToken(system, uid);
        Asserts.isTrue(StrUtil.equals(token, refreshToken), new TokenException(RCode.TOKEN_EXCEPTION));
        return generateSaveToken(claimToString(claimMap), uid, system);
    }

    /**
     * 销毁指定系统和用户的token
     *
     * @param uid
     */
    public void destroyToken(System system, String uid) {
        tokenHandler.delete(getAccessTokenKey(system, uid), uid);
        tokenHandler.delete(getRefreshTokenKey(system, uid), uid);
    }

    /**
     * 销毁用户的所有token
     *
     * @param uid
     */
    public void destroyToken(String uid) {
        for (System system : System.values()) {
            destroyToken(system, uid);
        }
    }

    private Map<String, String> claimToString(Map<String, Claim> claimMap) {
        Map<String, String> map = new HashMap<>(claimMap.size());
        for (Map.Entry<String, Claim> claimEntry : claimMap.entrySet()) {
            map.put(claimEntry.getKey(), claimEntry.getValue().asString());
        }
        return map;
    }

    private String getRefreshTokenKey(System system, String uid) {
        return String.format(REFRESH_TOKEN_KEY_FMT, system.getCatchName(), uid);
    }

    private String getAccessTokenKey(System system, String uid) {
        return String.format(ACCESS_TOKEN_KEY_FMT, system.getCatchName(), uid);
    }

}
