package com.scaffold.security.authentication;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.scaffold.common.util.RequestUtil;
import com.scaffold.security.SecurityConfigProperties;
import com.scaffold.security.authentication.captcha.CaptchaHandler;
import com.scaffold.security.authentication.password.PasswordHandler;
import jakarta.annotation.Nullable;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.nio.charset.StandardCharsets;

/**
 * @创建人: 李亮
 * @创建时间: 2022/6/2 13:28:07
 * @描述: 用户登录过滤器，覆盖原始过滤器中获取登录用户名和密码的方式。
 * 仅支持json post方式提交登录验证
 */
public class UserAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private static final Logger logger = LoggerFactory.getLogger(UserAuthenticationFilter.class);

    private CaptchaHandler captchaHandler;
    private PasswordHandler passwordHandler;

    private SecurityConfigProperties.Login loginConfig;

    public void setCaptchaHandler(CaptchaHandler captchaHandler) {
        this.captchaHandler = captchaHandler;
    }

    public void setPasswordHandler(PasswordHandler passwordHandler) {
        this.passwordHandler = passwordHandler;
    }

    public void setLoginConfig(SecurityConfigProperties.Login loginConfig) {
        this.loginConfig = loginConfig;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (logger.isDebugEnabled()) {
            logger.debug("自定义用户名密码登录过滤器");
        }
        if (!request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }

        JSONObject object = getLoginParams(request);
        String username = object.getString(getUsernameParameter());
        String password = object.getString(getPasswordParameter());
        if (StrUtil.hasBlank(username, password)) {
            throw new BadCredentialsException("用户名、密码不能为空！");
        }
        String requestId = object.getString("requestId");
        if (StrUtil.isBlank(requestId)) {
            throw new BadCredentialsException("requestId不能为空！");
        }
        validateCaptcha(requestId, object);
        password = decryptPassword(requestId, password);

        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
        setDetails(request, authRequest);
        return this.getAuthenticationManager().authenticate(authRequest);
    }

    @Nullable
    private JSONObject getLoginParams(HttpServletRequest request) {
        try {
            return JSON.parseObject(request.getInputStream(), StandardCharsets.UTF_8);
        } catch (Exception e) {
            logger.error("登陆请求信息解析失败！", e);
            throw new BadCredentialsException("不合法的身份认证信息");
        }
    }

    private void validateCaptcha(String requestId, JSONObject object) {
//        Object sessionCaptcha = session.getAttribute(SessionAttributeEnum.CAPTCHA.name());
//        if (!StrUtil.equals(captcha.toLowerCase(), String.valueOf(sessionCaptcha))) {
//            throw new BadCredentialsException("验证码错误！");
//        }
        if (loginConfig.getEnableLoginCaptcha()) {
            String captcha = object.getString(loginConfig.getCaptchaParameter());
            if (StrUtil.isBlank(captcha)) {
                throw new BadCredentialsException("验证码不能为空！");
            }
            if (!captchaHandler.validate(requestId, captcha)) {
                throw new BadCredentialsException("验证码错误！");
            }
        }

    }

    private String decryptPassword(String requestId, String password) {
//        Object rsaObj = session.getAttribute(SessionAttributeEnum.RSA_KEY.name());
//        if (null == rsaObj) {
//            throw new BadCredentialsException("未找到有效的秘钥！");
//        }
//        RSA rsa = (RSA) rsaObj;
//        try {
//            return rsa.decryptStr(password, KeyType.PrivateKey);
//        } catch (Exception e) {
//            logger.error("密码解密失败", e);
//            throw new BadCredentialsException("密码解密失败！");
//        }
        return passwordHandler.decryptPassword(requestId, password);
    }
}
