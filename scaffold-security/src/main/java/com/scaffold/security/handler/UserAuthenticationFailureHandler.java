package com.scaffold.security.handler;


import com.scaffold.common.result.R;
import com.scaffold.common.result.RCode;
import com.scaffold.security.util.HandlerUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import java.io.IOException;

/**
 * @创建人: 李亮
 * @创建时间: 2022/5/27 11:30:21
 * @描述: 用户认证失败处理类
 */
public class UserAuthenticationFailureHandler implements AuthenticationFailureHandler {
    private static final Logger logger = LoggerFactory.getLogger(UserAuthenticationFailureHandler.class);

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        if (logger.isDebugEnabled()) {
            logger.debug("用户身份认证失败！", exception);
        }
        if (exception instanceof CredentialsExpiredException) {
            HandlerUtil.responseJson(response, R.failure(RCode.AUTH_FAIL_PWD_EXPIRED, "密码已过期，请联系管理员重置密码！"));
        } else {
            HandlerUtil.responseJson(response, R.failure(RCode.AUTH_FAIL, exception.getMessage()));
        }

    }
}
