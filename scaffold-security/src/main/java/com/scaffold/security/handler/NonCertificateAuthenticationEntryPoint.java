package com.scaffold.security.handler;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.ContentType;
import cn.hutool.http.Header;
import com.scaffold.common.result.R;
import com.scaffold.common.result.RCode;
import com.scaffold.security.SecurityConfigProperties;
import com.scaffold.security.util.HandlerUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import java.io.IOException;

/**
 * @创建人: 李亮
 * @创建时间: 2022/5/27 10:42:33
 * @描述: 无凭证处理类.当用户没有携带有效凭证时的处理。根据请求类型进行响应
 */
public class NonCertificateAuthenticationEntryPoint implements AuthenticationEntryPoint {
    private static final Logger logger = LoggerFactory.getLogger(NonCertificateAuthenticationEntryPoint.class);

    private final SecurityConfigProperties securityConfigProperties;

    public NonCertificateAuthenticationEntryPoint(SecurityConfigProperties securityConfigProperties) {
        this.securityConfigProperties = securityConfigProperties;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) {
        if (logger.isDebugEnabled()) {
            logger.debug("未进行份认证", authException);
        }
        HandlerUtil.responseJson(response, R.failure(RCode.AUTH_NON_CERT));
    }
}
