package com.scaffold.security.handler;

import com.scaffold.common.result.R;
import com.scaffold.common.result.RCode;
import com.scaffold.security.util.HandlerUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import java.io.IOException;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/8 15:04:47
 * @描述: 权限校验失败处理
 */
public class UserAccessDeniedHandler implements AccessDeniedHandler {
    private static final Logger logger = LoggerFactory.getLogger(UserAccessDeniedHandler.class);

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        if (logger.isDebugEnabled()) {
            logger.debug("权限不足", accessDeniedException);
        }
        HandlerUtil.responseHandler(
                request,
                response,
                R.failure(RCode.ACCESS_DENIED, "无访问权限")
        );
    }
}
