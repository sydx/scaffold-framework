package com.scaffold.security.handler;


import com.scaffold.common.result.R;
import com.scaffold.common.util.SpringUtil;
import com.scaffold.security.authentication.token.TokenManager;
import com.scaffold.security.util.HandlerUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @创建人: 李亮
 * @创建时间: 2022/5/27 13:29:58
 * @描述: 用户注销成功处理
 */
public class UserAuthenticationLogoutSuccessHandler implements LogoutSuccessHandler {
    private static final Logger logger = LoggerFactory.getLogger(UserAuthenticationLogoutSuccessHandler.class);


    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        if (logger.isDebugEnabled()) {
            logger.debug("用户注销");
        }
        destroyToken(authentication);
        saveLog(request, authentication);
        HandlerUtil.responseJson(response, R.success());
    }


    /**
     * @描述 销毁token
     * @参数 authentication
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-30 11:38:52
     */
    private void destroyToken(Authentication authentication) {
        if (null == authentication || !(authentication.getPrincipal() instanceof UserDetails)) {
            return;
        }
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        TokenManager tokenManager = SpringUtil.getBean(TokenManager.class);
        tokenManager.destroyToken(TokenManager.System.MANAGER, userDetails.getUsername());
    }

    private void saveLog(HttpServletRequest request, Authentication authentication) {
//        if (null == authentication || !(authentication.getPrincipal() instanceof AdminUser)) {
//            return;
//        }
//        AdminUser adminUser = (AdminUser) authentication.getPrincipal();
//        SysOperationLogDTO log = new SysOperationLogDTO();
//        log.setLogType(LogTypeEnum.LOG_IN.getVal());
//        log.setOperation("退出登录");
//        log.setTime(LocalDateTime.now());
//        log.setIp(RequestUtil.getClientIP(request));
//        log.setRequestMethod(request.getMethod());
//        log.setRequestUrl(request.getRequestURI());
//        log.setSuccess(YesNoEnum.YES.getVal());
//        log.setCreator(adminUser.getFullName());
//        log.setCreatedBy(adminUser.getUserId());
//        log.setUpdatedBy(adminUser.getUserId());
//        try {
//            RedisService redisService = SpringUtil.getBean(RedisService.class);
//            //放入缓存队列
//            redisService.sendToTopic(TopicEnum.SYS_OPERATION_LOG_TOPIC.name(), log);
//        } catch (Exception e) {
//            logger.warn("日志保存失败", e);
//        }

    }

}
