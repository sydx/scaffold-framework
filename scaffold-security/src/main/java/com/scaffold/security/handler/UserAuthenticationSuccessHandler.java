package com.scaffold.security.handler;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson2.JSONObject;
import com.scaffold.common.result.R;
import com.scaffold.common.util.SpringUtil;
import com.scaffold.security.SecurityConfigProperties;
import com.scaffold.security.authentication.token.Token;
import com.scaffold.security.authentication.token.TokenManager;
import com.scaffold.security.util.HandlerUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import java.io.IOException;

import static com.scaffold.security.util.AuthenticationUtil.UID_KEY;


/**
 * @创建人: 李亮
 * @创建时间: 2022/6/1 14:44:54
 * @描述: 自定义认证成功处理
 */
public class UserAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private static final Logger logger = LoggerFactory.getLogger(UserAuthenticationSuccessHandler.class);
    private final SecurityConfigProperties securityConfigProperties;

    public UserAuthenticationSuccessHandler(SecurityConfigProperties securityConfigProperties) {
        this.securityConfigProperties = securityConfigProperties;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        if (logger.isDebugEnabled()) {
            logger.debug("用户认证成功");
        }
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        TokenManager tokenManager = SpringUtil.getBean(TokenManager.class);
        Token token = tokenManager.generateSaveToken(
                MapUtil.of(UID_KEY, userDetails.getUsername()),
                userDetails.getUsername(),
                TokenManager.System.MANAGER);
//        saveLog(request, adminUser);
        HandlerUtil.responseJson(response, R.success(getResponseToken(token)));
    }

    private JSONObject getResponseToken(Token token) {
        return token.generateTokenJson();
    }

//    private void saveLog(HttpServletRequest request, AdminUser adminUser) {
//        SysOperationLogDTO log = new SysOperationLogDTO();
//        log.setLogType(LogTypeEnum.LOG_IN.getVal());
//        log.setOperation(LogTypeEnum.LOG_IN.getLabel());
//        log.setTime(LocalDateTime.now());
//        log.setIp(RequestUtil.getClientIP(request));
//        log.setRequestMethod(request.getMethod());
//        log.setRequestUrl(request.getRequestURI());
//        log.setSuccess(YesNoEnum.YES.getVal());
//        log.setCreator(adminUser.getFullName());
//        log.setCreatedBy(adminUser.getUserId());
//        log.setUpdatedBy(adminUser.getUserId());
//        try {
//            RedisService redisService = SpringUtil.getBean(RedisService.class);
//            //放入缓存队列
//            redisService.sendToTopic(TopicEnum.SYS_OPERATION_LOG_TOPIC.name(), log);
//        } catch (Exception e) {
//            logger.warn("日志保存失败", e);
//        }

//    }

}
