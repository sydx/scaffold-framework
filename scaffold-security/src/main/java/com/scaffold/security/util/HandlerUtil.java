package com.scaffold.security.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.ContentType;
import cn.hutool.http.Header;
import com.alibaba.fastjson2.JSON;
import com.scaffold.common.exception.SystemException;
import com.scaffold.common.result.R;
import com.scaffold.security.authentication.token.Token;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import static com.scaffold.security.util.AuthenticationUtil.ACCESS_TOKEN_KEY;
import static com.scaffold.security.util.AuthenticationUtil.REFRESH_CODE_KEY;


/**
 * @创建人: 李亮
 * @创建时间: 2022/7/11 11:05:42
 * @描述: Handler工具类
 */
public class HandlerUtil {

    /**
     * @描述 响应处理。
     * 接口请求返回json
     * 页面请求跳转页面
     * @参数 request
     * @参数 response
     * @参数 result
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-30 14:08:37
     */
    public static void responseHandler(HttpServletRequest request, HttpServletResponse response, R result) {
        responseJson(response, result);
    }

    /**
     * @描述 响应处理。
     * 接口请求返回json
     * 页面请求跳转页面
     * @参数 request
     * @参数 response
     * @参数 result
     * @参数 url
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-30 14:08:37
     */
    public static void responseHandler(HttpServletRequest request, HttpServletResponse response, R result, String url) {
        if (isAsynchronousRequest(request)) {
            //返回JSON
            responseJson(response, result);
        } else {
            //跳转登录页
            redirect(response, url);
        }
    }

    /**
     * @描述 请求是否为异步请求。
     * @参数 request
     * @返回值: boolean
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-30 14:08:06
     */
    public static boolean isAsynchronousRequest(HttpServletRequest request) {
        String accept = request.getHeader(Header.ACCEPT.getValue());
        String requestContentType = request.getHeader(Header.CONTENT_TYPE.getValue());
        ContentType contentType = ContentType.TEXT_HTML;
        if (StrUtil.contains(accept, ContentType.JSON.getValue())
                || StrUtil.contains(accept, ContentType.TEXT_PLAIN.getValue())
                || StrUtil.contains(requestContentType, ContentType.JSON.getValue())) {
            contentType = ContentType.JSON;
        }
        return contentType.equals(ContentType.JSON);
    }

    public static void responseJson(HttpServletResponse response, R r) {
        String result = JSON.toJSONString(r);
        response.setContentType(ContentType.JSON.toString(StandardCharsets.UTF_8));
        try (PrintWriter writer = response.getWriter()) {
            writer.write(result);
            writer.flush();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    public static void redirect(HttpServletResponse response, String url) {
        try {
            response.sendRedirect(url);
        } catch (IOException e) {
            throw new SystemException(e);
        }
    }

    /**
     * @描述 清空token cookie
     * @参数 response
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-30 11:38:40
     */
    public static void clearTokenCookie(HttpServletResponse response) {
        Cookie accessToken = new Cookie(ACCESS_TOKEN_KEY, null);
        accessToken.setMaxAge(0);
        response.addCookie(accessToken);

        Cookie refreshToken = new Cookie(REFRESH_CODE_KEY, null);
        refreshToken.setMaxAge(0);
        response.addCookie(refreshToken);
    }

    /**
     * 设置cookie,有效期为RefreshToken有效期
     *
     * @param response
     * @param token
     */
    public static void setTokenCookie(HttpServletResponse response, Token token) {
        Cookie accessToken = new Cookie(ACCESS_TOKEN_KEY, token.getAccessToken());
        accessToken.setMaxAge(Math.toIntExact(token.getRefreshTokenExpiresIn()));
        response.addCookie(accessToken);

        Cookie refreshToken = new Cookie(REFRESH_CODE_KEY, token.getRefreshCode());
        refreshToken.setMaxAge(Math.toIntExact(token.getRefreshTokenExpiresIn()));
        response.addCookie(refreshToken);
    }
}
