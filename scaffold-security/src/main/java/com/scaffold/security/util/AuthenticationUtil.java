package com.scaffold.security.util;

import jakarta.annotation.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/29 9:19:52
 * @描述: 认证用户身份信息工具类
 */
public class AuthenticationUtil {

    public static final String USERNAME_KEY = "username";
    public static final String UID_KEY = "uid";
    public static final String ACCESS_TOKEN_KEY = "ACCESS_TOKEN";
    public static final String REFRESH_CODE_KEY = "REFRESH_CODE";
    public static final String ACCESS_TOKEN_HEADER = "Authentication";
    public static final String REFRESH_CODE_HEADER = "refresh-code";

    public static final String REFRESH_TOKEN_KEY = "REFRESH_TOKEN";

    /**
     * @描述 获取登录用户信息
     * @参数
     * @返回值: com.lls.security.authentication.user.AdminUser
     * @创建人 李亮
     * @创建时间 2022-07-29 09:21:31
     */
    public static Object getUser() {
        Authentication authentication = getAuthentication();
        if (authentication == null) return null;

        Object principal = getPrincipal(authentication);
        if (principal == null || "anonymousUser".equals(principal)) return null;
        return principal;
    }

    @Nullable
    public static Object getPrincipal(Authentication authentication) {
        return null != authentication ? authentication.getPrincipal() : null;
    }

    @Nullable
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
