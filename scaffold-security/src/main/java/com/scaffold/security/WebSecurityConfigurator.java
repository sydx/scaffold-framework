package com.scaffold.security;

import com.scaffold.security.authentication.APIAuthenticationFilter;
import com.scaffold.security.authentication.UserAuthenticationFilter;
import com.scaffold.security.authentication.password.MD5PasswordEncoder;
import com.scaffold.security.authentication.user.UserDetailsManager;
import com.scaffold.security.authorization.AccessDecisionHandler;
import com.scaffold.security.authorization.DynamicAccessDecisionManager;
import com.scaffold.security.authorization.DynamicSecurityFilter;
import com.scaffold.security.handler.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.intercept.AuthorizationFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.ArrayList;
import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/6/1 13:42:16
 * @描述: TODO
 */
public abstract class WebSecurityConfigurator {
    private static final Logger logger = LoggerFactory.getLogger(WebSecurityConfigurator.class);
    @Autowired
    private SecurityConfigProperties securityConfigProperties;
    @Autowired
    AuthenticationConfiguration authenticationConfiguration;

    @Autowired
    AuthenticationManagerBuilder authenticationManagerBuilder;

//    @Autowired
//    DynamicAccessDecisionManager dynamicAccessDecisionManager;


    public abstract UserDetailsService getUserDetailsService();


    //作用： 用来将自定义的AuthenticationManager在工厂中暴露，可以在任何位置注入
    public abstract AuthenticationManager authenticationManagerBean(AuthenticationConfiguration authenticationConfiguration) throws Exception;

    /**
     * 自定义用户认证
     **/

    @Bean
    protected SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        //放行地址，必须放在anyRequest之前
//        for (String ignoredUrl : securityConfigProperties.getAuthentication().getIgnoredUrls()) {
//            registry.requestMatchers().permitAll();
//        }
        http.authorizeHttpRequests((authorizeHttpRequests) -> {
//            String[] patterns = securityConfigProperties.getAuthentication().getIgnoredUrls().toArray(new String[0]);
            List<RequestMatcher> matchers = new ArrayList<>(securityConfigProperties.getAuthentication().getIgnoredUrls().size());
            for (String ignoredUrl : securityConfigProperties.getAuthentication().getIgnoredUrls()) {
                matchers.add(new AntPathRequestMatcher(ignoredUrl));
            }
            authorizeHttpRequests
                    .requestMatchers(matchers.toArray(new AntPathRequestMatcher[0]))
                    .permitAll()
                    .anyRequest()
                    .authenticated();
        });

        //开启登录认证
        //需要注意：提交登录信息时不支持json方式提交，因spring security是通过 request.getParameter 获取参数
        //自定义登录页面, 注意：一旦自定义登录页面，需要自定义登录请求地址
//                .loginPage(securityConfig.getAuthentication().getLoginPage())
        //执行登录请求的地址，该地址可以不存在实际的request mapping
//                .loginProcessingUrl("/doLogin")
        //自定义用户名和密码参数名称
        //默认的登录参数名为username和password
//                .usernameParameter("uname")
//                .passwordParameter("passwd")
        /** 传统action登录方式 **/
        //认证成功forward跳转地址，认证成功后始终跳转到这个地址
//                .successForwardUrl("/test")
        //认证成功默认跳转地址，redirect重定向跳转
        //登录前如果访问受限资源，认证成功后会跳转认证前访问的地址
//                .defaultSuccessUrl("/test")
        //如果需要登录后始终跳转配置的地址，将alwaysUse设置为true
//                .defaultSuccessUrl("/test", true)

        /** 自定义登录成功处理，主要应用于前后端分离接口调用或者 Restful方式登录方式 **/
//                .successHandler(new UserAuthenticationSuccessHandler())

        //认证失败后的forward跳转地址
//                .failureForwardUrl("/failure")
        //认证失败后redirect重定向跳转地址
//                .failureUrl("/failure")
        //自定义登录失败处理，主要应用于前后端分离 接口调用或者 Restful方式登录方式
//                .failureHandler(new UserAuthenticationFailureHandler())
        //前端后端分离的未登录处理，调整页面或者返回json提示
//                .exceptionHandling()
//                .authenticationEntryPoint(new NonCertificateAuthenticationEntryPoint())
//                .and()
        /** 注销登录 默认是开启的，GET方式访问 /logout 即可退出登录**/
        http.logout((logout) -> {
            logout
//                    .deleteCookies("remove")
                    .invalidateHttpSession(false)
                    .logoutUrl(securityConfigProperties.getLoginConfig().getLogoutApi())
//                            .logoutSuccessUrl("/logout-success")
                    .logoutSuccessHandler(new UserAuthenticationLogoutSuccessHandler());
        });
        //设置默认注销登录地址,只能通过get方式访问
//                .logoutUrl("/logout")
        //指定多个logout地址，并指定地址访问方式
//                .logoutRequestMatcher(new OrRequestMatcher(
//                        new AntPathRequestMatcher("/logout", "GET"),
//                        new AntPathRequestMatcher("/logout_post", "POST")
//                ))
        //默认,可不配置 会话失效
//                .invalidateHttpSession(true)
        //默认,可不配置  清除认证信息
//                .clearAuthentication(true)
        //注销登录成功后跳转的页面
//                .logoutSuccessUrl("/auth/singIn")
        //自定义注销成功处理
        http.csrf(AbstractHttpConfigurer::disable);
        //支持iframe
        http.headers((headers) ->
                headers.frameOptions(HeadersConfigurer.FrameOptionsConfig::sameOrigin));
//        http.headers().frameOptions().sameOrigin();
        ;
        // at: 用来某个 filter 替换过滤器链中哪个 filter
        // before: 放在过滤器链中哪个 filter 之前
        // after: 放在过滤器链中那个 filter 之后

        http.addFilterAt(userAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(new APIAuthenticationFilter(getUserDetailsService(), securityConfigProperties),
                UsernamePasswordAuthenticationFilter.class);
        //权限验证，开启动态权限
        if (securityConfigProperties.getAuthorization().getEnableDynamic()) {
            http.addFilterBefore(dynamicSecurityFilter(), AuthorizationFilter.class);
        }

        http.exceptionHandling((exceptionHandling) -> {
            //权限不足
            exceptionHandling.accessDeniedHandler(new UserAccessDeniedHandler());
            //无登录凭证处理，此配置会覆盖自动跳转登录页面
            exceptionHandling.authenticationEntryPoint(new NonCertificateAuthenticationEntryPoint(securityConfigProperties));
        });
        return http.build();

    }

    public DynamicSecurityFilter dynamicSecurityFilter() {
        return new DynamicSecurityFilter(dynamicAccessDecisionManager(), securityConfigProperties);
    }

    public DynamicAccessDecisionManager dynamicAccessDecisionManager() {
        return new DynamicAccessDecisionManager(accessDecisionHandler());
    }

    public abstract AccessDecisionHandler accessDecisionHandler();

    public UserAuthenticationFilter userAuthenticationFilter() throws Exception {
        SecurityConfigProperties.Login loginConfig = securityConfigProperties.getLoginConfig();
        UserAuthenticationFilter userAuthenticationFilter = new UserAuthenticationFilter();
        userAuthenticationFilter.setFilterProcessesUrl(loginConfig.getLoginApi());
        userAuthenticationFilter.setUsernameParameter(loginConfig.getUsernameParameter());
        userAuthenticationFilter.setPasswordParameter(loginConfig.getPasswordParameter());
        userAuthenticationFilter.setAuthenticationManager(authenticationManagerBean(authenticationConfiguration));
        userAuthenticationFilter.setAuthenticationSuccessHandler(new UserAuthenticationSuccessHandler(securityConfigProperties));
        userAuthenticationFilter.setAuthenticationFailureHandler(new UserAuthenticationFailureHandler());
        userAuthenticationFilter.setLoginConfig(loginConfig);
        userAuthenticationFilter.setCaptchaHandler(null);
        userAuthenticationFilter.setPasswordHandler(null);
        return userAuthenticationFilter;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new MD5PasswordEncoder();
    }

}
