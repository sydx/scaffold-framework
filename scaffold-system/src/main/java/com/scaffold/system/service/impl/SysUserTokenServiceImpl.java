package com.scaffold.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.scaffold.system.dao.SysUserTokenMapper;
import com.scaffold.system.entity.model.SysUserToken;
import com.scaffold.system.service.SysUserTokenService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.time.Instant;

/**
 * 系统用户访问凭证-业务处理实现类
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-08-15 14:23:55
 */
@Service
public class SysUserTokenServiceImpl extends ServiceImpl<SysUserTokenMapper, SysUserToken> implements SysUserTokenService {

    @Override
    public SysUserToken getTokenByUid(String uid) {
        Wrapper<SysUserToken> wrapper = new LambdaQueryWrapper<SysUserToken>()
                .eq(SysUserToken::getUid, uid);
        return baseMapper.selectOne(wrapper);
    }


    @Override
    public SysUserToken getAccessToken(String uid) {
        Wrapper<SysUserToken> wrapper = new LambdaQueryWrapper<SysUserToken>()
                .eq(SysUserToken::getUid, uid)
                .ge(SysUserToken::getAccessTokenExpiresTime, Instant.now().getEpochSecond())
                .select(SysUserToken::getAccessToken, SysUserToken::getAccessTokenExpiresTime);
        return baseMapper.selectOne(wrapper);
    }

    /**
     * 获取RefreshToken
     *
     * @param uid
     * @return
     */
    @Override
    public SysUserToken getRefreshToken(String uid) {
        Wrapper<SysUserToken> wrapper = new LambdaQueryWrapper<SysUserToken>()
                .eq(SysUserToken::getUid, uid)
                .ge(SysUserToken::getRefreshTokenExpiresTime, Instant.now().getEpochSecond())
                .select(SysUserToken::getRefreshToken, SysUserToken::getRefreshTokenExpiresTime);
        return baseMapper.selectOne(wrapper);
    }

    /**
     * 销毁用户的token
     *
     * @param uid
     */
    @Override
    public void destroyToken(String uid) {
        Wrapper<SysUserToken> wrapper = new LambdaQueryWrapper<SysUserToken>()
                .eq(SysUserToken::getUid, uid);
        baseMapper.delete(wrapper);
    }

}