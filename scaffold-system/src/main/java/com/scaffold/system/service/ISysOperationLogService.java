package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.dto.SysOperationLogDTO;
import com.scaffold.system.entity.model.SysOperationLog;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-08-03 11:55:34
 */
public interface ISysOperationLogService extends IService<SysOperationLog> {

    @Transactional(rollbackFor = Exception.class)
    void addLog(SysOperationLogDTO dto);
}
