package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.dto.SysRoleEditDTO;
import com.scaffold.system.entity.model.SysRole;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 系统用户角色 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
public interface ISysRoleService extends IService<SysRole> {

    void addRole(SysRoleEditDTO role);

    void updateRole(SysRoleEditDTO role);

    @Transactional(rollbackFor = Exception.class)
    void deleteRole(Integer roleId);
}
