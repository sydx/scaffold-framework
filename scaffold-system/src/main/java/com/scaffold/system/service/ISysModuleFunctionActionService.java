package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.dto.ModuleActionDTO;
import com.scaffold.system.entity.dto.PermissionDTO;
import com.scaffold.system.entity.dto.UserPermissionsDTO;
import com.scaffold.system.entity.model.SysModuleFunctionAction;
import com.scaffold.system.entity.vo.PermissionTreeVO;

import java.util.List;

/**
 * <p>
 * 功能的操作点 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
public interface ISysModuleFunctionActionService extends IService<SysModuleFunctionAction> {

    List<UserPermissionsDTO> getAllAction();

    void addAction(SysModuleFunctionAction action);

    void updateAction(SysModuleFunctionAction action);

    List<ModuleActionDTO> getActionByModule(Integer moduleId);

    List<PermissionTreeVO> getPermissionTreeByModule();

    List<PermissionTreeVO> getPermissionTreeByModule(Integer moduleId);

    void removeAction(Integer actionId);

    void updateActionState(Integer actionId, Integer state);
    List<PermissionDTO> getAllPermission();
}
