package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.model.SysUserAccount;

/**
 * <p>
 * 用户账户信息 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-08-17 12:13:03
 */
public interface ISysUserAccountService extends IService<SysUserAccount> {

}
