package com.scaffold.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.common.enums.StateEnum;
import com.scaffold.system.dao.SysOrganizationMapper;
import com.scaffold.system.entity.model.SysOrganization;
import com.scaffold.system.entity.vo.OrganizationTreeVO;
import com.scaffold.system.service.ISysOrganizationService;
import com.scaffold.common.util.CodeUtil;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 组织架构 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@Service
public class SysOrganizationServiceImpl extends ServiceImpl<SysOrganizationMapper, SysOrganization> implements ISysOrganizationService {

    /**
     * @描述 新增顶级组织
     * @参数 organization
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-15 10:04:48
     */
    @Override
    public void addTopOrganization(SysOrganization organization) {
        organization.setParentId(0);
        checkOrgName(organization);
        String orgCode = generateOrgCode(organization);
        checkNewOrgCode(orgCode);

        SysOrganization addOrg = new SysOrganization();
        addOrg.setOrgName(organization.getOrgName());
        addOrg.setOrgCode(orgCode);
        addOrg.setState(organization.getState());
        addOrg.setOrgPath(orgCode);
        boolean result = save(addOrg);
        Assert.isTrue(result, "组织架构新增失败");
    }

    /**
     * @描述 新增子组织
     * @参数 organization
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-15 10:04:48
     */
    @Override
    public void addChildrenOrganization(SysOrganization organization) {
        SysOrganization parent = checkOrganizationExist(organization.getParentId());
        checkOrgName(organization);
        String orgCode = parent.getOrgCode() + generateOrgCode(organization);
        checkNewOrgCode(orgCode);

        SysOrganization addOrg = new SysOrganization();
        addOrg.setOrgName(organization.getOrgName());
        addOrg.setParentId(parent.getOrgId());
        addOrg.setOrgCode(orgCode);
        addOrg.setOrgLevel(parent.getOrgLevel() + 1);
        addOrg.setState(organization.getState());
        addOrg.setOrgPath(String.format("%s|%s", parent.getOrgPath(), orgCode));
        boolean result = save(addOrg);
        Assert.isTrue(result, "组织架构新增失败");
    }

    private void checkNewOrgCode(String orgCode) {
        Wrapper<SysOrganization> condition = new LambdaQueryWrapper<SysOrganization>()
                .eq(SysOrganization::getOrgCode, orgCode);
        long count = count(condition);
        Assert.isTrue(count == 0, "权限标识已存在");
    }

    @NotNull
    private String generateOrgCode(SysOrganization organization) {
        Wrapper<SysOrganization> condition;
        condition = new LambdaQueryWrapper<SysOrganization>()
                .eq(SysOrganization::getParentId, organization.getParentId())
                .select(SysOrganization::getOrgCode);
        List<String> usedCode = listObjs(condition, String::valueOf);
        return CodeUtil.generate(usedCode);
    }

    private void checkOrgName(SysOrganization organization) {
        Wrapper<SysOrganization> condition = new LambdaQueryWrapper<SysOrganization>()
                .eq(SysOrganization::getOrgName, organization.getOrgName())
                .eq(SysOrganization::getParentId, organization.getParentId());
        long count = count(condition);
        Assert.isTrue(count == 0, "组织名称已存在");
    }

    /**
     * @描述 修改组织架构
     * @参数 organization
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-15 10:40:51
     */
    @Override
    public void updateOrganization(SysOrganization organization) {
        SysOrganization org = checkOrganizationExist(organization.getOrgId());
        //修改组织名称需要检查名称是否存在
        if (!StrUtil.equals(organization.getOrgName(), org.getOrgName())) {
            checkOrgName(organization);
        }
        SysOrganization update = new SysOrganization();
        update.setOrgId(organization.getOrgId());
        update.setOrgName(organization.getOrgName());
        update.setState(organization.getState());
        boolean result = updateById(update);
        Assert.isTrue(result, "修改组织失败");
    }

    @NotNull
    private SysOrganization checkOrganizationExist(Integer orgId) {
        SysOrganization org = getById(orgId);
        Assert.notNull(org, "组织不存在");
        return org;
    }

    /**
     * @描述 删除组织。需要先删除子组织
     * @参数 orgId
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-15 10:51:09
     */
    @Override
    public void deleteOrganization(Integer orgId) {
        checkOrganizationExist(orgId);
        Wrapper<SysOrganization> condition = new LambdaQueryWrapper<SysOrganization>()
                .eq(SysOrganization::getParentId, orgId);
        long count = count(condition);
        Assert.isTrue(count == 0, "组织下存在子级组织");
        boolean result = removeById(orgId);
        Assert.isTrue(result, "删除组织失败");
    }

    /**
     * @描述 获取组织架构树
     * @参数 orgId
     * @返回值: java.util.List<com.scaffold.system.entity.vo.OrganizationTreeVO>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-16 15:01:11
     */
    @Override
    public List<OrganizationTreeVO> getOrganizationTree(Integer orgId) {
        String orgPath = null;
        if (null != orgId) {
            SysOrganization organization = checkOrganizationExist(orgId);
            orgPath = organization.getOrgPath();
        }
        Wrapper<SysOrganization> condition = new LambdaQueryWrapper<SysOrganization>()
                .likeLeft(null != orgPath, SysOrganization::getOrgPath, orgPath);
        List<SysOrganization> organizations = list(condition);
        List<SysOrganization> top = null;

        if (null == orgId) {
            top = organizations.stream().filter(o -> o.getParentId() == 0).collect(Collectors.toList());
        } else {
            top = organizations.stream().filter(o -> o.getOrgId().equals(orgId)).collect(Collectors.toList());
        }

        List<OrganizationTreeVO> tree = new ArrayList<>();
        OrganizationTreeVO treeVO = null;
        for (SysOrganization topOrg : top) {
            treeVO = new OrganizationTreeVO();
            BeanUtils.copyProperties(topOrg, treeVO);
            treeVO.setChildren(getChildren(topOrg.getOrgId(), organizations));
            tree.add(treeVO);
        }
        return tree;
    }

    /**
     * 根据多个orgCode， 获取各自的完整组织树（传入org为叶子节点）
     *
     * @param orgCodeList
     * @return
     */
    @Override
    public List<OrganizationTreeVO> getOrganizationTree(List<String> orgCodeList) {
        Wrapper<SysOrganization> wrapper = new LambdaQueryWrapper<SysOrganization>()
                .in(SysOrganization::getOrgCode, orgCodeList)
                .eq(SysOrganization::getState, StateEnum.ENABLED.val())
                .select(SysOrganization::getOrgPath);
        List<String> list = listObjs(wrapper, String::valueOf);

        List<String> topCode = new ArrayList<>();
        for (String s : list) {
            List<String> split = StrUtil.split(s, '|');
            topCode.addAll(split);
        }

        ArrayList<String> distinct = CollUtil.distinct(topCode);
        Wrapper<SysOrganization> query = new LambdaQueryWrapper<SysOrganization>()
                .in(SysOrganization::getOrgCode, distinct)
                .eq(SysOrganization::getState, StateEnum.ENABLED.val());
        List<SysOrganization> organizations = list(query);

        List<SysOrganization> top = organizations.stream().filter(o -> o.getParentId() == 0).collect(Collectors.toList());
        List<OrganizationTreeVO> tree = new ArrayList<>();
        OrganizationTreeVO treeVO = null;
        for (SysOrganization topOrg : top) {
            treeVO = new OrganizationTreeVO();
            BeanUtils.copyProperties(topOrg, treeVO);
            treeVO.setChildren(getChildren(topOrg.getOrgId(), organizations));
            tree.add(treeVO);
        }
        return tree;
    }

    private List<OrganizationTreeVO> getChildren(Integer parentId, List<SysOrganization> list) {
        List<OrganizationTreeVO> children = new ArrayList<>();
        List<SysOrganization> organizations = list.stream().filter(o -> o.getParentId().equals(parentId)).collect(Collectors.toList());
        OrganizationTreeVO treeVO = null;
        for (SysOrganization organization : organizations) {
            treeVO = new OrganizationTreeVO();
            BeanUtils.copyProperties(organization, treeVO);
            treeVO.setChildren(getChildren(organization.getOrgId(), list));
            children.add(treeVO);
        }
        return children;
    }

    @Override
    public List<SysOrganization> getByOrgCodes(List<String> orgCodeList) {
        Wrapper<SysOrganization> wrapper = new LambdaQueryWrapper<SysOrganization>()
                .in(SysOrganization::getOrgCode, orgCodeList)
                .eq(SysOrganization::getState, StateEnum.ENABLED.val())
                .select(SysOrganization::getOrgCode, SysOrganization::getOrgName);
        return list(wrapper);
    }

    @Override
    public SysOrganization getByOrgCode(String orgCode) {
        Wrapper<SysOrganization> wrapper = new LambdaQueryWrapper<SysOrganization>()
                .eq(SysOrganization::getOrgCode, orgCode)
                .eq(SysOrganization::getState, StateEnum.ENABLED.val());
        return getOne(wrapper);
    }
}
