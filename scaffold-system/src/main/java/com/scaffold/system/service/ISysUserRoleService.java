package com.scaffold.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.model.SysUser;
import com.scaffold.system.entity.model.SysUserRole;
import com.scaffold.system.entity.query.SysRoleQuery;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 用户角色 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

    IPage<SysUser> getUserListByRole(SysRoleQuery query);

    void addRoleUser(Integer roleId, List<Integer> userIds);

    @Transactional(rollbackFor = Exception.class)
    void addUserRole(Integer userId, List<Integer> roleIds);
}
