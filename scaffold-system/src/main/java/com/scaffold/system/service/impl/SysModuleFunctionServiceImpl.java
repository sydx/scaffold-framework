package com.scaffold.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.common.constants.CacheNameConstant;
import com.scaffold.common.util.CodeUtil;
import com.scaffold.system.dao.SysModuleFunctionMapper;
import com.scaffold.system.dao.SysModuleMapper;
import com.scaffold.system.entity.model.SysModule;
import com.scaffold.system.entity.model.SysModuleFunction;
import com.scaffold.system.entity.vo.SysModuleFunctionVO;
import com.scaffold.system.service.ISysModuleFunctionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 * 系统模块下的功能 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@Service
public class SysModuleFunctionServiceImpl extends ServiceImpl<SysModuleFunctionMapper, SysModuleFunction> implements ISysModuleFunctionService {

    @Autowired
    private SysModuleMapper sysModuleMapper;

    @Override
    public List<String> getFunctionNames(String url, String method) {
        Assert.hasText(url, "URL不能为空");
        return getBaseMapper().getFunctionNames(url, method);
    }

    /**
     * @描述 新增一级功能
     * @参数 function
     * @返回值: void
     * @创建人 李亮
     * @创建时间 2022-07-28 15:32:12
     */
    @Override
    @CacheEvict(value = CacheNameConstant.SYS_PERMISSION_CODE_URL, key = "'all'")
    public void addTopFunction(SysModuleFunction function) {
        SysModule module = sysModuleMapper.selectById(function.getModuleId());
        Assert.notNull(module, "模块不存在");

        //获取所有模块标识
        function.setParentId(0);
        List<String> used = getAllPermissionCode(function.getModuleId(), function.getParentId());
        //获取模块标识
        String permissionCode = module.getPermissionCode() + CodeUtil.function(CodeUtil.generate(used));

        //检查权限标识始发存在，如果存在再尝试生成一次
        Wrapper<SysModuleFunction> query = new LambdaQueryWrapper<SysModuleFunction>()
                .eq(SysModuleFunction::getPermissionCode, permissionCode);
        Long count = getBaseMapper().selectCount(query);
        Assert.isTrue(count <= 1L, "权限标识已存在");

        //创建模块
        SysModuleFunction saveFunction = new SysModuleFunction();
        saveFunction.setFunctionName(function.getFunctionName());
        saveFunction.setModuleId(function.getModuleId());
        saveFunction.setUrl(function.getUrl());
        saveFunction.setFunctionLevel(1);
        saveFunction.setParentId(function.getParentId());
        saveFunction.setFunctionIcon(function.getFunctionIcon());
        saveFunction.setState(function.getState());
        saveFunction.setFunctionPath(permissionCode);
        saveFunction.setPermissionCode(permissionCode);

        doAdd(saveFunction);
    }

    /**
     * @描述 新增子功能
     * @参数 sysModule
     * @返回值: boolean
     * @创建人 李亮
     * @创建时间 2022-07-28 15:32:12
     */
    @Override
    @CacheEvict(value = CacheNameConstant.SYS_PERMISSION_CODE_URL, key = "'all'")
    public void addChildFunction(SysModuleFunction function) {
        SysModule module = sysModuleMapper.selectById(function.getModuleId());
        Assert.notNull(module, "模块不存在");

        SysModuleFunction parent = getById(function.getParentId());
        Assert.notNull(parent, "上级功能不存在");

        //获取当前模块和同级功能的所有权限标识
        List<String> used = getAllPermissionCode(function.getModuleId(), function.getParentId());

        //获取权限标识
        String permissionCode = CodeUtil.function(parent.getPermissionCode(), CodeUtil.generate(used));

        //检查权限标识始发存在，如果存在再尝试生成一次
        Wrapper<SysModuleFunction> query = new LambdaQueryWrapper<SysModuleFunction>()
                .eq(SysModuleFunction::getPermissionCode, permissionCode);
        Long count = getBaseMapper().selectCount(query);
        Assert.isTrue(count <= 1L, "权限标识已存在");

        //创建模块
        SysModuleFunction saveFunction = new SysModuleFunction();
        saveFunction.setFunctionName(function.getFunctionName());
        saveFunction.setModuleId(function.getModuleId());
        saveFunction.setUrl(function.getUrl());
        saveFunction.setFunctionLevel(parent.getFunctionLevel() + 1);
        saveFunction.setParentId(function.getParentId());
        saveFunction.setFunctionIcon(function.getFunctionIcon());
        saveFunction.setState(function.getState());
        saveFunction.setFunctionPath(String.format("%s|%s", parent.getFunctionPath(), permissionCode));
        saveFunction.setPermissionCode(permissionCode);

        doAdd(saveFunction);
    }

    @Transactional(rollbackFor = Exception.class)
    public void doAdd(SysModuleFunction saveFunction) {
        boolean result = save(saveFunction);
        String message = "新增功能失败！";
        Assert.isTrue(result, message);
        //排序字段值使用自增ID
        Wrapper<SysModuleFunction> updateIndex = new UpdateWrapper<SysModuleFunction>()
                .lambda().setSql("function_index=function_id");
        result = update(updateIndex);
        Assert.isTrue(result, message);
    }

    private List<String> getAllPermissionCode(Integer moduleId, Integer parentId) {
        Wrapper<SysModuleFunction> query = new LambdaQueryWrapper<SysModuleFunction>()
                .eq(SysModuleFunction::getModuleId, moduleId)
                .eq(SysModuleFunction::getParentId, parentId)
                .select(SysModuleFunction::getPermissionCode);
        return listObjs(query, String::valueOf);
    }

    /**
     * @描述 修改功能
     * @参数 function
     * @返回值: void
     * @创建人 李亮
     * @创建时间 2022-07-29 16:01:39
     */
    @Override
    @CacheEvict(value = CacheNameConstant.SYS_PERMISSION_CODE_URL, key = "'all'")
    public void updateFunction(SysModuleFunction function) {
        SysModuleFunction saveFunction = new SysModuleFunction();
        saveFunction.setFunctionId(function.getFunctionId());
        saveFunction.setFunctionName(function.getFunctionName());
        saveFunction.setUrl(function.getUrl());
        saveFunction.setFunctionIcon(function.getFunctionIcon());
        saveFunction.setState(function.getState());
        boolean result = updateById(saveFunction);
        Assert.isTrue(result, "修改功能失败！");
    }

    /**
     * @描述 修改排序
     * @参数 sourceId
     * @参数 targetId
     * @返回值: void
     * @创建人 李亮
     * @创建时间 2022-07-29 16:46:04
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateSort(Integer sourceId, Integer targetId) {
        Wrapper<SysModuleFunction> query = new LambdaQueryWrapper<SysModuleFunction>()
                .eq(SysModuleFunction::getFunctionId, sourceId).
                select(SysModuleFunction::getFunctionIndex, SysModuleFunction::getFunctionId);
        SysModuleFunction source = getOne(query);
        Assert.notNull(source, "功能不存在");

        query = new LambdaQueryWrapper<SysModuleFunction>()
                .eq(SysModuleFunction::getFunctionId, targetId).
                select(SysModuleFunction::getFunctionIndex, SysModuleFunction::getFunctionId);
        SysModuleFunction target = getOne(query);
        Assert.notNull(target, "功能不存在");

        source.setFunctionId(targetId);
        boolean result = updateById(source);
        Assert.isTrue(result, "修改排序失败");

        target.setFunctionId(sourceId);
        result = updateById(target);
        Assert.isTrue(result, "修改排序失败");
    }

    /**
     * @描述 查询模块下的功能树
     * @参数 moduleId
     * @返回值: java.util.List<com.scaffold.system.entity.vo.SysModuleFunctionVO>
     * @创建人 李亮
     * @创建时间 2022-07-30 10:47:44
     */
    @Override
    public List<SysModuleFunctionVO> getFunctionTree(Integer moduleId) {
        Wrapper<SysModuleFunction> wrapper = new LambdaQueryWrapper<SysModuleFunction>()
                .eq(SysModuleFunction::getModuleId, moduleId)
                .orderByAsc(SysModuleFunction::getFunctionIndex);
        List<SysModuleFunction> list = list(wrapper);
        return getChild(list, 0);
    }

    private List<SysModuleFunctionVO> getChild(List<SysModuleFunction> list, Integer parentId) {
        List<SysModuleFunctionVO> childVOList = new ArrayList<>();
        List<SysModuleFunction> childFunction = list.stream().filter(function -> function.getParentId().equals(parentId)).collect(Collectors.toList());
        for (SysModuleFunction function : childFunction) {
            SysModuleFunctionVO vo = new SysModuleFunctionVO();
            BeanUtils.copyProperties(function, vo);
            vo.setChildren(getChild(list, function.getFunctionId()));
            childVOList.add(vo);
        }
        return childVOList;
    }

    /**
     * 删除功能
     *
     * @param functionId
     */
    @Override
    @CacheEvict(value = CacheNameConstant.SYS_PERMISSION_CODE_URL, key = "'all'")
    public void removeFunction(Integer functionId) {
        boolean update = removeById(functionId);
        Assert.isTrue(update, "删除失败！");
    }

    /**
     * 修改功能状态
     *
     * @param functionId
     * @param state
     */
    @Override
    @CacheEvict(value = CacheNameConstant.SYS_PERMISSION_CODE_URL, key = "'all'")
    public void updateFunctionState(Integer functionId, Integer state) {
        SysModuleFunction update = new SysModuleFunction();
        update.setFunctionId(functionId);
        update.setState(state);
        boolean result = updateById(update);
        Assert.isTrue(result, "状态修改失败！");
    }
}
