package com.scaffold.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysOrganizationMapper;
import com.scaffold.system.dao.SysUserMapper;
import com.scaffold.system.dao.SysUserOrganizationMapper;
import com.scaffold.system.entity.model.SysOrganization;
import com.scaffold.system.entity.model.SysUser;
import com.scaffold.system.entity.model.SysUserOrganization;
import com.scaffold.system.entity.query.SysOrganizationQuery;
import com.scaffold.system.entity.vo.OrganizationUserVO;
import com.scaffold.mybatisplus.factory.SFunctionFactory;
import com.scaffold.system.service.ISysUserOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户组织 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-08-15 11:41:05
 */
@Service
public class SysUserOrganizationServiceImpl extends ServiceImpl<SysUserOrganizationMapper, SysUserOrganization> implements ISysUserOrganizationService {

    @Autowired
    private SysOrganizationMapper sysOrganizationMapper;
    @Autowired
    private SysUserMapper sysUserMapper;


    /**
     * @描述 获取组织用户列表
     * @参数 orgId
     * @返回值: java.util.List<com.scaffold.system.entity.vo.OrganizationUserVO>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-15 11:42:59
     */
    @Override
    public IPage<OrganizationUserVO> getOrgUsers(SysOrganizationQuery query) {
        SysOrganization organization = sysOrganizationMapper.selectById(query.getOrgId());
        Assert.notNull(organization, "组织不存在");
        Page<OrganizationUserVO> page = new Page<>(query.getPage(), query.getSize());
        return baseMapper.getOrgUsers(page, organization.getOrgPath());
    }

    /**
     * @描述 向组织中添加用户
     * @参数 orgId
     * @参数 userIds
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-15 12:04:51
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addOrgUser(Integer orgId, List<Integer> userIds) {
        SysOrganization organization = sysOrganizationMapper.selectById(orgId);
        Assert.notNull(organization, "组织不存在");

        //检查用户是否已经设置为当前角色
        Wrapper<SysUserOrganization> wrapper = new LambdaQueryWrapper<SysUserOrganization>()
                .eq(SysUserOrganization::getOrgId, orgId)
                .in(SysUserOrganization::getUserId, userIds)
                .select(SysUserOrganization::getUserId);
        List<Integer> orgExistUserIdList = listObjs(wrapper, SFunctionFactory.newMapper());

        //通过选择的用户ID和已存在的用户ID列表取差集获取没有添加过的用户ID
        List<Integer> orgNotExistUserIdList = CollUtil.subtractToList(userIds, orgExistUserIdList);
        Assert.notEmpty(orgNotExistUserIdList, "用户已存在组织中");

        //过滤不存在的用户
        Wrapper<SysUser> userQuery = new LambdaQueryWrapper<SysUser>()
                .in(SysUser::getUserId, orgNotExistUserIdList)
                .select(SysUser::getUserId);
        List<Integer> existUserIdList = sysUserMapper.selectList(userQuery)
                .stream().filter(Objects::nonNull)
                .map(SysUser::getUserId)
                .collect(Collectors.toList());
        Assert.notEmpty(existUserIdList, "用户信息不存在");

        //执行添加
        List<SysUserOrganization> sysUserOrganizationList = new ArrayList<>(existUserIdList.size());
        SysUserOrganization userOrganization = null;
        for (Integer userId : existUserIdList) {
            userOrganization = new SysUserOrganization();
            userOrganization.setUserId(userId);
            userOrganization.setOrgId(orgId);
            userOrganization.setOrgPath(organization.getOrgPath());
            userOrganization.setUserOrgCode(organization.getOrgCode());
            userOrganization.setOrgName(organization.getOrgName());
            sysUserOrganizationList.add(userOrganization);
        }
        boolean result = saveBatch(sysUserOrganizationList);
        Assert.isTrue(result, "添加用户失败！");
    }

    /**
     * @描述 给指定用户设置组织
     * @参数 userId
     * @参数 orgIds
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-16 16:58:16
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addUserOrg(Integer userId, List<Integer> orgIds) {
        Wrapper<SysUserOrganization> wrapper = new LambdaQueryWrapper<SysUserOrganization>()
                .eq(SysUserOrganization::getUserId, userId);
        remove(wrapper);

        if (CollUtil.isEmpty(orgIds)) {
            return;
        }
        Wrapper<SysOrganization> orgQuery = new LambdaQueryWrapper<SysOrganization>()
                .in(SysOrganization::getOrgId, orgIds);
        List<SysOrganization> organizations = sysOrganizationMapper.selectList(orgQuery);
        if (CollUtil.isEmpty(organizations)) {
            return;
        }
        //执行添加
        List<SysUserOrganization> sysUserOrganizationList = new ArrayList<>(organizations.size());
        SysUserOrganization userOrganization = null;
        for (SysOrganization organization : organizations) {
            userOrganization = new SysUserOrganization();
            userOrganization.setUserId(userId);
            userOrganization.setOrgId(organization.getOrgId());
            userOrganization.setOrgPath(organization.getOrgPath());
            userOrganization.setUserOrgCode(organization.getOrgCode());
            userOrganization.setOrgName(organization.getOrgName());
            sysUserOrganizationList.add(userOrganization);
        }
        boolean result = saveBatch(sysUserOrganizationList);
        Assert.isTrue(result, "设置用户组织失败！");
    }

}
