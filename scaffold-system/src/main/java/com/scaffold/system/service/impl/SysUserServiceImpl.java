package com.scaffold.system.service.impl;

import cn.hutool.core.text.StrSplitter;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.common.asserts.Asserts;
import com.scaffold.common.enums.StateEnum;
import com.scaffold.common.enums.UserEnum;
import com.scaffold.common.enums.YesNoEnum;
import com.scaffold.common.util.PasswordUtil;
import com.scaffold.system.dao.SysModuleFunctionActionMapper;
import com.scaffold.system.dao.SysUserAccountMapper;
import com.scaffold.system.dao.SysUserMapper;
import com.scaffold.system.entity.dto.SysUserEditDTO;
import com.scaffold.system.entity.dto.SysUserUpdatePasswordDTO;
import com.scaffold.system.entity.dto.UserPermissionsDTO;
import com.scaffold.system.entity.model.SysOrganization;
import com.scaffold.system.entity.model.SysUser;
import com.scaffold.system.entity.model.SysUserAccount;
import com.scaffold.system.entity.model.SysUserOrganization;
import com.scaffold.system.entity.query.SysUserQuery;
import com.scaffold.system.entity.vo.SysUserVO;
import com.scaffold.system.service.ISysOrganizationService;
import com.scaffold.system.service.ISysUserOrganizationService;
import com.scaffold.system.service.ISysUserRoleService;
import com.scaffold.system.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 系统用户，保存用户登录信息 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Autowired
    private SysModuleFunctionActionMapper sysModuleFunctionActionMapper;
    @Autowired
    private ISysUserOrganizationService sysUserOrganizationService;
    @Autowired
    private ISysUserRoleService sysUserRoleService;
    @Autowired
    private SysUserAccountMapper sysUserAccountMapper;
    @Autowired
    private ISysOrganizationService sysOrganizationService;

    /**
     * @描述 根据用户名或uid查询用户信息
     * @参数 usernameOrUid
     * @返回值: com.scaffold.system.entity.model.SysUser
     * @创建人 李亮
     * @创建时间 2022-07-07 11:06:24
     */
    @Override
    public SysUser getByUsernameOrUid(String usernameOrUid) {
        Wrapper<SysUser> query = new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getUsername, usernameOrUid)
                .or().eq(SysUser::getUid, usernameOrUid)
                .eq(SysUser::getState, YesNoEnum.YES.getVal());
        return getOne(query, true);
    }

    /**
     * @描述 根据用户ID获取用户的盐信息
     * @参数 userId
     * @返回值: com.scaffold.system.entity.model.SysUserSalt
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-17 13:27:14
     */
    @Override
    public SysUserAccount getSaltByUserId(Integer userId) {
        Wrapper<SysUserAccount> query = new LambdaQueryWrapper<SysUserAccount>()
                .eq(SysUserAccount::getUserId, userId);
        return sysUserAccountMapper.selectOne(query);
    }

    /**
     * 获取用户的权限集合
     *
     * @param userId
     * @return
     */
    @Override
    public List<UserPermissionsDTO> getUserPermissions(Integer userId) {
        return baseMapper.getUserPermissions(userId);
    }

    /**
     * 获取用户所有权限编码
     * @param userId
     * @return java.util.List<java.lang.String>
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-08-22 09:16:13
     */
    @Override
    public List<String> getUserAllPermissionCode(Integer userId) {
        return baseMapper.getUserAllPermissionCode(userId);
    }

    /**
     * 获取用户的权限集合
     *
     * @param sysUser
     * @return
     */
    @Override
    public List<UserPermissionsDTO> getUserPermissions(SysUser sysUser) {
        //超级用户获取所有权限
        if (UserEnum.Type.SUPPER.getVal().equals(sysUser.getUserType())) {
            return sysModuleFunctionActionMapper.getAllAction();
        }
        return getUserPermissions(sysUser.getUserId());
    }

    /**
     * @描述 新增用户
     * @参数 user
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-16 17:04:52
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addUser(SysUserEditDTO user) {
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(user, sysUser);
        sysUser.setUid(IdUtil.simpleUUID());

        SysUserAccount salt = new SysUserAccount();
        salt.setSalt(IdUtil.simpleUUID());
        Instant instant = Instant.ofEpochMilli(Instant.now().toEpochMilli() + PasswordUtil.EXPIRE_TIME_MILLI);
        salt.setPasswordExpireTime(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()));
        salt.setPasswordNeedReset(YesNoEnum.YES.getVal());

        //生成密码
        String content = user.getPassword() + salt.getSalt();
        String password = PasswordUtil.encodeMD5Password(content);
        sysUser.setPassword(password);
        //保存用户信息
        boolean result = save(sysUser);
        Assert.isTrue(result, "用户保存失败");

        salt.setUserId(sysUser.getUserId());
        int insertResult = sysUserAccountMapper.insert(salt);
        Assert.isTrue(insertResult >= 1, "用户保存失败");

        //保存组织架构
        if (StrUtil.isNotBlank(user.getOrgIds())) {
            List<Integer> orgIds = JSON.parseArray(user.getOrgIds(), Integer.class);
            sysUserOrganizationService.addUserOrg(sysUser.getUserId(), orgIds);
        }

        //保存角色信息
        if (StrUtil.isNotBlank(user.getRoleIds())) {
            List<Integer> roleIds = JSON.parseArray(user.getRoleIds(), Integer.class);
            sysUserRoleService.addUserRole(sysUser.getUserId(), roleIds);
        }
    }

    /**
     * @描述 新增用户
     * @参数 user
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-16 17:04:52
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateUser(SysUserEditDTO user) {
        SysUser sysUser = getById(user.getUserId());
        Assert.notNull(sysUser, "用户不存在");

        SysUser update = new SysUser();
        update.setUserId(user.getUserId());
        update.setFullName(user.getFullName());
        update.setMobile(user.getMobile());
        update.setMail(user.getMail());
        update.setState(user.getState());
        update.setUserType(user.getUserType());
        update.setDataScope(user.getDataScope());
        boolean result = updateById(update);
        Assert.isTrue(result, "用户信息修改失败");

        //保存组织架构
        List<Integer> orgIds = new ArrayList<>();
        if (StrUtil.isNotBlank(user.getOrgIds())) {
            orgIds = JSON.parseArray(user.getOrgIds(), Integer.class);
        }
        sysUserOrganizationService.addUserOrg(sysUser.getUserId(), orgIds);

        //保存角色信息
        List<Integer> roleIds = new ArrayList<>();
        if (StrUtil.isNotBlank(user.getRoleIds())) {
            roleIds = JSON.parseArray(user.getRoleIds(), Integer.class);
        }
        sysUserRoleService.addUserRole(sysUser.getUserId(), roleIds);
    }

    /**
     * @描述 根据uid获取用户信息
     * @参数 uid
     * @返回值: com.scaffold.system.entity.model.SysUser
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-17 14:12:05
     */
    @Override
    public SysUser getByUid(String uid) {
        Wrapper<SysUser> query = new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getUid, uid);
        return getOne(query);
    }

    /**
     * @描述 重置密码
     * @参数 uid
     * @返回值: java.lang.String
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-17 14:12:34
     */
    @Override
    public String resetPassword(String uid) {
        SysUser sysUser = getByUid(uid);
        Assert.notNull(sysUser, "用户不存在");

        //删除旧盐
        sysUserAccountMapper.deleteSaltByUserId(sysUser.getUserId());

        SysUserAccount salt = new SysUserAccount();
        salt.setSalt(IdUtil.simpleUUID());
        Instant instant = Instant.ofEpochMilli(Instant.now().toEpochMilli() + PasswordUtil.EXPIRE_TIME_MILLI);
        salt.setPasswordExpireTime(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()));
        salt.setPasswordNeedReset(YesNoEnum.YES.getVal());

        SysUser update = new SysUser();
        update.setUserId(sysUser.getUserId());
        //生成密码
        String content = PasswordUtil.randomPassword();
        String password = PasswordUtil.encodeMD5Password(content + salt.getSalt());
        sysUser.setPassword(password);

        //保存用户信息
        boolean result = updateById(sysUser);
        Assert.isTrue(result, "用户保存失败");

        salt.setUserId(sysUser.getUserId());
        int insertResult = sysUserAccountMapper.insert(salt);
        Assert.isTrue(insertResult >= 1, "用户保存失败");

        return content;
    }

    /**
     * @描述 修改密码
     * @参数 dto
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-18 13:58:20
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updatePassword(SysUserUpdatePasswordDTO dto) {
        Assert.isTrue(!StrUtil.equals(dto.getPassword(), dto.getNewPassword()), "新密码和旧密码不能相同");

        SysUser sysUser = getByUid(dto.getUid());
        Assert.notNull(sysUser, "用户不存在");

        //验证旧密码
        Wrapper<SysUserAccount> condition = new LambdaQueryWrapper<SysUserAccount>()
                .eq(SysUserAccount::getUserId, sysUser.getUserId());
        SysUserAccount userAccount = sysUserAccountMapper.selectOne(condition);
        String password = PasswordUtil.encodeMD5Password(dto.getPassword() + userAccount.getSalt());
        Assert.isTrue(StrUtil.equals(sysUser.getPassword(), password), "旧密码不正确");

        //更新盐值、密码过期时间和重置密码标识
        SysUserAccount salt = new SysUserAccount();
        salt.setSaltId(userAccount.getSaltId());
        salt.setSalt(IdUtil.simpleUUID());
        Instant instant = Instant.ofEpochMilli(Instant.now().toEpochMilli() + PasswordUtil.EXPIRE_TIME_MILLI);
        salt.setPasswordExpireTime(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()));
        salt.setPasswordNeedReset(YesNoEnum.NO.getVal());
        Assert.isTrue(sysUserAccountMapper.updateById(salt) > 0, "密码修改失败");

        //更新密码密文
        SysUser updateUser = new SysUser();
        updateUser.setUserId(sysUser.getUserId());
        updateUser.setPassword(PasswordUtil.encodeMD5Password(dto.getNewPassword() + salt.getSalt()));
        Assert.isTrue(updateById(updateUser), "密码修改失败");

    }

    /**
     * @描述 根据uid删除用户
     * @参数 uid
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-17 15:19:10
     */
    @Override
    public void removeByUid(String uid) {
        SysUser sysUser = getByUid(uid);
        Assert.notNull(sysUser, "用户不存在");
        boolean result = removeById(sysUser.getUserId());
        Assert.isTrue(result, "用户删除失败");
    }


    /**
     * @描述 用户分页列表
     * @参数 query
     * @返回值: com.baomidou.mybatisplus.core.metadata.IPage<com.scaffold.system.entity.vo.SysUserVO>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-17 15:36:40
     */
    @Override
    public IPage<SysUserVO> pageUser(SysUserQuery query) {
        query.setOrgIdList(listStringToInteger(query.getOrgIds()));
        query.setRoleIdList(listStringToInteger(query.getRoleIds()));
        Page<SysUserVO> userPage = new Page<>(query.getPage(), query.getSize());
        return baseMapper.pageUser(userPage, query);
    }

    private List<Integer> listStringToInteger(String content) {
        if (StrUtil.isBlank(content)) {
            return null;
        }
        List<String> strings = StrSplitter.split(content, CharUtil.COMMA, 0, true, true);
        List<Integer> integers = new ArrayList<>(strings.size());
        for (String string : strings) {
            integers.add(Integer.parseInt(string));
        }
        return integers;
    }

    /**
     * 获取用户的组织编码列表
     * @param userId 用户ID
     * @return java.util.List<java.lang.String>
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-02-15 15:44:02
     */
    @Override
    public List<String> getUserOrgCodes(Integer userId) {
        Wrapper<SysUserOrganization> orgQuery = new LambdaQueryWrapper<SysUserOrganization>()
                .eq(SysUserOrganization::getUserId, userId)
                .select(SysUserOrganization::getUserOrgCode);
        return sysUserOrganizationService.listObjs(orgQuery, String::valueOf);
    }

    /**
     * 获取用户所属的顶级组织编码列表
     * @param userId 用户ID
     * @return java.util.List<java.lang.String>
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-02-15 15:44:02
     */
    @Override
    public List<String> getUserTopOrgCodes(Integer userId) {
        Wrapper<SysUserOrganization> orgQuery = new LambdaQueryWrapper<SysUserOrganization>()
                .eq(SysUserOrganization::getUserId, userId)
                .select(SysUserOrganization::getUserOrgCode, SysUserOrganization::getOrgPath);
        List<SysUserOrganization> list = sysUserOrganizationService.list(orgQuery);
        List<String> topOrgCodes = new ArrayList<>();
        for (SysUserOrganization organization : list) {
            String[] split = organization.getOrgPath().split("\\|");
            topOrgCodes.add(split[0]);
        }
        return topOrgCodes;
    }

    /**
     * 根据组织编号，获取顶级组织编号
     * @param orgCode
     * @return
     */
    @Override
    public String getTopOrgCode(String orgCode) {
        SysOrganization organization = sysOrganizationService.getByOrgCode(orgCode);
        if (organization == null) {
            return "";
        }
        String[] split = organization.getOrgPath().split("\\|");
        return split[0];
    }

    /**
     * 更新用户当前组织
     * @param userId
     * @param orgCode
     * @return boolean
     * @exception
     * @version 1.0
     * @author 李亮
     * @time 2023-03-14 09:50:28
     */
    @Override
    public boolean updateUserCurrentOrgCode(Integer userId, String orgCode) {
        SysUser user = getById(userId);
        Asserts.notNull(user, "用户不存在或已停用");
        Asserts.isTrue(StateEnum.ENABLED.val() == user.getState(), "用户不存在或已停用");
        SysUser update = new SysUser();
        update.setUserId(userId);
        update.setCurrentOrgCode(orgCode);
        return updateById(update);
    }

}
