package com.scaffold.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysModuleMapper;
import com.scaffold.system.entity.dto.SysModuleDTO;
import com.scaffold.system.entity.model.SysModule;
import com.scaffold.system.service.ISysModuleService;
import com.scaffold.common.util.CodeUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * <p>
 * 系统模块 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@Service
public class SysModuleServiceImpl extends ServiceImpl<SysModuleMapper, SysModule> implements ISysModuleService {

    /**
     * @描述 新增模块
     * @参数 sysModule
     * @返回值: boolean
     * @创建人 李亮
     * @创建时间 2022-07-28 15:32:12
     */
    @Override
    public boolean addModule(SysModule sysModule) {
        //获取所有模块标识
        List<String> used = getAllPermissionCode();
        //获取模块标识
        String permissionCode = CodeUtil.module(CodeUtil.generate(used));

        //检查权限标识始发存在，如果存在再尝试生成一次
        Wrapper<SysModule> query = new LambdaQueryWrapper<SysModule>()
                .eq(SysModule::getPermissionCode, permissionCode);
        Long count = getBaseMapper().selectCount(query);
        Assert.isTrue(count <= 1L, "权限标识已存在");
        //创建模块
        SysModule module = new SysModule();
        setEditModule(sysModule, module);
        module.setPermissionCode(permissionCode);
        boolean result = save(module);
        Assert.isTrue(result, "新增模块失败！");
        //排序字段值使用自增ID
        Wrapper<SysModule> updateIndex = new UpdateWrapper<SysModule>()
                .lambda().setSql("module_index=module_id");
        return update(updateIndex);
    }

    /**
     * 修改模块
     *
     * @param sysModule
     * @return
     */
    @Override
    public boolean updateModule(SysModule sysModule) {
        SysModule update = new SysModule();
        update.setModuleId(sysModule.getModuleId());
        setEditModule(sysModule, update);
        return updateById(update);
    }

    private void setEditModule(SysModule source, SysModule target) {
        target.setModuleName(source.getModuleName());
        target.setSystemId(source.getSystemId());
        target.setModuleLogo(source.getModuleLogo());
        target.setModuleLink(source.getModuleLink());
        target.setState(source.getState());
    }

    /**
     * 获取所有模块权限编码
     *
     * @return
     */
    @Override
    public List<String> getAllPermissionCode() {
        Wrapper<SysModule> query = new LambdaQueryWrapper<SysModule>().select(SysModule::getPermissionCode);
        return listObjs(query, String::valueOf);
    }


    /**
     * @描述 修改排序
     * @参数 sourceId 需要修改的模块ID
     * @参数 targetId 目标位置的模块ID
     * @返回值: void
     * @创建人 李亮
     * @创建时间 2022-07-29 14:26:48
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateSort(Integer sourceId, Integer targetId) {
        Wrapper<SysModule> query = new LambdaQueryWrapper<SysModule>()
                .eq(SysModule::getModuleId, sourceId).
                select(SysModule::getModuleIndex, SysModule::getModuleId);
        SysModule source = getOne(query);
        Assert.notNull(source, "模块不存在");

        query = new LambdaQueryWrapper<SysModule>()
                .eq(SysModule::getModuleId, targetId).
                select(SysModule::getModuleIndex, SysModule::getModuleId);
        SysModule target = getOne(query);
        Assert.notNull(target, "模块不存在");

        source.setModuleId(targetId);
        boolean result = updateById(source);
        Assert.isTrue(result, "修改排序失败");

        target.setModuleId(sourceId);
        result = updateById(target);
        Assert.isTrue(result, "修改排序失败");
    }

    @Override
    public List<SysModuleDTO> getModuleFunction(int systemId) {
        return baseMapper.getModuleFunction(systemId);
    }

    @Override
    public List<SysModuleDTO> getUserModuleFunction(int systemId, List<String> actionPermissionCodes) {
        return baseMapper.getUserModuleFunction(systemId, actionPermissionCodes);
    }

}
