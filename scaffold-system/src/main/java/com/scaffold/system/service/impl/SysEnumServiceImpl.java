package com.scaffold.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysEnumMapper;
import com.scaffold.system.entity.model.SysEnum;
import com.scaffold.system.service.SysEnumService;
import org.springframework.stereotype.Service;
/**
 * 系统枚举-业务处理实现类
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-02-11 16:12:47
 */
@Service
public class SysEnumServiceImpl extends ServiceImpl<SysEnumMapper, SysEnum> implements SysEnumService {

}