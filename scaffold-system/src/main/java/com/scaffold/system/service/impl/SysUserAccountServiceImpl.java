package com.scaffold.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysUserAccountMapper;
import com.scaffold.system.entity.model.SysUserAccount;
import com.scaffold.system.service.ISysUserAccountService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户账户信息 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-08-17 12:13:03
 */
@Service
public class SysUserAccountServiceImpl extends ServiceImpl<SysUserAccountMapper, SysUserAccount> implements ISysUserAccountService {

}
