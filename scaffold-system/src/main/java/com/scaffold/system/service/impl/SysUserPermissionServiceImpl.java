package com.scaffold.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysUserPermissionMapper;
import com.scaffold.system.entity.model.SysUserPermission;
import com.scaffold.system.service.ISysUserPermissionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户权限 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@Service
public class SysUserPermissionServiceImpl extends ServiceImpl<SysUserPermissionMapper, SysUserPermission> implements ISysUserPermissionService {

}
