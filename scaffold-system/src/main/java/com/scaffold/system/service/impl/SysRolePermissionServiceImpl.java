package com.scaffold.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysRolePermissionMapper;
import com.scaffold.system.entity.model.SysRolePermission;
import com.scaffold.system.service.ISysRolePermissionService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统用户角色权限 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements ISysRolePermissionService {

    /**
     * @描述 获取角色权限列表
     * @参数 roleId
     * @返回值: java.util.List<com.scaffold.system.entity.model.SysRolePermission>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-10 15:55:00
     */
    @Override
    public List<SysRolePermission> getRolePermissions(Integer roleId) {
        return baseMapper.getRolePermissions(roleId);
    }

    /**
     * @描述 获取角色的所有权限编码
     * @参数 roleId
     * @返回值: java.util.List<java.lang.String>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-11 15:06:21
     */
    @Override
    public List<String> getActionPermissionCodeByRole(Integer roleId) {
        return baseMapper.getActionPermissionCodeByRole(roleId);
    }

}
