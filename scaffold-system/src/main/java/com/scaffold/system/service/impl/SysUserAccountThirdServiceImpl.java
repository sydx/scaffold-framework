package com.scaffold.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysUserAccountThirdMapper;
import com.scaffold.system.entity.model.SysUserAccountThird;
import com.scaffold.system.service.ISysUserAccountThirdService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户第三方平台账户信息 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-09-13 10:34:23
 */
@Service
public class SysUserAccountThirdServiceImpl extends ServiceImpl<SysUserAccountThirdMapper, SysUserAccountThird> implements ISysUserAccountThirdService {

}
