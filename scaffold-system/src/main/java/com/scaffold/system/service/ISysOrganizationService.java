package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.model.SysOrganization;
import com.scaffold.system.entity.vo.OrganizationTreeVO;

import java.util.List;

/**
 * <p>
 * 组织架构 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
public interface ISysOrganizationService extends IService<SysOrganization> {

    void addTopOrganization(SysOrganization organization);

    void addChildrenOrganization(SysOrganization organization);

    void updateOrganization(SysOrganization organization);

    void deleteOrganization(Integer orgId);

    List<OrganizationTreeVO> getOrganizationTree(Integer orgId);

    List<OrganizationTreeVO> getOrganizationTree(List<String> orgCodeList);

    List<SysOrganization> getByOrgCodes(List<String> orgCodeList);

    SysOrganization getByOrgCode(String orgCode);
}
