package com.scaffold.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysEnumItemMapper;
import com.scaffold.system.entity.model.SysEnumItem;
import com.scaffold.system.service.SysEnumItemService;
import org.springframework.stereotype.Service;
/**
 * 系统枚举项-业务处理实现类
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-02-11 16:12:47
 */
@Service
public class SysEnumItemServiceImpl extends ServiceImpl<SysEnumItemMapper, SysEnumItem> implements SysEnumItemService {

}