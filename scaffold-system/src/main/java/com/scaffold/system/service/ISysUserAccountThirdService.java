package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.model.SysUserAccountThird;

/**
 * <p>
 * 系统用户第三方平台账户信息 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-09-13 10:34:23
 */
public interface ISysUserAccountThirdService extends IService<SysUserAccountThird> {

}
