package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.model.SysUserToken;

/**
 * 系统用户访问凭证-业务处理接口
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-08-15 14:23:55
 */
public interface SysUserTokenService extends IService<SysUserToken>  {

    SysUserToken getTokenByUid(String uid);

    SysUserToken getAccessToken(String uid);

    SysUserToken getRefreshToken(String uid);

    void destroyToken(String uid);
}