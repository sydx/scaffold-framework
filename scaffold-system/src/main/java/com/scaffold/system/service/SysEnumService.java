package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.model.SysEnum;
/**
 * 系统枚举-业务处理接口
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-02-11 16:12:47
 */
public interface SysEnumService extends IService<SysEnum>  {

}