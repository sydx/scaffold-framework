package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.model.SysModuleFunction;
import com.scaffold.system.entity.vo.SysModuleFunctionVO;

import java.util.List;

/**
 * <p>
 * 系统模块下的功能 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
public interface ISysModuleFunctionService extends IService<SysModuleFunction> {
    List<String> getFunctionNames(String url, String method);

    void addTopFunction(SysModuleFunction function);

    void addChildFunction(SysModuleFunction function);

    void updateFunction(SysModuleFunction function);

    void updateSort(Integer sourceId, Integer targetId);

    List<SysModuleFunctionVO> getFunctionTree(Integer moduleId);

    void removeFunction(Integer functionId);

    void updateFunctionState(Integer functionId, Integer state);
}
