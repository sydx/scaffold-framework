package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.model.SysUserPermission;

/**
 * <p>
 * 用户权限 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
public interface ISysUserPermissionService extends IService<SysUserPermission> {

}
