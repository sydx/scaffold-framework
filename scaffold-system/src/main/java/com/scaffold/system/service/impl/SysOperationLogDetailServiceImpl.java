package com.scaffold.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysOperationLogDetailMapper;
import com.scaffold.system.entity.model.SysOperationLogDetail;
import com.scaffold.system.service.ISysOperationLogDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统日志 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-08-03 11:55:34
 */
@Service
public class SysOperationLogDetailServiceImpl extends ServiceImpl<SysOperationLogDetailMapper, SysOperationLogDetail> implements ISysOperationLogDetailService {

}
