package com.scaffold.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysRoleMapper;
import com.scaffold.system.dao.SysUserMapper;
import com.scaffold.system.dao.SysUserRoleMapper;
import com.scaffold.system.entity.model.SysRole;
import com.scaffold.system.entity.model.SysUser;
import com.scaffold.system.entity.model.SysUserRole;
import com.scaffold.system.entity.query.SysRoleQuery;
import com.scaffold.mybatisplus.factory.SFunctionFactory;
import com.scaffold.system.service.ISysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户角色 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysRoleMapper sysRoleMapper;

    /**
     * @描述 获取角色下的用户列表
     * @参数 roleId
     * @返回值: java.util.List<com.scaffold.system.entity.model.SysUser>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-11 15:56:30
     */
    public IPage<SysUser> getUserListByRole(SysRoleQuery query) {
        Page<SysUser> page = new Page<>(query.getPage(), query.getSize());
        return baseMapper.getUserListByRole(page, query);
    }

    /**
     * @描述 向角色中添加用户
     * @参数 roleId
     * @参数 userIds
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-12 17:18:45
     */
    @Override
    public void addRoleUser(Integer roleId, List<Integer> userIds) {
        //检查用户是否已经设置为当前角色
        Wrapper<SysUserRole> wrapper = new LambdaQueryWrapper<SysUserRole>()
                .eq(SysUserRole::getRoleId, roleId)
                .in(SysUserRole::getUserId, userIds)
                .select(SysUserRole::getUserId);
        List<Integer> roleExistUserIdList = listObjs(wrapper, SFunctionFactory.newMapper());

        //通过选择的用户ID和已存在的用户ID列表取差集获取没有添加过的用户ID
        List<Integer> roleNotExistUserIdList = CollUtil.subtractToList(userIds, roleExistUserIdList);
        Assert.notEmpty(roleNotExistUserIdList, "用户已存在角色中");

        //过滤不存在的用户
        Wrapper<SysUser> userQuery = new LambdaQueryWrapper<SysUser>()
                .in(SysUser::getUserId, roleNotExistUserIdList)
                .select(SysUser::getUserId);
        List<Integer> existUserIdList = sysUserMapper.selectList(userQuery)
                .stream().filter(Objects::nonNull)
                .map(SysUser::getUserId)
                .collect(Collectors.toList());
        Assert.notEmpty(existUserIdList, "用户信息不存在");

        //执行添加
        List<SysUserRole> sysUserRoleList = new ArrayList<>(existUserIdList.size());
        SysUserRole sysUserRole = null;
        for (Integer userId : existUserIdList) {
            sysUserRole = new SysUserRole();
            sysUserRole.setRoleId(roleId);
            sysUserRole.setUserId(userId);
            sysUserRoleList.add(sysUserRole);
        }
        boolean result = saveBatch(sysUserRoleList);
        Assert.isTrue(result, "添加用户失败！");
    }

    /**
     * @描述 向用户设置角色
     * @参数 roleId
     * @参数 userIds
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-12 17:18:45
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addUserRole(Integer userId, List<Integer> roleIds) {
        Wrapper<SysUserRole> wrapper = new LambdaQueryWrapper<SysUserRole>()
                .eq(SysUserRole::getUserId, userId);
        remove(wrapper);

        if (CollUtil.isEmpty(roleIds)) {
            return;
        }

        Wrapper<SysRole> query = new LambdaQueryWrapper<SysRole>()
                .in(SysRole::getRoleId, roleIds);
        List<SysRole> roles = sysRoleMapper.selectList(query);
        if (CollUtil.isEmpty(roles)) {
            return;
        }

        //执行添加
        List<SysUserRole> sysUserRoleList = new ArrayList<>(roles.size());
        SysUserRole sysUserRole = null;
        for (SysRole role : roles) {
            sysUserRole = new SysUserRole();
            sysUserRole.setRoleId(role.getRoleId());
            sysUserRole.setUserId(userId);
            sysUserRoleList.add(sysUserRole);
        }
        boolean result = saveBatch(sysUserRoleList);
        Assert.isTrue(result, "设置用户角色失败！");
    }

}
