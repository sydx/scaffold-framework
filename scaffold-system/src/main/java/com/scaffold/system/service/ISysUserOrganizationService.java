package com.scaffold.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.model.SysUserOrganization;
import com.scaffold.system.entity.query.SysOrganizationQuery;
import com.scaffold.system.entity.vo.OrganizationUserVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 用户组织 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-08-15 11:41:05
 */
public interface ISysUserOrganizationService extends IService<SysUserOrganization> {

    IPage<OrganizationUserVO> getOrgUsers(SysOrganizationQuery query);

    void addOrgUser(Integer orgId, List<Integer> userIds);

    @Transactional(rollbackFor = Exception.class)
    void addUserOrg(Integer userId, List<Integer> orgIds);
}
