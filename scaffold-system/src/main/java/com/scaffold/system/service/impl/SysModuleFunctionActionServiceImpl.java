package com.scaffold.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.common.constants.CacheNameConstant;
import com.scaffold.common.enums.YesNoEnum;
import com.scaffold.common.util.CodeUtil;
import com.scaffold.system.dao.SysModuleFunctionActionMapper;
import com.scaffold.system.dao.SysModuleFunctionMapper;
import com.scaffold.system.dao.SysModuleMapper;
import com.scaffold.system.entity.dto.ModuleActionDTO;
import com.scaffold.system.entity.dto.PermissionDTO;
import com.scaffold.system.entity.dto.UserPermissionsDTO;
import com.scaffold.system.entity.model.SysModule;
import com.scaffold.system.entity.model.SysModuleFunction;
import com.scaffold.system.entity.model.SysModuleFunctionAction;
import com.scaffold.system.entity.vo.PermissionTreeVO;
import com.scaffold.system.service.ISysModuleFunctionActionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 功能的操作点 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@Service
public class SysModuleFunctionActionServiceImpl extends ServiceImpl<SysModuleFunctionActionMapper, SysModuleFunctionAction> implements ISysModuleFunctionActionService {


    @Autowired
    private SysModuleMapper sysModuleMapper;
    @Autowired
    private SysModuleFunctionMapper sysModuleFunctionMapper;

    /**
     * @描述 获取所有动作点
     * @参数
     * @返回值: java.util.List<com.scaffold.system.entity.dto.UserPermissionsDTO>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-10 16:10:38
     */
    @Override
    public List<UserPermissionsDTO> getAllAction() {
        return baseMapper.getAllAction();
    }

    /**
     * @描述 新增操作
     * @参数 action
     * @返回值: void
     * @创建人 李亮
     * @创建时间 2022-07-30 14:11:12
     */
    @Override
    @CacheEvict(value = CacheNameConstant.SYS_PERMISSION_CODE_URL, key = "'all'")
    public void addAction(SysModuleFunctionAction action) {
        SysModule module = sysModuleMapper.selectById(action.getModuleId());
        Assert.notNull(module, "模块不存在");

        SysModuleFunction function = sysModuleFunctionMapper.selectById(action.getFunctionId());
        Assert.notNull(function, "功能不存在");

        //获取所有模块标识
        List<String> used = getAllPermissionCode(function.getFunctionId());
        //获取模块标识
        String permissionCode = function.getPermissionCode() + CodeUtil.action(CodeUtil.generate(used));

        //检查权限标识始发存在，如果存在再尝试生成一次
        Wrapper<SysModuleFunctionAction> query = new LambdaQueryWrapper<SysModuleFunctionAction>()
                .eq(SysModuleFunctionAction::getPermissionCode, permissionCode);
        Long count = baseMapper.selectCount(query);
        Assert.isTrue(count <= 1L, "权限标识已存在");

        //创建模块
        SysModuleFunctionAction saveAction = new SysModuleFunctionAction();
        saveAction.setActionName(action.getActionName());
        saveAction.setModuleId(action.getModuleId());
        saveAction.setFunctionId(action.getFunctionId());
        saveAction.setActionType(action.getActionType());
        saveAction.setUrl(action.getUrl());
        saveAction.setState(action.getState());
        saveAction.setPermissionCode(permissionCode);
        boolean result = save(saveAction);
        Assert.isTrue(result, "新增操作失败！");
    }

    private List<String> getAllPermissionCode(Integer functionId) {
        Wrapper<SysModuleFunctionAction> query = new LambdaQueryWrapper<SysModuleFunctionAction>()
                .eq(SysModuleFunctionAction::getFunctionId, functionId)
                .select(SysModuleFunctionAction::getPermissionCode);
        return listObjs(query, String::valueOf);
    }

    /**
     * @描述 修改操作
     * @参数 action
     * @返回值: void
     * @创建人 李亮
     * @创建时间 2022-07-30 14:11:25
     */
    @Override
    @CacheEvict(value = CacheNameConstant.SYS_PERMISSION_CODE_URL, key = "'all'")
    public void updateAction(SysModuleFunctionAction action) {
        SysModuleFunctionAction saveAction = new SysModuleFunctionAction();
        saveAction.setActionId(action.getActionId());
        saveAction.setActionName(action.getActionName());
        saveAction.setActionType(action.getActionType());
        saveAction.setUrl(action.getUrl());
        saveAction.setState(action.getState());
        boolean result = updateById(saveAction);
        Assert.isTrue(result, "修改操作失败！");
    }

    /**
     * @描述 获取模块下的所有动作点
     * @参数 moduleId
     * @返回值: java.util.List<com.scaffold.system.entity.dto.ModuleActionDTO>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-10 16:10:29
     */
    @Override
    public List<ModuleActionDTO> getActionByModule(Integer moduleId) {
        return baseMapper.getActionByModule(moduleId);
    }

    /**
     * @描述 查询模块下的权限树
     * @参数 moduleId
     * @返回值: java.util.List<com.scaffold.system.entity.vo.PermissionTreeVO>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-10 16:52:01
     */
    @Override
    public List<PermissionTreeVO> getPermissionTreeByModule() {
        List<SysModule> modules = sysModuleMapper.selectList(Wrappers.emptyWrapper());
        List<PermissionTreeVO> tree = new ArrayList<>();
        for (SysModule module : modules) {
            List<ModuleActionDTO> actionList = getActionByModule(module.getModuleId());
            if (CollUtil.isEmpty(actionList)) {
                continue;
            }
            List<PermissionTreeVO> moduleTree = getPermissionTree(0, actionList);
            if (CollUtil.isEmpty(moduleTree)) {
                continue;
            }
            tree.addAll(moduleTree);
        }
        return tree;
    }

    /**
     * @描述 查询模块下的权限树
     * @参数 moduleId
     * @返回值: java.util.List<com.scaffold.system.entity.vo.PermissionTreeVO>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-10 16:52:01
     */
    @Override
    public List<PermissionTreeVO> getPermissionTreeByModule(Integer moduleId) {
        if (null == moduleId) {
            return getPermissionTreeByModule();
        }
        List<ModuleActionDTO> actionList = getActionByModule(moduleId);
        return getPermissionTree(0, actionList);
    }

    /**
     * @描述 递归获取功能树
     * @参数 parentId
     * @参数 list
     * @返回值: java.util.List<com.scaffold.system.entity.vo.PermissionTreeVO>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-10 16:53:02
     */
    private List<PermissionTreeVO> getPermissionTree(Integer parentId, List<ModuleActionDTO> list) {
        List<PermissionTreeVO> children = new ArrayList<>();
        String[] ignoreProperties = {"actionId", "actionName"};
        for (ModuleActionDTO actionDTO : list) {
            Optional<PermissionTreeVO> optional = children.stream()
                    .filter(o -> o.getFunctionId().equals(actionDTO.getFunctionId())).findFirst();
            if (!optional.isPresent()
                    && parentId.equals(actionDTO.getFunctionParentId())) {
                PermissionTreeVO functionChild = new PermissionTreeVO();
                BeanUtils.copyProperties(actionDTO, functionChild, ignoreProperties);
                functionChild.setState(actionDTO.getFunctionState());
                functionChild.setNodeId(actionDTO.getFunctionCode());
                functionChild.setNodeLabel(actionDTO.getFunctionName());
                functionChild.setNodeType(YesNoEnum.NO.getVal());
                List<PermissionTreeVO> functionChildren = getPermissionTree(actionDTO.getFunctionId(), list);
                //没有子功能，查询操作点
                if (CollUtil.isEmpty(functionChildren)) {
                    functionChildren = getActionChildren(actionDTO.getFunctionId(), list);
                }
                functionChild.setChildren(functionChildren);
                children.add(functionChild);
            }
        }
        children.sort(Comparator.comparingInt(PermissionTreeVO::getFunctionIndex));
        return children;
    }

    /**
     * @描述 获取功能下的操作点
     * @参数 functionId
     * @参数 list
     * @返回值: java.util.List<com.scaffold.system.entity.vo.PermissionTreeVO>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-10 16:53:23
     */
    private List<PermissionTreeVO> getActionChildren(Integer functionId, List<ModuleActionDTO> list) {
        List<PermissionTreeVO> children = new ArrayList<>();
        for (ModuleActionDTO actionDTO : list) {
            Optional<PermissionTreeVO> optional = children.stream()
                    .filter(o -> o.getActionId().equals(actionDTO.getActionId())).findFirst();
            if (!optional.isPresent()
                    && functionId.equals(actionDTO.getFunctionId())) {
                PermissionTreeVO action = new PermissionTreeVO();
                BeanUtils.copyProperties(actionDTO, action);
                action.setState(actionDTO.getActionState());
                action.setNodeId(actionDTO.getActionCode());
                action.setNodeLabel(actionDTO.getActionName());
                action.setNodeType(YesNoEnum.YES.getVal());
                children.add(action);
            }
        }
        return children;
    }

    /**
     * 删除功能
     *
     * @param actionId
     */
    @Override
    @CacheEvict(value = CacheNameConstant.SYS_PERMISSION_CODE_URL, key = "'all'")
    public void removeAction(Integer actionId) {
        boolean update = removeById(actionId);
        Assert.isTrue(update, "删除失败！");
    }

    /**
     * 修改功能状态
     *
     * @param actionId
     * @param state
     */
    @Override
    @CacheEvict(value = CacheNameConstant.SYS_PERMISSION_CODE_URL, key = "'all'")
    public void updateActionState(Integer actionId, Integer state) {
        SysModuleFunctionAction update = new SysModuleFunctionAction();
        update.setActionId(actionId);
        update.setState(state);
        boolean result = updateById(update);
        Assert.isTrue(result, "状态修改失败！");
    }

    /**
     * @描述 获取所有权限。所有的function和action的地址和权限编码
     * @返回值: java.util.List<com.scaffold.system.entity.vo.PermissionTreeVO>
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-23 22:40:23
     */
    @Override
    @Cacheable(value = CacheNameConstant.SYS_PERMISSION_CODE_URL, key = "'all'")
    public List<PermissionDTO> getAllPermission() {
//        Wrapper<SysModuleFunction> functionWrapper = new LambdaQueryWrapper<SysModuleFunction>()
//                .eq(SysModuleFunction::getState, YesNoEnum.YES.getVal())
//                .select(SysModuleFunction::getUrl, SysModuleFunction::getPermissionCode);
//        List<SysModuleFunction> functions = sysModuleFunctionMapper.selectList(functionWrapper);

        Wrapper<SysModuleFunctionAction> actionWrapper = new LambdaQueryWrapper<SysModuleFunctionAction>()
                .eq(SysModuleFunctionAction::getState, YesNoEnum.YES.getVal())
                .select(SysModuleFunctionAction::getUrl, SysModuleFunctionAction::getPermissionCode, SysModuleFunctionAction::getActionType);
        List<SysModuleFunctionAction> actions = list(actionWrapper);

        List<PermissionDTO> permissionDTOList = new ArrayList<>(actions.size());
//        for (SysModuleFunction function : functions) {
//            permissionDTOList.add(new PermissionDTO(function));
//        }
        for (SysModuleFunctionAction action : actions) {
            permissionDTOList.add(new PermissionDTO(action));
        }
        return permissionDTOList;
    }
}
