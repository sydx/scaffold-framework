package com.scaffold.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.dto.SysUserEditDTO;
import com.scaffold.system.entity.dto.SysUserUpdatePasswordDTO;
import com.scaffold.system.entity.dto.UserModuleFunctionDTO;
import com.scaffold.system.entity.dto.UserPermissionsDTO;
import com.scaffold.system.entity.model.SysUser;
import com.scaffold.system.entity.model.SysUserAccount;
import com.scaffold.system.entity.query.SysUserQuery;
import com.scaffold.system.entity.vo.SysUserVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 系统用户，保存用户登录信息 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
public interface ISysUserService extends IService<SysUser> {

    SysUser getByUsernameOrUid(String username);

    SysUserAccount getSaltByUserId(Integer userId);

    List<UserPermissionsDTO> getUserPermissions(Integer userId);

    List<String> getUserAllPermissionCode(Integer userId);

    List<UserPermissionsDTO> getUserPermissions(SysUser sysUser);

    @Transactional(rollbackFor = Exception.class)
    void addUser(SysUserEditDTO user);

    @Transactional(rollbackFor = Exception.class)
    void updateUser(SysUserEditDTO user);

    SysUser getByUid(String uid);

    String resetPassword(String uid);

    @Transactional(rollbackFor = Exception.class)
    void updatePassword(SysUserUpdatePasswordDTO dto);

    void removeByUid(String uid);

    IPage<SysUserVO> pageUser(SysUserQuery query);

    List<String> getUserOrgCodes(Integer userId);

    List<String> getUserTopOrgCodes(Integer userId);

    String getTopOrgCode(String orgCode);

    boolean updateUserCurrentOrgCode(Integer userId, String orgCode);
}
