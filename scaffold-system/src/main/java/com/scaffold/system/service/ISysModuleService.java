package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.dto.SysModuleDTO;
import com.scaffold.system.entity.model.SysModule;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 系统模块 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
public interface ISysModuleService extends IService<SysModule> {

    boolean addModule(SysModule sysModule);

    boolean updateModule(SysModule sysModule);

    List<String> getAllPermissionCode();

    @Transactional(rollbackFor = Exception.class)
    void updateSort(Integer sourceId, Integer targetId);

    List<SysModuleDTO> getModuleFunction(int systemId);

    List<SysModuleDTO> getUserModuleFunction(int systemId, List<String> actionPermissionCodes);
}
