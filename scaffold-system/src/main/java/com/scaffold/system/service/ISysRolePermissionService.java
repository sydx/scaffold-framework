package com.scaffold.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffold.system.entity.model.SysRolePermission;

import java.util.List;

/**
 * <p>
 * 系统用户角色权限 服务类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
public interface ISysRolePermissionService extends IService<SysRolePermission> {

    List<SysRolePermission> getRolePermissions(Integer roleId);

    List<String> getActionPermissionCodeByRole(Integer roleId);
}
