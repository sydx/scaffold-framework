package com.scaffold.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.system.dao.SysRoleMapper;
import com.scaffold.system.entity.dto.SysRoleEditDTO;
import com.scaffold.system.entity.model.SysModuleFunctionAction;
import com.scaffold.system.entity.model.SysRole;
import com.scaffold.system.entity.model.SysRolePermission;
import com.scaffold.system.service.ISysModuleFunctionActionService;
import com.scaffold.system.service.ISysRolePermissionService;
import com.scaffold.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 系统用户角色 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Autowired
    private ISysRolePermissionService sysRolePermissionService;
    @Autowired
    private ISysModuleFunctionActionService sysModuleFunctionActionService;

    /**
     * @描述 新增角色
     * @参数 role
     * @参数 actionIds
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-11 13:50:08
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addRole(SysRoleEditDTO role) {
        checkRoleName(role.getRoleName());
        SysRole add = new SysRole();
        add.setRoleName(role.getRoleName());
        add.setState(role.getState());
        add.setRemark(role.getRemark());
        boolean result = save(add);
        Assert.isTrue(result, "新增角色失败！");

        if (StrUtil.isNotBlank(role.getActionIds())) {
            List<Integer> actionIds = JSON.parseArray(role.getActionIds(), Integer.class);
            setRolePermission(actionIds, add.getRoleId());
        }

    }

    /**
     * @描述 修改角色
     * @参数 role
     * @参数 actionIds
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-11 13:50:35
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateRole(SysRoleEditDTO role) {
        SysRole sysRole = getById(role.getRoleId());
        Assert.notNull(sysRole, "角色不存在！");
        //修改角色名需要检查是否已存在
        if (!StrUtil.equals(role.getRoleName(), sysRole.getRoleName())) {
            checkRoleName(role.getRoleName());
        }
        //修改角色信息
        doUpdateRole(role);
        //删除已存在的权限
        deleteRolePermission(role.getRoleId());
        //重新设置权限
        if (StrUtil.isNotBlank(role.getActionIds())) {
            List<Integer> actionIds = JSON.parseArray(role.getActionIds(), Integer.class);
            setRolePermission(actionIds, role.getRoleId());
        }
    }

    protected void deleteRolePermission(Integer roleId) {
        Wrapper<SysRolePermission> wrapper = new LambdaQueryWrapper<SysRolePermission>()
                .eq(SysRolePermission::getRoleId, roleId);
        long count = sysRolePermissionService.count(wrapper);
        if (count > 0) {
            boolean result = sysRolePermissionService.remove(wrapper);
            Assert.isTrue(result, "删除角色已存在的权限失败！");
        }
    }

    protected void doUpdateRole(SysRoleEditDTO role) {
        SysRole update = new SysRole();
        update.setRoleId(role.getRoleId());
        update.setRoleName(role.getRoleName());
        update.setState(role.getState());
        update.setRemark(role.getRemark());
        boolean result = updateById(update);
        Assert.isTrue(result, "修改角色失败！");
    }

    private void checkRoleName(String roleName) {
        Wrapper<SysRole> wrapper = new LambdaQueryWrapper<SysRole>()
                .eq(SysRole::getRoleName, roleName);
        SysRole sysRole = getOne(wrapper);
        Assert.isNull(sysRole, "角色名称已经存在");
    }

    protected void setRolePermission(List<Integer> actionIds, Integer roleId) {
        if (CollUtil.isEmpty(actionIds)) {
            return;
        }
        List<SysModuleFunctionAction> actions = sysModuleFunctionActionService.listByIds(actionIds);
        if (CollUtil.isEmpty(actions)) {
            return;
        }
        List<SysRolePermission> permissions = convert(roleId, actions);
        boolean result = sysRolePermissionService.saveBatch(permissions);
        Assert.isTrue(result, "设置角色权限失败！");
    }

    private List<SysRolePermission> convert(Integer roleId, List<SysModuleFunctionAction> actions) {
        List<SysRolePermission> permissions = new ArrayList<>();
        SysRolePermission permission = null;
        for (SysModuleFunctionAction action : actions) {
            permission = new SysRolePermission();
            permission.setRoleId(roleId);
            permission.setActionId(action.getActionId());
            permission.setFunctionId(action.getFunctionId());
            permission.setModuleId(action.getModuleId());
            permissions.add(permission);
        }
        return permissions;
    }


    /**
     * @描述 删除角色
     * @参数 roleId
     * @返回值: void
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-11 14:44:49
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteRole(Integer roleId) {
        SysRole role = getById(roleId);
        Assert.notNull(role, "角色不存在");
        boolean result = removeById(roleId);
        Assert.isTrue(result, "删除角色失败");
        deleteRolePermission(roleId);
    }

}
