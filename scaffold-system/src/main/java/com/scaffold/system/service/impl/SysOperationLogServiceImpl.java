package com.scaffold.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffold.common.enums.LogTypeEnum;
import com.scaffold.system.dao.SysOperationLogMapper;
import com.scaffold.system.entity.dto.SysOperationLogDTO;
import com.scaffold.system.entity.model.SysOperationLog;
import com.scaffold.system.entity.model.SysOperationLogDetail;
import com.scaffold.system.service.ISysModuleFunctionService;
import com.scaffold.system.service.ISysOperationLogDetailService;
import com.scaffold.system.service.ISysOperationLogService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * <p>
 * 系统日志 服务实现类
 * </p>
 *
 * @author 李亮
 * @since 2022-08-03 11:55:34
 */
@Service
public class SysOperationLogServiceImpl extends ServiceImpl<SysOperationLogMapper, SysOperationLog> implements ISysOperationLogService {

    @Autowired
    private ISysOperationLogDetailService sysOperationLogDetailService;
    @Autowired
    private ISysModuleFunctionService sysModuleFunctionService;

    /**
     * @描述 新增日志
     * @参数 dto
     * @返回值: void
     * @创建人 李亮
     * @创建时间 2022-08-03 11:46:50
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addLog(SysOperationLogDTO dto) {

        if (dto.isNeedGetOptionName()) {
            List<String> names = sysModuleFunctionService.getFunctionNames(dto.getRequestUrl(), dto.getRequestMethod());
            if (CollUtil.isNotEmpty(names)) {
                dto.setOperation(CollUtil.join(names, StrUtil.COMMA));
            } else {
                dto.setOperation(LogTypeEnum.OPERATION.getLabel());
            }
        }

        SysOperationLog log = new SysOperationLog();
        BeanUtils.copyProperties(dto, log);
        boolean result = save(log);
        Assert.isTrue(result, "日志保存失败！");

        if (null != dto.getDetail()) {
            addLogDetail(dto, log);
        }

    }

    public void addLogDetail(SysOperationLogDTO dto, SysOperationLog log) {
        boolean result;
        SysOperationLogDetail detail = new SysOperationLogDetail();
        BeanUtils.copyProperties(dto.getDetail(), detail);
        BeanUtils.copyProperties(log, detail);
        detail.setLogId(log.getLogId());
        result = sysOperationLogDetailService.save(detail);
        Assert.isTrue(result, "日志详情保存失败！");
    }
}
