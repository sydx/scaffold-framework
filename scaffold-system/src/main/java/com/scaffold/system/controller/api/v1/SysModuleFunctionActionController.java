package com.scaffold.system.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.scaffold.common.result.R;
import com.scaffold.system.entity.model.SysModuleFunctionAction;
import com.scaffold.system.entity.vo.PermissionTreeVO;
import com.scaffold.system.service.ISysModuleFunctionActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 功能的操作点 前端控制器
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@RestController
@RequestMapping("/api/v1/system/actions")
public class SysModuleFunctionActionController {

    @Autowired
    private ISysModuleFunctionActionService sysModuleFunctionActionService;

    @GetMapping
    public R<List<SysModuleFunctionAction>> list(SysModuleFunctionAction query) {
        Wrapper<SysModuleFunctionAction> wrapper = new LambdaQueryWrapper<SysModuleFunctionAction>()
                .eq(null != query.getFunctionId(), SysModuleFunctionAction::getFunctionId, query.getFunctionId())
                .eq(null != query.getModuleId(), SysModuleFunctionAction::getModuleId, query.getModuleId())
                .eq(null != query.getActionId(), SysModuleFunctionAction::getActionId, query.getActionId())
                .orderByAsc(SysModuleFunctionAction::getActionId);
        List<SysModuleFunctionAction> list = sysModuleFunctionActionService.list(wrapper);
        return R.success(list);
    }


    @PostMapping
    public R add(@RequestBody @Validated SysModuleFunctionAction action) {
        sysModuleFunctionActionService.addAction(action);
        return R.success();
    }

    @PutMapping
    public R edit(@RequestBody @Validated SysModuleFunctionAction action) {
        sysModuleFunctionActionService.updateAction(action);
        return R.success();
    }


    @DeleteMapping("/{actionId}")
    public R delete(@PathVariable Integer actionId) {
        Assert.notNull(actionId, "操作ID不能为空");
        sysModuleFunctionActionService.removeAction(actionId);
        return R.success();
    }

    @PatchMapping("/state/{actionId}/{state}")
    public R state(@PathVariable Integer actionId, @PathVariable Integer state) {
        Assert.notNull(actionId, "操作ID不能为空");
        Assert.notNull(state, "状态不能为空");
        sysModuleFunctionActionService.updateActionState(actionId, state);
        return R.success();
    }

    @GetMapping("/permissionTree")
    public R<List<PermissionTreeVO>> getPermissionTree(Integer moduleId) {
        return R.success(sysModuleFunctionActionService.getPermissionTreeByModule(moduleId));
    }

}
