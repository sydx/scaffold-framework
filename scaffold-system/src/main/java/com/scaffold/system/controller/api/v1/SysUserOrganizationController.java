package com.scaffold.system.controller.api.v1;

import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.scaffold.common.result.R;
import com.scaffold.system.entity.model.SysUserOrganization;
import com.scaffold.system.entity.query.SysOrganizationQuery;
import com.scaffold.system.service.ISysUserOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户组织 前端控制器
 * </p>
 *
 * @author 李亮
 * @since 2022-08-15 11:41:05
 */
@RestController
@RequestMapping("/api/v1/system/user/organizations")
public class SysUserOrganizationController {

    @Autowired
    ISysUserOrganizationService sysUserOrganizationService;

    /**
     * @描述 获取组织用户列表
     * @参数 orgId
     * @返回值: com.scaffold.common.result.R
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-15 11:44:54
     */
    @GetMapping("/{orgId}/users")
    public R getOrgUsers(@PathVariable Integer orgId, SysOrganizationQuery query) {
        query.setOrgId(orgId);
        return R.success(sysUserOrganizationService.getOrgUsers(query));
    }

    /**
     * @描述 获取组织用户列表
     * @参数 orgId
     * @返回值: com.scaffold.common.result.R
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-15 11:44:54
     */
    @PostMapping("/{orgId}/users")
    public R addOrgUsers(@PathVariable Integer orgId, @RequestBody String userIds) {
        List<Integer> userIdsList = getUserIdsList(userIds);
        sysUserOrganizationService.addOrgUser(orgId, userIdsList);
        return R.success();
    }

    /**
     * @描述 删除组织用户
     * @参数 orgId
     * @返回值: com.scaffold.common.result.R
     * @异常
     * @创建人 李亮
     * @创建时间 2022-08-15 11:44:54
     */
    @DeleteMapping("/remove")
    public R deleteOrgUsers(@RequestParam String userOrgIds) {
        Assert.hasText(userOrgIds, "请选择用户");
        int[] userOrgIdsList = StrUtil.splitToInt(userOrgIds, CharUtil.COMMA);
        List<Integer> idList = new ArrayList<>(userOrgIdsList.length);
        for (int i : userOrgIdsList) {
            idList.add(i);
        }
        Wrapper<SysUserOrganization> condition = new LambdaQueryWrapper<SysUserOrganization>()
                .in(SysUserOrganization::getUserOrgId, idList);
        sysUserOrganizationService.remove(condition);
        return R.success();
    }

    private static List<Integer> getUserIdsList(String userIds) {
        String message = "请选择用户";
        Assert.hasText(userIds, message);
        JSONObject jsonObject = JSON.parseObject(userIds);
        Assert.notNull(jsonObject, message);
        JSONArray jsonArray = jsonObject.getJSONArray("userIds");
        Assert.notEmpty(jsonArray, message);
        String jsonString = jsonArray.toJSONString();
        return JSON.parseArray(jsonString, Integer.class);
    }

}
