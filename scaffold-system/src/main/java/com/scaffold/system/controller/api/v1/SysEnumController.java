package com.scaffold.system.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scaffold.common.asserts.Asserts;
import com.scaffold.common.result.R;
import com.scaffold.system.entity.model.SysEnum;
import com.scaffold.system.entity.model.SysEnumItem;
import com.scaffold.system.entity.query.SysEnumQuery;
import com.scaffold.mybatisplus.factory.WrapperFactory;
import com.scaffold.system.service.SysEnumItemService;
import com.scaffold.system.service.SysEnumService;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 系统枚举-接口控制器
 *
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-02-11 16:12:47
 */
@RestController
@RequestMapping("/api/v1/system/enum")
public class SysEnumController {

    private final SysEnumService sysEnumService;
    private final SysEnumItemService sysEnumItemService;

    public SysEnumController(SysEnumService sysEnumService, SysEnumItemService sysEnumItemService) {
        this.sysEnumService = sysEnumService;
        this.sysEnumItemService = sysEnumItemService;
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping
    public R<Page<SysEnum>> list(SysEnumQuery query) {
        WrapperFactory<SysEnum> factory = WrapperFactory.build();
        LambdaQueryWrapper<SysEnum> queryWrapper = factory.query(query).lambda()
                .orderByAsc(SysEnum::getEnumId);
        Page<SysEnum> page = new Page<>(query.getPage(), query.getSize());
        sysEnumService.page(page, queryWrapper);
        return R.success(page);
    }

    /**
     * 新增
     *
     * @return
     */
    @PostMapping
    public R add(@RequestBody @Validated SysEnum sysEnum) {
        boolean update = sysEnumService.save(sysEnum);
        Assert.isTrue(update, "新增失败！");
        return R.success();
    }

    /**
     * 修改
     *
     * @return
     */
    @PutMapping
    public R update(@RequestBody @Validated SysEnum sysEnum) {
        boolean update = sysEnumService.updateById(sysEnum);
        Assert.isTrue(update, "修改失败！");
        return R.success();
    }

    /**
     * 删除
     *
     * @return
     */
    @DeleteMapping("/{enumId}")
    public R delete(@PathVariable Integer enumId) {
        Assert.notNull(enumId, "enumId不能为空");
        SysEnum sysEnum = sysEnumService.getById(enumId);
        Assert.notNull(sysEnum, "枚举不存在");
        Wrapper<SysEnumItem> wrapper = new LambdaQueryWrapper<SysEnumItem>()
                .eq(SysEnumItem::getEnumCode, sysEnum.getEnumCode());
        long count = sysEnumItemService.count(wrapper);
        Asserts.isTrue(count == 0, "请先删除枚举项");
        boolean update = sysEnumService.removeById(enumId);
        Assert.isTrue(update, "删除失败！");
        return R.success();
    }

    /**
     * 修改状态
     *
     * @return
     */
    @PatchMapping("/state/{enumId}/{state}")
    public R state1(@PathVariable Integer enumId, @PathVariable Integer state) {
        Assert.notNull(enumId, "enumId不能为空");
        Assert.notNull(state, "状态不能为空");
        SysEnum sysEnum = sysEnumService.getById(enumId);
        Assert.notNull(sysEnum, "数据不存在");
        SysEnum update = new SysEnum();
        update.setEnumId(sysEnum.getEnumId());
        update.setState(state);
        boolean result = sysEnumService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }

}