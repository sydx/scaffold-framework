package com.scaffold.system.controller.api.v1;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.scaffold.common.result.R;
import com.scaffold.common.validation.group.Add;
import com.scaffold.common.validation.group.Update;
import com.scaffold.system.entity.dto.SysUserEditDTO;
import com.scaffold.system.entity.dto.SysUserUpdatePasswordDTO;
import com.scaffold.system.entity.model.SysUser;
import com.scaffold.system.entity.model.SysUserOrganization;
import com.scaffold.system.entity.model.SysUserRole;
import com.scaffold.system.entity.query.SysUserQuery;
import com.scaffold.system.entity.vo.SysUserVO;
import com.scaffold.mybatisplus.factory.SFunctionFactory;
import com.scaffold.system.service.ISysUserOrganizationService;
import com.scaffold.system.service.ISysUserRoleService;
import com.scaffold.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 系统用户 前端控制器
 * </p>
 *
 * @author liliang
 * @since 2022-08-10
 */
@RestController
@RequestMapping("/api/v1/system/users")
public class SysUserController {

    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ISysUserOrganizationService sysUserOrganizationService;
    @Autowired
    private ISysUserRoleService sysUserRoleService;

    @GetMapping
    public R<IPage<SysUserVO>> list(SysUserQuery query) {
//        WrapperFactory<SysUser> factory = WrapperFactory.build();
//        LambdaQueryWrapper<SysUser> queryWrapper = factory.query(query).lambda().orderByAsc(SysUser::getUserId);
//        Page<SysUser> userPage = new Page<>(query.getPage(), query.getSize());
//        sysUserService.page(userPage, queryWrapper);
        IPage<SysUserVO> pageUser = sysUserService.pageUser(query);
        return R.success(pageUser);
    }

    @GetMapping("/{userId}")
    public R<JSONObject> getDetail(@PathVariable Integer userId) {
        Wrapper<SysUserOrganization> orgQuery = new LambdaQueryWrapper<SysUserOrganization>()
                .eq(SysUserOrganization::getUserId, userId)
                .select(SysUserOrganization::getOrgId);
        List<Integer> orgIds = sysUserOrganizationService.listObjs(orgQuery, SFunctionFactory.newMapper());

        Wrapper<SysUserRole> roleQuery = new LambdaQueryWrapper<SysUserRole>()
                .eq(SysUserRole::getUserId, userId)
                .select(SysUserRole::getRoleId);
        List<Integer> roleIds = sysUserRoleService.listObjs(roleQuery, SFunctionFactory.newMapper());
        JSONObject result = JSONObject.of("orgIds", orgIds, "roleIds", roleIds);
        return R.success(result);
    }

    @PostMapping
    public R add(@RequestBody @Validated(Add.class) SysUserEditDTO user) {
        sysUserService.addUser(user);
        return R.success();
    }

    @PutMapping
    public R update(@RequestBody @Validated(Update.class) SysUserEditDTO user) {
        sysUserService.updateUser(user);
        return R.success();
    }

    @DeleteMapping("/{uid}")
    public R delete(@PathVariable String uid) {
        Assert.hasText(uid, "用户ID不能为空");
        sysUserService.removeByUid(uid);
        return R.success();
    }

    @PatchMapping("/state/{uid}/{state}")
    public R state(@PathVariable String uid, @PathVariable Integer state) {
        Assert.hasText(uid, "用户ID不能为空");
        Assert.notNull(state, "状态不能为空");
        SysUser sysUser = sysUserService.getByUid(uid);
        Assert.notNull(sysUser, "用户不存在");
        SysUser update = new SysUser();
        update.setUserId(sysUser.getUserId());
        update.setState(state);
        boolean result = sysUserService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }

    @PatchMapping("/reset/{uid}")
    public R resetPassword(@PathVariable String uid) {
        Assert.notNull(uid, "用户ID不能为空");
        String password = sysUserService.resetPassword(uid);
        return R.success(password);
    }

    @PatchMapping("/password")
    public R updatePassword(@RequestBody @Validated SysUserUpdatePasswordDTO dto) {
        sysUserService.updatePassword(dto);
        return R.success();
    }

}
