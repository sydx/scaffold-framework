package com.scaffold.system.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.scaffold.common.result.R;
import com.scaffold.system.entity.model.SysModuleFunction;
import com.scaffold.system.entity.vo.SysModuleFunctionVO;
import com.scaffold.system.service.ISysModuleFunctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 系统功能下的功能 前端控制器
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@RestController
@RequestMapping("/api/v1/system/functions")
public class SysModuleFunctionController {

    @Autowired
    private ISysModuleFunctionService sysModuleFunctionService;

    @GetMapping
    public R<List<SysModuleFunction>> list(SysModuleFunction query) {
        Wrapper<SysModuleFunction> wrapper = new LambdaQueryWrapper<SysModuleFunction>()
                .eq(null != query.getModuleId(), SysModuleFunction::getModuleId, query.getModuleId())
                .eq(null == query.getParentId(), SysModuleFunction::getParentId, 0)
                .eq(null != query.getParentId(), SysModuleFunction::getParentId, query.getParentId())
                .orderByAsc(SysModuleFunction::getFunctionIndex);
        List<SysModuleFunction> list = sysModuleFunctionService.list(wrapper);
        return R.success(list);
    }

    @GetMapping("/tree/{moduleId}")
    public R<List<SysModuleFunctionVO>> tree(@PathVariable Integer moduleId) {
        Assert.notNull(moduleId, "模块ID不能为空");
        List<SysModuleFunctionVO> list = sysModuleFunctionService.getFunctionTree(moduleId);
        return R.success(list);
    }

    @PostMapping
    public R add(@RequestBody @Validated SysModuleFunction function) {
        if (null == function.getParentId() || function.getParentId().equals(0)) {
            sysModuleFunctionService.addTopFunction(function);
        } else {
            sysModuleFunctionService.addChildFunction(function);
        }
        return R.success();
    }

    @PutMapping
    public R edit(@RequestBody @Validated SysModuleFunction function) {
        sysModuleFunctionService.updateFunction(function);
        return R.success();
    }


    @DeleteMapping("/{functionId}")
    public R delete(@PathVariable Integer functionId) {
        Assert.notNull(functionId, "功能ID不能为空");
       sysModuleFunctionService.removeFunction(functionId);
        return R.success();
    }

    @PatchMapping("/state/{functionId}/{state}")
    public R state(@PathVariable Integer functionId, @PathVariable Integer state) {
        Assert.notNull(functionId, "功能ID不能为空");
        Assert.notNull(state, "状态不能为空");
        sysModuleFunctionService.updateFunctionState(functionId, state);
        return R.success();
    }

    @PatchMapping("/sort/{sourceId}/{targetId}")
    public R sort(@PathVariable Integer sourceId, @PathVariable Integer targetId) {
        Assert.notNull(sourceId, "功能ID不能为空");
        Assert.notNull(targetId, "功能ID不能为空");
        sysModuleFunctionService.updateSort(sourceId, targetId);
        return R.success();
    }

}
