package com.scaffold.system.controller.api.v1;

import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.scaffold.common.result.R;
import com.scaffold.system.entity.model.SysUser;
import com.scaffold.system.entity.model.SysUserRole;
import com.scaffold.system.entity.query.SysRoleQuery;
import com.scaffold.system.service.ISysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 系统用户角色 前端控制器
 * </p>
 *
 * @author liliang
 * @since 2022-08-10
 */
@RestController
@RequestMapping("/api/v1/system/user/roles")
public class SysUserRoleController {

    @Autowired
    private ISysUserRoleService sysUserRoleService;

    @GetMapping
    public R<List<SysUserRole>> list(SysUserRole query) {
        Wrapper<SysUserRole> wrapper = new LambdaQueryWrapper<SysUserRole>()
                .orderByAsc(SysUserRole::getUserId);
        List<SysUserRole> list = sysUserRoleService.list(wrapper);
        return R.success(list);
    }

    @GetMapping("/{roleId}/users")
    public R<IPage<SysUser>> list(@PathVariable Integer roleId, SysRoleQuery query) {
        query.setRoleId(roleId);
        IPage<SysUser> list = sysUserRoleService.getUserListByRole(query);
        return R.success(list);
    }

    @PostMapping("/{roleId}/users")
    public R addRoleUser(@PathVariable Integer roleId, @RequestBody String userIds) {
        List<Integer> userIdsList = getUserIdsList(userIds);
        sysUserRoleService.addRoleUser(roleId, userIdsList);
        return R.success();
    }

    @DeleteMapping("/{roleId}/users")
    public R<List<SysUserRole>> delete(@PathVariable Integer roleId, @RequestParam String userIds) {
        Assert.hasText(userIds, "请选择用户");
        int[] userIdsList = StrUtil.splitToInt(userIds, CharUtil.COMMA);
        List<Integer> idList = new ArrayList<>(userIdsList.length);
        for (int i : userIdsList) {
            idList.add(i);
        }
        Wrapper<SysUserRole> condition = new LambdaQueryWrapper<SysUserRole>()
                .eq(SysUserRole::getRoleId, roleId)
                .in(SysUserRole::getUserId, idList);
        sysUserRoleService.remove(condition);
        return R.success();
    }

    private static List<Integer> getUserIdsList(String userIds) {
        String message = "请选择用户";
        Assert.hasText(userIds, message);
        JSONObject jsonObject = JSON.parseObject(userIds);
        Assert.notNull(jsonObject, message);
        JSONArray jsonArray = jsonObject.getJSONArray("userIds");
        Assert.notEmpty(jsonArray, message);
        String jsonString = jsonArray.toJSONString();
        return JSON.parseArray(jsonString, Integer.class);
    }

}
