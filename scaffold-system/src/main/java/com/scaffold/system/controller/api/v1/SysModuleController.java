package com.scaffold.system.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.scaffold.common.result.R;
import com.scaffold.system.entity.model.SysModule;
import com.scaffold.system.service.ISysModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 系统模块 前端控制器
 * </p>
 *
 * @author liliang
 * @since 2022-07-06
 */
@RestController
@RequestMapping("/api/v1/system/modules")
public class SysModuleController {

    @Autowired
    private ISysModuleService sysModuleService;

    @GetMapping
    public R<List<SysModule>> list() {
        Wrapper<SysModule> query = new LambdaQueryWrapper<SysModule>()
                .orderByAsc(SysModule::getModuleIndex);
        List<SysModule> list = sysModuleService.list(query);
        return R.success(list);
    }

    @PostMapping
    public R add(@RequestBody @Validated SysModule module) {
        boolean update = sysModuleService.addModule(module);
        Assert.isTrue(update, "新增失败！");
        return R.success();
    }

    @PutMapping
    public R update(@RequestBody @Validated SysModule module) {
        boolean update = sysModuleService.updateModule(module);
        Assert.isTrue(update, "修改失败！");
        return R.success();
    }

    @DeleteMapping("/{moduleId}")
    public R delete(@PathVariable Integer moduleId) {
        Assert.notNull(moduleId, "模块ID不能为空");
        boolean update = sysModuleService.removeById(moduleId);
        Assert.isTrue(update, "删除失败！");
        return R.success();
    }

    @PatchMapping("/state/{moduleId}/{state}")
    public R state(@PathVariable Integer moduleId, @PathVariable Integer state) {
        Assert.notNull(moduleId, "模块ID不能为空");
        Assert.notNull(state, "状态不能为空");
        SysModule update = new SysModule();
        update.setModuleId(moduleId);
        update.setState(state);
        boolean result = sysModuleService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }

    @PatchMapping("/sort/{sourceId}/{targetId}")
    public R sort(@PathVariable Integer sourceId, @PathVariable Integer targetId) {
        Assert.notNull(sourceId, "模块ID不能为空");
        Assert.notNull(targetId, "模块ID不能为空");
        sysModuleService.updateSort(sourceId, targetId);
        return R.success();
    }
}
