package com.scaffold.system.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scaffold.common.result.R;
import com.scaffold.common.validation.group.Update;
import com.scaffold.system.entity.dto.SysRoleEditDTO;
import com.scaffold.system.entity.model.SysRole;
import com.scaffold.system.entity.query.SysRoleQuery;
import com.scaffold.mybatisplus.factory.WrapperFactory;
import com.scaffold.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 系统用户角色 前端控制器
 * </p>
 *
 * @author liliang
 * @since 2022-08-10
 */
@RestController
@RequestMapping("/api/v1/system/roles")
public class SysRoleController {

    @Autowired
    private ISysRoleService sysRoleService;

    /**
     * 列表查询
     * @return
     */
    @GetMapping("/pageList")
    public R<Page<SysRole>> pageList(SysRoleQuery query) {
        WrapperFactory<SysRole> factory = WrapperFactory.build();
        LambdaQueryWrapper<SysRole> queryWrapper = factory.query(query).lambda()
                .orderByAsc(SysRole::getRoleId);
        Page<SysRole> page = new Page<>(query.getPage(), query.getSize());
        sysRoleService.page(page, queryWrapper);
        return R.success(page);
    }

    @GetMapping
    public R<List<SysRole>> list() {
        Wrapper<SysRole> query = new LambdaQueryWrapper<SysRole>()
                .orderByAsc(SysRole::getRoleId);
        List<SysRole> list = sysRoleService.list(query);
        return R.success(list);
    }

    @PostMapping
    public R add(@RequestBody @Validated SysRoleEditDTO role) {
        sysRoleService.addRole(role);
        return R.success();
    }

    @PutMapping
    public R update(@RequestBody @Validated(Update.class) SysRoleEditDTO role) {
        sysRoleService.updateRole(role);
        return R.success();
    }

    @DeleteMapping("/{roleId}")
    public R delete(@PathVariable Integer roleId) {
        Assert.notNull(roleId, "角色ID不能为空");
        sysRoleService.deleteRole(roleId);
        return R.success();
    }

    @PatchMapping("/state/{roleId}/{state}")
    public R state(@PathVariable Integer roleId, @PathVariable Integer state) {
        Assert.notNull(roleId, "角色ID不能为空");
        Assert.notNull(state, "状态不能为空");
        SysRole update = new SysRole();
        update.setRoleId(roleId);
        update.setState(state);
        boolean result = sysRoleService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }

}
