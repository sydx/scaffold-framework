package com.scaffold.system.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scaffold.common.result.R;
import com.scaffold.common.validation.group.Update;
import com.scaffold.system.entity.model.SysOrganization;
import com.scaffold.system.entity.query.SysOrganizationQuery;
import com.scaffold.system.entity.vo.OrganizationTreeVO;
import com.scaffold.mybatisplus.factory.WrapperFactory;
import com.scaffold.system.service.ISysOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/13 14:39:38
 * @描述: 组织架构 前端控制器
 */
@RestController
@RequestMapping("/api/v1/system/organizations")
public class SysOrganizationController {

    @Autowired
    private ISysOrganizationService sysOrganizationService;

    @GetMapping("/pageList")
    public R<Page<SysOrganization>> pageList(SysOrganizationQuery query) {
        WrapperFactory<SysOrganization> factory = WrapperFactory.build();
        LambdaQueryWrapper<SysOrganization> queryWrapper = factory.query(query).lambda()
                .orderByAsc(SysOrganization::getOrgId);
        Page<SysOrganization> page = new Page<>(query.getPage(), query.getSize());
        sysOrganizationService.page(page, queryWrapper);
        return R.success(page);
    }
    @GetMapping
    public R<List<SysOrganization>> list(SysOrganization query) {
        Wrapper<SysOrganization> condition = new LambdaQueryWrapper<SysOrganization>()
                .eq(null != query.getParentId(), SysOrganization::getParentId, query.getParentId())
                .orderByAsc(SysOrganization::getOrgId);
        List<SysOrganization> list = sysOrganizationService.list(condition);
        return R.success(list);
    }

    @PostMapping
    public R add(@RequestBody @Validated SysOrganization organization) {
        if (null == organization.getParentId() || organization.getParentId() == 0) {
            sysOrganizationService.addTopOrganization(organization);
        } else {
            sysOrganizationService.addChildrenOrganization(organization);
        }
        return R.success();
    }

    @PutMapping
    public R update(@RequestBody @Validated(Update.class) SysOrganization organization) {
        sysOrganizationService.updateOrganization(organization);
        return R.success();
    }

    @DeleteMapping("/{orgId}")
    public R delete(@PathVariable Integer orgId) {
        Assert.notNull(orgId, "组织架构ID不能为空");
        sysOrganizationService.deleteOrganization(orgId);
        return R.success();
    }

    @PatchMapping("/state/{orgId}/{state}")
    public R state(@PathVariable Integer orgId, @PathVariable Integer state) {
        Assert.notNull(orgId, "组织架构ID不能为空");
        Assert.notNull(state, "状态不能为空");
        SysOrganization update = new SysOrganization();
        update.setOrgId(orgId);
        update.setState(state);
        boolean result = sysOrganizationService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }

    @GetMapping("/tree")
    public R<List<OrganizationTreeVO>> tree(Integer orgId) {
        return R.success(sysOrganizationService.getOrganizationTree(orgId));
    }

}
