package com.scaffold.system.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.scaffold.common.result.R;
import com.scaffold.system.entity.model.SysRolePermission;
import com.scaffold.system.service.ISysRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 系统用户角色权限 前端控制器
 * </p>
 *
 * @author liliang
 * @since 2022-08-10
 */
@RestController
@RequestMapping("/api/v1/system/role/permissions")
public class SysRolePermissionController {

    @Autowired
    private ISysRolePermissionService sysRolePermissionService;

    @GetMapping
    public R<List<SysRolePermission>> list() {
        Wrapper<SysRolePermission> query = new LambdaQueryWrapper<SysRolePermission>()
                .orderByAsc(SysRolePermission::getRoleId);
        List<SysRolePermission> list = sysRolePermissionService.list(query);
        return R.success(list);
    }

    @GetMapping("/{roleId}/actionCode")
    public R<List<String>> getActionPermissionCodeByRole(@PathVariable Integer roleId) {
        List<String> list = sysRolePermissionService.getActionPermissionCodeByRole(roleId);
        return R.success(list);
    }


}
