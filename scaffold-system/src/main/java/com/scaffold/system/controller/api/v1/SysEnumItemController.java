package com.scaffold.system.controller.api.v1;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scaffold.common.result.R;
import com.scaffold.system.entity.model.SysEnumItem;
import com.scaffold.system.entity.query.SysEnumItemQuery;
import com.scaffold.mybatisplus.factory.WrapperFactory;
import com.scaffold.system.service.SysEnumItemService;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 系统枚举项-接口控制器
 *
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-02-11 16:12:47
 */
@RestController
@RequestMapping("/api/v1/system/enum/item")
public class SysEnumItemController {

    private final SysEnumItemService sysEnumItemService;

    public SysEnumItemController(SysEnumItemService sysEnumItemService) {
        this.sysEnumItemService = sysEnumItemService;
    }

    /**
     * 列表查询
     *
     * @return
     */
    @GetMapping
    public R<Page<SysEnumItem>> list(SysEnumItemQuery query) {
        WrapperFactory<SysEnumItem> factory = WrapperFactory.build();
        LambdaQueryWrapper<SysEnumItem> queryWrapper = factory.query(query).lambda()
                .orderByAsc(SysEnumItem::getItemId);
        Page<SysEnumItem> page = new Page<>(query.getPage(), query.getSize());
        sysEnumItemService.page(page, queryWrapper);
        return R.success(page);
    }

    /**
     * 新增
     *
     * @return
     */
    @PostMapping
    public R add(@RequestBody @Validated SysEnumItem sysEnumItem) {
        boolean update = sysEnumItemService.save(sysEnumItem);
        Assert.isTrue(update, "新增失败！");
        return R.success();
    }

    /**
     * 修改
     *
     * @return
     */
    @PutMapping
    public R update(@RequestBody @Validated SysEnumItem sysEnumItem) {
        boolean update = sysEnumItemService.updateById(sysEnumItem);
        Assert.isTrue(update, "修改失败！");
        return R.success();
    }

    /**
     * 删除
     *
     * @return
     */
    @DeleteMapping("/{itemId}")
    public R delete(@PathVariable Integer itemId) {
        Assert.notNull(itemId, "itemId不能为空");
        boolean update = sysEnumItemService.removeById(itemId);
        Assert.isTrue(update, "删除失败！");
        return R.success();
    }

    /**
     * 修改状态
     *
     * @return
     */
    @PatchMapping("/state/{itemId}/{state}")
    public R state1(@PathVariable Integer itemId, @PathVariable Integer state) {
        Assert.notNull(itemId, "itemId不能为空");
        Assert.notNull(state, "状态不能为空");
        SysEnumItem sysEnumItem = sysEnumItemService.getById(itemId);
        Assert.notNull(sysEnumItem, "数据不存在");
        SysEnumItem update = new SysEnumItem();
        update.setItemId(sysEnumItem.getItemId());
        update.setState(state);
        boolean result = sysEnumItemService.updateById(update);
        Assert.isTrue(result, "状态修改失败！");
        return R.success();
    }

}