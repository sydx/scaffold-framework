package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import com.scaffold.common.validation.group.Update;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 组织架构
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 09:43:46
 */
@TableName("sys_organization")
@Data
public class SysOrganization extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "org_id", type = IdType.AUTO)
    @NotNull(message = "组织ID不能为空", groups = Update.class)
    private Integer orgId;

    @NotBlank(message = "组织名称不能为空")
    private String orgName;

    private String orgCode;

    private Integer orgLevel;

    private Integer parentId;

    private String orgPath;

    @NotNull(message = "组织状态不能为空")
    private Integer state;

}
