package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统用户，保存用户登录信息
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 09:43:46
 */
@TableName("sys_user")
@Data
public class SysUser extends BaseModel implements Serializable {

    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;
    /**
     * 用户UUID
     */
    private String uid;
    /**
     * 用户登录名
     */
    private String username;
    /**
     * 用户登录密码
     */
    private String password;
    /**
     * 用户姓名
     */
    private String fullName;
    /**
     * 用户手机号
     */
    private String mobile;
    /**
     * 用户邮箱
     */
    private String mail;
    /**
     * 用户类型。99：超级管理员；1：普通用户；2：系统管理员；
     */
    private Integer userType;
    /**
     * 状态。0：停用；1：启用
     */
    private Integer state;
    /**
     * 当前激活的组织编码
     */
    private String currentOrgCode;
    /**
     * 数据权限类型
     */
    private String dataScope;

}
