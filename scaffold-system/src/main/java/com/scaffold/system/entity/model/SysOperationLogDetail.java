package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统日志
 * </p>
 *
 * @author 李亮
 * @since 2022-08-03 11:55:34
 */
@TableName("sys_operation_log_detail")
@Data
public class SysOperationLogDetail extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "detail_id", type = IdType.AUTO)
    private Long detailId;

    private Long logId;

    private String params;

    private String response;

    private String failReason;

    private String message;

    private String creator;

}
