package com.scaffold.system.entity.vo;

import lombok.Data;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/16 14:34:14
 * @描述: 组织架构树对象
 */
@Data
public class OrganizationTreeVO {
    private Integer orgId;
    private String orgName;
    private String orgCode;
    private Integer parentId;
    private Integer state;
    private Integer orgLevel;
    List<OrganizationTreeVO> children;

}
