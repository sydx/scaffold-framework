package com.scaffold.system.entity.vo;

import com.scaffold.system.entity.model.SysModuleFunction;
import lombok.Data;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/30 10:27:44
 * @描述: 功能
 */
@Data
public class SysModuleFunctionVO extends SysModuleFunction {

    private List<SysModuleFunctionVO> children;

}
