package com.scaffold.system.entity.query;

import java.io.Serializable;

import com.scaffold.mybatisplus.model.BaseQuery;
import com.scaffold.mybatisplus.annotations.Condition;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import com.baomidou.mybatisplus.core.enums.SqlKeyword;

/**
 * 系统用户角色-查询对象
 * @author: 李亮
 * @since: 1.0.0
 * @datetime: 2023-08-21 15:05:41
 */
@Getter
@Setter
public class SysRoleQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer roleId;
    /**
     * 角色名称
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String roleName;
    /**
     * 状态。0：停用；1：启用
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer state;
    /**
     * 角色说明
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String remark;
    /**
     * 创建人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer createdBy;
    /**
     * 创建时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date createdTime;
    /**
     * 修改人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer updatedBy;
    /**
     * 修改时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date updatedTime;
    /**
     * 删除状态。等于0：否；大于0：是
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer deleted;

}