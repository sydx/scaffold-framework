package com.scaffold.system.entity.vo;

import com.scaffold.system.entity.model.SysUser;
import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/17 15:10:20
 * @描述: 系统用户VO对象
 */
@Data
public class SysUserVO extends SysUser {
    
}
