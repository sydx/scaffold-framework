package com.scaffold.system.entity.dto;

import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/18 16:58:50
 * @描述: 用户权限对象
 */
@Data
public class UserPermissionsDTO {
    private Integer moduleId;
    private String moduleName;
    private String modulePermissionCode;
    private Integer moduleIndex;
    private String moduleLogo;
    private String moduleLink;
    private Integer functionId;
    private Integer functionParentId;
    private String functionName;
    private String functionPermissionCode;
    private Integer functionIndex;
    private String functionIcon;
    private String functionUrl;
    private Integer actionId;
    private String actionName;
    private String actionPermissionCode;
    private String actionUrl;

}
