package com.scaffold.system.entity.vo;


import lombok.Data;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/19 15:24:27
 * @描述: 菜单对象
 */
@Data
public class MenuVO {
    private String name;
    private String code;
    private String url;
    private String icon;
    private Integer index;
    private Boolean hasChild;
    List<MenuVO> children;
}
