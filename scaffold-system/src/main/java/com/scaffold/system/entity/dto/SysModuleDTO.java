package com.scaffold.system.entity.dto;

import com.scaffold.system.entity.model.SysModule;
import com.scaffold.system.entity.model.SysModuleFunction;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 系统模块数据传输对象
 *
 * @author 李亮
 * @version 1.0
 * @time 2023/8/16 13:45:49
 */
@Getter
@Setter
public class SysModuleDTO extends SysModule {
    private List<SysModuleFunction> functions;
}
