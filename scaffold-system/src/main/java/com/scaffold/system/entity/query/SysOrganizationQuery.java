package com.scaffold.system.entity.query;

import java.io.Serializable;

import com.scaffold.mybatisplus.model.BaseQuery;
import com.scaffold.mybatisplus.annotations.Condition;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import com.baomidou.mybatisplus.core.enums.SqlKeyword;

/**
 * 组织架构-查询对象
 * @author: 李亮
 * @since: 1.0.0
 * @datetime: 2023-08-21 14:56:26
 */
@Getter
@Setter
public class SysOrganizationQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer orgId;
    /**
     * 组织名称
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String orgName;
    /**
     * 组织编码
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String orgCode;
    /**
     * 0：集团/公司；1：公司/子公司；2：部门
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer orgLevel;
    /**
     * 父级组织
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer parentId;
    /**
     * 组织路径
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String orgPath;
    /**
     * 状态。0：停用；1：启用
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer state;
    /**
     * 创建人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer createdBy;
    /**
     * 创建时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date createdTime;
    /**
     * 修改人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer updatedBy;
    /**
     * 修改时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date updatedTime;
    /**
     * 删除状态。等于0：否；大于0：是
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer deleted;

}
