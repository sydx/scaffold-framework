package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统日志
 * </p>
 *
 * @author 李亮
 * @since 2022-08-03 11:55:34
 */
@TableName("sys_operation_log")
@Data
public class SysOperationLog extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "log_id", type = IdType.AUTO)
    private Long logId;

    private Integer logType;

    private String operation;

    private LocalDateTime time;

    private String className;

    private String method;

    private String ip;

    private String requestMethod;

    private String requestUrl;

    private Integer success;

    private String creator;

}
