package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户账户信息
 * </p>
 *
 * @author 李亮
 * @since 2022-08-17 12:13:03
 */
@TableName("sys_user_account")
@Data
public class SysUserAccount extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "salt_id", type = IdType.AUTO)
    private Integer saltId;

    private Integer userId;

    private String salt;

    private Integer saltType;

    private LocalDateTime passwordExpireTime;
    
    private Integer passwordNeedReset;

}
