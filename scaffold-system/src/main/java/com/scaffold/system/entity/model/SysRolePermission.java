package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统用户角色权限
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 09:43:46
 */
@TableName("sys_role_permission")
@Data
public class SysRolePermission extends BaseModel implements Serializable {

    @TableId(value = "rp_id", type = IdType.AUTO)
    private Integer rpId;

    private Integer roleId;

    private Integer moduleId;

    private Integer functionId;

    private Integer actionId;

}
