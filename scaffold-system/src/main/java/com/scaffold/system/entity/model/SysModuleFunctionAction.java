package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 功能的操作点
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 09:43:46
 */
@TableName("sys_module_function_action")
@Data
public class SysModuleFunctionAction extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "action_id", type = IdType.AUTO)
    private Integer actionId;

    @NotNull(message = "模块ID不能为空")
    private Integer moduleId;

    @NotNull(message = "功能ID不能为空")
    private Integer functionId;

    @NotBlank(message = "操作点名称不能为空")
    private String actionName;

    private String permissionCode;

    @NotBlank(message = "操作类型不能为空")
    private String actionType;

    @NotBlank(message = "操作点地址不能为空")
    private String url;

    @NotNull(message = "模块状态不能为空")
    private Integer state;

}
