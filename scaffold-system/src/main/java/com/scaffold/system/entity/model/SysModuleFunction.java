package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 系统模块下的功能
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 09:43:46
 */
@TableName("sys_module_function")
@Data
public class SysModuleFunction extends BaseModel implements Serializable {

    @TableId(value = "function_id", type = IdType.AUTO)
    private Integer functionId;

    @NotNull(message = "模块ID不能为空")
    private Integer moduleId;

    @NotBlank(message = "功能名称不能为空")
    private String functionName;

    @NotBlank(message = "功能地址不能为空")
    private String url;

    private String permissionCode;

    private Integer functionLevel;

    private String functionPath;

    private Integer parentId;

    @NotBlank(message = "功能图标不能为空")
    private String functionIcon;

    private BigDecimal functionIndex;

    @NotNull(message = "状态不能为空")
    private Integer state;

}
