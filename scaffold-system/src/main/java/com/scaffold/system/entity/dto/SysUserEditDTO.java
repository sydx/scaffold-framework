package com.scaffold.system.entity.dto;

import com.scaffold.common.validation.group.Add;
import com.scaffold.common.validation.group.Update;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/11 13:54:26
 * @描述: 用户编辑数据传输对象
 */
@Data
public class SysUserEditDTO {

    @NotNull(message = "用户ID不能为空", groups = Update.class)
    private Integer userId;

    @NotBlank(message = "用户登录名不能为空", groups = Add.class)
    private String username;

    @NotBlank(message = "用户登录密码不能为空", groups = Add.class)
    private String password;

    @NotBlank(message = "用户姓名不能为空")
    private String fullName;

    @NotBlank(message = "用户手机号不能为空")
    private String mobile;

    private String mail;

    @NotNull(message = "用户类型不能为空")
    private Integer userType;

    @NotNull(message = "用户状态不能为空")
    private Integer state;

    private String orgIds;

    private String roleIds;
    /**
     * 数据权限类型
     */
    private String dataScope;

}
