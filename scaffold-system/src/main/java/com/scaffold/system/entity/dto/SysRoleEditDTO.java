package com.scaffold.system.entity.dto;

import com.scaffold.common.validation.group.Update;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/11 13:54:26
 * @描述: 角色编辑数据传输对象
 */
@Data
public class SysRoleEditDTO {

    @NotNull(message = "角色ID不能为空", groups = Update.class)
    private Integer roleId;

    @NotBlank(message = "角色名称不能为空")
    private String roleName;

    @NotNull(message = "角色状态不能为空")
    private Integer state;

    @Size(max = 200, message = "角色说明不能超过200个字符")
    private String remark;

    private String actionIds;
}
