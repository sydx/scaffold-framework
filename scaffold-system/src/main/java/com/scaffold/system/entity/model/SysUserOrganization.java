package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户组织
 * </p>
 *
 * @author 李亮
 * @since 2022-08-15 11:41:05
 */
@TableName("sys_user_organization")
@Data
public class SysUserOrganization extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "user_org_id", type = IdType.AUTO)
    private Integer userOrgId;

    private Integer userId;

    private Integer orgId;

    private String userOrgCode;

    private String orgName;

    private String orgPath;

}
