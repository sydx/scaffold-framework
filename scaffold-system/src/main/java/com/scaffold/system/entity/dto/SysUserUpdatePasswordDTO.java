package com.scaffold.system.entity.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/11 13:54:26
 * @描述: 用户修改密码数据传输对象
 */
@Data
public class SysUserUpdatePasswordDTO {

    private String uid;

    @NotBlank(message = "旧登录密码不能为空")
    private String password;

    @NotBlank(message = "新登录密码不能为空")
    private String newPassword;

}
