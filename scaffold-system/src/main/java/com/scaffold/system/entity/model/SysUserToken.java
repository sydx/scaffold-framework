package com.scaffold.system.entity.model;

import java.io.Serializable;

import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
* 系统用户访问凭证
* @author: 李亮
* @since: ver.0.0.1
* @datetime: 2023-08-15 14:23:55
*/
@Getter
@Setter
@TableName("sys_user_token")
public class SysUserToken extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 
     */
    @TableId(value = "token_id", type = IdType.AUTO)
    private Integer tokenId;
    /**
     * 用户ID
     */
    private String uid;
    /**
     * 调用受限资源的凭证
     */
    private String accessToken;
    /**
     * Access Token有效时长
     */
    private Integer accessTokenExpiresIn;
    /**
     * Access Token失效时间戳
     */
    private Long accessTokenExpiresTime;
    /**
     * Access Token创建时间戳
     */
    private Long accessTokenCreateTime;
    /**
     * 获取Refresh Token的code
     */
    private String refreshCode;
    /**
     * 用于刷新Access Token的凭证
     */
    private String refreshToken;
    /**
     * Refresh Token有效时长
     */
    private Integer refreshTokenExpiresIn;
    /**
     * Refresh Token失效时间戳
     */
    private Long refreshTokenExpiresTime;
    /**
     * Refresh Token创建时间戳
     */
    private Long refreshTokenCreateTime;

}