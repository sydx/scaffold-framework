package com.scaffold.system.entity.query;

import java.io.Serializable;

import com.scaffold.mybatisplus.model.BaseQuery;
import com.scaffold.mybatisplus.annotations.Condition;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import com.baomidou.mybatisplus.core.enums.SqlKeyword;

/**
 * 系统用户访问凭证-查询对象
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-08-15 14:23:55
 */
@Getter
@Setter
public class SysUserTokenQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer tokenId;
    /**
     * 用户ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String uid;
    /**
     * 调用受限资源的凭证
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String accessToken;
    /**
     * Access Token有效时长
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer accessTokenExpiresIn;
    /**
     * Access Token失效时间戳
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Long accessTokenExpiresTime;
    /**
     * Access Token创建时间戳
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Long accessTokenCreateTime;
    /**
     * 获取Refresh Token的code
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String refreshCode;
    /**
     * 用于刷新Access Token的凭证
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String refreshToken;
    /**
     * Refresh Token有效时长
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer refreshTokenExpiresIn;
    /**
     * Refresh Token失效时间戳
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Long refreshTokenExpiresTime;
    /**
     * Refresh Token创建时间戳
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Long refreshTokenCreateTime;
    /**
     * 创建人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer createdBy;
    /**
     * 创建时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date createdTime;
    /**
     * 修改人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer updatedBy;
    /**
     * 修改时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date updatedTime;
    /**
     * 删除状态。等于0：否；大于0：是
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer deleted;

}