package com.scaffold.system.entity.query;

import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import com.scaffold.mybatisplus.model.BaseQuery;
import com.scaffold.mybatisplus.annotations.Condition;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统枚举项-查询对象
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-02-11 16:12:47
 */
@Getter
@Setter
public class SysEnumItemQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer itemId;
    /**
     * 枚举编号
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String enumCode;
    /**
     * 枚举项值
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String itemValue;
    /**
     * 枚举项显示名称
     */
    @Condition(keyword = SqlKeyword.EQ)
    private String itemLabel;
    /**
     * 枚举项显示顺序
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer itemIndex;
    /**
     * 状态。0：停用；1：启用
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer state;
    /**
     * 创建人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer createdBy;
    /**
     * 创建时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date createdTime;
    /**
     * 修改人ID
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Integer updatedBy;
    /**
     * 修改时间
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Date updatedTime;
    /**
     * 删除状态。等于0：否；大于0：是
     */
    @Condition(keyword = SqlKeyword.EQ)
    private Long deleted;

}