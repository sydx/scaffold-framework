package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统用户角色
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 09:43:46
 */
@TableName("sys_role")
@Data
public class SysRole extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "role_id", type = IdType.AUTO)
    private Integer roleId;

    private String roleName;

    private Integer state;

    private String remark;

}
