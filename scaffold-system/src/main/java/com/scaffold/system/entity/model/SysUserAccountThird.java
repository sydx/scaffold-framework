package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统用户第三方平台账户信息
 * </p>
 *
 * @author 李亮
 * @since 2022-09-13 10:34:23
 */
@TableName("sys_user_account_third")
@Data
public class SysUserAccountThird extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "third_id", type = IdType.AUTO)
    private Integer thirdId;

    private Integer userId;

    private String uid;

    private Integer thirdPlant;

    private String thirdUserId;

    private String thirdUnionId;

    private String nickName;

    private String headPicture;

    private String gender;
}
