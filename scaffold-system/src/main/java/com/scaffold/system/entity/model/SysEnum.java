package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 系统枚举
 *
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-02-11 16:12:47
 */
@Getter
@Setter
@TableName("sys_enum")
public class SysEnum extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(value = "enum_id", type = IdType.AUTO)
    private Integer enumId;
    /**
     * 枚举编号
     */
    private String enumCode;
    /**
     * 枚举名称
     */
    private String enumName;
    /**
     * 状态。0：停用；1：启用
     */
    private Integer state;
    /**
     * 枚举说明
     */
    private String remark;

}