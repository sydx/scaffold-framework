package com.scaffold.system.entity.query;

import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import com.scaffold.mybatisplus.model.BaseQuery;
import com.scaffold.mybatisplus.annotations.Condition;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/10 15:15:01
 * @描述: 用户查询条件对象
 */
@Getter
@Setter
public class SysUserQuery extends BaseQuery {

    @Condition(keyword = SqlKeyword.LIKE)
    private String username;

    @Condition(keyword = SqlKeyword.LIKE)
    private String fullName;

    @Condition(keyword = SqlKeyword.LIKE)
    private String mobile;

    @Condition(keyword = SqlKeyword.LIKE)
    private String mail;

    @Condition
    private Integer state;

    private Integer userType;

    private String orgIds;

    private String roleIds;

    private Integer roleId;

    private List<Integer> orgIdList;

    private List<Integer> roleIdList;
    /**
     * 当前激活的组织编码
     */
    private String currentOrgCode;
    /**
     * 数据权限类型
     */
    private String dataScope;
}
