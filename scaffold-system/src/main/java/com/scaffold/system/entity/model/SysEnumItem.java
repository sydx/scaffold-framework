package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 系统枚举项
 *
 * @author: 李亮
 * @since: ver.0.0.1
 * @datetime: 2023-02-11 16:12:47
 */
@Getter
@Setter
@TableName("sys_enum_item")
public class SysEnumItem extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(value = "item_id", type = IdType.AUTO)
    private Integer itemId;
    /**
     * 枚举编号
     */
    private String enumCode;
    /**
     * 枚举项值
     */
    private String itemValue;
    /**
     * 枚举项显示名称
     */
    private String itemLabel;
    /**
     * 枚举项显示顺序
     */
    private Integer itemIndex;
    /**
     * 状态。0：停用；1：启用
     */
    private Integer state;
    /**
     * 枚举项说明
     */
    private String remark;

}