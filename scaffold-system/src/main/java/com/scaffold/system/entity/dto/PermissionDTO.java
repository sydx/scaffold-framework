package com.scaffold.system.entity.dto;

import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import com.scaffold.system.entity.model.SysModuleFunction;
import com.scaffold.system.entity.model.SysModuleFunctionAction;
import lombok.Data;

import java.io.Serializable;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/18 16:58:50
 * @描述: 权限对象
 */
@Data
public class PermissionDTO implements Serializable {
    /**
     * 地址
     */
    private String url;
    /**
     * 请求方法
     */
    private String method;
    /**
     * 权限编码
     */
    private String permissionCode;

    /**
     * url层级，即/的个数
     */
    private int urlLevels;

    /**
     * 是否含有动态层级
     */
    private boolean hasPathVariable = false;

    public PermissionDTO() {
    }

    public PermissionDTO(SysModuleFunction function) {
        this(function.getUrl(), function.getPermissionCode(), "GET");
    }
    public PermissionDTO(SysModuleFunctionAction action) {
        this(action.getUrl(), action.getPermissionCode(), action.getActionType());
    }
    public PermissionDTO(String url, String permissionCode, String method) {
        this.url = url;
        this.permissionCode = permissionCode;
        this.urlLevels = StrUtil.count(url, CharUtil.SLASH);
        this.hasPathVariable = StrUtil.contains(url, CharUtil.DELIM_START);
        this.method = method;
    }
}
