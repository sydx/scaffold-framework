package com.scaffold.system.entity.vo;


import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/15 11:29:50
 * @描述: 组织中的用户view对象
 */
@Data
public class OrganizationUserVO {

    private Integer userOrgId;
    
    private Integer userId;

    private String fullName;

    private String mobile;

    private String mail;

    private Integer state;

    private Integer orgId;

    private String orgName;
}
