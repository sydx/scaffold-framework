package com.scaffold.system.entity.dto;

import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/7/18 16:58:50
 * @描述: 模块下的操作点对象
 */
@Data
public class ModuleActionDTO {
    private Integer moduleId;
    private Integer functionId;
    private String functionCode;
    private Integer functionParentId;
    private String functionName;
    private Integer functionIndex;
    private Integer functionState;
    private Integer actionId;
    private String actionName;
    private String actionCode;
    private Integer actionState;
}
