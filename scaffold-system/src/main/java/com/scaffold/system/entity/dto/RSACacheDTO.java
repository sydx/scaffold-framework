package com.scaffold.system.entity.dto;

import cn.hutool.crypto.asymmetric.RSA;
import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/9/14 11:43:55
 * @描述: RSA秘钥缓存对象
 */
@Data
public class RSACacheDTO {

    /**
     * 公钥base64编码字符串
     */
    private String publicKey;
    /**
     * 私钥base64编码字符串
     */
    private String privateKey;

    public RSACacheDTO() {
    }

    public RSACacheDTO(RSA rsa) {
        this.privateKey = rsa.getPrivateKeyBase64();
        this.publicKey = rsa.getPublicKeyBase64();
    }

    public RSA rsa() {
        return new RSA(this.privateKey, this.publicKey);
    }
}
