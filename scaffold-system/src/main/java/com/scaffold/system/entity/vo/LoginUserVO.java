package com.scaffold.system.entity.vo;


import lombok.Data;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/18 14:55:53
 * @描述: 登录用户信息
 */
@Data
public class LoginUserVO {

    private String uid;

    private String username;

    private String fullName;

    private String mobile;

    private String mail;

    private Integer userType;

    private Integer state;

    private Boolean passwordNeedReset;

    private List<String> permissions;

}
