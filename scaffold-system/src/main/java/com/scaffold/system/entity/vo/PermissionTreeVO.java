package com.scaffold.system.entity.vo;


import lombok.Data;

import java.util.List;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/10 16:12:47
 * @描述: 权限树展示对象. 以模块为单位，保存功能和操作点两种类型的数据对象。
 * 功能可以有多级，叶子节点是必须是操作
 * 根据actionId字段判断是否是叶子节点，叶子节点actionId不为空
 */
@Data
public class PermissionTreeVO {
    private Integer moduleId;

    private Integer functionId;

    private Integer functionParentId;

    private String functionName;

    private Integer functionIndex;

    private Integer actionId;

    private String actionName;

    private Integer state;

    private String nodeId;
    private String nodeLabel;
    private Integer nodeType;

    List<PermissionTreeVO> children;
}
