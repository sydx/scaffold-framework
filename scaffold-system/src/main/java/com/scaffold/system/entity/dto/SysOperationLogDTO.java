package com.scaffold.system.entity.dto;

import com.scaffold.system.entity.model.SysOperationLog;
import com.scaffold.system.entity.model.SysOperationLogDetail;
import lombok.Data;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/3 11:21:14
 * @描述: 系统操作日志数据传输类
 */
@Data
public class SysOperationLogDTO extends SysOperationLog {

    /**
     * 日志详情
     */
    private SysOperationLogDetail detail;

    /**
     * 是否需要根据URL查询操作名称
     */
    private boolean needGetOptionName = false;

}
