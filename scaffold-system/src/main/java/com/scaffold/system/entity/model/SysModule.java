package com.scaffold.system.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.scaffold.mybatisplus.model.BaseModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统模块
 * </p>
 *
 * @author liliang
 * @since 2022-07-06
 */
@TableName("sys_module")
@Data
public class SysModule extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "module_id", type = IdType.AUTO)
    private Integer moduleId;

    @NotBlank(message = "模块名称不能为空")
    private String moduleName;

    private String permissionCode;

    @NotNull(message = "模块所属系统不能为空")
    private Integer systemId;

    @NotBlank(message = "模块图标不能为空")
    private String moduleLogo;

    private Integer moduleIndex;

    private String moduleLink;

    @NotNull(message = "模块状态不能为空")
    private Integer state;

}
