package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffold.system.entity.model.SysRolePermission;
import com.scaffold.mybatisplus.annotations.DataScope;

import java.util.List;

/**
 * <p>
 * 系统用户角色权限 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@DataScope(ignore = true)
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

    List<SysRolePermission> getRolePermissions(Integer roleId);

    List<String> getActionPermissionCodeByRole(Integer roleId);
}
