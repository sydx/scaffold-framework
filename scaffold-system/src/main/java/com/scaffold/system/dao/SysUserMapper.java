package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scaffold.system.entity.dto.UserModuleFunctionDTO;
import com.scaffold.system.entity.dto.UserPermissionsDTO;
import com.scaffold.system.entity.model.SysUser;
import com.scaffold.system.entity.query.SysUserQuery;
import com.scaffold.system.entity.vo.SysUserVO;
import com.scaffold.mybatisplus.annotations.DataScope;

import java.util.List;

/**
 * <p>
 * 系统用户，保存用户登录信息 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@DataScope(ignore = true)
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<UserPermissionsDTO> getUserPermissions(Integer userId);

    IPage<SysUserVO> pageUser(Page<SysUserVO> page, SysUserQuery query);

    List<String> getUserAllPermissionCode(int userId);
}
