package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffold.system.entity.model.SysModuleFunction;
import com.scaffold.mybatisplus.annotations.DataScope;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统模块下的功能 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@DataScope(ignore = true)
public interface SysModuleFunctionMapper extends BaseMapper<SysModuleFunction> {

    List<String> getFunctionPermissionCodes(String url);

    List<String> getFunctionNames(@Param("url") String url, @Param("actionType") String actionType);

}
