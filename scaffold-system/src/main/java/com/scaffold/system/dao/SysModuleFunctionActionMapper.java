package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffold.system.entity.dto.ModuleActionDTO;
import com.scaffold.system.entity.dto.UserPermissionsDTO;
import com.scaffold.system.entity.model.SysModuleFunctionAction;
import com.scaffold.mybatisplus.annotations.DataScope;

import java.util.List;

/**
 * <p>
 * 功能的操作点 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@DataScope(ignore = true)
public interface SysModuleFunctionActionMapper extends BaseMapper<SysModuleFunctionAction> {

    List<UserPermissionsDTO> getAllAction();

    List<ModuleActionDTO> getActionByModule(Integer moduleId);

}
