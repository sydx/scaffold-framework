package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffold.system.entity.model.SysOrganization;
import com.scaffold.mybatisplus.annotations.DataScope;

/**
 * <p>
 * 组织架构 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@DataScope(ignore = true)
public interface SysOrganizationMapper extends BaseMapper<SysOrganization> {

}
