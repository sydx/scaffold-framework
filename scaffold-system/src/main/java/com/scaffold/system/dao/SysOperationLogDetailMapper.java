package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffold.system.entity.model.SysOperationLogDetail;
import com.scaffold.mybatisplus.annotations.DataScope;

/**
 * <p>
 * 系统日志 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-08-03 11:55:34
 */
@DataScope(ignore = true)
public interface SysOperationLogDetailMapper extends BaseMapper<SysOperationLogDetail> {

}
