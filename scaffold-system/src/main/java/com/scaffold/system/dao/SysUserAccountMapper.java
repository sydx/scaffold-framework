package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffold.system.entity.model.SysUserAccount;
import com.scaffold.mybatisplus.annotations.DataScope;

/**
 * <p>
 * 用户盐数据 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-08-17 12:13:03
 */
@DataScope(ignore = true)
public interface SysUserAccountMapper extends BaseMapper<SysUserAccount> {

    void deleteSaltByUserId(Integer userId);
}
