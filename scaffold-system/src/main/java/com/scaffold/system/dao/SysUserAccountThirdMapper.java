package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffold.system.entity.model.SysUserAccountThird;
import com.scaffold.mybatisplus.annotations.DataScope;

/**
 * <p>
 * 系统用户第三方平台账户信息 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-09-13 10:34:23
 */
@DataScope(ignore = true)
public interface SysUserAccountThirdMapper extends BaseMapper<SysUserAccountThird> {

}
