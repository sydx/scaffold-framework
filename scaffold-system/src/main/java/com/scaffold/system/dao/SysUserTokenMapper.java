package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffold.system.entity.model.SysUserToken;
import com.scaffold.mybatisplus.annotations.DataScope;

/**
* 系统用户访问凭证
* @author: 李亮
* @since: ver.0.0.1
* @datetime: 2023-08-15 14:23:55
*/
@DataScope(ignore = true)
public interface SysUserTokenMapper extends BaseMapper<SysUserToken>  {

}