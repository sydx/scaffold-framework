package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffold.system.entity.model.SysEnumItem;
import com.scaffold.mybatisplus.annotations.DataScope;

/**
* 系统枚举项
* @author: 李亮
* @since: ver.0.0.1
* @datetime: 2023-02-11 16:12:47
*/
@DataScope(ignore = true)
public interface SysEnumItemMapper extends BaseMapper<SysEnumItem>  {

}