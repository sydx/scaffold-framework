package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffold.system.entity.dto.SysModuleDTO;
import com.scaffold.system.entity.model.SysModule;
import com.scaffold.mybatisplus.annotations.DataScope;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统模块 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@DataScope(ignore = true)
public interface SysModuleMapper extends BaseMapper<SysModule> {
    List<SysModuleDTO> getModuleFunction(int systemId);
    List<SysModuleDTO> getUserModuleFunction(@Param("systemId") int systemId, @Param("list") List<String> actionPermissionCodes);

}
