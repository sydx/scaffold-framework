package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scaffold.system.entity.model.SysUserOrganization;
import com.scaffold.system.entity.vo.OrganizationUserVO;
import com.scaffold.mybatisplus.annotations.DataScope;

/**
 * <p>
 * 用户组织 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-08-15 11:41:05
 */
@DataScope(ignore = true)
public interface SysUserOrganizationMapper extends BaseMapper<SysUserOrganization> {

    IPage<OrganizationUserVO> getOrgUsers(Page<OrganizationUserVO> page, String path);
}
