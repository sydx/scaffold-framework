package com.scaffold.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scaffold.system.entity.model.SysUser;
import com.scaffold.system.entity.model.SysUserRole;
import com.scaffold.system.entity.query.SysRoleQuery;
import com.scaffold.mybatisplus.annotations.DataScope;

/**
 * <p>
 * 用户角色 Mapper 接口
 * </p>
 *
 * @author 李亮
 * @since 2022-07-07 10:01:03
 */
@DataScope(ignore = true)
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    IPage<SysUser> getUserListByRole(Page<SysUser> page, SysRoleQuery query);
}
