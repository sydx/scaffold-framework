package com.scaffold.redis;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/3 10:37:59
 * @描述: Redis 消息队列枚举
 */
public enum TopicEnum {
    /**
     * 系统操作日志队列
     */
    SYS_OPERATION_LOG_TOPIC;
}
