package com.scaffold.redis;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/3 13:54:37
 * @描述: Redis 配置项
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "redis", ignoreInvalidFields = true)
public class RedisConfigProperties {

    /**
     * Redis服务地址
     */
    private String host = "localhost";

    /**
     * Redis服务端口
     */
    private Integer port = 6379;
    /**
     * Redis数据库
     */
    private Integer database = 0;

    /**
     * Redis服务密码
     */
    private String password;

    /**
     * lettuce 客户端
     */
    private Lettuce lettuce = new Lettuce();

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    public static class Lettuce {
        /**
         * 连接池
         */
        private Pool pool = new Pool();

        /**
         * 命令超时时间：毫秒
         */
        private Long commandTimeout = 1000L;
        /**
         * 关闭连接超时时间：毫秒
         */
        private Long shutdownTimeout = 1000L;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    public static class Pool {
        /**
         * 连接池最大连接数（使用负值表示没有限制） 默认 8
         */
        private Integer maxActive = 8;

        /**
         * 连接池最大阻塞等待时间（使用负值表示没有限制） 默认 -1
         * 单位：毫秒
         */
        private Long maxWait = -1L;

        /**
         * 连接池中的最大空闲连接 默认 8
         */
        private Integer maxIdle = 8;

        /**
         * 连接池中的最小空闲连接 默认 0
         */
        private Integer minIdle = 0;
    }
}
