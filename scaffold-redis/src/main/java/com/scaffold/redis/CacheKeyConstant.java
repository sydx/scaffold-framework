package com.scaffold.redis;

/**
 * @创建人: 李亮
 * @创建时间: 2022/8/3 10:37:59
 * @描述: Redis 缓存Key常量
 */
public class CacheKeyConstant {
    /**
     * 所有数据key
     */
    public final static String ALL_KEY = "all";

    public final static String MINIPROGRAM_CAPTCHA_KEY_FMT =
            CacheNameConstant.SECURITY_AUTHENTICATION_CAPTCHA_MINIPROGRAM + ":openid_%s";

    public final static String MINIPROGRAM_RSA_KEY_FMT =
            CacheNameConstant.SECURITY_AUTHENTICATION_RSA_MINIPROGRAM + ":openid_%s";

    public final static String SECURITY_CAPTCHA_KEY_FMT = "captcha_%s";


    public final static String SECURITY_PASSWORD_RSA_KEY_FMT = "pwdrsa_%s";
}
